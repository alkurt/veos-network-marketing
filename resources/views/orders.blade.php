@extends('layouts/main')

@section('content')

<div class="container" style="margin-top: 170px;">

    <div class="row">
        <div class="col-sm-12 locations ">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>  <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">SİPARİŞLERİM</h3><br><br>
            @if (count($orders) == 0)
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 order-summary">
                            <div class="alert alert-danger text-center">
                                <h2 style="font-family: 'Rage Italic'">Henüz Bir Siparişiniz Bulunmamaktadır.!</h2>
                            </div>
                        </div>
                    </div>
                </div>
            @else
               <h6 class="text-center"><p>Siparişler Toplam Tutarı</p> {{$toplam}} ₺</h6>
                <table class="table table-bordererd table-hover">
                    <tr>
                        <th>Sipariş Kodu</th>
                        <th>Sipariş Tutarı</th>
                        <th>Durum</th>
                        <th></th>
                    </tr>
                    @foreach($orders as $order)
                        <tr class="text-$order">
                            <td>veos-{{$order->id }}</td>
                            <td>{{ $order->order_price }} ₺</td>
                            <td>{{ $order->status }}</td>
                            <td><a href="/orders/{{$order->id}}" class="btn-sm btn-success">Detaylar</a></td>
                        </tr>
                    @endforeach
                </table>
            @endif
        </div>
    </div>
</div><!-- Container /- -->
@endsection
