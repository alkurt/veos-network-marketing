@extends("layouts.main")
@section("content")
    <div class="" style="margin-top: 78px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
        <link rel="stylesheet" href="{{asset('css/menu.css')}}">
        <!-- Modal -->
        <!-- Button trigger modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"style="
                                background: rgba(0,0,0,.8);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.5);
                                border-radius: 10px;
                                color: white;
                            ">
            <div class="modal-dialog" role="document" style="margin-top: 180px">
                <div class="modal-content" style="background-color: seashell">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"><strong>VeosNet</strong> Duyurular</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"  >
                        @forelse($duyuru as $d)
                            <h5 class="text-center">{{$d->duyuru_basligi}}</h5>
                            <p class="mx-5 ">{{$d->duyuru_icerigi}}</p>
                        @empty
                            <p>Güncel Bir Duyuru Bulunmamaktadır.</p>
                        @endforelse
                    </div>

                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="margin-top: 100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> Veosnet </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p> <a href="{{route('il_ciro')}}">İL Ciro</a><strong class="mx-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aylık olarak hesabınıza geçer. </strong></p>
                        <p> <a href="{{route('tr_ciro')}}">Türkiye Ciro</a><strong class="mx-4">&nbsp;&nbsp;&nbsp; Yıllık olarak hesabınıza geçer.</strong></p>
                        <p><a href="{{route('dunya_ciro')}}">Dünya Ciro</a><strong class="mx-5"> Yıllık olarak hesabınıza geçer.</strong></p>
                        <p><a href="{{route('araba_ciro')}}">Araba Ciro</a><strong class="mx-5"> Aylık olarak hesabınıza geçer.</strong></p>
                        <p><a href="{{route('bolge_ciro')}}">Bölge Ciro</a><strong class="mx-5"> &nbsp;Aylık olarak hesabınıza geçer.</strong></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-3">
                    <ul id="menu-v">
                        <li>  <h5 class=" text-white  text-center">BAYİ MENÜ</h5></li>
                        <li><a href="{{route('home')}}">Anasayfa</a></li>
                        <li><a href="{{ url('/profile')}}"> Profilim</a>
                        <li><a href="{{route("pmenu")}}"> Sistem Yönetimi</a></li>
                        <li> <a href="{{route("hesap")}}"> Hesap Yönetimi</a></li>
                        <li><a href="{{route('odemetalebi.create')}}"> Ödeme Talebi</a></li>
                        <li><a href="{{route('musteri_eft_durum')}}"> Eft/Havale Durum</a></li>
                        <li>
                            <a class="arrow" href="#">Siparişler</a>
                            <ul>
                                <li>
                                    <a href="{{route('home')}}"> Sipariş Vermek İstiyorum </a>
                                </li>
                                <li>
                                    <a  href="/orders"> Siparişlerim</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="{{route("banka")}}"> Veosnet Hesap Bilgileri</a></li>
                        <li><a  type="button" data-toggle="modal" data-target="#exampleModal">
                                Dağıtılan Ciro Primleri
                            </a></li>
                        <li><a type="button"  href="{{route('gelenpvsyf')}}">
                                Pv/Cv Gelirlerim
                            </a></li>
                        <li><a type="button"  href="{{route('alt-bayiliklerim')}}">
                                İllerdeki Bayiliklerim
                            </a></li>
                        <li><a type="button" href="{{route('yeni-alt-bayi')}}">
                                Yeni İlde Bayi Aç
                            </a></li>
                        <li class="mt-3">  <h5 class="text-white  text-center" >DESTEK HİZMETLER</h5></li>
                        <li><a type="button"  data-toggle="modal" data-target="#exampleModalLong">
                                Duyurular
                            </a></li>
                        <li><a href="{{route('contact')}}"> Destek Hattı</a></li>
                        <li><a href="{{route('sozlesmemusteri')}}"> Sözleşmeler</a></li>
                        <li><a href="{{route('etkinlikler')}}"> Etkinliklerimiz</a></li>
                        <li><a href="{{route("sss")}}">Sıkça Sorulan Sorular</a></li>
                        <li><a href="{{route('bilgilendirme21')}}"> Kariyer Bilgilendirme</a></li>
                    </ul>
                </div>
                <div class="col-md-9 col-sm-12 mb-2"
                     style="background-image: url('https://cdn.lowgif.com/small/49d60bc6bd314079-chris-cubellis-sticker-for-ios-android-giphy.gif');">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 style="color: salmon"> Sayın; <strong style="color: darkturquoise">  {{auth()->user()->name}}&nbsp;{{auth()->user()->surname}} </strong></h5>
                            <p style="color: salmon"> Kullanıcı Numaranız: <strong style="color: darkturquoise">&nbsp;&nbsp; {{auth()->user()->id}}&nbsp;&nbsp</strong></p>
                            <p style="color: salmon"> Sponsor Numaranız: <strong style="color: darkturquoise">@if(auth()->user()->sponsor_id ==null)Şirket @else {{auth()->user()->sponsor_id}} @endif&nbsp;&nbsp;</strong></p>
                            <h6 style="color: salmon"> Durumunuz: <strong style="color: darkturquoise">@if(auth()->user()->durum == 1)Aktif @else Pasif @endif</strong></h6>
                        </div>
                        <div class="col-md-4">
                            @if($profil == 0)
                                <img class="img-fluid float-right mt-4" style="height: 100px;width: 100px;margin-left: 100px;border-radius: 100px;width:100px;height:100px;}"
                                     src="https://burbilder.org/wp-content/uploads/2020/01/bos-profil.jpg"  alt=""/>

                            @else
                                <img class="img-fluid float-right mt-4" style="height: 100px;width: 100px;margin-left: 100px;border-radius: 100px;width:100px;height:100px;}"
                                     src="{{\Illuminate\Support\Facades\Storage::url($profil_creates->cover)}}" alt=""/>
                            @endif
                        </div>
                    </div>
                    <div class="text-center">
                        <div class="row mb-5">
                            <div class="col-md-12 col-sm-12">
                                <h3 style="font-family: 'Harlow Solid Italic';color: salmon"> KARİYERİNİZ</h3>
                                <span class="alert alert-light "> <strong style="background-color: transparent;color: darkturquoise;font-size: medium">&nbsp;&nbsp;&nbsp;{{auth()->user()->kariyer->kariyername}}&nbsp;&nbsp;&nbsp;</strong> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 ">
                 <div class="col-12">
                     <di class="row">
                         <div class="col-md-5 item"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                             <div class="row">
                                 <div class="col-12">
                                     <div class="row">
                                         <div class="col-6">
                                             <p class="text-center">Kendi CV (cv) </p>
                                             <p class="text-center">{{auth()->user()->cv}} CV</p>
                                             <div class="row">
                                                 <div class="col-12">
                                                     <div class="row">
                                                         <div class="col-6">
                                                             <p>Sol CV</p>
                                                             <p> {{$solcv}}</p>
                                                         </div>
                                                         <div class="col-6">
                                                             <p>Sağ CV</p>
                                                             <p >{{$sagcv}}</p>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="col-6">
                                             <p class=" text-center">Kendi PV (pv): </p>
                                             <p class="text-center">{{auth()->user()->ara_pv}} PV</p>
                                             <div class="row">
                                                 <div class="col-12">
                                                     <div class="row">
                                                         <div class="col-6">
                                                             <p>Sol PV</p>
                                                             <p class="">{{$solpv}}</p>
                                                         </div>
                                                         <div class="col-6">
                                                             <p>Sağ PV</p>
                                                             <p class="">{{$sagpv}}</p>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="col-md-5 mx-5">
                             <div class="row">
                                 <div class="col-md-12 col-sm-12 item "><span class="icon feature_box_col_two"><i class="fa fa-cc-paypal"></i></span>
                                     <br>
                                     <h4 class="text-center"><i class="fa fa-shopping-cart text-dark mb-4"> Sipariş Özetim  </i> </h4>  <br>
                                     <h6 class="text-center" style="color: black">Toplam Sipariş Sayınız:
                                         <strong style="color: black"> [ {{$orderCount}} ]</strong></h6>
                                     <br>
                                 </div>
                             </div>
                         </div>
                     </di>
                 </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("js")
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
@endsection
@section("css")
    <style>
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {}
        a,
        a:hover,
        a:focus,
        a:active {
            text-decoration: none;
            outline: none;
        }

        a,
        a:active,
        a:focus {
            color: #333;
            text-decoration: none;
            transition-timing-function: ease-in-out;
            -ms-transition-timing-function: ease-in-out;
            -moz-transition-timing-function: ease-in-out;
            -webkit-transition-timing-function: ease-in-out;
            -o-transition-timing-function: ease-in-out;
            transition-duration: .2s;
            -ms-transition-duration: .2s;
            -moz-transition-duration: .2s;
            -webkit-transition-duration: .2s;
            -o-transition-duration: .2s;
        }

        ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        img {
            max-width: 100%;
            height: auto;
        }
        span, a, a:hover {
            display: inline-block;
            text-decoration: none;
            color: inherit;
        }
        .section-head {
            margin-bottom: 60px;
        }
        .section-head h4 {
            position: relative;
            padding:0;
            color:#f91942;
            line-height: 1;
            letter-spacing:0.3px;
            font-size: 34px;
            font-weight: 700;
            text-align:center;
            text-transform:none;
            margin-bottom:30px;
        }
        .section-head h4:before {
            content: '';
            width: 60px;
            height: 3px;
            background: #f91942;
            position: absolute;
            left: 0px;
            bottom: -10px;
            right:0;
            margin:0 auto;
        }
        .section-head h4 span {
            font-weight: 700;
            padding-bottom: 5px;
            color:#2f2f2f
        }
        p.service_text{
            color:#cccccc !important;
            font-size:16px;
            line-height:28px;
            text-align:center;
        }
        .section-head p, p.awesome_line{
            color:#818181;
            font-size:16px;
            line-height:28px;
            text-align:center;
        }

        .extra-text {
            font-size:34px;
            font-weight: 700;
            color:#2f2f2f;
            margin-bottom: 25px;
            position:relative;
            text-transform: none;
        }
        .extra-text::before {
            content: '';
            width: 60px;
            height: 3px;
            background: #f91942;
            position: absolute;
            left: 0px;
            bottom: -10px;
            right: 0;
            margin: 0 auto;
        }
        .extra-text span {
            font-weight: 700;
            color:#f91942;
        }
        .item {
            background: #fff;
            text-align: center;
            padding: 30px 25px;
            -webkit-box-shadow:0 0px 25px rgba(0, 0, 0, 0.07);
            box-shadow:0 0px 25px rgba(0, 0, 0, 0.07);
            border-radius: 20px;
            border:5px solid rgba(0, 0, 0, 0.07);
            margin-bottom: 30px;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover{
            background:#f91942;
            box-shadow:0 8px 20px 0px rgba(0, 0, 0, 0.2);
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover .item, .item:hover span.icon{
            background:#fff;
            border-radius:10px;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover h6, .item:hover p{
            color:#fff;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item .icon {
            font-size: 40px;
            margin-bottom:25px;
            color: #f91942;
            width: 90px;
            height: 90px;
            line-height: 96px;
            border-radius: 50px;
        }
        .item .feature_box_col_one{
            background:rgba(247, 198, 5, 0.20);
            color:#f91942
        }
        .item .feature_box_col_two{
            background:rgba(255, 77, 28, 0.15);
            color:#f91942
        }
        .item .feature_box_col_three{
            background:rgba(0, 147, 38, 0.15);
            color:#f91942
        }
        .item .feature_box_col_four{
            background:rgba(0, 108, 255, 0.15);
            color:#f91942
        }
        .item .feature_box_col_five{
            background:rgba(146, 39, 255, 0.15);
            color:#f91942
        }
        .item .feature_box_col_six{
            background:rgba(23, 39, 246, 0.15);
            color:#f91942
        }
        .item p{
            font-size:15px;
            line-height:26px;
        }
        .item h6 {
            margin-bottom:20px;
            color:#2f2f2f;
        }
        .mission p {
            margin-bottom: 10px;
            font-size: 15px;
            line-height: 28px;
            font-weight: 500;
        }
        .mission i {
            display: inline-block;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            background: #f91942;
            border-radius: 50%;
            color: #fff;
            font-size: 25px;
        }
        .mission .small-text {
            margin-left: 10px;
            font-size: 13px;
            color: #666;
        }
        .skills {
            padding-top:0px;
        }
        .skills .prog-item {
            margin-bottom: 25px;
        }
        .skills .prog-item:last-child {
            margin-bottom: 0;
        }
        .skills .prog-item p {
            font-weight: 500;
            font-size: 15px;
            margin-bottom: 10px;
        }
        .skills .prog-item .skills-progress {
            width: 100%;
            height: 10px;
            background: #e0e0e0;
            border-radius:20px;
            position: relative;
        }
        .skills .prog-item .skills-progress span {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            background: #f91942;
            width: 10%;
            border-radius: 10px;
            -webkit-transition: all 1s;
            transition: all 1s;
        }
        .skills .prog-item .skills-progress span:after {
            content: attr(data-value);
            position: absolute;
            top: -5px;
            right: 0;
            font-size: 10px;
            font-weight:600;
            color: #fff;
            background:rgba(0, 0, 0, 0.9);
            padding: 3px 7px;
            border-radius: 30px;
        }
    </style>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection
