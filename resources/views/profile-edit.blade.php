@extends('layouts.main')
@section('content')
    <div class="container product_section_container" style="padding: 30px;margin-top: 170px">
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document"style="margin-top: 90px">
                    <div class="modal-content">
                        <div class="modal-body">
                           İban Numaranızın Başında TR Kullanmadan Boşluksuz Bir Şekilde Giriniz...
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                {!!Form::model($userDetails, ['route' => ['profile.update', $userDetails->id], "method" =>  "put","files" => true])!!}
                <button type="button" class="btn btn-outline-warning mb-2" data-toggle="modal" data-target="#exampleModal" style="font-size: x-small">
                    Bilgilendirme
                </button>
                {!! Form::bsText("phone","İban Numarası",null,['class'=>'form-control select-input','maxlength'=>'20', 'style' => 'color:black']) !!}<!-- Button trigger modal -->
                {!! Form::bsText("m_phone","Cep Telefonu",null,['class'=>'form-control select-input','maxlength'=>'11', 'style' => 'color:black']) !!}
                {!! Form::bsText("address","Adres",null,['class'=>'form-control select-input', 'style' => 'color:black']) !!}
                {!! Form::bsText("city","Şehir",null,['class'=>'form-control select-input','maxlength'=>'50', 'style' => 'color:black']) !!}
                {!! Form::bsText("country","Ülke",null,['class'=>'form-control select-input','maxlength'=>'50', 'style' => 'color:black']) !!}
                {!! Form::bsText("zipcode","Posta Kodu",null,['class'=>'form-control select-input','maxlength'=>'11', 'style' => 'color:black']) !!}
                {!! Form::bsSubmit("Güncelle") !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
