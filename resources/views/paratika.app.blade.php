<!DOCTYPE html>
<html>
<head>
    <title>Sample POST Form</title>
    <meta charset="utf-8">
    <script>
        function hideCardPANData() {
            if (document
                .getElementById('isPayWithCardToken').checked) {
                document.getElementById('cardPANData').style.display = 'none';
                document
                    .getElementById('cardTokenContainer').style.display = 'block';
            } else {
                document
                    .getElementById('cardTokenContainer').style.display = 'none';
                document.getElementById('cardPANData').style.display = 'block';
            }
        }
    </script>
    <style type="text/css">
        form {
            display: table;
        }

        p {
            display: table-row;
        }

        label {
            display: table-cell;
        }

        input {
            display: table-cell;
        }
    </style>
</head>
<body onload="hideCardPANData();">
<form
        action="https://entegrasyon.paratika.com.tr/merchant/post/sale/[SECURE_SESSION_TOKEN]"
        method="post">
    <div>
        <label for="isPayWithCardToken">Pay with Card Token</label>
        <input type="checkbox" onclick="hideCardPANData();"
               name="isPayWithCardToken" id="isPayWithCardToken" autocomplete="off"
               maxlength="32" />
    </div>
    <div class="container">
        <div id="cardPANData">
            <p>
                <label for="cardOwner">Card Owner Name</label>
                <input type="text" name="cardOwner" id="cardOwner"
                       autocomplete="off" maxlength="32" />
            </p>
            <p>
                <label for="pan">Card Number (PAN) </label>
                <input type="text" id="pan" name="pan" autocomplete="off"
                       maxlength="19" />
            </p>
            <p>
                <label for="expiryMonth">Expiration Date</label>
                <select name="expiryMonth" id="expiryMonth">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <select name="expiryYear" id="expiryYear">
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                    <option value="2029">2029</option>
                    <option value="2030">2030</option>
                    <option value="2031">2031</option>
                    <option value="2032">2032</option>
                    <option value="2033">2033</option>
                    <option value="2034">2034</option>
                </select>
            </p>
            <p>
                <label for="cvv">Security Code (CVV)</label>
                <input type="input" name="cvv" id="cvv"
                       autocomplete="off" maxlength="4" />
            </p>
            <p>
                <label for="saveCard">Save Card</label>
                <input type="checkbox" name="saveCard" id="saveCard"
                       value="YES" />
            </p>
            <p>
                <label for="cardName">Card Name</label>
                <input type="text" name="cardName" id="cardName" />
            </p>
            <p>
                <label for="installmentCount">Installment Count</label>
                <input type="text" name="installmentCount"
                       id="installmentCount" />
            </p>
            <input type="hidden" value="" name="points" id="points" />
            <input type="hidden" value="" name="paymentSystem" id="paymentSystem" />
        </div>
        <div id="cardTokenContainer">
            <p>
                <label for="cardToken">>Card Token</label>
                <input type="text" name="cardToken" id="cardToken"
                       autocomplete="off" maxlength="64" />
            </p>
            <p>
                <label for="installmentCount">Installment Count</label>
                <input type="text" name="installmentCount"
                       id="installmentCount" />
            </p>
        </div>
        <input type="submit" value="Submit" />
</form>
<!-- TMX Profiling -->
<script type="text/javascript"
        src="[https://h.online-metrix.net/fp/tags.js?org_id=6bmm5c3v&&session_id=\{SESSIONTOKEN}&pageid=1|https://h.online-metrix.net/fp/tags.js?org_id=6bmm5c3v&&session_id=%7bSESSIONTOKEN%7d&pageid=1]">
</script>
<noscript>
    <iframe style="width: 100px; height: 100px; border: 0; position: absolute; top: -5000px;" src="[https://h.online-metrix.net/fp/tags.js?org_id=6bmm5c3v&&session_id=\{SESSIONTOKEN}&pageid=1 |https://h.online-metrix.net/fp/tags.js?org_id=6bmm5c3v&&session_id=%7bSESSIONTOKEN%7d&pageid=1%20]"></iframe>
</noscript>

<!-- End profiling tags -->
</body>
</html>
