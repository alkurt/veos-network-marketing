@extends('layouts/main')

@section('content')

    <!-- Checkout Content -->
    <div class="container-fluid no-padding checkout-content" style="margin-top: 170px;"
         xmlns:color="http://www.w3.org/1999/xhtml">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ürün Miktar Bilgilendirme</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Ürün Miktarını Text İçinde rakamları arttırarak veya azaltarak Boş Bir Alana Tıklamanızla Ürün Miktarını Güncellemiş Olursunuz..</p>
                                <p>Ürün Miktarını Text İçinde "0" Girerek Boş Bir Alana Tıklamanızla Ürünü Silmiş Olursunuz..</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModalmb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelmb" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabelmb" style="
                                background: rgba(0,0,0,.6);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.5);
                                border-radius: 10px;
                                color: white;
                            ">&nbsp;Bilgilendirme&nbsp;</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Bayi Yükselt Ürünlerimiz Nakit İndirimini Kapsamamaktadır...</p>
                                <p>Nakit İndiriminizi Diğer Ürünlerimizde Kullanabilirsiniz...</p>
                                <p><strong>Veosnet</strong> ile İyi Alışverişler Dileriz ... </p>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Order Summary -->
                <div class="col-md-12 order-summary">
                    <div class="section-padding"></div>
                    <!-- Section Header -->
                    <div class="section-header">
                        <h3 style="font-family: 'Rage Italic'">SEPETİM</h3>
                    </div><!-- Section Header /- -->
                    <div class="order-summary-content">
                        @if(count(Cart::content())>0)
                            <table class="shop_cart">
                                <thead>
                                <tr>
                                    <th class="product-name">Ürün İsmi</th>
                                    <th class="product-quantity">Ürün Miktarı
                                        <button type="button" class="btn btn-outline-warning mx-3" data-toggle="modal" data-target="#exampleModal" style="font-size: x-small">
                                            ?
                                        </button>
                                    </th><!-- Button trigger modal -->
                                    <th class="product-remove">Ürün Adet Fiyatı</th>
                                    <th class="product-remove">Adet x Tutar</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Cart::content() as $productCartItem)
                                    <tr class="cart_item">
                                        <td data-title="{{$productCartItem->name}}" class="product-name">
                                            <a title="{{$productCartItem->name}}" href="{{ route('product', $productCartItem->options->slug) }}">
                                                {{$productCartItem->name}}
                                            </a>
                                        </td>
                                        <td data-title="Quantity" class="product-quantity">
                                            <div class="quantity">
                                                @php $max=\App\Product::find($productCartItem->id)->miktar;
                                                @endphp
                                                <input type="number" class="quantityf" data-id="{{$productCartItem->rowId}}" value="{{$productCartItem->qty}}" min="0" max="{{$max}}">
                                            </div>
                                        </td>
                                        <td data-title="Total" class="product-subtotal">
                                            <span>{{$productCartItem->price}}
                                    <small> ₺ </small></span></td>
                                        <td data-title="Total" class="product-remove">
                                            <span>{{$productCartItem->price * ($productCartItem->qty) }} <small> ₺</small></span>
                                        </td>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <form action="{{route('basket.destroy')}}" method="POST">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}
                                            <input type="submit" class="btn pull-left " style="background: #fe4c50 ;color:white" value="Tümünü Sil" .>
                                        </form>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <div>
                                <div class="modal fade" id="ustbayi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel1"><p class="">Bayi Yükseltmeye Ne Kaldı ?</p></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @isset ($kariyer)
                                                    @foreach($kariyer as $k)
                                                        @if( Cart::subtotal()  >= $k->kariyerb->tutar - $toplam )
                                                        @else
                                                            <h6> <p> <strong>{{($k->kariyerb->tutar) - $toplam}} ₺</strong> alışveriş yaparsanız <strong>{{$k->kariyername}}</strong> Bayi Olabilirsiniz</p></h6>
                                                        @endif
                                                    @endforeach
                                                @endisset
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Kapat</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Button trigger modal -->
                            </div>
                            <!-- Proceed To Checkout -->
                            <div class="col-md-12 col-sm-12 d-inline-flex">
                                <div class="wc-proceed-to-checkout text-left">
                                    @php
                                        $user=auth()->user();
                                            if ($user->nakitindirim()->exists()){
                                                    if($user->nakitindirim->status)
                                                   {
                                                       $indirim= $user->nakitindirim->indirim_tutar;
                                                   }
                                                }
                                                if($user->kariyer_id >=9 && $user->kariyer_id <= 15){ //Bayi Promosyon Oranı
                                                    if ($user->kariyer->id ==9 ||$user->kariyer->id==10){
                                                        $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get();
                                                        $urun_sayisi=$urunler->count();
                                                        if ($user->kariyer->bayipromosyon()->where('urun_adet',$urun_sayisi)->exists()){
                                                            $indirim_yuzdesi = $user->kariyer->bayipromosyon()->where('urun_adet','>=',$urun_sayisi)->first()->yuzde;
                                                        }
                                                      echo "<p></p>";
                                                    }else{
                                                        $indirim_yuzdesi = $user->kariyer->bayipromosyon()->first()->yuzde;
                                                    }
                                                }
                                    @endphp
                                    @php $sepeturun = \App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get(); @endphp
                                    @foreach($sepeturun as $uruns) @endforeach
                                    @if($uruns->product->turu == 'paket')
                                        @if( auth()->user()->kariyer_id >=1 && auth()->user()->kariyer_id <=4)
                                            <button type="button" class="btn btn-outline-warning mt-4 mb-2" data-toggle="modal" data-target="#ustbayi">
                                                Üst Bayiye Geçmeye Ne Kaldı ?
                                            </button>
                                        @endif
                                            <button type="button" class="mx-2 mt-3 btn btn-outline-info" data-toggle="modal" data-target="#exampleModalmb">
                                                Bayi Yükselt Ürünü Seçtiniz !!
                                            </button>
                                    @else
                                        <div class="mt-4">
                                            <p class="product-price mx-3">Ödemede Uygulanacak İndirimler </p>
                                            <table class="table-responsive table-bordered">
                                                <thead>
                                                <tr class="text-center">
                                                    <th>İndirim</th>
                                                    <th>Miktar / Yuzde</th>
                                                </tr>
                                                </thead>
                                                <tbody class="text-right">
                                                @isset($indirim)
                                                    <tr class="text-left">
                                                        <td>Eft Alışveriş İndirimi </td>
                                                        <td class="text-right">{{$indirim}} <small class="text-muted"> ₺</small></td>
                                                    </tr>
                                                @endisset
                                                @isset($indirim_yuzdesi)
                                                    <tr class="text-left">
                                                        <td>Bayi Promosyon İndirim Yüzdesi </td>
                                                        <td class="text-right"> %{{$indirim_yuzdesi}}</td>
                                                    </tr>
                                                @endisset
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif

                                </div>
                                <div class="wc-proceed-to-checkout ml-auto">
                                    @php
                                        $user=auth()->user();
                                            if ($user->nakitindirim()->exists()){
                                                    if($user->nakitindirim->status)
                                                   {
                                                       $indirim= $user->nakitindirim->indirim_tutar;
                                                      $tutar=Cart::subtotal()-($indirim);
                                                   }
                                                }
                                                if($user->kariyer_id >=9 && $user->kariyer_id <= 15){ //Bayi Promosyon Oranı
                                                    if ($user->kariyer->id ==9 ||$user->kariyer->id==10){
                                                        $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get();
                                                        $urun_sayisi=$urunler->count();
                                                         if ($user->kariyer->bayipromosyon()->where('urun_adet',$urun_sayisi)->exists()){
                                                            $indirim_yuzdesi = $user->kariyer->bayipromosyon()->where('urun_adet','>=',$urun_sayisi)->first()->yuzde;
                                                        }
                                                         else $indirim_yuzdesi = 0;
                                                      echo "<p></p>";
                                                    }else{
                                                        $indirim_yuzdesi = $user->kariyer->bayipromosyon()->first()->yuzde;
                                                    }
                                                    if (isset($tutar)) {
                                                        $tutar =$tutar- $indirim_yuzdesi*$tutar/100;
                                                    }
                                                    else {
                                                        $tutar=Cart::subtotal()-(Cart::subtotal() * $indirim_yuzdesi /100);
                                                    }
                                                }
                                    @endphp
                                    <div>
                                        <h4 style="font-family: 'Harlow Solid Italic'"> Ödenecek Toplam Tutar</h4>
                                        <div style="color: black">
                                            @php
                                                $user=auth()->user();
                                                    if ($user->nakitindirim()->exists()){
                                                            if($user->nakitindirim->status)
                                                           {
                                                               $indirim= $user->nakitindirim->indirim_tutar;
                                                              $tutar=Cart::subtotal()-($indirim);
                                                           }
                                                        }
                                                        if($user->kariyer_id >=9 && $user->kariyer_id <= 15){ //Bayi Promosyon Oranı
                                                            if ($user->kariyer->id ==9 ||$user->kariyer->id==10){
                                                                $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get();
                                                                $urun_sayisi=$urunler->count();
                                                                 if ($user->kariyer->bayipromosyon()->where('urun_adet',$urun_sayisi)->exists()){
                                                                    $indirim_yuzdesi = $user->kariyer->bayipromosyon()->where('urun_adet','>=',$urun_sayisi)->first()->yuzde;
                                                                }
                                                                 else $indirim_yuzdesi = 0;
                                                              echo "<p></p>";
                                                            }else{
                                                                $indirim_yuzdesi = $user->kariyer->bayipromosyon()->first()->yuzde;
                                                            }
                                                            if (isset($tutar)) {
                                                                $tutar =$tutar- $indirim_yuzdesi*$tutar/100;
                                                            }
                                                            else {
                                                                $tutar=Cart::subtotal()-(Cart::subtotal() * $indirim_yuzdesi /100);
                                                            }

                                                        }
                                            @endphp
                                            @php $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get(); @endphp
                                            @foreach($urunler as $urunler)@endforeach
                                            @if($urunler->product-> turu == 'urun')
                                                <span class="price">
                                           @isset($tutar) {{$tutar}} @else{{Cart::subtotal()}} @endisset
                                        <small> ₺</small>
                                        </span>
                                            @else
                                                <span class="price">
                                           {{Cart::subtotal()}}
                                        <small> ₺</small>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <a href="{{route('payment')}}" class="mt-3  btn-danger text-white btn-lg" title="Fatura Bilgileri">Ödemeye Geç</a>

                                </div>
                            </div><!-- Proceed To Checkout /- -->
                        @else
                            <div class="container-fluid no-padding checkout-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 order-summary">
                                            <div class="alert alert-danger text-center">
                                                <h2 style="font-family: 'Rage Italic'">Sepetinizde Ürün Bulunmamaktadır!</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </div><!-- Order Summary /- -->
            </div>
        </div><!-- Container /- -->
        <div class="section-padding"></div>
    </div><!-- Checkout Content /- -->
@endsection
@section('js')
    <script type="text/javascript">
        $(function(){
            $('.quantityf').on('change', function() {
                var id = $(this).attr('data-id');
                var max = $(this).attr('max');
                var quantity = $(this).val();
                max = parseInt(max);
                quantity = parseInt(quantity);
                if(quantity > max) {
                    document.getElementsByClassName('quantityf').value=max;
                    Swal.fire({
                        icon: 'error',
                        title: 'Uyarı',
                        text: 'Girdiğiniz ürün miktarı ürün stoğunu aşmaktadır.',
                        footer: '<span class="alert alert-primary w-auto font-weight-bold"> Ürün stok durumu :'+ max +' ürün mevcut</span>'
                    }).then(function () {
                        window.location.href = '{{ route('basket') }}';
                    })
                }else{
                    toastr.options.timeOut = 4500;
                    $.ajax({
                        type: "PATCH",
                        url: '{{ url('basket/update') }}' + '/' + id,
                        data: {
                            'quantity': this.value,
                        },
                        success: function(data) {
                            console.log(data);
                            toastr.success('Güncelleme Başarılı!!');
                            window.location.href = '{{ route('basket') }}';
                        }
                    });
                }
            });
        });
    </script>
@endsection
