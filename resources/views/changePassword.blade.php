@extends('layouts.app')
@section('content')
    <div class="container" >
        <link rel="stylesheet" href="{{asset('css/change.css')}}">
        <div class="row justify-content-center mt-5">
            <div class="col-md-8">
                <div class=" ">
                    <div class="box">
                    <div  style="color: white" class="mb-3">VEOS NETWORK & MARKETİNG</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('change.password') }}">
                            @csrf

                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach

                            <div class="form-group row">
                                    <label style="color: white" for="password" class="col-md-4 col-form-label text-md-right">Şifreniz</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label style="color: white" for="password" class="col-md-4 col-form-label text-md-right">Yeni Şifreniz</label>

                                <div class="col-md-6">
                                    <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">

                                </div>
                            </div>

                            <div class="form-group row">
                                <label style="color: white" for="password" class="col-md-4 col-form-label text-md-right">Yeni Şifrenizi Onaylayınız</label>

                                <div class="col-md-6">
                                    <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">

                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-lg w-50 btn-outline-light text-secondary font-weight-bold mx-4 mt-3" >
                                        Güncelle
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')

    @endsection


