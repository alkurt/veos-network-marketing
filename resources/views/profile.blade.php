@extends('layouts.main')
@section('content')
    <div class=" product_section_container" style="margin-top: 140px">
        <div class="row">
            <div class="col-md-12">
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Profil Fotoğrafı Yükle</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <form method="post" action="{{route('profil-create.store')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="file" name="cover">
                                        <button class="btn btn-outline-dark" style="font-size: small" type="submit">Yükle</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               @if($varmi == true)
                    <img class="img-fluid float-right mb-3 mx-2"  style="height: 100px;width: 100px;border-radius: 120px" src="{{\Illuminate\Support\Facades\Storage::url($profil_create->cover)}}" alt="">
                   @else
                    <img class="img-fluid float-right mb-3 "  style="height: 100px;width: 100px;border-radius: 60px" src="{{asset('assets/new images/bos-profil.jpg')}}" alt="">
                @endif
                <div class="table-responsive">
                    @if($varmi ==true)
                        <a class="btn btn-outline-warning float-right mx-3" style="font-size: small" onclick="return confirm('Emin Misiniz?')" data-method="delete" href="{{route('psil',$profil_create->id)}}">Profil Fotoğrafımı Kaldır</a>
                        @else
                        <button type="button" class="mx-2 btn btn-outline-info float-right" style="font-size: small" data-toggle="modal" data-target="#exampleModal">
                            Profil Fotoğrafı Yükle
                        </button>
                    @endif
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>İsim Soyisim</th>
                        <th>E-Mail</th>
                        <th>Adres</th>
                        <th>İban Numarası</th>
                        <th>Cep Telefonu</th>
                        <th>Şehir</th>
                        <th>Ülke</th>
                        <th>Posta Kodu</th>
                        <th>Güncelle</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($userDetails as $userDetail)
                        <tr>
                            <td>{{ $userDetail->user->name }} {{ $userDetail->user->surname }}</td>
                            <td>{{ $userDetail->user->email }}</td>
                            <td>{{ $userDetail->address }} </td>
                            <td>{{ $userDetail->phone }}</td>
                            <td>{{ $userDetail->m_phone }}</td>
                            <td>{{ $userDetail->city }}</td>
                            <td>{{ $userDetail->country }}</td>
                            <td>{{ $userDetail->zipcode }}</td>
                            <td class="text-center">
                                <a href="/profile/{{auth()->user()->id}}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection
