@extends('layouts.main')
@section('content')
    <!-- Checkout Content -->
    <div class="container-fluid no-padding checkout-content" style="margin-top: 140px">
        <!-- Container -->
        <div class="container" >
            <br>
            @if(!Auth::user()->detail->address)
                <div class="alert alert-danger">
                    Lütfen <strong>Profilinizi</strong> Tamamlayınız!
                    <br>
                    <a class="text-info" href="/profile/{{auth()->user()->id}}/edit">Profili Düzenle</a>
                </div>
                <br>
                <br>
            @else
                <div class="row">
                    <div class="col-md-12 payment-mode">
                        <div class="section-padding"></div>
                        <div class="container">
                            <div class="col-md-12 mb-4">
                                <div class="row">
                                    <div class="section-title col-md-6 col-sm-12">
                                        <h3 class="text-info" style="font-family: 'Harlow Solid Italic'"><strong>İletişim ve Fatura Bilgileriniz</strong></h3>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <a class="btn btn-outline-info"
                                           href="{{ url('/profile')}}">Farklı Adrese Göndermek İstiyorum !!</a>
                                    </div>
                                </div>
                            </div>
                            <form action="{{route('pay')}}" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">İsim</label>
                                            <input type="text" class="form-control text-secondary" name="name" id="name"
                                                   value="{{Auth::user()->name}}" required disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="surname">Soyisim</label>
                                            <input type="text" class="form-control text-secondary" name="surname" id="surname"
                                                   value="{{Auth::user()->surname}}" required disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">İban Numarası</label>
                                            <input type="text" class="form-control text-secondary phone" name="phone" id="phone"
                                                   value="{{$user_detail->phone}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="m_phone">Telefon Numaranız</label>
                                            <input type="text" class="form-control m_phone text-secondary" name="m_phone" id="m_phone"
                                                   value="{{$user_detail->m_phone}}" required disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">Şehir</label>
                                            <input type="text" class="form-control city" name="city" id="city"
                                                   placeholder="{{$user_detail->city}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Ülke</label>
                                            <input type="text" class="form-control country" name="country" id="country"
                                                   placeholder="{{$user_detail->country}}" required disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="zipcode">Posta Kodu</label>
                                            <input type="text" class="form-control zipcode" name="zipcode" id="zipcode"
                                                   placeholder="{{$user_detail->zipcode}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="address">Adres</label>
                                            <input type="text" class="form-control text-secondary" name="address" id="address"
                                                   value="{{ $user_detail->address }}" required disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center alert alert-secondary" style="border-color: black">
                                    <h3 style="font-family: 'Harlow Solid Italic'"> Ödenecek Toplam Tutar</h3>
                                    <div style="color: black">
                                        @php
                                            $user=auth()->user();
                                                if ($user->nakitindirim()->exists()){
                                                        if($user->nakitindirim->status)
                                                       {
                                                           $indirim= $user->nakitindirim->indirim_tutar;
                                                          $tutar=Cart::subtotal()-($indirim);
                                                       }
                                                    }
                                                    if($user->kariyer_id >=9 && $user->kariyer_id <= 15){ //Bayi Promosyon Oranı
                                                        if ($user->kariyer->id ==9 ||$user->kariyer->id==10){
                                                            $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get();
                                                            $urun_sayisi=$urunler->count();
                                                             if ($user->kariyer->bayipromosyon()->where('urun_adet',$urun_sayisi)->exists()){
                                                                $indirim_yuzdesi = $user->kariyer->bayipromosyon()->where('urun_adet','>=',$urun_sayisi)->first()->yuzde;
                                                            }
                                                             else $indirim_yuzdesi = 0;
                                                          echo "<p></p>";
                                                        }else{
                                                            $indirim_yuzdesi = $user->kariyer->bayipromosyon()->first()->yuzde;
                                                        }
                                                        if (isset($tutar)) {
                                                            $tutar =$tutar- $indirim_yuzdesi*$tutar/100;
                                                        }
                                                        else {
                                                            $tutar=Cart::subtotal()-(Cart::subtotal() * $indirim_yuzdesi /100);
                                                        }

                                                    }
                                        @endphp
                                        @php $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get(); @endphp
                                        @foreach($urunler as $urunler)@endforeach
                                            @if($urunler->product-> turu == 'urun')
                                                <span class="price">
                                           @isset($tutar) {{$tutar}} @else{{Cart::subtotal()}} @endisset
                                        <small> ₺</small>
                                        </span>
                                            @else
                                                <span class="price">
                                           {{Cart::subtotal()}}
                                        <small> ₺</small>
                                        </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="modal fade" id="ms" tabindex="-1" role="dialog" aria-labelledby="mlabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ödeme Seçenekleri Bilgilendirme</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p> Şuan Şeçtiğiniz Ödeme Türünde, Ödeme Hizmeti Verememekteyiz.</p>
                                                <p>Anlayışınız İçin Teşekkür Eder, Sağlıklı Günler Dileriz!! <strong>{{config('app.name')}}</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center " style="border-color: black">
                                    <div class="form-group text-center">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" id="Havale/Eft" value="Havale/Eft" required>
                                            <label class="form-check-label" style="font-size: 15px; font-weight: bold;color: gray" for="Havale/Eft">Havale / Eft ile</label>
                                        </div>
                                        <div class="form-check form-check-inline ml-5">
                                            <input class="form-check-input" type="radio" name="type" id="mailorder" value="mailorder" disabled required>
                                            <label  type="button"  data-toggle="modal" data-target="#mb" class="form-check-label" style="font-size: 15px; font-weight: bold;color: gray" for="mailorder">Mail Order ile</label>
                                        </div>
                                        <div class="form-check form-check-inline ml-5">
                                            <input class="form-check-input" type="radio" name="type" id="Kredi Kartı" value="Kredi Kartı">
                                            <label class="form-check-label" style="font-size: 15px; font-weight: bold;color: gray" for="Kredi Kartı">Banka / Kredi Kartı ile</label>
                                        </div>
                                        @if((float)$cuzdan->toplam_bakiye > (float)Cart::subtotal())
                                            <div class="form-check form-check-inline ml-5">
                                                <button type="button" class="btn btn-outline-secondary" style="font-size: 15px; font-weight: bold" data-target="#exampleModal" data-toggle="modal" for="Kredi Kartı">Cüzdanımdan Öde</button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!-- Modal -->

                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-outline-info btn-lg"> Ödeme Yap</button>
                                    </div>
                                </div>
                            </form>
                            @if((float)$cuzdan->toplam_bakiye > (float)Cart::subtotal())
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ödeme Ayrıntısı</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body " style="background-color: #fff;">
                                                <div class="row alert alert-info">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="row" >
                                                            <div class="col-md-4 col-sm-12 ">
                                                                <span><strong>Hesabınızdaki Bakiye :</strong> {{$cuzdan->toplam_bakiye}}</span>
                                                            </div>
                                                            <div class="col-md-4 col-sm-12">
                                                                <span> <strong>Ödenecek Tutar</strong>   {{Cart::subtotal()}}</span>
                                                            </div>
                                                            <div class="col-md-4 col-sm-12"><strong>Kalacak Hesap Bakiyesi : </strong> {{  (float)$cuzdan->toplam_bakiye - (float)Cart::subtotal()}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Vazgeç</button>
                                                <form action="{{route('cuzdan.odeme',(float)Cart::subtotal())}}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-success">Onayla</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="section-padding"></div>
                </div>
            @endif
        </div>
    </div>
@endsection
