@extends('layouts/main')
@section('title','Anasayfa || Sipariş Ver Üye Ol Üye Ekle||VeosNet Network&Marketing')
@section('meta')
    <meta type="keywords" content="Veosnet.com, Kişisel Bakım, Temizlik, Kazanç, Kazandıran Girişim">
@endsection
@section('content')
    <div>
        <div  style="margin-top: 118px">
            <!-- slider start -->
            <div class="CSSgal">
                <!-- Don't wrap targets in parent -->
                <s id="s1"></s>
                <s id="s2"></s>
                <s id="s3"></s>
                <s id="s4"></s>
                <div class="slider">
                    @forelse($slider as $slider)
                        <div>
                            <img class="img-fluid" style="height: 520px;width:100%"  src="{{\Illuminate\Support\Facades\Storage::url($slider->cover)}}" alt="" />
                        </div>
                    @empty
                        <img class="img-fluid" style="height: 520px" src='https://images.pexels.com/photos/572056/pexels-photo-572056.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' alt="">
                    @endforelse
                </div>
                <div class="prevNext ">
                    <div><a href="#s4"></a><a href="#s2"></a></div>
                    <div><a href="#s1"></a><a href="#s3"></a></div>
                    <div><a href="#s2"></a><a href="#s4"></a></div>
                    <div><a href="#s3"></a><a href="#s1"></a></div>
                </div>
                <div class="bullets">
                    <a href="#s1"></a>
                    <a href="#s2"></a>
                    <a href="#s3"></a>
                    <a href="#s4"></a>
                </div>
            </div>
            <!-- slider end -->
        </div>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="section_title new_arrivals_title">
                        <div class="text-center">
                            @if($varmi == true)
                                <img style="height: 100px;width: 100px;border-radius: 100px;height: 70px;width: 70px;width:100px;height:100px;}"  src="{{\Illuminate\Support\Facades\Storage::url($logo->cover)}}"
                                     alt=""/>
                            @else
                                <p style="font-size: small">Yüklü Logo Bulunamadı</p>
                            @endif
                        </div>
                        <h4 style="font-family: 'Harlow Solid Italic';opacity: 0.6">Size Özel Öneriler</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="product-grid"
                         data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
                        @foreach($products as $product)
                            <a href="/product/{{$product->slug}}">
                                <div class="product-item {{$product->category_id}} ">
                                    <div class="product discount product_filter">
                                        <div class="product_image"style="margin-top: -22px">
                                            {!! $product->thumbs !!}
                                        </div>
                                        <div class="product_info mt-3">
                                            <small class="product_name " ><a title="{{$product->product_name}} href="/product/{{$product->slug}}">{{str_limit( $product->product_name,20 )}}</a></small><br>
                            <small class="product_name" ><a href="/product/{{$product->slug}}">{{ $product->code }}</a><i class="fa fa-search mx-2"></i></small><br>
                            <div class="col-md-4 col-sm-12 product_price" style="font-size: 12px">{{ number_format($product->dolar,2,',','.') }}$</div>
                            <div class="col-md-4 col-sm-12 product_price" style="font-size:12px">{{ number_format($product->kisiself,2,',','.') }}₺</div>
                            <div class="col-md-4 col-sm-12 product_price " style="font-size: 12px">{{ number_format($product->euro,2,',','.') }}€</div>
                            <input type="number" class="quantity" id="quantity" name="quantity" value="1" min="0" max="{{$product->miktar}}"
                                   style="width: 50px; margin-right: 10px;height: 18px;border: 1px solid #FE7C7F;margin-bottom: 5px">
                    </div>
                    <div class="add_to_cart_button red_button"><a href="{{ route('basket.create', ['id' => $product->id]) }}">Ürünü Ekle</a></div>
                </div>
            </div>
            </a>
            @endforeach
        </div>
    </div>
    </div>
    </div>
    <!-- Deal of the week -->
    <div class="container-fluid mb-5" style="width: 100%" >
        <h2 class="text-center" style="color:white;background:#cdd6f9;font-family:'Open Sans'" ><strong>  ↓  &nbsp;F I R S A T&nbsp;&nbsp; Ü R Ü N L E R  &nbsp; ↓  </strong></h2><br><br>
        <section class="customer-logos slider">
            @foreach($f_products as $f_products)
                <div class="slide" style="border:3px solid darkslategray;border-radius: 80px"><a href="/product/{{$f_products->slug}}"><span class="" title="{{$f_products->product_name}} {{$f_products->product_price}} ₺">{!! $f_products->thumbs !!} <p class="text-center"><i class="fa fa-search-plus text-info"></i>{{$f_products->code}}</p> </span></a></div>
            @endforeach
        </section>
    </div>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <br>
    <div class=""  style="width: 100%" >
        <h2 class="text-center " style="color:white;background:#cdd6f9;font-family:'Open Sans'" ><strong>  ↓ &nbsp;V E O S &nbsp;K A T E G O R İ  &nbsp; ↓  </strong></h2>
        <br>
        <section class="customer-logos slider">
            <div class="slide" ><a href="{{route("haberler2")}}"><img style="border-radius: 20px" src="{{asset("assets/new images/hb.png")}}"></a></div>
            <div class="slide"><a href="{{route('kullanici_katalog')}}"><img style="border-radius: 20px" src="{{asset("assets/new images/kt.png")}}"></a></div>
            <div class="slide" ><a href="/category/temizlik"><img style="border-radius: 20px" src="{{asset("assets/new images/kp.png")}}"></a></div>
            <div class="slide"><a href="{{route("sss")}}"><img src="{{asset("assets/new images/sss.png")}}"></a></div>
            <div class="slide" ><a href="{{route("basari")}}"><img src="{{asset("assets/new images/basari.jpeg")}}"></a></div>
            <div class="slide"><a href="{{route("etkinlikler")}}"><img  style="border-radius: 20px" src="{{asset("assets/new images/yeniyasam.jpg")}}"></a></div>
            <div class="slide" ><a href="{{route("etkinlikler")}}"><img style="border-radius: 20px" src="{{asset("assets/new images/dogruekip.jpg")}}"></a></div>
        </section>
    </div>

    <div class="benefit">
        <div class="container">
            <div class="row ">
                @isset($firsat)
                    <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                        <p>{!! $firsat->bir !!}</p>
                    </div>
                    <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                        <p>{!! $firsat->iki !!}</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                        <p>{!! $firsat->uc !!}</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                        <p>{!! $firsat->dort !!}</p>
                    </div>
                @else
                    <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                        <p>300 ₺ ve Üzeri Alışverişlerde <br> Ücretsiz Kargo</p>
                    </div>
                    <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                        <p>Kazanıyor, Kazandırıyor...</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                        <p>Bizim İçin Değerlisiniz... </p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                        <p>Açılış Saatimiz 09:00</p>
                        <p>Kapanış Saatimiz 18:00</p>
                    </div>
                @endisset
            </div>
        </div>
    </div>
    </div>
@endsection
@section('js')
    <script type='text/javascript'>
        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
    <script>
        var slideIndex = 1;
        showDivs(slideIndex);
        function plusDivs(n) {
            showDivs(slideIndex += n);
        }
        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            if (n > x.length) {slideIndex = 1}
            if (n < 1) {slideIndex = x.length} ;
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            x[slideIndex-1].style.display = "block";
        }
    </script>
    <script>
        $('.add_to_cart_button').find('a').click(function (event) {
            event.preventDefault();
            var quantity = $(this).parent().prev().find('input').val();
            var max= $(this).parent().prev().find('input').attr('max')
            console.log(quantity,max)
            if(quantity > max) {
                Swal.fire({
                    icon: 'error',
                    title: 'Uyarı',
                    text: 'Girdiğiniz ürün miktarı ürün stoğunu aşmaktadır.',
                    footer: '<span class="alert alert-primary w-auto font-weight-bold"> Ürün stok durumu :'+ max +' ürün mevcut</span>'
                })
            }else{
                $.ajax({
                    type: "POST",
                    url: $(this).attr('href'),
                    data: {quantity: quantity , _token: '{{csrf_token()}}' }
                    , success: function (data) {
                        console.log(data);
                        $('#checkout_items').html(data.cartCount);
                    }
                });
                return false; //for good measure
            }
        });
    </script>
    <script>
        (function() {
            var $$ = function(selector, context) {
                var context = context || document;
                var elements = context.querySelectorAll(selector);
                return [].slice.call(elements);
            };
            function _fncSliderInit($slider, options) {
                var prefix = ".fnc-";
                var $slider = $slider;
                var $slidesCont = $slider.querySelector(prefix + "slider__slides");
                var $slides = $$(prefix + "slide", $slider);
                var $controls = $$(prefix + "nav__control", $slider);
                var $controlsBgs = $$(prefix + "nav__bg", $slider);
                var $progressAS = $$(prefix + "nav__control-progress", $slider);

                var numOfSlides = $slides.length;
                var curSlide = 1;
                var sliding = false;
                var slidingAT = +parseFloat(getComputedStyle($slidesCont)["transition-duration"]) * 1000;
                var slidingDelay = +parseFloat(getComputedStyle($slidesCont)["transition-delay"]) * 1000;

                var autoSlidingActive = false;
                var autoSlidingTO;
                var autoSlidingDelay = 5000; // default autosliding delay value
                var autoSlidingBlocked = false;

                var $activeSlide;
                var $activeControlsBg;
                var $prevControl;

                function setIDs() {
                    $slides.forEach(function($slide, index) {
                        $slide.classList.add("fnc-slide-" + (index + 1));
                    });

                    $controls.forEach(function($control, index) {
                        $control.setAttribute("data-slide", index + 1);
                        $control.classList.add("fnc-nav__control-" + (index + 1));
                    });

                    $controlsBgs.forEach(function($bg, index) {
                        $bg.classList.add("fnc-nav__bg-" + (index + 1));
                    });
                };

                setIDs();
                function afterSlidingHandler() {
                    $slider.querySelector(".m--previous-slide").classList.remove("m--active-slide", "m--previous-slide");
                    $slider.querySelector(".m--previous-nav-bg").classList.remove("m--active-nav-bg", "m--previous-nav-bg");

                    $activeSlide.classList.remove("m--before-sliding");
                    $activeControlsBg.classList.remove("m--nav-bg-before");
                    $prevControl.classList.remove("m--prev-control");
                    $prevControl.classList.add("m--reset-progress");
                    var triggerLayout = $prevControl.offsetTop;
                    $prevControl.classList.remove("m--reset-progress");

                    sliding = false;
                    var layoutTrigger = $slider.offsetTop;

                    if (autoSlidingActive && !autoSlidingBlocked) {
                        setAutoslidingTO();
                    }
                };

                function performSliding(slideID) {
                    if (sliding) return;
                    sliding = true;
                    window.clearTimeout(autoSlidingTO);
                    curSlide = slideID;

                    $prevControl = $slider.querySelector(".m--active-control");
                    $prevControl.classList.remove("m--active-control");
                    $prevControl.classList.add("m--prev-control");
                    $slider.querySelector(prefix + "nav__control-" + slideID).classList.add("m--active-control");

                    $activeSlide = $slider.querySelector(prefix + "slide-" + slideID);
                    $activeControlsBg = $slider.querySelector(prefix + "nav__bg-" + slideID);

                    $slider.querySelector(".m--active-slide").classList.add("m--previous-slide");
                    $slider.querySelector(".m--active-nav-bg").classList.add("m--previous-nav-bg");

                    $activeSlide.classList.add("m--before-sliding");
                    $activeControlsBg.classList.add("m--nav-bg-before");

                    var layoutTrigger = $activeSlide.offsetTop;

                    $activeSlide.classList.add("m--active-slide");
                    $activeControlsBg.classList.add("m--active-nav-bg");

                    setTimeout(afterSlidingHandler, slidingAT + slidingDelay);
                };

                function controlClickHandler() {
                    if (sliding) return;
                    if (this.classList.contains("m--active-control")) return;
                    if (options.blockASafterClick) {
                        autoSlidingBlocked = true;
                        $slider.classList.add("m--autosliding-blocked");
                    }

                    var slideID = +this.getAttribute("data-slide");

                    performSliding(slideID);
                };

                $controls.forEach(function($control) {
                    $control.addEventListener("click", controlClickHandler);
                });

                function setAutoslidingTO() {
                    window.clearTimeout(autoSlidingTO);
                    var delay = +options.autoSlidingDelay || autoSlidingDelay;
                    curSlide++;
                    if (curSlide > numOfSlides) curSlide = 1;

                    autoSlidingTO = setTimeout(function() {
                        performSliding(curSlide);
                    }, delay);
                };

                if (options.autoSliding || +options.autoSlidingDelay > 0) {
                    if (options.autoSliding === false) return;

                    autoSlidingActive = true;
                    setAutoslidingTO();

                    $slider.classList.add("m--with-autosliding");
                    var triggerLayout = $slider.offsetTop;

                    var delay = +options.autoSlidingDelay || autoSlidingDelay;
                    delay += slidingDelay + slidingAT;

                    $progressAS.forEach(function($progress) {
                        $progress.style.transition = "transform " + (delay / 1000) + "s";
                    });
                }
                $slider.querySelector(".fnc-nav__control:first-child").classList.add("m--active-control");
            };
            var fncSlider = function(sliderSelector, options) {
                var $sliders = $$(sliderSelector);

                $sliders.forEach(function($slider) {
                    _fncSliderInit($slider, options);
                });
            };
            window.fncSlider = fncSlider;
        }());
        /* not part of the slider scripts */
        /* Slider initialization
        options:
        autoSliding - boolean
        autoSlidingDelay - delay in ms. If audoSliding is on and no value provided, default value is 5000
        blockASafterClick - boolean. If user clicked any sliding control, autosliding won't start again
        */
        fncSlider(".example-slider", {autoSlidingDelay: 4000});
        var $demoCont = document.querySelector(".demo-cont");
        [].slice.call(document.querySelectorAll(".fnc-slide__action-btn")).forEach(function($btn) {
            $btn.addEventListener("click", function() {
                $demoCont.classList.toggle("credits-active");
            });
        });
        document.querySelector(".demo-cont__credits-close").addEventListener("click", function() {
            $demoCont.classList.remove("credits-active");
        });
        document.querySelector(".js-activate-global-blending").addEventListener("click", function() {
            document.querySelector(".example-slider").classList.toggle("m--global-blending-active");
        });
    </script>
    <script type="text/javascript" src="/kar_3.js"></script>

    <script type="text/javascript">
        snowStorm.flakesMax = 256;
        snowStorm.flakesMaxActive = 128;
        snowStorm.snowCharacter = '*';
    </script>

    <script>
        $(document).ready(function(){
            $('.customer-logos').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1500,
                arrows: false,
                dots: false,
                pauseOnHover: false,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
            });
        });
    </script>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection
@section("css")
    <style>
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {}
        a,
        a:hover,
        a:focus,
        a:active {
            text-decoration: none;
            outline: none;
        }

        a,
        a:active,
        a:focus {
            color: #333;
            text-decoration: none;
            transition-timing-function: ease-in-out;
            -ms-transition-timing-function: ease-in-out;
            -moz-transition-timing-function: ease-in-out;
            -webkit-transition-timing-function: ease-in-out;
            -o-transition-timing-function: ease-in-out;
            transition-duration: .2s;
            -ms-transition-duration: .2s;
            -moz-transition-duration: .2s;
            -webkit-transition-duration: .2s;
            -o-transition-duration: .2s;
        }

        ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        img {
            max-width: 100%;
            height: auto;
        }
        span, a, a:hover {
            display: inline-block;
            text-decoration: none;
            color: inherit;
        }
        .section-head {
            margin-bottom: 60px;
        }
        .section-head h4 {
            position: relative;
            padding:0;
            color:#f91942;
            line-height: 1;
            letter-spacing:0.3px;
            font-size: 34px;
            font-weight: 700;
            text-align:center;
            text-transform:none;
            margin-bottom:30px;
        }
        .section-head h4:before {
            content: '';
            width: 60px;
            height: 3px;
            background: #f91942;
            position: absolute;
            left: 0px;
            bottom: -10px;
            right:0;
            margin:0 auto;
        }
        .section-head h4 span {
            font-weight: 700;
            padding-bottom: 5px;
            color:#2f2f2f
        }
        p.service_text{
            color:#cccccc !important;
            font-size:16px;
            line-height:28px;
            text-align:center;
        }
        .section-head p, p.awesome_line{
            color:#818181;
            font-size:16px;
            line-height:28px;
            text-align:center;
        }

        .extra-text {
            font-size:34px;
            font-weight: 700;
            color:#2f2f2f;
            margin-bottom: 25px;
            position:relative;
            text-transform: none;
        }
        .extra-text::before {
            content: '';
            width: 60px;
            height: 3px;
            background: #f91942;
            position: absolute;
            left: 0px;
            bottom: -10px;
            right: 0;
            margin: 0 auto;
        }
        .extra-text span {
            font-weight: 700;
            color:#f91942;
        }
        .item {
            background: #fff;
            text-align: center;
            padding: 30px 25px;
            -webkit-box-shadow:0 0px 25px rgba(0, 0, 0, 0.07);
            box-shadow:0 0px 25px rgba(0, 0, 0, 0.07);
            border-radius: 20px;
            border:5px solid rgba(0, 0, 0, 0.07);
            margin-bottom: 30px;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover{
            background:#f91942;
            box-shadow:0 8px 20px 0px rgba(0, 0, 0, 0.2);
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover .item, .item:hover span.icon{
            background:#fff;
            border-radius:10px;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover h6, .item:hover p{
            color:#fff;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item .icon {
            font-size: 40px;
            margin-bottom:25px;
            color: #f91942;
            width: 90px;
            height: 90px;
            line-height: 96px;
            border-radius: 50px;
        }
        .item .feature_box_col_one{
            background:rgba(247, 198, 5, 0.20);
            color:#f91942
        }
        .item .feature_box_col_two{
            background:rgba(255, 77, 28, 0.15);
            color:#f91942
        }
        .item .feature_box_col_three{
            background:rgba(0, 147, 38, 0.15);
            color:#f91942
        }
        .item .feature_box_col_four{
            background:rgba(0, 108, 255, 0.15);
            color:#f91942
        }
        .item .feature_box_col_five{
            background:rgba(146, 39, 255, 0.15);
            color:#f91942
        }
        .item .feature_box_col_six{
            background:rgba(23, 39, 246, 0.15);
            color:#f91942
        }
        .item p{
            font-size:15px;
            line-height:26px;
        }
        .item h6 {
            margin-bottom:20px;
            color:#2f2f2f;
        }
        .mission p {
            margin-bottom: 10px;
            font-size: 15px;
            line-height: 28px;
            font-weight: 500;
        }
        .mission i {
            display: inline-block;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            background: #f91942;
            border-radius: 50%;
            color: #fff;
            font-size: 25px;
        }
        .mission .small-text {
            margin-left: 10px;
            font-size: 13px;
            color: #666;
        }
        .skills {
            padding-top:0px;
        }
        .skills .prog-item {
            margin-bottom: 25px;
        }
        .skills .prog-item:last-child {
            margin-bottom: 0;
        }
        .skills .prog-item p {
            font-weight: 500;
            font-size: 15px;
            margin-bottom: 10px;
        }
        .skills .prog-item .skills-progress {
            width: 100%;
            height: 10px;
            background: #e0e0e0;
            border-radius:20px;
            position: relative;
        }
        .skills .prog-item .skills-progress span {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            background: #f91942;
            width: 10%;
            border-radius: 10px;
            -webkit-transition: all 1s;
            transition: all 1s;
        }
        .skills .prog-item .skills-progress span:after {
            content: attr(data-value);
            position: absolute;
            top: -5px;
            right: 0;
            font-size: 10px;
            font-weight:600;
            color: #fff;
            background:rgba(0, 0, 0, 0.9);
            padding: 3px 7px;
            border-radius: 30px;
        }
    </style>
    <link rel="stylesheet" href="{{asset("css/slider.css")}}">
    <link rel="stylesheet" href="{{asset("css/show.css")}}">
    <link rel="stylesheet" href="{{asset("css/altslider.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/categories_responsive.css')}}">
    <style>
        .mySlides {display:none}
        .w3-left, .w3-right, .w3-badge {cursor:pointer}
        .w3-badge {height:15px;width:15px;padding:0}
    </style>
@endsection


