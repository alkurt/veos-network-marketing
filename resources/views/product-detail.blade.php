@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/styles/single_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/styles/single_responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/tasarim.css') }}">
@endsection
@section('meta')
    <meta type="keywords" content="Ürünler, veosnet, {{$product->slug}},{{$product->product_name}}">
@endsection
@section('content')
    <div class="container single_product_container" style="margin-top: 170px">
        <div class="row">
            <div class="col">
                <!-- Breadcrumbs -->
                <div class="breadcrumbs d-flex flex-row align-items-center">
                    <ul>
                        <li><a href="/">Anasayfa</a></li>
                        @foreach($bcrumb as $bc)
                            <li><a href="/category/{{$bc->slug}}"><i class="fa fa-angle-right"
                                                                     aria-hidden="true"></i>{{ $bc->category_name }}</a>
                            </li>
                        @endforeach
                        <li class="active"><a href="{{route('product', $product->slug)}}"><i class="fa fa-angle-right"
                                                                                             aria-hidden="true"></i>{{ $product->product_name }}
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <div class="single_product_pics">
                    <div class="row">
                        <div class="col-lg-3 thumbnails_col order-lg-1 order-2">
                            <div class="single_product_thumbnails">
                                <ul>
                                    @foreach($product->images as $image)
                                        <li class="active">
                                            <img src="{!! asset("uploads/thumb_".$image->name)!!}" alt=""
                                                 data-image="{!! asset("uploads/".$image->name)!!}">
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 image_col order-lg-2 order-1">
                            <div class="single_product_image">
                                @foreach($product->images as $image)
                                    <div class="single_product_image_background"
                                         style="background-image:url('{!! asset("uploads/thumb_".$image->name)!!}')"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5">
                <div class="product_details">
                    <div class="product_details_title">
                        <h5> {{ $product->product_name }}</h5>
                        <p>Ürün Kodu: <i class="text-muted"> {{ $product->code }}</i></p>
                        <p>{!! $product->product_detail !!}</p>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <div class="product_price col-md-4 col-sm-4">{{ number_format($product->product_price) }} <small>₺</small></div>
                                <div class="product_price col-md-4 col-sm-4 ">{{ number_format($product->dolar) }} <small>$</small></div>
                                <div class="product_price col-md-4 col-sm-4">{{ number_format($product->euro) }} <small>€</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="product_details_title">
                        <span>Miktar:</span>
                        <input type="number" class="quantity" id="quantity" name="quantity" value="1" min="0" max="{{$product->miktar}}"
                               style="width: 50px; margin-right: 10px;">
                    </div>
                    <div class="red_button" style="margin-top: 30px;">
                        <a href="{{ route('basket.create', ['id' => $product->id]) }}">Sepete ekle</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Benefit -->
    <div class="benefit">
        <div class="container">
            <div class="row ">
                @isset($firsat)
                    <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                        <p>{!! $firsat->bir !!}</p>
                    </div>
                    <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                        <p>{!! $firsat->iki !!}</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                        <p>{!! $firsat->uc !!}</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                        <p>{!! $firsat->dort !!}</p>
                    </div>
                @else
                    <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                        <p>300 ₺ ve Üzeri Alışverişlerde <br> Ücretsiz Kargo</p>
                    </div>
                    <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                        <p>Kazanıyor, Kazandırıyor...</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                        <p>Bizim İçin Değerlisiniz... </p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                        <p>Açılış Saatimiz 09:00</p>
                        <p>Kapanış Saatimiz 18:00</p>
                    </div>
                @endisset
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.red_button').find('a').click(function (event) {
            event.preventDefault();
            var quantity = $(this).parent().prev().find('input').val();
            var max= $(this).parent().prev().find('input').attr('max');
            max = parseInt(max);
            quantity = parseInt(quantity);
            if(quantity > max) {
                Swal.fire({
                    icon: 'error',
                    title: 'Uyarı',
                    text: 'Girdiğiniz ürün miktarı ürün stoğunu aşmaktadır.',
                    footer: '<span class="alert alert-primary w-auto font-weight-bold"> Ürün stok durumu :'+ max +' ürün mevcut</span>'
                })
            }else{
                $.ajax({
                    type: "POST",
                    url: $(this).attr('href'),
                    data: {quantity: quantity}
                    , success: function (data) {
                        console.log(data);
                        $('#checkout_items').html(data.cartCount);
                    }
                });
                return false; //for good measure
            }
        });
    </script>
    <script src="{{ asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/js/single_custom.js') }}"></script>
@endsection
