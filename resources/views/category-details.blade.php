@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/categories_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/categories_responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/tasarim.css')}}">
@endsection
@section('meta')
    <meta type="keywords" content="Ürünler, {{$category->slug}}">
@endsection
@section('title','VeosNet Kategori Ürün Sayfası || VeosNet Network&Marketing')
@section('content')
    <div class="container product_section_container">
        <div class="row">
            <div class="col product_section clearfix">
                <!-- Breadcrumbs -->
                <div class="breadcrumbs d-flex flex-row align-items-center">
                    <ul>
                        <li><a href="{{url('/')}}">Anasayfa</a></li>
                        <li class="active"><a href="{{route('category', $category->slug)}}"><i class="fa fa-angle-right" aria-hidden="true"></i>{{ $category->category_name }}</a></li>
                    </ul>
                </div>
                <!-- Sidebar -->
                <!-- Main Content -->
                <div class="main_content">
                    <!-- Products -->
                    <div class="products_iso">
                        <div class="row">
                            <div class="col">
                                <div class="product-grid"
                                     data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
                                    @foreach($products as $product)
                                        <a href="/product/{{$product->slug}}">
                                            <div class="product-item {{$product->category_id}} ">
                                                <div class="product discount product_filter">
                                                    <div class="product_image"style="margin-top: -22px">
                                                        {!! $product->thumbs !!}
                                                    </div>
                                                    <div class="product_info ">
                                                        <small class="product_name" ><a href="/product/{{$product->slug}}">{{ $product->product_name }}</a></small><br>
                                                        <small class="product_name" ><a href="/product/{{$product->slug}}">{{ $product->code }}</a><i class="fa fa-search mx-2"></i></small><br>
                                                        <div class="row" style="margin-left: 10px">
                                                            <div class="col-md-3 col-sm-12 product_price" style="font-size: 12px">{{ number_format($product->dolar) }} <small>$</small></div>
                                                            <div class="col-md-3 col-sm-12 product_price" style="font-size:12px">{{ number_format($product->kisiself) }}<small>₺</small></div>
                                                            <div class="col-md-3 col-sm-12 product_price " style="font-size: 12px">{{ number_format($product->euro) }} <small>€</small> </div>
                                                        </div>
                                                        <input type="number" class="quantity" id="quantity" name="quantity" value="1" min="0" max="{{$product->miktar}}"
                                                               style="width: 50px; margin-right: 10px;height: 18px;border: 1px solid #FE7C7F;margin-top: -8px">
                                                    </div>
                                                    <div class="add_to_cart_button red_button"><a href="{{ route('basket.create', ['id' => $product->id]) }}">Ürünü Ekle</a></div>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Benefit -->
    <div class="mt-5">
        <div class="container">
            <div class="row ">
                @isset($firsat)
                    <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                        <p>{!! $firsat->bir !!}</p>
                    </div>
                    <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                        <p>{!! $firsat->iki !!}</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                        <p>{!! $firsat->uc !!}</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                        <p>{!! $firsat->dort !!}</p>
                    </div>
                @else
                    <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                        <p>300 ₺ ve Üzeri Alışverişlerde <br> Ücretsiz Kargo</p>
                    </div>
                    <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                        <p>Kazanıyor, Kazandırıyor...</p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                        <p>Bizim İçin Değerlisiniz... </p>
                    </div>
                    <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                        <p>Açılış Saatimiz 09:00</p>
                        <p>Kapanış Saatimiz 18:00</p>
                    </div>
                @endisset
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script>
        $('.add_to_cart_button').find('a').click(function (event) {
            event.preventDefault();
            var quantity = $(this).parent().prev().find('input').val();
            var max= $(this).parent().prev().find('input').attr('max');
            max = parseInt(max);
            quantity = parseInt(quantity);
            if(quantity > max) {
                Swal.fire({
                    icon: 'error',
                    title: 'Uyarı',
                    text: 'Girdiğiniz ürün miktarı ürün stoğunu aşmaktadır.',
                    footer: '<span class="alert alert-primary w-auto font-weight-bold"> Ürün stok durumu :'+ max +' ürün mevcut</span>'
                })
            }else{
                $.ajax({
                    type: "POST",
                    url: $(this).attr('href'),
                    data: {quantity: quantity}
                    , success: function (data) {
                        console.log(data);
                        $('#checkout_items').html(data.cartCount);
                    }
                });
                return false; //for good measure
            }
        });
    </script>
    <script src="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/js/categories_custom.js')}}"></script>
@endsection
@section('css')

@endsection
