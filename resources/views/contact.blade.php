@extends('layouts.main')
@section('meta')
    <meta type="keywords" content="Veosnet, İletişim, Telefon, Konum ,Sosyal Medya , Facebook, İnstagram , Twitter">
@endsection
@section('title','VeosNet Network&Marketing || İletişim')
@section('content')
    <div class=" contact_container mx-3" style="margin-top: 150px">

        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
        <ul>
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        </ul>
        <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">BİZE BURADAN ULAŞABİİRSİNİZ</h4>
        <!-- Map Container -->

        <div class="row">
            <div class="col">
                <div>
                    <div class="">
                        <div class="table-responsive text-center" >
                            <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3153.5794617059164!2d29.080669515200544!3d37.776457779759085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14c73faf16cfc169%3A0x2b0fd7620ec7a1a0!2sS%C4%B1rakap%C4%B1lar%2C%201585.%20Sk.%20No%3A8%2C%2020010%20Denizli%20Merkez%2FDenizli!5e0!3m2!1str!2str!4v1581623783909!5m2!1str!2str" width="1200" height="500" frameborder="0" style="border:0;border: 5px solid darkslategray; border-radius: 50px" allowfullscreen=""></iframe>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Us -->
         <div class="row">
                <div class="col-lg-6 contact_col">
                <div class="contact_contents">
                    @forelse($iletisim as $i)
                    <p class="mt-4" >Bizi Burada Bulabilirsiniz...</p>
                    <p> {{$i->merkez_adresi}}</p>
                    <div>
                        <p ><i class="fa fa-volume-control-phone font-weight-bold text-dark" ></i> İletişim Numaralarımız</p>
                        <p>-> {{$i->musteri_hatti}}</p>
                        <p>-> {{$i->telefon_hatti}}</p>
                        <br>
                        <p ><i class="fa fa-mail-forward text-dark font-weight-bold" ></i> Öneri ve Şikayetlerinizi Buradanda Gönderebilirsiniz </p>
                            <p> {{$i->email}}</p>
                        <br>
                            <p><i class="fa fa-whatsapp text-success" ></i> Whatsapp Hattımız</p>
                        <p>{{$i->whatsapp}}</p>
                    </div>
                    <div>

                        <br>
                        <p><i class="fa fa-clock-o font-weight-bold" ></i>  Ofis Mesai Saatimiz</p>
                            <p> {{$i->kapanis}} / {{$i->acilis}}</p>
                    </div>
                </div>
                @empty
                        <marquee behavior="" direction="down"> <p class="text-danger">İletişim Alanı Bilgileri Boş Görünüyor...</p></marquee>
                    @endforelse
                </div>
                <!-- Follow Us -->
                <div class="follow_us_contents">
                    @if(Auth::check())
                        <div class="get_in_touch_contents">
                            <h4 style="font-family: 'Harlow Solid Italic'">Bizimle temasa geçebilirsiniz!</h4>
                            <p>Ücretsiz ve gizli bir bilgi almak için aşağıdaki formu doldurunuz.</p>
                            <div class="form-group">
                                <form method="post" action="{{route('message.store')}}">
                                    @csrf
                                    <div class="form-group">
                                        <input id="input_name" class="form_input input_name input_ph" value="{{$user->name}}" type="text" name="name" placeholder="isim " required="required" data-error="Name is required.">
                                    </div>
                                    <div class="form-group">
                                        <input id="input_name" class="form_input input_name input_ph" value="{{$user->surname}}" type="text" name="surname" placeholder="Soyisim" required="required" data-error="Name is required.">
                                    </div>
                                    <div class="form-group">
                                        <input id="input_email" class="form_input input_email input_ph" value="{{$user->email}}" type="email" name="email" placeholder="Email" required="required" data-error="Valid email is required.">
                                    </div>
                                    <div class="form-group">
                                        <textarea id="input_message" class="input_ph input_message" name="message"  placeholder="Mesajınız" rows="3" required data-error="Lütfen bize bir mesaj yazın."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-info send-button mx-5" id="submit" type="submit" >
                                            <div class="button">
                                                <i class="fa fa-paper-plane"></i><span class="send-text">GÖNDER</span>
                                            </div>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @else
                       <div class="get_in_touch_contents">
                           <h4 style="font-family: 'Harlow Solid Italic'">Bizimle temasa geçebilirsiniz!</h4>
                           <p>Ücretsiz ve gizli bir bilgi almak için aşağıdaki formu doldurunuz.</p>
                           <div class="form-group">
                               <form method="post" action="{{route('message.store')}}">
                                   @csrf
                                   <div class="form-group">
                                       <input id="input_name" class="form_input input_name input_ph" type="text" name="name" placeholder="isim " required="required" data-error="Name is required.">
                                   </div>
                                   <div class="form-group">
                                       <input id="input_name" class="form_input input_name input_ph" type="text" name="surname" placeholder="Soyisim" required="required" data-error="Name is required.">
                                   </div>
                                   <div class="form-group">
                                       <input id="input_email" class="form_input input_email input_ph" type="email" name="email" placeholder="Email" required="required" data-error="Valid email is required.">
                                   </div>
                                   <div class="form-group">
                                       <textarea id="input_message" class="input_ph input_message" name="message"  placeholder="Mesajınız" rows="3" required data-error="Lütfen bize bir mesaj yazın."></textarea>
                                   </div>
                                   <div class="form-group">
                                       <button class="btn btn-info send-button mx-5" id="submit" type="submit" >
                                           <div class="button">
                                               <i class="fa fa-paper-plane"></i><span class="send-text">GÖNDER</span>
                                           </div>
                                       </button>
                                   </div>
                               </form>
                           </div>
                       </div>
                    @endif
                </div>
         </div>
        <div class="col-lg-6 get_in_touch_col">
            <h4 class="mt-5 text-center" style="font-family: 'Harlow Solid Italic'">Sosyal Medyada da Birlikte Olmak İsteriz ...</h4>
            @forelse($medya as $m)
            <ul class=" social d-flex flex-row">
                <li><a href='{{$m->facebook}}' style="background-color: #3a61c9"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href='{{$m->twitter}}' style="background-color: #41a1f6"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href='{{$m->instagram}}' style="background-color: #8f6247"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#" style="background-color: #fb4343"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            </ul>
                @empty
                <ul class=" social d-flex flex-row">
                <li><a href='#' style="background-color: #3a61c9"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href='#' style="background-color: #41a1f6"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href='#' style="background-color: #8f6247"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#" style="background-color: #fb4343"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  </ul>
                <marquee  behavior="" direction="down"> <p class="text-danger">Sosyal Medya Bilgileri Boş Görünüyor...</p></marquee>
                @endforelse
            </div>
    </div>
    </div>
@endsection
@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
    <script src="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/js/contact_custom.js')}}"></script>
@endsection
@section('css')
    <style>
        .send-button {
            margin-top: 15px;
            height: 34px;
            width: 400px;
            overflow: hidden;
            transition: all .2s ease-in-out;
        }

        .button {
            width: 400px;
            height: 34px;
            transition: all .2s ease-in-out;
        }
        .send-text {
            display: block;
            margin-top: 10px;
            font: 300 14px 'Lato', sans-serif;
            letter-spacing: 2px;
        }

        .button:hover {
            transform: translate3d(0px, -29px, 0px);
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/contact_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/contact_responsive.css')}}">

@endsection
