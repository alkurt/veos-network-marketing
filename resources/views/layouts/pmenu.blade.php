@extends("layouts.main")
@section('content')
    <div class="" style="margin-top:80px">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <link rel="stylesheet" href="{{asset('css/kback.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="margin-top: 150px">
                    <div class="modal-header">
                        <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="text-success fa fa-money"> </i> Gelirlerim</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <i class="text-success fa fa-tree"></i> <a href="{{route('binary')}}">Binary Kazancım</a>
                        <br>
                        <i class="text-success fa fa-safari"></i> <a href="#">Hesap Hareketlerim</a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
                <div class="col-md-3 mt-5">
                    <ul id="menu-v">
                        <li>  <h5 class="arrow text-white  text-center">SİSTEM YÖNETİMİ</h5></li>
                        <li><a class=""title="Geri" onclick="window.history.back()">Geri Gitmek İstiyorum</a></li>
                        <li>
                            <a class="arrow" href="#">Genel Bakış</a>
                            <ul>
                                <li>
                                    <a class="arrow" href="{{route('alt-bayiliklerim')}}">
                                        İllerdeki Bayiliklerim
                                    </a>
                                </li>
                                <li>
                                    <a  class="arrow" href="{{route('yeni-alt-bayi')}}">
                                        Yeni İlde Bayi Aç
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li><a href= "{{route("kazanc")}}"> Ekibim</a>
                        <li><a href="{{route('binary')}}"> Soy Ağacı Kazancım</a></li>
                        <li> <a href="{{route("cuzdan")}}"> Cüzdanım</a></li>
                        <li><a  href="{{route("isteklerim")}}"> İsteklerim</a></li>
                        <li><a href="{{route("kazancozeti")}}">Gelirlerim</a> </li>
                        <li> <a  href="{{route("gelenpvsyf")}}"> Pv/Cv Gelirlerim</a></li>
                        <li><a href="{{route("hesap")}}">Hesap Menü</a></li>
                    </ul>
                </div>
                <div class="col-md-9" style="margin-top: 33px">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                            <div class="row my-8">
                                <div class="col-md-4 col-xs-12 text-center mb-1">
                                    <a href="{{route('menu')}}" class="btn btn-sq-lg text-dark kback zoom">
                                        <i class="fa fa-eye fa-5x"></i><br/>
                                        <br> Genel Bakış
                                    </a>
                                </div>
                                <div class="col-md-4 col-xs-12 text-center mb-1">
                                    <a href="{{route("kazanc")}}" class="btn btn-sq-lg text-dark kback zoom mx-5">
                                        <i class="fa fa-users  fa-5x"></i><br/>
                                        <br>Ekibim
                                    </a>
                                </div>
                                <div class="col-md-4 col-xs-12 text-center mb-1">
                                    <a href="{{route('binary')}}" class="btn btn-sq-lg text-dark kback zoom mx-5">
                                        <i class="fa fa-money fa-5x"></i><br/>
                                        <br>Soy Ağacı <br>Kazancım
                                    </a>
                                </div>

                            </div>

                            <div class="row my-5">
                                <div class="col-md-4 col-xs-12 text-center mb-1">
                                    <a href="{{route("cuzdan")}}" class="btn btn-sq-lg text-dark kback zoom">
                                        <i class="fa fa-google-wallet fa-5x"></i><br/>
                                        <br>Cüzdanım
                                    </a>
                                </div>
                                <div class="col-md-4 col-xs-12 text-center mb-1">

                                    <a href="{{route('kazancozeti')}}" class="btn btn-sq-lg text-dark kback zoom mx-5">
                                        <i class="glyphicon glyphicon-stats fa-5x"></i><br/>
                                        <br>Gelirlerim
                                    </a>
                                </div>
                                <div class="col-md-4 col-xs-12 text-center mb-1">
                                    <a href="{{route('hesap')}}" class="btn btn-sq-lg kback text-dark zoom mx-5">
                                        <i class="fa fa-server  fa-5x"></i><br/>
                                        <br>Hesap Menü
                                    </a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <!------ Include the above in your HEAD tag ---------->

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/button.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('js')
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
@endsection
