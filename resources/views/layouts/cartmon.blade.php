@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 160px">
    <div class="row">
<div class="col-md-12">
   <h5 class="mt-3" style="color: slategray"><i style="color: black">Sayın;</i> {{$kisi->name}} {{$kisi->surname}}</h5>
    <h4 class="mb-5 mt-5 mx-5" style="font-family: 'Harlow Solid Italic'"> VEOSNET HESAP BİLGİLERİ</h4>
    <div class="row">
       <div class="col-md-6">
           <div class="table-responsive">
               <table class="table table-hover">
                   <thead>
                   <tr>
                       <th>Hesap Sahibi</th>
                       <th>İban Nu:</th>
                       <th>Hesap Nu:</th>
                       <th>Banka Adı</th>
                       <th>Şube Kodu</th>
                       <th>Şube Adı</th>
                       <th>Kayıt Tarihi</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($banka as $b)
                       <tr>
                           <td>{{$b->hesap_sahibi}}</td>
                           <td>{{$b->iban_no}}</td>
                           <td>{{$b->hesap_no}}</td>
                           <td>{{$b->banka_adi}}</td>
                           <td>{{$b->sube_kodu}}</td>
                           <td>{{$b->sube_adi}}</td>
                           <td>{{$b->created_at}}</td>
                       </tr>
                   @endforeach
                   </tbody>
               </table>
           </div>
       </div>
            <div class="col-md-6 col-sm-12">
                     <div class="row">
                         <div class="col-md-12">
                            <p>
                              VeosNet Eft Hesap Bilgileri Yan Tarafta Yer Almaktadır.
                            </p>
                             <p>
                                 Eft Yaptıktan Sonra Talebiniz Listemize Düşecektir.
                             </p>
                         </div>
                     </div>
                       <img src="{{asset('assets/new images/kartlar.jpg')}}" alt="">
            </div>
    </div>
</div>
</div>
    </div>
@endsection
@section("js")
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
@section("css")
@endsection
