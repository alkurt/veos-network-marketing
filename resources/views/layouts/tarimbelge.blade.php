@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 160px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h4 class="text-center mt-3 mb-5" style="font-family: 'Harlow Solid Italic'">TARIM BAKANLIĞI ONAYLI</h4>
        <div class="row ">
            <div class="col-md-12 col-sm-12">
               <div class="row">
                   <div class="col-md-6 col-sm-12 mb-5">
                       <img class="img-fluid" src="{{asset('assets/new images/t1.jpeg')}}" alt="">
                   </div>
                   <div class="col-md-6 col-sm-6 mb-5 ">
                       <img class="img-fluid" src="{{asset('assets/new images/t2.jpeg')}}" alt="">
                   </div>
               </div>
            </div>

        </div>
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
