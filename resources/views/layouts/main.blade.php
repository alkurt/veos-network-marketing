<!DOCTYPE html>
<html lang="tr">
<head>
    <title> @yield('title','VeosNet Network&Marketing') </title>
    <meta name="title" content="VeosNet Networkamp;Marketing">
    <meta name="description" content="VeosNet Networkamp;Marketing hem kariyer yapabileceiniz hem de rnler alp evre edinebileceiniz alveri platformu">
    <meta name="keywords" content="VEOS, Alisveris, Network, Bayi, Yeni, Nesil, Kazandıran, Denizli, Türkiye, Yatırım, Girişimcilik, Girişim, Satın Al ">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <meta name="language" content="Turkish">
    <meta name="revisit-after" content="1 days">
    @yield('meta')
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/new images/logoms.png')}}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/styles/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/OwlCarousel2-2.2.1/animate.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/styles/main_styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/styles/responsive.css') }}">
    <link href="{{asset("assets/styles/toastr.min.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("css/footer.css")}}">
    <link rel="stylesheet" href="https://konudenizi.com/assets/other/cookieconsent.min.css">

    @yield('css')

    <script>
        window.csrfToken = "{{ csrf_token() }}"
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-171285600-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-171285600-1');
    </script>


</head>
<body data-status="{{Session::get("status")}}">
<div id="app">
    <div class="super_container">
        <!-- Header -->
        <header class="header trans_300">
            <!-- Top Navigation -->
            <div class="top_nav " style="
            background-image: url('https://thumbs.gfycat.com/PettyJealousArcticfox-size_restricted.gif');
            background-size: cover;
            background-repeat: repeat-y;
            background-position:center ;
            opacity: 1;
            ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    @auth()
                                        <div class="top_nav_left col-md-6 fa fa-user-circle text-white font-weight-bold">
                                            Hoşgeldin!! {{Auth::user()->name}}
                                        </div>
                                    @endauth
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="top_nav_right">
                                <ul class="top_nav_menu ">
                                    <!-- Currency / Language / My Account -->
                                    @if(Auth::guest())
                                        <div class="mt-2 font-weight-bold d-inline-flex">
                                            <li >
                                                <a href="{{ route('login') }}" class="btn btn-outline-light border-light font-weight-bold"><i class="fa fa-sign-in" aria-hidden="true"></i>
                                                    Giriş Yap</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('register') }}" class="btn btn-outline-light border-light font-weight-bold ml-2"><i class="fa fa-user-plus"
                                                                                     aria-hidden="true"></i> Bize Katılın</a>
                                            </li>
                                        </div>
                                    @else
                                        @if(Auth::user()->id =='10001')
                                            <strong style="color:white;"> Yönetici Paneli</strong>
                                        @elseif(Auth::user()->kariyer_id == '17')
                                            <strong class="text-white"> Alışveriş Hesabı</strong>
                                        @elseif(Auth::user()->kariyer_id=='16')
                                            <a href="{{route("menu")}}" class="btn btn-danger mx-3 w-25"> MENÜ</a>
                                            <a class="text-white btn btn-warning" href="/category/paketler">Bayi Yükselt</a>
                                        @elseif(Auth::user()->kariyer_id=='1')
                                            <a href="{{route("menu")}}" class="btn btn-danger mx-3 w-25"> MENÜ</a>
                                            <a class="text-white btn btn-warning" href="/category/paketler">Bayi Yükselt</a>
                                        @elseif(Auth::user()->kariyer_id=='2')
                                            <a href="{{route("menu")}}" class="btn btn-danger mx-3 w-25"> MENÜ</a>
                                            <a class="text-white btn btn-warning" href="/category/paketler">Bayi Yükselt</a>
                                        @elseif(Auth::user()->kariyer_id=='3')
                                            <a href="{{route("menu")}}" class="btn btn-danger mx-3 w-25"> MENÜ</a>
                                            <a class="text-white btn btn-warning" href="/category/paketler">Bayi Yükselt</a>
                                        @elseif(Auth::user()->kariyer_id=='4')
                                            <a href="{{route("menu")}}" class="btn btn-danger mx-3 w-25"> MENÜ</a>
                                            <a class="text-white btn btn-warning" href="/category/paketler">Bayi Yükselt</a>
                                        @else
                                            <a href="{{route("menu")}}" class="btn btn-danger mx-5 w-25"> MENÜ</a>

                                        @endif
                                        <li class="account" style="background-color: transparent">
                                            <a style="color: white;font-weight: bold" href="#"> Kullanıcı Nu: [
                                                {{ Auth::user()->id}} ]
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="account_selection" style="z-index:101   ">
                                                @if(Auth::user()->isItAuthorized("admin"))
                                                    <li><b style="font-family: 'Harlow Solid Italic'">YÖNETİCİ</b></li>
                                                    <li><a href="{{ url('/admin-users') }}"><i
                                                                    class="fa fa-btn fa-users"></i>Kullanıcılar</a>
                                                    </li>
                                                    <li><a href="{{route('kontrolmenu')}}"><i
                                                                    class="fa fa-btn fa fa-support"></i>Kontrol Menü</a>
                                                    </li>
                                                    <li><a href="{{ url('/admin-orders') }}"><i
                                                                    class="fa fa-btn fa fa-pencil-square-o"></i>Sipariş Takip</a></li>
                                                    <li class="divider"></li>

                                                @endif
                                                @if(auth()->user()->id==10001)
                                                    <li><a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Yönetici Profili</a></li>
                                                    <li><a href="{{route('isteklerim')}}"><i class="fa fa-safari"></i> İsteklerim </a></li>
                                                    <li><a href="{{route('change')}}"><i class="fa fa-key"></i>Şifre Yenile</a></li>

                                                @else
                                                    @if(Auth::user())
                                                        <li><b style="font-family: 'Harlow Solid Italic'">KULLANICI</b></li>
                                                        <li><a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Profilim</a></li>
                                                        <li><a href="{{ url('/orders') }}"><i class="fa fa-btn fa-list-alt"></i>Siparişlerim</a> </li>
                                                        @if(Auth::user()->kariyer_id == 17)
                                                            <li> <a class="text-dark" href="{{route('musteri_eft_durum')}}"><i class="fa fa-btn fa-money"></i> Eft/Havale Durum</a></li>
                                                        @else
                                                            <li><a  href="{{route('isteklerim')}}"><i class="fa fa-safari"></i> İsteklerim </a>  </li>
                                                        @endif
                                                        <li><a href="{{route('change')}}"><i class="fa fa-key"></i>Şifre Yenile</a></li>
                                                    @endif
                                                @endif
                                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Çıkış Yap</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Navigation -->
            <div class="main_nav_container ">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-12 text-right align-items-center">
                            <div class="logo_container text-center">
                                @isset($logo->cover)
                                    <a href="{{route('home')}}">  <img style="height: 50px;width: 50px;border-radius: 50px;height: 65px;width: 65px;width:50px;height:50px;}" src="{{\Illuminate\Support\Facades\Storage::url($logo->cover)}}"
                                                      alt=""/></a>
                                @else
                                    <a href="{{route('home')}}"><p style="font-size: small">Yüklü Logo Bulunamadı</p></a>
                                @endisset
                            </div>
                            <nav class="navbar ">
                                <ul class="navbar_menu">
                                    <li><a href="/"  style="font-size:14px">Anasayfa</a></li>
                                    <div class="dropdown"style="z-index: 100">
                                        <button class="dropbtn " style="font-size:14px">VEOSNET</button>
                                        <div class="dropdown-content">
                                            <a href="{{route("hk")}}">Hakkımızda</a>
                                            <a href="{{route('sozlesmemusteri')}}">Sözleşmelerimiz</a>
                                            <a href="{{route("basari")}}">Başarı Hikayelerimiz</a>
                                            <a href="{{route("ppazarlama")}}">Profesyonel Pazarlama</a>
                                            <link rel="stylesheet" href="{{asset("css/veosnet.css")}}">
                                        </div>
                                    </div>
                                    <div class="dropdown"style="z-index: 100">
                                        <button class="dropbtn " style="font-size:14px">VEOSNET KATEGORİ</button>
                                        <div class="dropdown-content">
                                            @foreach($categoryMenu as $menu)
                                                @if($menu->id !=3)
                                                    <a href="/category/{{ $menu->slug }}">{{ $menu->category_name }}</a>
                                                @endif
                                            @endforeach
                                            <link rel="stylesheet" href="{{asset("css/veosnet.css")}}">
                                        </div>
                                    </div>
                                    <li><a href="{{ route('contact') }}"  style="font-size:14px">İletişim</a></li>
                                </ul>
                                @auth()
                                    <ul class="hidden-sm hidden-xs">
                                        <form class="navbar-form" action="{{route('search.product')}}" method="post">
                                            @csrf
                                            <div class="form-group-sm">
                                                <input type="text" class="form-control input-group-sm submit_on_enter" name="search" placeholder="Ürün Kodu">
                                            </div>
                                            <button type="submit"  class="btn btn-default hide" hidden>Ara</button>
                                        </form>
                                    </ul>
                                @endauth
                                <ul class="navbar_user">
                                    <li class="checkout">
                                        <a href="{{route('basket')}}">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                            <span id="checkout_items" class="checkout_items">{{ Cart::count() }}</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="hamburger_container">
                                    <i class="fa fa-bars" aria-hidden="true"></i>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="fs_menu_overlay"></div>
        <div class="hamburger_menu">
            <div class="hamburger_close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <div class="hamburger_menu_content text-right">
                <ul class="menu_top_nav">
                    <li class="menu_item" style="font-size:x-small"><a href="{{ route('home') }}">anasayfa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <div class="dropdown menu_item"style="z-index: 100">
                        <button class="dropbtn " style="font-size:small">VEOSNET</button>
                        <div class="dropdown-content">
                            <a href="{{route("hk")}}">Hakkımızda&nbsp;</a>
                            <a href="{{route('sozlesmemusteri')}}">Sözleşmelerimiz</a>
                            <a href="{{route("basari")}}">Başarı Hikayelerimiz</a>
                            <a href="{{route("ppazarlama")}}">Profesyonel Pazarlama</a>

                            <link rel="stylesheet" href="{{asset("css/veosnet.css")}}">
                        </div>
                    </div>
                    <div class="dropdown menu_item"style="z-index: 100">
                        <button class="dropbtn " style="font-size:small">VEOSNET KATEGORİ</button>
                        <div class="dropdown-content">
                            @foreach($categoryMenu as $menu)
                                @if($menu->id !=3)
                                    <li><a href="/category/{{ $menu->slug }}">{{ $menu->category_name }}</a></li>
                                @endif
                            @endforeach
                            <link rel="stylesheet" href="{{asset("css/veosnet.css")}}">
                        </div>
                    </div>
                    <li class="menu_item" style="font-size:small"><a href="{{ route('contact') }}">İLETİŞİM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    @if(Auth::guest())
                        <li class="menu_item">
                            <a href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>
                                Giriş Yap</a>
                        </li>
                        <li class="menu_item">
                            <a href="{{ route('register') }}"><i class="fa fa-user-plus"
                                                                 aria-hidden="true"></i> Kayıt yap</a>
                        </li>
                    @else
                        @if(Auth::user()->id =='10001')
                            <strong style="color:gray;"> Yönetici Paneli</strong>
                        @elseif(Auth::user()->kariyer_id == '17')
                            <strong class="text-white"> Alışveriş Hesabı</strong>
                        @elseif(Auth::user()->kariyer_id=='16')
                            <li class="menu_item"> <a href="{{route("menu")}}" class="btn btn-danger text-white "> MENÜ</a><br></li>
                            <li class="menu_item"> <a class="btn btn-warning text-white " href="/category/paketler">Bayi Yükselt</a></li>
                        @elseif(Auth::user()->kariyer_id=='1')
                            <li class="menu_item"> <a href="{{route("menu")}}" class="btn btn-danger text-white "> MENÜ</a><br></li>
                            <li class="menu_item"> <a class="btn btn-warning text-white " href="/category/paketler">Bayi Yükselt</a></li>
                        @elseif(Auth::user()->kariyer_id=='2')
                            <li class="menu_item"> <a href="{{route("menu")}}" class="btn btn-danger text-white "> MENÜ</a><br></li>
                            <li class="menu_item"> <a class="btn btn-warning text-white " href="/category/paketler">Bayi Yükselt</a></li>
                        @elseif(Auth::user()->kariyer_id=='3')
                            <li class="menu_item"> <a href="{{route("menu")}}" class="btn btn-danger text-white "> MENÜ</a><br></li>
                            <li class="menu_item"> <a class="btn btn-warning text-white " href="/category/paketler">Bayi Yükselt</a></li>
                        @elseif(Auth::user()->kariyer_id=='4')
                            <li class="menu_item"> <a href="{{route("menu")}}" class="btn btn-danger text-white "> MENÜ</a><br></li>
                            <li class="menu_item"> <a class="btn btn-warning text-white " href="/category/paketler">Bayi Yükselt</a></li>
                        @else
                            <li class="menu_item" > <a href="{{route("menu")}}" class="btn btn-danger text-white "> MENÜ</a><br></li>
                        @endif
                        <li class="menu_item has-children">
                            <a href="#">
                                {{ Auth::user()->name }} {{ Auth::user()->surname}}
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="menu_selection">
                                @if(Auth::user()->isItAuthorized("admin"))
                                    <li><b style="font-family: 'Harlow Solid Italic'">YÖNETİCİ</b></li>
                                    <li><a href="{{ url('/admin-users') }}"><i
                                                    class="fa fa-btn fa-users"></i>Kullanıcılar</a>
                                    </li>
                                    <li><a href="{{route('kontrolmenu')}}"><i
                                                    class="fa fa-btn fa fa-support"></i>Kontrol Menü</a>
                                    </li>
                                    <li><a href="{{ url('/admin-orders') }}"><i
                                                    class="fa fa-btn fa fa-pencil-square-o"></i>Sipariş Takip</a></li>
                                    <li class="divider"></li>
                                @endif
                                @if(auth()->user()->id==10001)
                                    <li><a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Yönetici Profili</a></li>
                                    <li><a href="{{route('isteklerim')}}"><i class="fa fa-safari"></i> İsteklerim </a></li>
                                    <li><a href="{{route('change')}}"><i class="fa fa-key"></i>Şifre Yenile</a></li>
                                @else
                                    @if(Auth::user())
                                        <li><b>Kullanıcı</b></li>
                                        <li><a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Profilim</a></li>
                                        <li><a href="{{ url('/orders') }}"><i class="fa fa-btn fa-list-alt"></i>Siparişlerim</a></li>
                                        @if(Auth::user()->kariyer_id == 17)
                                            <li> <a class="text-dark" href="{{route('musteri_eft_durum')}}"><i class="fa fa-btn fa-money"></i> Eft/Havale Durum</a></li>
                                        @else
                                            <li><a href="{{route('isteklerim')}}"><i class="fa fa-safari"></i> İsteklerim </a></li>
                                        @endif
                                        <li><a href="{{route('change')}}"><i class="fa fa-key"></i>Şifre Yenile</a></li>
                                    @endif
                                @endif
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Çıkış Yap</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@yield('content')
<!-- Site footer -->
    <footer class="site-footer img-fluid" style="background-size: cover;
            background-repeat: repeat-y;
            background-position:center ;
            opacity: 1;background-image: url('https://thumbs.gfycat.com/PettyJealousArcticfox-size_restricted.gif')">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <h4 style="color: navajowhite" >Kurumsal</h4>
                    <ul class="footer-links">
                        <li ><a  style="color: white"  href="{{route("hk")}}">Hakkımızda</a></li>
                        <li ><a style="color: white" href="{{route("misyon")}}">Misyonumuz ve Vizyonumuz</a></li>
                        <li ><a style="color: white" href="{{route("tarimbelge")}}">Tarım Bakanlığı Onaylıdır</a></li>
                        <li ><a  style="color: white" href="{{route("bayi")}}">Bayi Sözleşmesi ve Prosedürler</a></li>
                        <li ><a style="color: white" href="{{route("dogrudansatis")}}">Doğrudan Satış Sektörü</a></li>
                        <li ><a style="color: white" href="{{route("temsilcilik")}}">Temsilcilik Sözleşmesi</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4 style="color: navajowhite">VeosNet </h4>
                    <ul class="footer-links">
                        <li ><a style="color: white"  href="{{route('nedenveos')}}">Neden VeosNetwork ?</a></li>
                        <li ><a style="color: white" href="{{route('ismodeli')}}">VeosNet İş Modeli</a></li>
                        <li ><a style="color: white" href="{{route('ppazarlama')}}">Profesyonel Pazarlama</a></li>
                        <li ><a style="color: white" href="{{route('yeniurunler')}}">VeosNet Yenilikçi Ürünler</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4 style="color: navajowhite">Güvenli Alışveriş</h4>
                    <ul class="footer-links">
                        <li ><a style="color: white"   href="{{route("basket")}}">Sepete Bak</a></li>
                        <li ><a style="color: white"  href="{{route("odemeplanı")}}">Ödeme Planı</a></li>
                        <li ><a  style="color: white" href="{{route("kargo")}}">Kargom Nerede?</a></li>
                        <li ><a style="color: white"  href="{{route("mesafelisatis")}}">Mesafeli Satış Sözleşmesi</a></li>
                        <li ><a style="color: white"  href="{{route("gizlilik")}}">Gizlilik Politikası Sözleşmesi</a></li>
                        <li ><a  style="color: white" href="{{route("iade")}}">İade ve Cayma Şartları</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4 style="color: navajowhite" >Bize Ulaşın</h4>
                    <p style="color: white"  class="fa fa-phone"> 0850 804 76 48</p><br>
                    <p style="color: white"  class="fa fa-mail-forward"> info@veosnet.com</p><br>
                    <p style="color: white"  class="fa fa-location-arrow"> DENİZLİ|MERKEZEFENDİ</p><br>
                    <p style="color: white"  class="fa fa-hourglass-start"> 09.00 - 18.00</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <p class="copyright-text text-white">Copyright &copy; 2020 Tüm Haklara Sahiptir.
                        <a class="text-white" href="">VEOSNET</a><br>
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <ul class="social-icons">
                        <li><a class="facebook" href='https://tr-tr.facebook.com/'><i class="fa fa-facebook"></i></a></li>
                        <li><a class="twitter" href='https://twitter.com/login?lang=tr'><i class="fa fa-twitter"></i></a></li>
                        <li><a class="instagram" href='https://www.instagram.com/?hl=tr'><i class="fa fa-instagram"></i></a></li>
                        <li><a class="linkedin" href="https://tr.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <img style="width:auto;height: 62px " src="{{asset("assets/new images/visa-mastercard-logo.png")}}">
            </div>
        </div>
    </footer>
    <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/styles/bootstrap4/popper.js') }}"></script>
    <script src="{{ asset('assets/styles/bootstrap4/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script src="{{ asset('assets/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/Isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/plugins/easing/easing.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{asset("assets/js//toastr.min.js")}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script type='text/javascript' src='https://oguzturk.net/lab/demo/cerez/cerez.js'></script>
    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#646478", // Uyarının arkaplan rengi
                        "text": "#ffffff" // Uyarının üzerindeki yazı rengi
                    },
                    "button": {
                        "background": "#8ec760", // Tamam butonunun arkaplan rengi - "transparent" kullanırsanız border özelliğini açabilirsiniz.
                        //"border": "#14a7d0", // Arkaplan rengini transparent yapıp çerçeve kullanabilmek için
                        "text": "#ffffff" // Tamam butonunun yazı rengi
                    }
                },
                "theme": "block", // Kullanabileceğiniz temalar; block, edgeless, classic. Sitenizin görüntüsüne uygun olanı seçebilirsiniz.
                "type": "opt-out", // Gizle uyarısını aktif etmek için
                 "position": "bottom", // Aktif ederseniz uyarı üst kısımda görünür
                //"static": true, //Aktif ederseniz uyarı üst kısımda sabit olarak görünür
                // "position": "bottom-left", // Aktif ederseniz uyarı solda görünür.
                //"position": "bottom-right", // Aktif ederseniz uyarı sağda görünür.
                "content": {
                    "message": "Sitemizden en iyi şekilde faydalanabilmeniz için çerezler kullanılmaktadır. Sitemize giriş yaparak çerez kullanımını kabul etmiş sayılıyorsunuz.",
                    "dismiss": "Tamam",
                    "link": "Daha fazla bilgi",
                    "href": "https://konudenizi.com"
                }
            })});
        $(document).ready(function() {

            $('.submit_on_enter').keydown(function(event) {
                // enter has keyCode = 13, change it if you want to use another button
                if (event.keyCode == 13) {
                    this.form.submit();
                    return false;
                }
            });

        });
    </script>
    @if(session()->has('success'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });
            Toast.fire({
                icon: 'success',
                title: '{{session()->get('success')}}'
            })
        </script>
    @endif
    @if(session()->has('error'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });
            Toast.fire({
                icon: 'warning',
                title: '{{session()->get('error')}}'
            })
        </script>
    @endif
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/contact_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/contact_responsive.css')}}">
</div>
</body>

@yield('js')


</html>
