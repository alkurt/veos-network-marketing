@extends('layouts.main')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <div style="margin-top: 80px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
        <div class="col-md-12 mb-2" >
            <div class="row">
                <div class="col-md-5 mt-3 ">
                    <p style="color: black"><i class="fa fa-user mx-3 "></i>Sayın;&nbsp; {{auth()->user()->name}} {{auth()->user()->surname}}</p>
                    <p style="color: black"><i class="fa fa-user-secret mx-3"></i>Kullanıcı Numaranız: [ {{auth()->user()->id}} ]</p>
                </div>
                <div class="col-md-5 mt-3">
                    <p class="mt-2 mx-5"style="color: black"><i class="fa fa-briefcase mx-3"></i> Kariyeriniz :&nbsp; {{auth()->user()->kariyer->kariyername}}</p>
                    <p class="mx-5"  style="color: black"> <i class="fa fa-battery-quarter mx-3"></i>Şuan ki Kariyer Puanınız :&nbsp; {{auth()->user()->ara_pv}} PV</p>
                </div>
                <div class="col-md-1">
                    <p style="height: 20px;width: 20px;background-color: cornflowerblue"></p>
                    <p style="height: 20px;width: 20px;background-color: salmon"></p>
                </div>
                <div class="col-md-1">
                    <p>Sol</p>
                    <p>Sağ</p>
                </div>
            </div>
            <div style="height: 1px;background-image: url('https://wallpaperaccess.com/full/1216046.jpg')"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(auth()->user()->id== 10001)
                @else
                    <div class="col-md-3">
                        <ul id="menu-v">
                            <li>  <h5 class=" text-white  text-center">EKİP LİSTESİ</h5></li>
                            <li><a class=""title="Geri" onclick="window.history.back()">Geri Gitmek İstiyorum</a></li>
                            <li>
                                <a class="arrow" href="#">Genel Bakış</a>
                                <ul>
                                    <li>
                                        <a class="" href="{{route('alt-bayiliklerim')}}">
                                            İllerdeki Bayiliklerim
                                        </a>
                                    </li>
                                    <li>
                                        <a  class="" href="{{route('yeni-alt-bayi')}}">
                                            Yeni İlde Bayi Aç
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="" href= "{{route("kazanc")}}"> Ekibim</a>
                            <li><a class="" href="{{route('binary')}}"> Soy Ağacı Kazancım</a></li>
                            <li>  <a class="" href="{{route("cuzdan")}}"> Cüzdanım</a></li>

                            <li><a class="" href="{{route("isteklerim")}}"> İsteklerim</a></li>
                            <li><a class="" href="{{route("kazancozeti")}}">Gelirlerim</a> </li>
                            <li> <a class="" href="{{route("gelenpvsyf")}}"> Pv/Cv Gelirlerim</a></li>
                            <li><a class="" href="{{route("hesap")}}">Hesap Menü</a></li>
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Kullanıcı Numarası</th>
                                    <th>İsim Soyisim </th>
                                    <th>Kariyeri</th>
                                    <th>Kolu</th>
                                    <th>Konumu</th>
                                    <th>Durumu</th>
                                    <th class="text-center">Kayıt Tarih</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}&nbsp;{{$user->surname}}</td>
                                        <td>{{$user->kariyer->kariyername}}</td>
                                        <td>
                                            @for($i=1; $i<=4; $i++)
                                                @if($user->konum == $i)
                                                    <p class="" style="color: cornflowerblue">Sol</p>
                                                @endif
                                                @for($k=1; $k<=9; $k++)
                                                    @if($user->konum == (($i).'.'.$k))
                                                            <p class="" style="color: cornflowerblue">Sol</p>
                                                    @endif
                                                @endfor
                                            @endfor
                                            @for($i=5; $i<=8; $i++)
                                                @if($user->konum == $i)
                                                        <p class=" " style="color: salmon">Sağ</p>
                                                @endif
                                                @for($k=1; $k<=9; $k++)
                                                    @if($user->konum == (($i).'.'.$k))
                                                            <p class=" " style="color: salmon">Sağ</p>
                                                    @endif
                                                @endfor
                                            @endfor
                                            @if($user->konum == null)
                                                <p class="text-danger">Boş</p>
                                            @endif
                                        </td>
                                        <td class="text-center">@if($user->konum ==null)
                                                <p class="text-danger">Boş</p>
                                            @else
                                                {{$user->konum}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->status == true)
                                                <p class="mx-3 text-success">Aktif</p>
                                            @elseif($user->status == false)
                                                <p class="mx-3 text-danger">Pasif</p>
                                            @endif
                                        </td>

                                        <td>{{$user->created_at}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
