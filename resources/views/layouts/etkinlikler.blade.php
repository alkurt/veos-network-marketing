@extends("layouts.main")

@section("content")
    <div class="" style="margin-top: 140px;background-color: ghostwhite">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a>><a class="mx-3" style="color: gray">Etkinliklerimiz</a>><a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="col-md-12 mt-5">
            <div class="row">
                @forelse($etkin as $e)
                    <div style="border:2px solid darkslategray;height: 220px;width: 5px" class="col-md-3 mx-5 mb-4">
                        <img class="mb-3 mt-2" style="height: 100px;width: 100px;margin-left: 100px;border-radius: 100px;width:100px;height:100px;}" src="{{$e->url}}" alt="">
                        <h4 class="mb-2 text-center" style="font-family: 'Harlow Solid Italic'">{{$e->title}}</h4>
                        <a class="btn btn-outline-info mt-2" style="margin-left: 85px" href="{{route('etkinlikler.edit',$e->id)}}">Devamını Oku</a>
                    </div>
                @empty
                    <br>
                    <br>
                    <br>
                    <br>
                    <marquee scrollamount="10"  direction=left""> <p class="text-center text-secondary"> [ Oluşturulmuş Bir Etkinlik Bulunmuyor. ] <strong>{{config('app.name')}} sağlıklı günler diler...</strong> </p></marquee>
                    <br>
                @endforelse
            </div>
            <br>
        </div>
    </div>
@endsection
@section('js')
@endsection

@section("css")
@endsection
