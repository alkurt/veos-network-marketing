@extends('layouts.main')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <div class="" style="margin-top: 100px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
        <div class="col-md-12 mb-2 ">
            <div class="row">
                <div class="col-md-6 mt-3 ">
                    <p style="color: black"><i class="fa fa-user mx-3 "></i>Sayın;&nbsp; {{auth()->user()->name}} {{auth()->user()->surname}}</p>
                    <p style="color: black"><i class="fa fa-user-secret mx-3"></i>Kullanıcı Numaranız: [ {{auth()->user()->id}} ]</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class=" mt-2 mx-5"style="color: black"><i class="fa fa-briefcase mx-3"></i> Kariyeriniz :&nbsp; {{auth()->user()->kariyer->kariyername}}</p>
                    <p class="mx-5"  style="color: black"> <i class="fa fa-battery-quarter mx-3"></i>Şuan ki Kariyer Puanınız :&nbsp; {{auth()->user()->ara_pv}} PV</p>
                </div>
            </div>
            <div style="height: 1px;background-image: url('https://wallpaperaccess.com/full/1216046.jpg')"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <ul id="menu-v">
                        <li>  <h5 class="arrow text-white  text-center">GELİRLERİM</h5></li>
                        <li><a class=""title="Geri" onclick="window.history.back()">Geri Gitmek İstiyorum</a></li>
                        <li>
                            <a class="arrow" href="#">Genel Bakış</a>
                            <ul>
                                <li>
                                    <a class="" href="{{route('alt-bayiliklerim')}}">
                                        İllerdeki Bayiliklerim
                                    </a>
                                </li>
                                <li>
                                    <a  class="" href="{{route('yeni-alt-bayi')}}">
                                        Yeni İlde Bayi Aç
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="" href= "{{route("kazanc")}}"> Ekibim</a>
                        <li><a class="" href="{{route('binary')}}"> Soy Ağacı Kazancım</a></li>
                        <li>  <a class="" href="{{route("cuzdan")}}"> Cüzdanım</a></li>

                        <li><a class="" href="{{route("isteklerim")}}"> İsteklerim</a></li>
                        <li><a class="" href="{{route("kazancozeti")}}">Gelirlerim</a> </li>
                        <li> <a class="" href="{{route("gelenpvsyf")}}"> Pv/Cv Gelirlerim</a></li>
                        <li><a class="" href="{{route("hesap")}}">Hesap Menü</a></li>


                    </ul>
                </div>
                <div class="col-md-9 col-sm-12">

                    <div class="table-responsive">
                        <div class="table table-bordered table-hover" >
                            <table class="table table-bordered table-hover" id="kazanc-table">
                                <thead>
                                <tr>
                                    <th>Kazanç Sahibi Numarası</th>
                                    <th>Ad Soyad</th>
                                    <th>Kazanç Türü</th>
                                    <th>Kazanç Getiren Kullanıcı</th>
                                    <th>Ad Soyad</th>
                                    <th>Kazanç Tutarı</th>
                                    <th>İşlem Tarihi</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#kazanc-table').DataTable({
                language:{
                    "sDecimal":        ",",
                    "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
                    "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "sInfoEmpty":      "Kayıt yok",
                    "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
                    "sLoadingRecords": "Yükleniyor...",
                    "sProcessing":     "İşleniyor...",
                    "sSearch":         "Ara:",
                    "sZeroRecords":    "Eşleşen kayıt bulunamadı",
                    "oPaginate": {
                        "sFirst":    "İlk",
                        "sLast":     "Son",
                        "sNext":     "Sonraki",
                        "sPrevious": "Önceki"
                    },
                    "oAria": {
                        "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
                        "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                    },
                    "select": {
                        "rows": {
                            "_": "%d kayıt seçildi",
                            "0": "",
                            "1": "1 kayıt seçildi"
                        }
                    }
                },
                processing: true,
                serverSide: true,
                order:[['6','desc']],
                ajax: '{!! route('kazancozeti.datatable') !!}',
                columns: [
                    { data: 'user_id', name: 'user_id'},
                    { data: 'ad', name: 'ad' },
                    { data: 'kazanc_turu', name: 'kazanc_turu' },
                    { data: 'islem_sahibi_id', name: 'islem_sahibi_id' },
                    { data: 'islem_sahibi_ad', name: 'islem_sahibi_ad' },
                    { data: 'kazanc_miktari', name: 'kazanc_miktari' },
                    { data: 'created_at', name:'created_at',searchable:true,orderable: true},
                ]
            });
        })
    </script>
@endsection

@section("css")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@endsection
