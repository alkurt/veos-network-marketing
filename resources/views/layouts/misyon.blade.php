@extends("layouts.main")
@section('meta')
    <meta type="keywords" content="Hakkımızda, veosnet.com, Denizli, Kazanç">
@endsection
@section('title','VeosNet Network&Marketing || Hakkımızda')
@section("content")
    <div class="container-fluid mb-5" style="margin-top: 160px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        @forelse($firsat as $h)
            <div class="card mb-5 mt-4">
                <div class="card-body">
                    <p class="card-text"> {!! $h->misyon !!}</p>
                </div>
            </div>
        @empty
            <marquee behavior="" direction=""><p>Boş Görünüyor ...</p></marquee>
        @endforelse
    </div>
@endsection

@section("js")
    <script src="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/js/contact_custom.js')}}"></script>
@endsection

@section("css")
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/contact_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/contact_responsive.css')}}">
@endsection
