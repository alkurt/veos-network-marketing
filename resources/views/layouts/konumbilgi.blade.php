@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 160px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h3 class="text-center mb-5 mt-3" style="font-family: 'Harlow Solid Italic'">KONUM BİLGİLENDİRME AĞACI</h3>
        <div class="row justify-content-center">

            <div class="col-md-8  col-sm-12 ">
             <img class="large mb-5" src="{{asset('assets/new images/agacbilgi.png')}}" alt="">
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection

@section("customCss")
@endsection
