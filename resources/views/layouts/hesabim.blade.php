@extends("layouts.main")
@section('content')
    <br>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <link rel="stylesheet" href="{{asset('css/kback.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="{{asset('css/button.css')}}" rel="stylesheet" type="text/css" />
    <!------ Include the above in your HEAD tag ---------->

    <div class="mx-2" style="margin-top: 50px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
        <div class="row">
            <div class="col-md-3 col-sm-12 mt-4">
                <ul id="menu-v">
                    <li><h5 class="arrow text-white  text-center">HESAP MENÜ</h5></li>
                    <li><a title="Geri" onclick="window.history.back()">Geri Gitmek İstiyorum</a></li>
                    <li><a href="{{route('home')}}">Sipariş Vermek İstiyorum</a></li>
                    <li><a href="{{ url('/profile') }}">Bilgilerim</a></li>
                    <li><a href="/orders">Geçmiş Siparişlerim</a></li>
                    <li><a href="{{route('pmenu')}}">Sistem Yönetimi</a></li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 text-center">
                                <a href="{{route('home')}}" class="btn btn-sq-lg kback text-dark zoom  my-2">
                                    <i class="fa fa-pencil fa-5x"></i><br/>
                                    Sipariş <br> Vermek  İstiyorum
                                </a>
                                <a href="{{ url('/profile') }}" class="btn btn-sq-lg text-dark kback zoom">
                                    <i class="fa fa-info-circle  fa-5x"></i><br/>
                                    <br>Bilgilerim
                                </a>
                            </div>
                            <div class="col-md-6  col-sm-12 text-center" >
                                <a href="/orders" class="btn btn-sq-lg kback text-dark zoom">
                                    <i class="fa fa-shopping-cart fa-5x"></i><br/>
                                    Geçmiş <br>Siparişlerim
                                </a>
                                <a  href="{{route('pmenu')}}" class="btn btn-sq-lg text-dark kback zoom">
                                    <i class="fa fa-folder-open fa-5x"></i><br/>
                                    <br>Sistem Yönetimi
                                </a>
                            </div>
                        </div>


            </div>
        </div>
    </div>
@endsection

@section('customJs')
@endsection

@section('customCss')
@endsection
