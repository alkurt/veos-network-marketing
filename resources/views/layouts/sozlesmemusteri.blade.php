@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 140px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">{{config('app.name')}} SÖZLEŞMELER</h4>
       <div class="row justify text-center">
           <div class="md-col-12">
               <div class="table-responsive">
                   <div class="table table-bordered">
                       @forelse($sozlesme as $s)
                               <table>
                                   <tr>
                                       <th>Sözleşme İsmi</th>
                                       <th>Sözleşme PDF</th>
                                   </tr>
                                   <tr>
                                       <td>{{$s->title}}</td>
                                       <td><a href="{{ route('sozlesme.download', $s->uuid) }}">{{ $s->cover }}</a></td>
                                   </tr>

                       @empty
                                       <marquee scrollamount="1" direction="down"> <p class="text-center text-dark"> [ Sözleşmelerimiz Aşağıda Yer Almaktadır Daha Güncel Sözleşmeleri Buradan Takip Edebilirsiniz...] <strong>VeosNet sağlıklı günler diler...</strong> </p></marquee>
                       @endforelse
                               </table>
                   </div>
               </div>





           </div>
       </div>




        </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
