@extends('layouts.main')
@section("content")
   <div class="container" style="margin-top: 170px;background-image: url('https://i.gifer.com/6k2.gif')">
       <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>  <h4 class="text-center" style="font-family: 'Harlow Solid Italic'">KARİYER BİLGİLENDİRME </h4>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4 mt-4">
                    <p>Kariyer(Bayi) Adımı</p>
                    @foreach($name as $name)
                        <div class="row">
                            <div class="col-md-8">
                                {{$name}}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-4 mt-4">
                    <p>Kariyer(Bayi) Tutarları</p>
                    @foreach($tutar as $tutar)
                        <div class="row">
                            <div class="col-md-2">
                                {{$tutar}}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-4 mt-4">
                    <p>Açıklama</p>
                    <small >Bayi Yükselt Alışveriş</small><br>
                    <small >Bayi Yükselt Alışveriş</small><br>
                    <small >Bayi Yükselt Alışveriş</small><br>
                    <small >Bayi Yükselt Alışveriş</small><br>
                    <small >Bayi Yükselt Alışveriş</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Soy Ağacı Kazancı</small><br>
                    <small >Bayi Olma Şartını Gerçekleştirmemiş Kullanıcı</small><br>
                    <small >Sadece Alışveriş İçin Kayıt Yapan Kullanıcı</small><br>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
   </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
