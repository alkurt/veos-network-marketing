@extends("layouts.main")
@section("content")
    <br>
    <br>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <div class="container"  style="margin-top: 170px">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div style="background: #ff820e" class="panel-heading">
                            <h4 class="panel-title">

                                <a style="color: #EFEFEF">  PRİM MENÜ</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-eye-open text-primary"></span><a href="http://www.jquery2dotnet.com">Genel Bakış</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-users text-success"> </span><a href="{{route("ekipagaci")}}"> Ekibim</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="text-success fa fa-tree"></i> <a href="{{route('binary')}}"> Binary Kazancım</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-google-wallet text-success"></span><a href="{{route("cuzdan")}}"> Cüzdanım</a>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-stats text-success"></span><a href="http://www.jquery2dotnet.com">Kazanç Özetim</a>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-server text-success"></span><a href="http://www.jquery2dotnet.com"> Hesap Menü</a>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>


    <div class="row justify-content-center" style="margin-left: 375px">
        <div class="col-12">
            <br>
            <br>
           &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <button class="fa fa-user-plus" style="height: 42px;width:45px"></button>
            @for($i=0;$i<8;$i++)
                <div class="row justify-content-center">
                    <div class="col-12">
                    @for($j=0;$j<8;$j++)
                        <div class="btn btn-sm btn btn-outline-primary ml-3 my-5">
                           <p class="fa fa-user-plus"></p>
                        </div>
                    @endfor
                    </div>
                </div>
                @endfor
        </div>
    </div>
</div>




@endsection

@section("customJs")


@endsection

@section("customCss")


@endsection
