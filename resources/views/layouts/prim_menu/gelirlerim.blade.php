@extends("layouts.main")
@section("content")
    <br>
    <br>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <div class="container"  style="margin-top: 170px">
        <div class="row">
            <div class="col-md-4">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div style="background: #ff820e" class="panel-heading">
                            <h4 class="panel-title">

                                <a style="color: #EFEFEF">  PRİM MENÜ</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-eye-open text-primary"></span><a href="http://www.jquery2dotnet.com">Genel Bakış</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-users text-success"> </span><a href="{{route("ekipagaci")}}"> Ekibim</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-money text-info"></span><a href="http://www.jquery2dotnet.com"> Gelirlerim</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-google-wallet text-success"></span><a href="{{route("cuzdan")}}"> Cüzdanım</a>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-stats text-success"></span><a href="http://www.jquery2dotnet.com">Kazanç Özetim</a>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-server text-success"></span><a href="http://www.jquery2dotnet.com"> Hesap Menü</a>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
