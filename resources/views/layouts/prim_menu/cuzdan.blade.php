@extends("layouts.main")
@section("content")
                 <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                 <link rel="stylesheet" href="{{asset('css/menu.css')}}">

                 <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
                 <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

                 <div class=""style="margin-top: 70px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')" >
                     <div class="col-md-12 mb-2 " >
                         <div class="row">
                             <div class="col-md-6 mt-3 ">
                                 <p style="color: black"><i class="fa fa-user mx-3 "></i>Sayın;&nbsp; {{auth()->user()->name}} {{auth()->user()->surname}}</p>
                                 <p style="color: black"><i class="fa fa-user-secret mx-3"></i>Kullanıcı Numaranız: [ {{auth()->user()->id}} ]</p>
                             </div>
                             <div class="col-md-6 mt-3">
                                 <p class=" mt-2 mx-5"style="color: black"><i class="fa fa-briefcase mx-3"></i> Kariyeriniz :&nbsp; {{auth()->user()->kariyer->kariyername}}</p>
                                 <p class="mx-5"  style="color: black"> <i class="fa fa-battery-quarter mx-3"></i>Şuan ki Kariyer Puanınız :&nbsp; {{auth()->user()->ara_pv}} PV</p>
                             </div>
                         </div>
                         <div style="height: 1px;background-image: url('https://wallpaperaccess.com/full/1216046.jpg')"></div>
                     </div>
                     <div class="row">
                         <div class="col-md-12">
                             <div class="col-md-3">
                                 <ul id="menu-v">
                                     <li>  <h5 class="arrow text-white  text-center">CÜZDAN BAKİYENİZ</h5></li>
                                     <li><a class=""title="Geri" onclick="window.history.back()">Geri Gitmek İstiyorum</a></li>
                                     <li>
                                         <a class="arrow" href="#">Genel Bakış</a>
                                         <ul>
                                             <li>
                                                 <a class="" href="{{route('alt-bayiliklerim')}}">
                                                     İllerdeki Bayiliklerim
                                                 </a>
                                             </li>
                                             <li>
                                                 <a  class="" href="{{route('yeni-alt-bayi')}}">
                                                     Yeni İlde Bayi Aç
                                                 </a>
                                             </li>
                                         </ul>
                                     </li>
                                     <li><a href= "{{route("kazanc")}}"> Ekibim</a>
                                     <li><a href="{{route('binary')}}"> Soy Ağacı Kazancım</a></li>
                                     <li> <a href="{{route("cuzdan")}}"> Cüzdanım</a></li>
                                     <li><a  href="{{route("isteklerim")}}"> İsteklerim</a></li>
                                     <li><a href="{{route("kazancozeti")}}">Gelirlerim</a> </li>
                                     <li> <a  href="{{route("gelenpvsyf")}}"> Pv/Cv Gelirlerim</a></li>
                                     <li><a href="{{route("hesap")}}">Hesap Menü</a></li>
                                 </ul>
                             </div>
                             <div class="col-md-9" >
                                 <div class=" offset-l3 z-depth-4 s12 m12">
                                     <h4 style="font-family: 'Harlow Solid Italic'"></h4>
                                     <hr>
                                     <ul id="tabs-swipe-demo" class="tabs d-inline-flex ">
                                         <li class="tab col-3 s3"><a class="text-secondary" href="#btc"><strong>Bakiyem</strong></a></li>
                                         <li class="tab col-3 s3" style="border-left: 2px solid #dc3545"><a class="text-secondary"  href="#eth"><strong>Brüt Bakiyem</strong></a></li>
                                         <li class="tab col-3 s3" style="border-left: 2px solid #dc3545"><a class="text-secondary"  href="#neo"><strong>Alınan Ödeme</strong></a></li>
                                         <li class="tab col-3 s3" style="border-left: 2px solid #dc3545"><a class="text-secondary"  href="#stx"><strong >Ödeme Talebi</strong></a></li>
                                     </ul>
                                     <div id="btc" class="col s12 mt-5 w-75">
                                         <div class="row">
                                             <div class="col s12 m12">
                                                 <div class="card no-shadows alert alert-info">
                                                     <div class="card-content ">
                                                         <h4 style="font-family: 'Harlow Solid Italic'" class="card-title">Cüzdan Bakiyeniz</h4>
                                                         <p>{{$cuzdan->toplam_bakiye}} <small> ₺</small></p>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     <div id="eth" class="col s12 mt-5 w-75">
                                         <div class="row">
                                             <div class="col s12 m12">
                                                 <div class="card no-shadows alert alert-success">
                                                     <div class="card-content ">
                                                         <h4 style="font-family: 'Harlow Solid Italic'" class="card-title">Bakiyenizin %20 Kesinti Gösterimi</h4>
                                                         @php $odenen=(auth()->user()->cuzdan->toplam_bakiye ) - ((auth()->user()->cuzdan->toplam_bakiye )*20)/100; @endphp
                                                         {{$odenen}} <small> ₺</small>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                     <div id="neo" class="col s12 mt-5 w-75">
                                         <div class="row">
                                             <div class="col s12 m12">
                                                 <div class="card no-shadows alert alert-success">
                                                     <div class="card-content ">
                                                         <h4 style="font-family: 'Harlow Solid Italic'" class="card-title mb-3">Hesabınıza Yatırılan Toplam Tutar </h4>
                                                         {{$cuzdan->odenen_bakiye}} <small> ₺</small>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                     <div id="stx" class="col s12 mt-5 w-75">
                                         <div class="row">
                                             <div class="col s12 m12 ">
                                                 <div class="card no-shadows alert alert-success">
                                                     <div class="card-content ">
                                                         <h4 class="card-title mb-2" style="font-family: 'Harlow Solid Italic'">Ödeme Talebinde Bulunmak mı İstiyorsunuz ?</h4>
                                                         <a class=" w-25 btn btn-info mt-3 mb-3 mx-5" href="{{route('odemetalebi.create')}}">EVET</a>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
@endsection
@section("js")
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.tabs').tabs();
        });

    </script>

@endsection
@section("css")
    <link rel="stylesheet" href="{{asset('css/cuzdan.css')}}">
@endsection
