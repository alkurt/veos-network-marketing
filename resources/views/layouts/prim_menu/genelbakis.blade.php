@extends("layouts.main")
@section("content")

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <div class="container"  style="margin-top: 150px">
<div class="row">
    <div class="col-md-12">

            <div class="col-md-4">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div style="background: #ff820e" class="panel-heading">
                            <h4 class="panel-title">

                                <a style="color: #EFEFEF">  PRİM MENÜ</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-eye-open text-primary"></span><a href="http://www.jquery2dotnet.com">Genel Bakış</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-users text-success"> </span><a href="{{route("ekipagaci")}}"> Ekibim</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-money text-info"></span><a href="http://www.jquery2dotnet.com"> Gelirlerim</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-google-wallet text-success"></span><a href="{{route("cuzdan")}}"> Cüzdanım</a>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-stats text-success"></span><a href="http://www.jquery2dotnet.com">Kazanç Özetim</a>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="fa fa-server text-success"></span><a href="http://www.jquery2dotnet.com"> Hesap Menü</a>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



                <div class="col-md-8">
                    <p>
                        <a href="#" class="btn btn-sq-lg btn-primary">
                            <i class="fa fa-pencil fa-5x"></i><br/>
                            <br> Sipariş Ver
                        </a>

                        <a href="#" class="btn btn-sq-lg btn-success">
                            <i class="fa fa-info-circle  fa-5x"></i><br/>
                            <br>Bilgilerim
                        </a>

                        <a href="#" class="btn btn-sq-lg btn-info">
                            <i class="fa fa-lock fa-5x"></i><br/>
                            <br>Şifre Değiştir
                        </a>
                        <br>
                        <br>
                        <br>

                        <a href="#" class="btn btn-sq-lg btn-warning">
                            <i class="fa fa-book fa-5x"></i><br/>
                            <br>Adres Defteri
                           </a>

                        <a href="#" class="btn btn-sq-lg btn-danger">
                            <i class="fa fa-shopping-cart fa-5x"></i><br/>
                            Geçmiş <br>Siparişlerim
                        </a>

                        <a href="#" class="btn btn-sq-lg btn-danger">
                            <i class="fa fa-folder-open fa-5x"></i><br/>
                            <br>Sistem Yönetimi
                        </a>
                    </p>
                </div>


        </div>
    </div>
</div>




@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
