    @extends("layouts.main")
    @section("content")
        @foreach($dallar as $dal)
            <!-- Modal -->
            <div class="modal fade" id="modal-{{$dal->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true" style="margin-top:80px ">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        @foreach(auth()->user()->roles as $role)
                            @if($role->name == "admin")
                                <div class="modal-body " style="margin-top: 140px">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark mt-2" id="exampleModalLabel"><strong>VeosNet </strong> Kullanıcı İşlem Paneli</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div>
                                        <p style="font-size: small">-> Kullanıcıyı transfer etmek için, göndereceğiniz kullanıcı nu:[ör:12034] girerek
                                            kullanıcıya <strong>Transfer </strong> edebilirsiniz.</p>
                                        <p style="font-size: small">-> Kullanıcıyı silmek için <strong>[ <i class="fa fa-trash"></i> ]</strong> butonuna tıklayınız</p>
                                        <p style="font-size: small">-> Konum bilgisi almak için <strong>Bilgi</strong> butonuna tıklayınız.</p>
                                        <p style="font-size: small">-> Kullanıcıya konum vermek için ör[1.1] <strong>Konumlandırma</strong> yapabilirsiniz</p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="row align-items-center">
                                        <div class="col-md-10 col-sm-10 " >
                                            <div class="row">
                                                <form method="post" action="{{route('transfer',$dal->id)}}">
                                                    @csrf
                                                    <div class="col-md-4 col-sm-4">
                                                        <input style="border-color: darkgreen; width: 100%" id="transfer" name="transfer"
                                                                type="text" required
                                                               onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'
                                                               maxlength="5" minlength="5">
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <select style="border-color: darkgreen; width: 100%" id="kdid"  name="konumlandir"
                                                                value="{{old('konumlandir')}}" type="text">
                                                            <option value="1">1</option>
                                                            <option value="1.1">1.1</option>
                                                            <option value="1.2">1.2</option>
                                                            <option value="1.3">1.3</option>
                                                            <option value="1.4">1.4</option>
                                                            <option value="1.5">1.5</option>
                                                            <option value="1.6">1.6</option>
                                                            <option value="1.7">1.7</option>
                                                            <option value="1.8">1.8</option>
                                                            <option value="1.9">1.9</option>
                                                            <option value="1.10">1.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="2">2</option>
                                                            <option value="2.1">2.1</option>
                                                            <option value="2.2">2.2</option>
                                                            <option value="2.3">2.3</option>
                                                            <option value="2.4">2.4</option>
                                                            <option value="2.5">2.5</option>
                                                            <option value="2.6">2.6</option>
                                                            <option value="2.7">2.7</option>
                                                            <option value="2.8">2.8</option>
                                                            <option value="2.9">2.9</option>
                                                            <option value="2.10">2.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="3">3</option>
                                                            <option value="3.1">3.1</option>
                                                            <option value="3.2">3.2</option>
                                                            <option value="3.3">3.3</option>
                                                            <option value="3.4">3.4</option>
                                                            <option value="3.5">3.5</option>
                                                            <option value="3.6">3.6</option>
                                                            <option value="3.7">3.7</option>
                                                            <option value="3.8">3.8</option>
                                                            <option value="3.9">3.9</option>
                                                            <option value="3.10">3.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="4">4</option>
                                                            <option value="4.1">4.1</option>
                                                            <option value="4.2">4.2</option>
                                                            <option value="4.3">4.3</option>
                                                            <option value="4.4">4.4</option>
                                                            <option value="4.5">4.5</option>
                                                            <option value="4.6">4.6</option>
                                                            <option value="4.7">4.7</option>
                                                            <option value="4.8">4.8</option>
                                                            <option value="4.9">4.9</option>
                                                            <option value="4.10">4.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="5">5</option>
                                                            <option value="5.1">5.1</option>
                                                            <option value="5.2">5.2</option>
                                                            <option value="5.3">5.3</option>
                                                            <option value="5.4">5.4</option>
                                                            <option value="5.5">5.5</option>
                                                            <option value="5.6">5.6</option>
                                                            <option value="5.7">5.7</option>
                                                            <option value="5.8">5.8</option>
                                                            <option value="5.9">5.9</option>
                                                            <option value="5.10">5.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="6">6</option>
                                                            <option value="6.1">6.1</option>
                                                            <option value="6.2">6.2</option>
                                                            <option value="6.3">6.3</option>
                                                            <option value="6.4">6.4</option>
                                                            <option value="6.5">6.5</option>
                                                            <option value="6.6">6.6</option>
                                                            <option value="6.7">6.7</option>
                                                            <option value="6.8">6.8</option>
                                                            <option value="6.9">6.9</option>
                                                            <option value="6.10">6.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="7">7</option>
                                                            <option value="7.1">7.1</option>
                                                            <option value="7.2">7.2</option>
                                                            <option value="7.3">7.3</option>
                                                            <option value="7.4">7.4</option>
                                                            <option value="7.5">7.5</option>
                                                            <option value="7.6">7.6</option>
                                                            <option value="7.7">7.7</option>
                                                            <option value="7.8">7.8</option>
                                                            <option value="7.9">7.9</option>
                                                            <option value="7.10">7.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="8">8</option>
                                                            <option value="8.1">8.1</option>
                                                            <option value="8.2">8.2</option>
                                                            <option value="8.3">8.3</option>
                                                            <option value="8.4">8.4</option>
                                                            <option value="8.5">8.5</option>
                                                            <option value="8.6">8.6</option>
                                                            <option value="8.7">8.7</option>
                                                            <option value="8.8">8.8</option>
                                                            <option value="8.9">8.9</option>
                                                            <option value="8.10">8.10</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <button type="submit"   class="btn btn-success btn-sm"
                                                                style="color: white ;width: 100%">Transfer Et</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="row">
                                                <form method="post" class="mt-2" action="{{route('konumlandir',$dal->id)}}">
                                                    @csrf
                                                    <div class="col-md-4 col-sm-4">
                                                        <select style="border-color: darkgreen; width: 100%" id="kdid"  name="konumlandir"
                                                                value="{{old('konumlandir')}}" type="text">
                                                            <option value="1">1</option>
                                                            <option value="1.1">1.1</option>
                                                            <option value="1.2">1.2</option>
                                                            <option value="1.3">1.3</option>
                                                            <option value="1.4">1.4</option>
                                                            <option value="1.5">1.5</option>
                                                            <option value="1.6">1.6</option>
                                                            <option value="1.7">1.7</option>
                                                            <option value="1.8">1.8</option>
                                                            <option value="1.9">1.9</option>
                                                            <option value="1.10">1.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="2">2</option>
                                                            <option value="2.1">2.1</option>
                                                            <option value="2.2">2.2</option>
                                                            <option value="2.3">2.3</option>
                                                            <option value="2.4">2.4</option>
                                                            <option value="2.5">2.5</option>
                                                            <option value="2.6">2.6</option>
                                                            <option value="2.7">2.7</option>
                                                            <option value="2.8">2.8</option>
                                                            <option value="2.9">2.9</option>
                                                            <option value="2.10">2.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="3">3</option>
                                                            <option value="3.1">3.1</option>
                                                            <option value="3.2">3.2</option>
                                                            <option value="3.3">3.3</option>
                                                            <option value="3.4">3.4</option>
                                                            <option value="3.5">3.5</option>
                                                            <option value="3.6">3.6</option>
                                                            <option value="3.7">3.7</option>
                                                            <option value="3.8">3.8</option>
                                                            <option value="3.9">3.9</option>
                                                            <option value="3.10">3.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="4">4</option>
                                                            <option value="4.1">4.1</option>
                                                            <option value="4.2">4.2</option>
                                                            <option value="4.3">4.3</option>
                                                            <option value="4.4">4.4</option>
                                                            <option value="4.5">4.5</option>
                                                            <option value="4.6">4.6</option>
                                                            <option value="4.7">4.7</option>
                                                            <option value="4.8">4.8</option>
                                                            <option value="4.9">4.9</option>
                                                            <option value="4.10">4.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="5">5</option>
                                                            <option value="5.1">5.1</option>
                                                            <option value="5.2">5.2</option>
                                                            <option value="5.3">5.3</option>
                                                            <option value="5.4">5.4</option>
                                                            <option value="5.5">5.5</option>
                                                            <option value="5.6">5.6</option>
                                                            <option value="5.7">5.7</option>
                                                            <option value="5.8">5.8</option>
                                                            <option value="5.9">5.9</option>
                                                            <option value="5.10">5.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="6">6</option>
                                                            <option value="6.1">6.1</option>
                                                            <option value="6.2">6.2</option>
                                                            <option value="6.3">6.3</option>
                                                            <option value="6.4">6.4</option>
                                                            <option value="6.5">6.5</option>
                                                            <option value="6.6">6.6</option>
                                                            <option value="6.7">6.7</option>
                                                            <option value="6.8">6.8</option>
                                                            <option value="6.9">6.9</option>
                                                            <option value="6.10">6.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="7">7</option>
                                                            <option value="7.1">7.1</option>
                                                            <option value="7.2">7.2</option>
                                                            <option value="7.3">7.3</option>
                                                            <option value="7.4">7.4</option>
                                                            <option value="7.5">7.5</option>
                                                            <option value="7.6">7.6</option>
                                                            <option value="7.7">7.7</option>
                                                            <option value="7.8">7.8</option>
                                                            <option value="7.9">7.9</option>
                                                            <option value="7.10">7.10</option>
                                                            <option value="">------------------------</option>
                                                            <option value="8">8</option>
                                                            <option value="8.1">8.1</option>
                                                            <option value="8.2">8.2</option>
                                                            <option value="8.3">8.3</option>
                                                            <option value="8.4">8.4</option>
                                                            <option value="8.5">8.5</option>
                                                            <option value="8.6">8.6</option>
                                                            <option value="8.7">8.7</option>
                                                            <option value="8.8">8.8</option>
                                                            <option value="8.9">8.9</option>
                                                            <option value="8.10">8.10</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4"></div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <button type="submit"
                                                                class="btn btn-success btn-sm ml-3"  style="color: white;width: 100%">Konumlandır</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <a class="btn btn-warning btn-sm" style="color: white"
                                               href="{{route('bilgilendirme')}}">Bilgi</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 d-flex">
                                        <div class="col-md-6 col-sm-12">
                                            <form action="{{route('admin.pv.ekle',$dal->id)}}" method="post">
                                                @csrf
                                                <div class="form-group text-center">
                                                    <label  for="pv">PV Ekle</label>
                                                    <input class="col-sm-12 col-md-12" type="text" id="pv" name="pv" required>
                                                </div>
                                                <div class="form-group text-center">
                                                    <button type="submit" class="btn btn-dark text-white mt-2">Pv Ekle </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <form action="{{route('admin.cv.ekle', $dal->id)}}" method="post">
                                                @csrf
                                                <div class="form-group text-center">
                                                    <label  for="pv">CV Ekle</label>
                                                    <input class="col-sm-12 col-md-12" type="text" id="cv" name="cv" required>
                                                </div>
                                                <div class="form-group text-center">
                                                    <button type="submit" class="btn btn-dark text-white mt-2">Cv Ekle </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="modal-body text-center">
                                    <p>-> Kullanıcının ağacına gitmek için Git'e Tıklayınız </p>
                                    <a class="btn btn-primary w-50" style="color: white" href="{{route('kid',$dal->id)}}">Git</a>
                                </div>
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>
        @endforeach
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="{{asset('css/menu.css')}}">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-11.min.js"></script>
        <div class="" style="margin-top: 100px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
            <div class="col-md-12" >
                <div class="row">
                    <div class="col-md-6 mt-3">
                        <p style="color: black"><i class="fa fa-user mx-3 "></i>Sayın;&nbsp; {{auth()->user()->name}} {{auth()->user()->surname}}</p>
                        <p style="color: black"><i class="fa fa-user-secret mx-3"></i>Kullanıcı Numaranız: {{auth()->user()->id}}</p>
                    </div>
                    <div class="col-md-6 mt-3">
                        <p class=" mt-2 mx-5"style="color: black"><i class="fa fa-briefcase mx-3"></i> Kariyeriniz :&nbsp; {{auth()->user()->kariyer->kariyername}}</p>
                        <p class="mx-5"  style="color: black"> <i class="fa fa-battery-quarter mx-3"></i>Şuan ki Kariyer Puanınız :&nbsp; {{auth()->user()->ara_pv}} PV</p>
                    </div>
                </div>
                <div style="height: 1px;background-image: url('https://wallpaperaccess.com/full/1216046.jpg')"></div>
            </div>
            <div class="row">
                <div class="col-md-12 mx-5">
                    <div class="col-sm-12">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                                   role="tab" aria-controls="nav-home" aria-selected="true">Ekip Ağacım</a>
                                <a class="nav-item nav-link text-dark" href="{{route('ekiptakip')}}" aria-selected="false">Ekip
                                    Takip Ekranı</a>
                                <a class="mx-5 nav-item nav-link text-dark" href="{{route('ekiplistesi')}}" aria-selected="false">Ekip
                                    Listesi</a>
                                    @if(auth()->user()->id == 10001)
                                        @else
                                    <li class="nav-item dropdown">
                                        <a style="margin-top: -3px" class="text-dark nav-link dropdown-toggl" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            MENÜ
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{route('menu')}}">Genel Bakış</a>
                                            <a class="dropdown-item" href="{{route('pmenu')}}">Sistem Yönetimi</a>
                                            <a class="dropdown-item" href="{{route('hesap')}}">Hesap Yönetimi</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{route('isteklerim')}}">isteklerim</a>
                                        </div>
                                    </li>
                                @endif
                            </div>
                        </nav>
                    </div>
                    <div class="table-responsive">
                        <div class="tree">
                            <ul>
                                <li class="mb-5 table-responsive">
                                    <img width="50" height="50" src="https://icons-for-free.com/iconfiles/png/512/boss+business+people+businessman+lawyer+owner+person+icon-1320086651899415591.png" alt=""><br>
                                    <strong style="color: gray;"> @isset($kisi){{$kisi->name}} @else{{auth()->user()->name}} @endisset</strong>
                                    <ul>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar) == 0)
                                                    <a href="{{route("kayityapma",['konum'=>1,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)
                                                        @if($dallar[$i]->konum == "1")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}}">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp
                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif
                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif
                                            <ul>
                                                <ul>
                                                    <li class="">
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)
                                                                @if($dallar[$i]->konum == "1.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp
                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>1.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            @endfor
                                                        @endif
                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)
                                                                        @if($dallar[$i]->konum == "1.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++ @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++ @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++ @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif

                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>1.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif


                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "1.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++   @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++   @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++   @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++   @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++   @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++   @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>1.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif

                                                                                @endif
                                                                            @endfor
                                                                        @endif
                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "1.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp

                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++ @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>1.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif

                                                                                        @endif
                                                                                    @endfor
                                                                                @endif
                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "1.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp

                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif

                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>1.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif

                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif
                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "1.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif

                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>1.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif

                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "1.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>1.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "1.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>1.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif

                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "1.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>1.9 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif

                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar)==0)
                                                    <a href="{{route("kayityapma",['konum'=>2,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)

                                                        @if($dallar[$i]->konum == "2")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp

                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif
                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif
                                            <ul>
                                                <li class="mt-2">
                                                    @if($dallar)
                                                        @php $olasilik=0; $bayrak=0; @endphp
                                                        @for($i=0;$i<count($dallar);$i++)
                                                            @if($dallar[$i]->konum == "2.1")
                                                                <a data-toggle="modal"
                                                                   data-target="#modal-{{$dallar[$i]->id}}"
                                                                   style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                            title="İsim: {{$dallar[$i]->name}}
                                                                                    Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                </a>
                                                                @php $olasilik=1; @endphp
                                                            @else
                                                                @if($dallar[$i]->konum =='1')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='2')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='3')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='4')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='5')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='6')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='7')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($dallar[$i]->konum =='8')
                                                                    @php $bayrak++  @endphp
                                                                @endif
                                                                @if($bayrak==8)
                                                                    @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                        <a href="{{route("kayityapma",['konum'=>2.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endfor
                                                    @endif
                                                    <ul>
                                                        <li>
                                                            @if($dallar)
                                                                @php $olasilik=0; $bayrak=0; @endphp
                                                                @for($i=0;$i<count($dallar);$i++)

                                                                    @if($dallar[$i]->konum == "2.2")
                                                                        <a data-toggle="modal"
                                                                           data-target="#modal-{{$dallar[$i]->id}}"
                                                                           style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                    title="İsim: {{$dallar[$i]->name}}
                                                                                            Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                        </a>
                                                                        @php $olasilik=1; @endphp
                                                                    @else
                                                                        @if($dallar[$i]->konum =='1.1')
                                                                            @php $bayrak++ @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='2.1')
                                                                            @php $bayrak++  @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='3.1')
                                                                            @php $bayrak++  @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='4.1')
                                                                            @php $bayrak++ @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='5.1')
                                                                            @php $bayrak++ @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='6.1')
                                                                            @php $bayrak++  @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='7.1')
                                                                            @php $bayrak++  @endphp
                                                                        @endif
                                                                        @if($dallar[$i]->konum =='8.1')
                                                                            @php $bayrak++  @endphp
                                                                        @endif

                                                                        @if($bayrak==8)
                                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                <a href="{{route("kayityapma",['konum'=>2.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                        src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                @endfor
                                                            @endif
                                                            <ul>
                                                                <li>
                                                                    @if($dallar)
                                                                        @php $olasilik=0; $bayrak=0; @endphp
                                                                        @for($i=0;$i<count($dallar);$i++)

                                                                            @if($dallar[$i]->konum == "2.3")
                                                                                <a data-toggle="modal"
                                                                                   data-target="#modal-{{$dallar[$i]->id}}"
                                                                                   style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                            title="İsim: {{$dallar[$i]->name}}
                                                                                                    Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                </a>
                                                                                @php $olasilik=1; @endphp

                                                                            @else
                                                                                @if($dallar[$i]->konum =='1.2')
                                                                                    @php $bayrak++   @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='2.2')
                                                                                    @php $bayrak++   @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='3.2')
                                                                                    @php $bayrak++   @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='4.2')
                                                                                    @php $bayrak++   @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='5.2')
                                                                                    @php $bayrak++  @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='6.2')
                                                                                    @php $bayrak++   @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='7.2')
                                                                                    @php $bayrak++   @endphp
                                                                                @endif
                                                                                @if($dallar[$i]->konum =='8.2')
                                                                                    @php $bayrak++  @endphp
                                                                                @endif

                                                                                @if($bayrak==8)
                                                                                    @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                        <a href="{{route("kayityapma",['konum'=>2.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                    @endif
                                                                                @endif

                                                                            @endif
                                                                        @endfor
                                                                    @endif

                                                                    <ul>
                                                                        <li>
                                                                            @if($dallar)
                                                                                @php $olasilik=0; $bayrak=0; @endphp
                                                                                @for($i=0;$i<count($dallar);$i++)

                                                                                    @if($dallar[$i]->konum == "2.4")
                                                                                        <a data-toggle="modal"
                                                                                           data-target="#modal-{{$dallar[$i]->id}}"
                                                                                           style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                    title="İsim: {{$dallar[$i]->name}}
                                                                                                            Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                        </a>
                                                                                        @php $olasilik=1; @endphp

                                                                                    @else
                                                                                        @if($dallar[$i]->konum =='1.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='2.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='3.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='4.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='5.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='6.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='7.3')
                                                                                            @php $bayrak++  @endphp
                                                                                        @endif
                                                                                        @if($dallar[$i]->konum =='8.3')
                                                                                            @php $bayrak++ @endphp
                                                                                        @endif

                                                                                        @if($bayrak==8)
                                                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                <a href="{{route("kayityapma",['konum'=>2.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                        src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                            @endif
                                                                                        @endif

                                                                                    @endif
                                                                                @endfor
                                                                            @endif

                                                                            <ul>
                                                                                <li>
                                                                                    @if($dallar)
                                                                                        @php $olasilik=0; $bayrak=0; @endphp
                                                                                        @for($i=0;$i<count($dallar);$i++)

                                                                                            @if($dallar[$i]->konum == "2.5")
                                                                                                <a data-toggle="modal"
                                                                                                   data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                   style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                            title="İsim: {{$dallar[$i]->name}}
                                                                                                                    Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                </a>
                                                                                                @php $olasilik=1; @endphp

                                                                                            @else
                                                                                                @if($dallar[$i]->konum =='1.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='2.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='3.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='4.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='5.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='6.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='7.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif
                                                                                                @if($dallar[$i]->konum =='8.4')
                                                                                                    @php $bayrak++  @endphp
                                                                                                @endif

                                                                                                @if($bayrak==8)
                                                                                                    @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                        <a href="{{route("kayityapma",['konum'=>2.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                    @endif
                                                                                                @endif

                                                                                            @endif
                                                                                        @endfor
                                                                                    @endif

                                                                                    <ul>
                                                                                        <li>
                                                                                            @if($dallar)
                                                                                                @php $olasilik=0; $bayrak=0; @endphp
                                                                                                @for($i=0;$i<count($dallar);$i++)

                                                                                                    @if($dallar[$i]->konum == "2.6")
                                                                                                        <a data-toggle="modal"
                                                                                                           data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                           style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                    title="İsim: {{$dallar[$i]->name}}
                                                                                                                            Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                        </a>
                                                                                                        @php $olasilik=1; @endphp

                                                                                                    @else
                                                                                                        @if($dallar[$i]->konum =='1.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='2.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='3.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='4.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='5.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='6.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='7.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif
                                                                                                        @if($dallar[$i]->konum =='8.5')
                                                                                                            @php $bayrak++  @endphp
                                                                                                        @endif

                                                                                                        @if($bayrak==8)
                                                                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                <a href="{{route("kayityapma",['konum'=>2.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                        src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                            @endif
                                                                                                        @endif

                                                                                                    @endif
                                                                                                @endfor
                                                                                            @endif

                                                                                            <ul>
                                                                                                <li>
                                                                                                    @if($dallar)
                                                                                                        @php $olasilik=0; $bayrak=0; @endphp
                                                                                                        @for($i=0;$i<count($dallar);$i++)

                                                                                                            @if($dallar[$i]->konum == "2.7")
                                                                                                                <a data-toggle="modal"
                                                                                                                   data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                   style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                            title="İsim: {{$dallar[$i]->name}}
                                                                                                                                    Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                </a>
                                                                                                                @php $olasilik=1; @endphp

                                                                                                            @else
                                                                                                                @if($dallar[$i]->konum =='1.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='2.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='3.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='4.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='5.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='6.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='7.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif
                                                                                                                @if($dallar[$i]->konum =='8.6')
                                                                                                                    @php $bayrak++  @endphp
                                                                                                                @endif

                                                                                                                @if($bayrak==8)
                                                                                                                    @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                        <a href="{{route("kayityapma",['konum'=>2.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                    @endif
                                                                                                                @endif

                                                                                                            @endif
                                                                                                        @endfor
                                                                                                    @endif
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            @if($dallar)
                                                                                                                @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                @for($i=0;$i<count($dallar);$i++)

                                                                                                                    @if($dallar[$i]->konum == "2.8")
                                                                                                                        <a data-toggle="modal"
                                                                                                                           data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                           style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                    title="İsim: {{$dallar[$i]->name}}
                                                                                                                                            Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                        </a>
                                                                                                                        @php $olasilik=1; @endphp

                                                                                                                    @else
                                                                                                                        @if($dallar[$i]->konum =='1.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='2.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='3.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='4.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='5.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='6.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='7.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif
                                                                                                                        @if($dallar[$i]->konum =='8.7')
                                                                                                                            @php $bayrak++  @endphp
                                                                                                                        @endif

                                                                                                                        @if($bayrak==8)
                                                                                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                <a href="{{route("kayityapma",['konum'=>2.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                        src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                            @endif
                                                                                                                        @endif

                                                                                                                    @endif
                                                                                                                @endfor
                                                                                                            @endif
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    @if($dallar)
                                                                                                                        @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                        @for($i=0;$i<count($dallar);$i++)

                                                                                                                            @if($dallar[$i]->konum == "2.9")
                                                                                                                                <a data-toggle="modal"
                                                                                                                                   data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                   style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                            title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                    Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                </a>
                                                                                                                                @php $olasilik=1; @endphp

                                                                                                                            @else
                                                                                                                                @if($dallar[$i]->konum =='1.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='2.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='3.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='4.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='5.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='6.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='7.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif
                                                                                                                                @if($dallar[$i]->konum =='8.8')
                                                                                                                                    @php $bayrak++  @endphp
                                                                                                                                @endif

                                                                                                                                @if($bayrak==8)
                                                                                                                                    @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                        <a href="{{route("kayityapma",['konum'=>2.9 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                    @endif
                                                                                                                                @endif

                                                                                                                            @endif
                                                                                                                        @endfor
                                                                                                                    @endif
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar) ==0)
                                                    <a href="{{route("kayityapma",['konum'=>3,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0;  @endphp
                                                    @for($i=0;$i<count($dallar);$i++)

                                                        @if($dallar[$i]->konum == "3")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp

                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif
                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif
                                            <ul>
                                                <ul>
                                                    <li>
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)

                                                                @if($dallar[$i]->konum == "3.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp

                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>3.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif

                                                                @endif
                                                            @endfor
                                                        @endif


                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                        @if($dallar[$i]->konum == "3.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif

                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>3.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif


                                                                        @endif
                                                                    @endfor
                                                                @endif

                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "3.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>3.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif

                                                                                @endif
                                                                            @endfor
                                                                        @endif

                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "3.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp

                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>3.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif

                                                                                        @endif
                                                                                    @endfor
                                                                                @endif

                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "3.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp

                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif

                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>3.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif

                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif

                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "3.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif

                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>3.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif

                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif

                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "3.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>3.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif
                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "3.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>3.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "3.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>3.9 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif
                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif

                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar) ==0)
                                                    <a href="{{route("kayityapma",['konum'=>4,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>

                                                @else
                                                    @php $olasilik=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)
                                                        @if($dallar[$i]->konum == "4")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp
                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif

                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif

                                            <ul>
                                                <ul>
                                                    <li>
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)

                                                                @if($dallar[$i]->konum == "4.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp

                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>4.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif

                                                                @endif
                                                            @endfor
                                                        @endif

                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                        @if($dallar[$i]->konum == "4.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif

                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>4.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif


                                                                        @endif
                                                                    @endfor
                                                                @endif

                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "4.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>4.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif

                                                                                @endif
                                                                            @endfor
                                                                        @endif

                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "4.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp

                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>4.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif

                                                                                        @endif
                                                                                    @endfor
                                                                                @endif

                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "4.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp

                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>4.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif

                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "4.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>4.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif

                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "4.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>4.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "4.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>4.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif

                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "4.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>4.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif

                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif

                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>

                                                                                </ul>
                                                                            </li>

                                                                        </ul>
                                                                    </li>

                                                                </ul>
                                                            </li>

                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar)==0)
                                                    <a href="{{route("kayityapma",['konum'=>5,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)

                                                        @if($dallar[$i]->konum == "5")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp
                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif
                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif
                                            <ul>
                                                <ul>
                                                    <li>
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)

                                                                @if($dallar[$i]->konum == "5.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp

                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>5.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif

                                                                @endif
                                                            @endfor
                                                        @endif

                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                        @if($dallar[$i]->konum == "5.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>5.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif
                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "5.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>5.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif
                                                                                @endif
                                                                            @endfor
                                                                        @endif
                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "5.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}}  || Konumu:{{$dallar[$i]->konum}}">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp
                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>5.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif
                                                                                        @endif
                                                                                    @endfor
                                                                                @endif
                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "5.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}}  || Konumu:{{$dallar[$i]->konum}}">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp
                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif

                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>5.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif
                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif

                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "5.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>5.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif

                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "5.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>5.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "5.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>5.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif

                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "5.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>5.9 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif

                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar)==0)
                                                    <a href="{{route("kayityapma",['konum'=>6,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)

                                                        @if($dallar[$i]->konum == "6")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp

                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif

                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif

                                            <ul>
                                                <ul>
                                                    <li>
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)

                                                                @if($dallar[$i]->konum == "6.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp

                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>6.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif

                                                                @endif
                                                            @endfor
                                                        @endif


                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                        @if($dallar[$i]->konum == "6.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif

                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>6.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif


                                                                        @endif
                                                                    @endfor
                                                                @endif

                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "6.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>6.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif

                                                                                @endif
                                                                            @endfor
                                                                        @endif

                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "6.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp

                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>6.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif

                                                                                        @endif
                                                                                    @endfor
                                                                                @endif

                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "6.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp

                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif

                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>6.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif

                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif

                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "6.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif

                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>6.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif

                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "6.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>6.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "6.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>6.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "6.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>6.9,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                   src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif
                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif

                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li>
                                            @if($dallar)
                                                @if(count($dallar)==0)
                                                    <a href="{{route("kayityapma",['konum'=>7,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)

                                                        @if($dallar[$i]->konum == "7")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp

                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif

                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif
                                            <ul>
                                                <ul>
                                                    <li>
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)

                                                                @if($dallar[$i]->konum == "7.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp

                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>7.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif

                                                                @endif
                                                            @endfor
                                                        @endif


                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                        @if($dallar[$i]->konum == "7.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif

                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>7.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif


                                                                        @endif
                                                                    @endfor
                                                                @endif


                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "7.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>7.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif
                                                                                @endif
                                                                            @endfor
                                                                        @endif

                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "7.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp

                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>7.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif

                                                                                        @endif
                                                                                    @endfor
                                                                                @endif

                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "7.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp

                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif

                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>7.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif

                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif

                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "7.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif

                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>7.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif

                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif

                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "7.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>7.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "7.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>7.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif

                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "7.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>7.9 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif

                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif

                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>

                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                        <li class="text">
                                            @if($dallar)
                                                @if(count($dallar)==0)
                                                    <a href="{{route("kayityapma",['konum'=>8,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                         src="{{asset("assets/new images/2.1.png")}}"></a>
                                                @else
                                                    @php $olasilik=0; @endphp
                                                    @for($i=0;$i<count($dallar);$i++)

                                                        @if($dallar[$i]->konum == "8")
                                                            <a data-toggle="modal"
                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                            </a>
                                                            @php $olasilik=1; @endphp

                                                        @else
                                                            @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                <a href="{{route("kayityapma",['konum'=>8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                      src="{{asset("assets/new images/2.1.png")}}"></a>
                                                            @endif
                                                        @endif
                                                    @endfor
                                                @endif
                                            @endif
                                            <ul>
                                                <ul>
                                                    <li>
                                                        @if($dallar)
                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                            @for($i=0;$i<count($dallar);$i++)

                                                                @if($dallar[$i]->konum == "8.1")
                                                                    <a data-toggle="modal"
                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                    </a>
                                                                    @php $olasilik=1; @endphp

                                                                @else
                                                                    @if($dallar[$i]->konum =='1')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='2')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='3')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='4')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='5')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='6')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='7')
                                                                        @php $bayrak++  @endphp
                                                                    @endif
                                                                    @if($dallar[$i]->konum =='8')
                                                                        @php $bayrak++  @endphp
                                                                    @endif

                                                                    @if($bayrak==8)
                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                            <a href="{{route("kayityapma",['konum'=>8.1 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            @endfor
                                                        @endif
                                                        <ul>
                                                            <li>
                                                                @if($dallar)
                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                        @if($dallar[$i]->konum == "8.2")
                                                                            <a data-toggle="modal"
                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                            </a>
                                                                            @php $olasilik=1; @endphp

                                                                        @else
                                                                            @if($dallar[$i]->konum =='1.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='2.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='3.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='4.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='5.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='6.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='7.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif
                                                                            @if($dallar[$i]->konum =='8.1')
                                                                                @php $bayrak++  @endphp
                                                                            @endif

                                                                            @if($bayrak==8)
                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                    <a href="{{route("kayityapma",['konum'=>8.2 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                @endif
                                                                            @endif


                                                                        @endif
                                                                    @endfor
                                                                @endif
                                                                <ul>
                                                                    <li>
                                                                        @if($dallar)
                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                @if($dallar[$i]->konum == "8.3")
                                                                                    <a data-toggle="modal"
                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                    </a>
                                                                                    @php $olasilik=1; @endphp

                                                                                @else
                                                                                    @if($dallar[$i]->konum =='1.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='2.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='3.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='4.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='5.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='6.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='7.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif
                                                                                    @if($dallar[$i]->konum =='8.2')
                                                                                        @php $bayrak++  @endphp
                                                                                    @endif

                                                                                    @if($bayrak==8)
                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                            <a href="{{route("kayityapma",['konum'=>8.3 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                        @endif
                                                                                    @endif

                                                                                @endif
                                                                            @endfor
                                                                        @endif

                                                                        <ul>
                                                                            <li>
                                                                                @if($dallar)
                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                        @if($dallar[$i]->konum == "8.4")
                                                                                            <a data-toggle="modal"
                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}}">{{$dallar[$i]->name}}</span>
                                                                                            </a>
                                                                                            @php $olasilik=1; @endphp

                                                                                        @else
                                                                                            @if($dallar[$i]->konum =='1.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='2.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='3.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='4.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='5.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='6.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='7.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif
                                                                                            @if($dallar[$i]->konum =='8.3')
                                                                                                @php $bayrak++  @endphp
                                                                                            @endif

                                                                                            @if($bayrak==8)
                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                    <a href="{{route("kayityapma",['konum'=>8.4 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                @endif
                                                                                            @endif

                                                                                        @endif
                                                                                    @endfor
                                                                                @endif

                                                                                <ul>
                                                                                    <li>
                                                                                        @if($dallar)
                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                @if($dallar[$i]->konum == "8.5")
                                                                                                    <a data-toggle="modal"
                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                    </a>
                                                                                                    @php $olasilik=1; @endphp

                                                                                                @else
                                                                                                    @if($dallar[$i]->konum =='1.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='2.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='3.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='4.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='5.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='6.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='7.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif
                                                                                                    @if($dallar[$i]->konum =='8.4')
                                                                                                        @php $bayrak++  @endphp
                                                                                                    @endif

                                                                                                    @if($bayrak==8)
                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                            <a href="{{route("kayityapma",['konum'=>8.5 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                        @endif
                                                                                                    @endif

                                                                                                @endif
                                                                                            @endfor
                                                                                        @endif

                                                                                        <ul>
                                                                                            <li>
                                                                                                @if($dallar)
                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                        @if($dallar[$i]->konum == "8.6")
                                                                                                            <a data-toggle="modal"
                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                            </a>
                                                                                                            @php $olasilik=1; @endphp

                                                                                                        @else
                                                                                                            @if($dallar[$i]->konum =='1.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='2.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='3.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='4.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='5.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='6.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='7.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($dallar[$i]->konum =='8.5')
                                                                                                                @php $bayrak++  @endphp
                                                                                                            @endif
                                                                                                            @if($bayrak==8)
                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                    <a href="{{route("kayityapma",['konum'=>8.6 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endif
                                                                                                    @endfor
                                                                                                @endif

                                                                                                <ul>
                                                                                                    <li>
                                                                                                        @if($dallar)
                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                @if($dallar[$i]->konum == "8.7")
                                                                                                                    <a data-toggle="modal"
                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                    </a>
                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                @else
                                                                                                                    @if($dallar[$i]->konum =='1.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='2.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='3.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='4.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='5.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='6.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='7.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif
                                                                                                                    @if($dallar[$i]->konum =='8.6')
                                                                                                                        @php $bayrak++  @endphp
                                                                                                                    @endif

                                                                                                                    @if($bayrak==8)
                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                            <a href="{{route("kayityapma",['konum'=>8.7 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                        @endif
                                                                                                                    @endif

                                                                                                                @endif
                                                                                                            @endfor
                                                                                                        @endif
                                                                                                        <ul>
                                                                                                            <li>
                                                                                                                @if($dallar)
                                                                                                                    @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                    @for($i=0;$i<count($dallar);$i++)

                                                                                                                        @if($dallar[$i]->konum == "8.8")
                                                                                                                            <a data-toggle="modal"
                                                                                                                               data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                               style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                        title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                            </a>
                                                                                                                            @php $olasilik=1; @endphp

                                                                                                                        @else
                                                                                                                            @if($dallar[$i]->konum =='1.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='2.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='3.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='4.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='5.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='6.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='7.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif
                                                                                                                            @if($dallar[$i]->konum =='8.7')
                                                                                                                                @php $bayrak++  @endphp
                                                                                                                            @endif

                                                                                                                            @if($bayrak==8)
                                                                                                                                @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                    <a href="{{route("kayityapma",['konum'=>8.8 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                            src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                @endif
                                                                                                                            @endif

                                                                                                                        @endif
                                                                                                                    @endfor
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li>
                                                                                                                        @if($dallar)
                                                                                                                            @php $olasilik=0; $bayrak=0; @endphp
                                                                                                                            @for($i=0;$i<count($dallar);$i++)

                                                                                                                                @if($dallar[$i]->konum == "8.9")
                                                                                                                                    <a data-toggle="modal"
                                                                                                                                       data-target="#modal-{{$dallar[$i]->id}}"
                                                                                                                                       style="height: 55px;width: 80px;font-size: xx-small"><span
                                                                                                                                                title="İsim: {{$dallar[$i]->name}}
                                                                                                                                                        Kullanıcı Nu: {{$dallar[$i]->id}} || Konumu:{{$dallar[$i]->konum}} ">{{$dallar[$i]->name}}</span>
                                                                                                                                    </a>
                                                                                                                                    @php $olasilik=1; @endphp

                                                                                                                                @else
                                                                                                                                    @if($dallar[$i]->konum =='1.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='2.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='3.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='4.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='5.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='6.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='7.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif
                                                                                                                                    @if($dallar[$i]->konum =='8.8')
                                                                                                                                        @php $bayrak++  @endphp
                                                                                                                                    @endif

                                                                                                                                    @if($bayrak==8)
                                                                                                                                        @if($olasilik == 0 && count($dallar)-$i ==1 )
                                                                                                                                            <a href="{{route("kayityapma",['konum'=>8.9 ,'sps'=>$kisi->id])}}"><img style="height: 43px;width: 48px"
                                                                                                                                                                                                                    src="{{asset("assets/new images/2.1.png")}}"></a>
                                                                                                                                        @endif
                                                                                                                                    @endif

                                                                                                                                @endif
                                                                                                                            @endfor
                                                                                                                        @endif

                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section("js")
        <script>
            $(document).ready(function () {
                $(".number").keydown(function (e) {
                    //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                        (e.keyCode == 67 && e.ctrlKey === true) ||
                        //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                        (e.keyCode == 88 && e.ctrlKey === true) ||
                        // home, end, left, right gibi tuşlara izin vermek için.
                        (e.keyCode >= 35 && e.keyCode <= 39)) {

                        return;
                    }
                    // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
            });
        </script>
    @endsection
    @section("css")
        <link rel="stylesheet" href="{{asset("css/agac.css")}}">

    @endsection
