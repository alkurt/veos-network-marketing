@extends('layouts.main')
@section("content")
    <div class=" " style="padding: 30px;margin-top: 110px;background-color: ghostwhite">
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5  class="modal-title" id="exampleModalLabel">&nbsp;Bilgilendirme&nbsp;</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><strong>Sayın;</strong> {{auth()->user()->name}} {{auth()->user()->surname}}</p>
                        <p class="alert alert-secondary">Ödeme Talebinizin %20 kesintisi, talebinizin onaylanması durumunda tarafımızca tahsis edilerek hesabınıza yatırılacaktır...</p>
                    </div>
                </div>
            </div>
        </div>
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">Ödeme Talebi İşlemi</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 mt-2">
                        <div class="form-group ">
                            <form method="POST" action="{{route('odemetalebi.store')}}" >
                                @csrf
                                <div class="form-group mb-3">
                                    <p class="text-center text-secondary"style="
                                background: rgba(0,0,0,.1);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.3);
                                border-radius: 10px;
                                color: darkturquoise;
                            ">Talep Tutarınızı Giriniz (₺)</p>
                                    <input style="
                                background: rgba(0,0,0,.15);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.5);
                                border-radius: 10px;
                                color: black;margin-left: 75px;
                            " class="text-dark mt-3 w-50 form-control {{ $errors->has('odeme_talebi') ? ' is-invalid' : '' }}"  name="odeme_talebi" type="number" value="0" >
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"><strong>Sayın;</strong> {{auth()->user()->name}} {{auth()->user()->surname}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p class="alert alert-secondary">Talebinizi Toplam Bakiyeniz Üzerinden Bulunabilirsiniz, Hesabınıza Yatırılıcak Tutar, <strong>Talep Tutarınızın %20 </strong> Kesintili Hali Olarak Hesabınıza Yatırılacaktır. </p>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="form-group">
                                                    <button class="btn btn-success" type="submit">Onayla</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Onaylama</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Button trigger modal -->
                                <button style="
                                background: rgba(0,0,0,.6);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.8);
                                border-radius: 10px;
                                color: white;
                            " type="button" class="mt-4 text-center btn btn-secondary w-75 mx-4" data-toggle="modal" data-target="#exampleModal1">
                                    Gönder
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8 mt-2" style="border: 2px solid darkslategray">
                        <div class="row">
                            <div class="col-md-6">
                                <h6 class="mb-4 text-secondary mt-4">Toplam Bakiyeniz : <strong class="mx-5">{{$musteri}} ₺</strong></h6>
                                <h6 class="text-secondary">%20 Kesintili Bakiyeniz : <strong class="mx-3">{{$musteri-(($musteri*20)/100)}} ₺</strong></h6>
                                <img class="h-25 w-25 mt-3" src="{{asset('assets/new images/master.jpg')}}" alt="">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class=" btn btn-outline-warning  mt-5" data-toggle="modal" data-target="#exampleModal">
                                    Bilgilendirme
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection

@section("css")
@endsection
