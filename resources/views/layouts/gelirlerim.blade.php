@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 170px">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div style="background: #1c7430" class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="fa fa-step-backward" style="color: white"  href="{{route('menu')}}"></a> <a style="color: #EFEFEF"> PRİM MENÜ</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body ">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <span class="glyphicon glyphicon-eye-open text-primary"></span><a
                                                        href="{{route("genelbakis")}}">Genel Bakış</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-users text-success"> </span><a
                                                        href="{{route("kazanc")}}"> Ekibim</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-money text-info"></span><a
                                                        href="{{route('gelirlerim')}}"> Gelirlerim</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-success"></span><a
                                                        href="{{route("cuzdan")}}"> Cüzdanım</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="glyphicon glyphicon-stats text-success"></span><a
                                                        href="{{route('kazancozeti')}}">Kazanç Özetim</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-server text-success"></span><a href="{{route("hesap")}}">
                                                    Hesap Menü</a>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-9">
                    <marquee direction="5" class="text-center" style="font-size: x-large"> GELİRLERİM HAZIRLANIYOR...</marquee>
                </div>
            </div>
        </div>


    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
