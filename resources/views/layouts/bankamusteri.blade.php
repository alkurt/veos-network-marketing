@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 170px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">MEVCUT HESAP BİLGİLERİ</h4>
        <div class="table-responsive">
            <div class="table table-bordered text-center">
                <table>
                    <tr>

                        <th>Hesap Sahibi</th>
                        <th>İban Numarası</th>
                        <th>Hesap Numarası</th>
                        <th>Banka Adı</th>
                        <th>Şube Kodu</th>
                        <th>Şube Adı</th>
                        <th>Eklenme Tarihi</th>
                        <th>Güncellenme Tarihi</th>

                    </tr>
                    @foreach($banka as $b)
                    <tr>
                        <td>{{$b->hesap_sahibi}}</td>
                        <td>{{$b->iban_no}}</td>
                        <td>{{$b->hesap_no}}</td>
                        <td>{{$b->banka_adi}}</td>
                        <td>{{$b->sube_kodu}}</td>
                        <td>{{$b->sube_adi}}</td>
                        <td>{{$b->created_at}}</td>
                        <td>{{$b->updated_at}}</td>

                    </tr>
                        @endforeach
                </table>
            </div>

        </div>
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
