@extends('layouts.main')
@section("content")
    <div class="container-fluid" style="margin-top: 140px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> ><a class="mx-3" style="color: gray" title="Haberler" >Haberler</a>> <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="col-md-12 mt-5">
            <div class="row">
                @forelse($haber as $h)
                   <div class="col-md-6">
                       <img class="h-50 w-25 mb-2" src="{{$h->url}}" alt=""><br>
                       <p class="mb-2 mx-5 mt-2">{{$h->haber_basligi}}</p><br>
                   </div>
                    <div class="col-md-6">
                        <p style="border: 2px solid darkslategray" class="mb-3">&nbsp;{!! $h->haber_icerik !!}</p>
                    </div>
                @empty
                    <marquee behavior="" direction="left"> <p> Şuan İçin Yüklü Bir Haber Bulunmuyor..</p></marquee>

                @endforelse
            </div>
        </div>
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
