@extends("layouts.main")
@section("content")
    <div class="container" style="margin-top: 160px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h4 class="text-center" style="font-family: 'Rage Italic'">PROFESYONEL PAZARLAMA</h4><br>
        <p>Profesyonel pazarlama, VEOS Network'un güçlü yönlerinden birini oluşturuyor.

            Dürüstlük bizim temel değerlerimizin başında geliyor. Dolayısıyla  VEOSNetwork bağımsız temsilcileri  de kendi networkleri ve potansiyel müşterileri ile yaptıkları işlerde her zaman dürüst ve adil olmaları gerekiyor.

            Herkesin ulaşabileceği etik kurallarımız ve birden fazla dilde sunulan ilke ve prosedür belgelerimiz var. Tüm temsilcilerin, işe girerken profesyonel ve etik marketing anlayışına bağlılık adına bir anlaşma imzalamaları gerekiyor.

            Ayrıca  VEOS Network temsilcileri, sektöre yönelik tavsiyeler ve profesyonel marketing yönergeleri sunan avukatlar ve doğrudan satış profesyonellerinden oluşan  VEOS Network Danışma Kurulu tarafından da desteklenmektedir.
        </p>
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
