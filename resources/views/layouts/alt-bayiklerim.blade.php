@extends('layouts.main')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <div class=""style="margin-top: 100px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')" >
        <div class="col-md-12 mb-2 ">
            <div class="row">
                <div class="col-md-6 mt-3 ">
                    <p style="color: black"><i class="fa fa-user mx-3 "></i>Sayın;&nbsp; {{auth()->user()->name}} {{auth()->user()->surname}}</p>
                    <p style="color: black"><i class="fa fa-user-secret mx-3"></i>Kullanıcı Numaranız: [ {{auth()->user()->id}} ]</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class=" mt-2 mx-5"style="color: black"><i class="fa fa-briefcase mx-3"></i> Kariyeriniz :&nbsp; {{auth()->user()->kariyer->kariyername}}</p>
                    <p class="mx-5"  style="color: black"> <i class="fa fa-battery-quarter mx-3"></i>Şuan ki Kariyer Puanınız :&nbsp; {{auth()->user()->ara_pv}} PV</p>
                </div>
            </div>
            <div style="height: 1px;background-image: url('https://wallpaperaccess.com/full/1216046.jpg')"></div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                @if(auth()->user()->id== 10001)
                @else
                    <div class="col-md-3">
                        <ul id="menu-v">
                            <li>  <h5 class="text-center text-white"  >BÖLGE ALT BAYİLİKLERİM</h5></li>
                            <li><a class=""title="Geri" onclick="window.history.back()">Geri Gitmek İstiyorum</a></li>
                            <li>
                                <a class="arrow" href="#">Genel Bakış</a>
                                <ul>
                                    <li>
                                        <a class="arrow" href="{{route('alt-bayiliklerim')}}">
                                            İllerdeki Bayiliklerim
                                        </a>
                                    </li>
                                    <li>
                                        <a  class="arrow" href="{{route('yeni-alt-bayi')}}">
                                            Yeni İlde Bayi Aç
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="" href= "{{route("kazanc")}}"> Ekibim</a>
                            <li><a class="" href="{{route('binary')}}"> Soy Ağacı Kazancım</a></li>
                            <li>  <a class="" href="{{route("cuzdan")}}"> Cüzdanım</a></li>
                            <li><a class="" href="{{route("isteklerim")}}"> İsteklerim</a></li>
                            <li><a class="" href="{{route("kazancozeti")}}">Gelirlerim</a> </li>
                            <li> <a class="" href="{{route("gelenpvsyf")}}"> Pv/Cv Gelirlerim</a></li>
                            <li><a class="" href="{{route("hesap")}}">Hesap Menü</a></li>
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Kullanıcı Numarası</th>
                                    <th>Bölge</th>
                                    <th>İller</th>
                                    <th class="text-center">Kayıt Tarih</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->bolge->name}}</td>
                                        <td>{{$user->iller->name}}</td>
                                        <td>{{$user->created_at}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("customJs")
@endsection
@section("customCss")
@endsection
