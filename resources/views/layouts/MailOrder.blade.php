@extends('layouts.main')
@section('content')
    @php $urunler=App\BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get(); @endphp
    <p style="margin-top: 160px" class="alert alert-info">veosnet.com firması tarafından sizin için
        @foreach($urunler as $urunler)
            @if($urunler->product-> turu == 'urun')
                <p class="text-dark"> {{$tutar}} </p>
            @else
                <p class="text-dark">
                    {{Cart::subtotal()}}
                </p>
            @endif @endforeach
       ₺ tutarında bir ödeme isteği oluşturuldu. Ödemeyi gerçekleştirebilmek için kart bilgilerinizi girip Ödeme Yap butonuna tıklamanız gerekmektedir.</p>
    <div id="root">
        <div class="mx-5" id="card">
            <div id="card-bottom"></div>
            <div id="card-top">
                <div class="logo">
                    <svg version="1.1" id="visa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="72px" height="72px" viewBox="0 0 47.834 47.834" style="enable-background:new 0 0 47.834 47.834;" fill="white">
                        <g>
                            <g>
                                <path d="M44.688,16.814h-3.004c-0.933,0-1.627,0.254-2.037,1.184l-5.773,13.074h4.083c0,0,0.666-1.758,0.817-2.143
                     c0.447,0,4.414,0.006,4.979,0.006c0.116,0.498,0.474,2.137,0.474,2.137h3.607L44.688,16.814z M39.893,26.01
                     c0.32-0.819,1.549-3.987,1.549-3.987c-0.021,0.039,0.317-0.825,0.518-1.362l0.262,1.23c0,0,0.745,3.406,0.901,4.119H39.893z
                     M34.146,26.404c-0.028,2.963-2.684,4.875-6.771,4.875c-1.743-0.018-3.422-0.361-4.332-0.76l0.547-3.193l0.501,0.228
                     c1.277,0.532,2.104,0.747,3.661,0.747c1.117,0,2.313-0.438,2.325-1.393c0.007-0.625-0.501-1.07-2.016-1.77
                     c-1.476-0.683-3.43-1.827-3.405-3.876c0.021-2.773,2.729-4.708,6.571-4.708c1.506,0,2.713,0.31,3.483,0.599l-0.526,3.092
                     l-0.351-0.165c-0.716-0.288-1.638-0.566-2.91-0.546c-1.522,0-2.228,0.634-2.228,1.227c-0.008,0.668,0.824,1.108,2.184,1.77
                     C33.126,23.546,34.163,24.783,34.146,26.404z M0,16.962l0.05-0.286h6.028c0.813,0.031,1.468,0.29,1.694,1.159l1.311,6.304
                     C7.795,20.842,4.691,18.099,0,16.962z M17.581,16.812l-6.123,14.239l-4.114,0.007L3.862,19.161
                     c2.503,1.602,4.635,4.144,5.386,5.914l0.406,1.469l3.808-9.729L17.581,16.812L17.581,16.812z M19.153,16.8h3.89L20.61,31.066
                     h-3.888L19.153,16.8z"/>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="card-number">
                    XXXX XXXX XXXX XXXX
                </div>
                <div class="row-container">
                    <div class="card-holder">
                        <span>Kart Sahibi</span>
                        <span></span>
                    </div>
                    <div class="expiry">
                        <span>Son Kullanım Tarihi</span>
                        <span>
            <span class="expiry-month">00</span>
            /
            <span class="expiry-year">00</span>
          </span>
                    </div>
                    <div class="cvc">
                        <span>CVC</span>
                        <span>___</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group mx-2 ">
        <form method="POST" action="{{route('mailorder.update', $mailorder->id)}}" id="form" >
            @csrf
            @isset($mailorder)
                @method('put')
            @endisset
               <header>
                   <h2 style="font-family: 'Harlow Solid Italic'">ÖDEME BİLGİSİ</h2>
               </header>
               <div class="">
                   <p>Kart Numarası</p>
                   <span>
                    <input class="form-control {{ $errors->has('cardname') ? ' is-invalid' : '' }}" name="cardnu"  maxlength="19" value="@isset($mailorder){{$mailorder->cardnu}}@else {{old('cardnu')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57' placeholder="1234 5678 9123 4567"/>
                </span>
               </div>
               <div class="card-holder">
                   <p>Kart Sahibi</p>
                   <input class="form-control {{ $errors->has('cardname') ? ' is-invalid' : '' }}" value="@isset($mailorder){{$mailorder->cardname}}@else {{old('cardname')}} @endisset"  name="cardname" type="text" placeholder="Veos Network"/>
               </div>
               <div class="row-container">
                   <div class="">
                       <p>Ay</p>
                       <select class="form-control {{ $errors->has('cardmonth') ? ' is-invalid' : '' }}" value="@isset($mailorder){{$mailorder->cardmonth}}@else {{old('cardmonth')}} @endisset"  name="cardmonth">
                           <option></option>
                           <option value="01">01</option>
                           <option value="02">02</option>
                           <option value="03">03</option>
                           <option value="04">04</option>
                           <option value="05">05</option>
                           <option value="06">06</option>
                           <option value="07">07</option>
                           <option value="08">08</option>
                           <option value="09">09</option>
                           <option value="10">10</option>
                           <option value="11">11</option>
                           <option value="12">12</option>
                       </select>
                   </div>
                   <div class="">
                       <p class="">Yıl</p>
                       <select class="form-control {{ $errors->has('cardyear') ? ' is-invalid' : '' }}"value="@isset($mailorder){{$mailorder->cardyear}}@else {{old('cardyear')}} @endisset"  name="cardyear">
                           <option></option>
                           <option value="20">2020</option>
                           <option value="21">2021</option>
                           <option value="22">2022</option>
                           <option value="23">2023</option>
                           <option value="24">2024</option>
                           <option value="25">2025</option>
                           <option value="26">2026</option>
                           <option value="27">2027</option>
                           <option value="28">2028</option>
                           <option value="29">2029</option>
                           <option value="30">2030</option>
                       </select>
                   </div>
                   <div class="">
                       <p>Cvc</p>
                       <input width="25" class="form-control {{ $errors->has('cardcvc') ? ' is-invalid' : '' }}" value="@isset($mailorder){{$mailorder->cardcvc}}@else {{old('cardcvc')}} @endisset"  name="cardcvc" type="text" maxlength="3" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'/>
                   </div>
               </div>
                   <button class="mt-3" type="submit">Ödeme Yap</button>
        </form>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>

@section('css')
    <link rel="stylesheet" href="{{asset('css/mailorder.css')}}">
@endsection
