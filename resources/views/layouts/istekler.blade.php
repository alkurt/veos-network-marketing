@extends("layouts.main")
@section("content")
    <div class="" style="margin-top: 140px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')"">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="text-center" >
            <h4 class="mb-5" style="font-family: 'Harlow Solid Italic'">İSTEKLERİM</h4>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-5">
                            <tr>
                                <th>Kullanıcı Numarası</th>
                                <th>İsim</th>
                                <th>Soyisim</th>
                                <th>Konum</th>
                                <th>İletişim</th>
                                <th>E-mail</th>
                                <th>Konum Ver</th>
                            </tr>
                            @forelse($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->surname}}</td>
                                    <td>{{$user->konum}}</td>
                                    <td>{{$user->telefon}}</td>
                                    <td>{{$user->email}}</td>
                                    <!-- Button trigger modal -->
                                    <td style="font-size: larger"><a class="fa fa-user-plus"  data-toggle="modal" data-target="#exampleModalLong-{{$user->id}}" href="#"></a></td>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModalLong-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Kişiyi Ağacında Konumlandır.</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p> !! Ağacınızdaki ilk 8 kişiyi konumlandırmadan alt sıraya konumlandırma seçeneği görünmeyecektir!!</p>
                                                    <p> !! Konum Bilgilerini Görmek İçin Bilgilendirme Butonuna Tıklayınız.</p>
                                                    <p> !! Konumlandırma için gerekli konum bilgisini girerek konumlandır diyebilirsiniz. </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('iskonum',$user->id)}}">
                                                        @csrf
                                                        <select class="col-md-4 col-sm-12" id="ikid" type="text" name="iskonum" value="{{old('iskonum')}}">
                                                            @if($agacim < 8)
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                            @endif
                                                            @if(8 <= $agacim && $agacim < 16 )
                                                                <option value="1.1">1.1</option>
                                                                <option value="2.1">2.1</option>
                                                                <option value="3.1">3.1</option>
                                                                <option value="4.1">4.1</option>
                                                                <option value="5.1">5.1</option>
                                                                <option value="6.1">6.1</option>
                                                                <option value="7.1">7.1</option>
                                                                <option value="8.1">8.1</option>
                                                            @endif
                                                            @if(16 <= $agacim && $agacim <24 )
                                                                <option value="1.2">1.2</option>
                                                                <option value="2.2">2.2</option>
                                                                <option value="3.2">3.2</option>
                                                                <option value="4.2">4.2</option>
                                                                <option value="5.2">5.2</option>
                                                                <option value="6.2">6.2</option>
                                                                <option value="7.2">7.2</option>
                                                                <option value="8.2">8.2</option>
                                                            @endif
                                                            @if(24 <= $agacim && $agacim < 32 )
                                                                <option value="1.3">1.3</option>
                                                                <option value="2.3">2.3</option>
                                                                <option value="3.3">3.3</option>
                                                                <option value="4.3">4.3</option>
                                                                <option value="5.3">5.3</option>
                                                                <option value="6.3">6.3</option>
                                                                <option value="7.3">7.3</option>
                                                                <option value="8.3">8.3</option>
                                                            @endif
                                                            @if(32 <= $agacim && $agacim < 40 )
                                                                <option value="1.4">1.4</option>
                                                                <option value="2.4">2.4</option>
                                                                <option value="3.4">3.4</option>
                                                                <option value="4.4">4.4</option>
                                                                <option value="5.4">5.4</option>
                                                                <option value="6.4">6.4</option>
                                                                <option value="7.4">7.4</option>
                                                                <option value="8.4">8.4</option>
                                                            @endif
                                                            @if(40 <= $agacim && $agacim < 48 )
                                                                <option value="1.5">1.5</option>
                                                                <option value="2.5">2.5</option>
                                                                <option value="3.5">3.5</option>
                                                                <option value="4.5">4.5</option>
                                                                <option value="5.5">5.5</option>
                                                                <option value="6.5">6.5</option>
                                                                <option value="7.5">7.5</option>
                                                                <option value="8.5">8.5</option>
                                                            @endif
                                                            @if(48 <= $agacim && $agacim < 56 )
                                                                <option value="1.6">1.6</option>
                                                                <option value="2.6">2.6</option>
                                                                <option value="3.6">3.6</option>
                                                                <option value="4.6">4.6</option>
                                                                <option value="5.6">5.6</option>
                                                                <option value="6.6">6.6</option>
                                                                <option value="7.6">7.6</option>
                                                                <option value="8.6">8.6</option>
                                                            @endif
                                                            @if(56 <= $agacim && $agacim < 64 )
                                                                <option value="1.7">1.7</option>
                                                                <option value="2.7">2.7</option>
                                                                <option value="3.7">3.7</option>
                                                                <option value="4.7">4.7</option>
                                                                <option value="5.7">5.7</option>
                                                                <option value="6.7">6.7</option>
                                                                <option value="7.7">7.7</option>
                                                                <option value="8.7">8.7</option>
                                                            @endif
                                                                @if(64 <= $agacim && $agacim < 72 )
                                                                    <option value="1.8">1.8</option>
                                                                    <option value="2.8">2.8</option>
                                                                    <option value="3.8">3.8</option>
                                                                    <option value="4.8">4.8</option>
                                                                    <option value="5.8">5.8</option>
                                                                    <option value="6.8">6.8</option>
                                                                    <option value="7.8">7.8</option>
                                                                    <option value="8.8">8.8</option>
                                                                @endif
                                                                @if(72 <= $agacim && $agacim < 80 )
                                                                    <option value="1.9">1.9</option>
                                                                    <option value="2.9">2.9</option>
                                                                    <option value="3.9">3.9</option>
                                                                    <option value="4.9">4.9</option>
                                                                    <option value="5.9">5.9</option>
                                                                    <option value="6.9">6.9</option>
                                                                    <option value="7.9">7.9</option>
                                                                    <option value="8.9">8.9</option>
                                                                @endif

                                                        </select>
                                                        <button type="submit" class="btn btn-success" style="color: white">Konumlandır</button>
                                                    </form>
                                                    <a href="{{route('bilgilendirme')}}" class="btn btn-danger" >Bilgilendirme</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @empty
                                <marquee scrollamount="4" behavior="" direction="left"><p style="color: gray">Sponsor Numaranız ile Kayıt Olan Kullanıcı Bulunmamaktadır.</p></marquee>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("js")
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
@section("css")
@endsection
