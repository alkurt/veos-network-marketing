@extends('layouts.mail.mail-layout')
@section('message')
    <table class="es-content" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
            <td class="esd-stripe" align="center">
                <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center">
                    <tbody>
                    <tr>
                        <td class="esd-structure" align="left">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="600" valign="top" align="center">
                                        <table style="border-radius: 3px; border-collapse: separate; background-color: #fcfcfc;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#fcfcfc">
                                            <tbody>
                                            <tr>
                                                <td class="esd-block-text es-m-txt-l es-p30t es-p20r es-p20l" align="center">
                                                    <h2 style="color: #333333;">Sayın; {{$order->user->name .' '. $order->user->surname}}</h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="esd-block-text es-p10t es-p20r es-p20l" bgcolor="#fcfcfc" align="center">
                                                    <p> Siparişiniz Başarıyla Alındı...<br></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="esd-structure es-p30t es-p20r es-p20l" style="background-color: #fcfcfc;" esd-custom-block-id="15791" bgcolor="#fcfcfc" align="left">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                        <table style="border-color: #efefef; border-style: solid; border-width: 1px; border-radius: 3px; border-collapse: separate; background-color: #ffffff;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                            <tbody>
                                            <tr>
                                                <td class="esd-block-text es-p20t es-p15b" align="center">
                                                    <h3 style="color: #333333;">Sipariş Detayınız</h3>
                                                    <table class="table table-bordered table-responsive table-success">
                                                        <thead>
                                                            <th>Ürün Adı</th>
                                                            <th>Ürün Adedi</th>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($urunler as $urun)
                                                                <td>{{$urun->product->product_name}}</td> <br>
                                                                <td>{{$urun->quantity}}</td>
                                                                @endforeach
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td class="esd-block-button es-p20t es-p20b es-p10r es-p10l" align="center"><span class="es-button-border" style="background: #f8f3ef none repeat scroll 0% 0%;"><a href="veosnet.com/" class="es-button" target="_blank" style="background: #f8f3ef none repeat scroll 0% 0%; border-color: #f8f3ef;">Hemen İşe Koyul</a></span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection
