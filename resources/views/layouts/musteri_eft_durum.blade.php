@extends('layouts.main')
@section("content")
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Bilgilendirme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <p>Eft/Havale İle Ödeme Yaptığınız Bilgiler Bekleniyor Durumdayken, Eft/Havale Bilginizin Silinmesi İşleminizin Onaylanmadığını Belirtir.</p>
                    <p>Onaylanmadığı Durumlarda İletişim Alanından Bize Mesaj Gönderebilir veya İletişim Numaramızdan Bize Ulaşabilirsiniz... </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 160px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModalCenter" style="font-size: small">
            ?
        </button>
        <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">EFT/HAVALE DURUMU</h4>
        <div class="row">
           <div class="col-md-12">
               <div class="row">
                   @forelse($eft as $eft)
                   <div class="col-sm-12 col-md-6 mb-3">
                       <p>Sipariş Numarası : {{$eft->order_no}} </p>
                       <p>Sipariş Tutarı : {{$eft->order_price}} ₺</p>
                       <p>İşlem Tarihi : {{$eft->created_at->format('d-m-y H:i')}}</p>
                   </div>
                   <div class="col-sm-12 col-md-6">
                           @if($eft->durum==0)
                           <p class="text-danger">Durum : EFT Beklemede </p>
                               <img class="h-25 w-25" src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" alt="">
                                       @else
                                       <p class="text-success">Durum : EFT Onaylandı</p>
                                   <img class="w-50 h-50" src="https://iasbh.tmgrup.com.tr/dac10e/1200/627/0/27/800/445?u=https://isbh.tmgrup.com.tr/sbh/2019/07/30/eft-nedir-havale-nedir-eft-ve-havale-arasindaki-fark-nedir-1564496863469.jpg" alt="">
                       @endif
                   </div>
                       @empty
                       <div class="col-sm-12 col-md-6">
                           <marquee behavior="" direction="left"> <p>Mevcut Herhangi Bir Eft/Havale İşleminiz Bulunmamaktadır.</p></marquee>
                       </div>
                       @endforelse
               </div>
           </div>
        </div>
    </div>
@endsection
@section("customJs")
@endsection
@section("customCss")
@endsection
