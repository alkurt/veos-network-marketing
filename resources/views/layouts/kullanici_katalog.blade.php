@extends('layouts.main')
@section("content")
    <div class="container" style="margin-top: 170px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">E-KATALOG</h4>

    @forelse($kullanici_katalog as $katalog)
          <div class="row">
              <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-6 col-sm-12 mb-5">
                          <h6 class="mx-5" >{{$katalog->title}}</h6>
                      </div>
                      <div class="col-md-6 col-sm-12">
                          <td><a href="{{ route('katalog.download', $katalog->uuid) }}">{{ $katalog->uuid }} .pdf</a></td>
                      </div>
                  </div>
              </div>
          </div>
        @empty
            <marquee behavior="" scrollamp="3" direction="left"> <p class="text-secondary">Henüz Yüklenmiş Bir Katalog Bulunmamaktadır.</p></marquee>
    @endforelse
    </div>
@endsection

@section("customJs")
@endsection
@section("customCss")
@endsection
