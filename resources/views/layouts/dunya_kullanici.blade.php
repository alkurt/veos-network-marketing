@extends('layouts.main')
@section("content")
    <div class="" style="margin-top: 135px">
        <a class="mx-2" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-2" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a><h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'"> DÜNYA CİRO DAĞILIMI</h4>
        @if($users >= 12 && $users < 16 )
            <marquee behavior="" direction="left"><p>Çok Yakında Bilgileri Sizinle Paylaşacağız. </p></marquee>
            <marquee behavior="" direction="left"><p class="text-info text-center">Anlayışınız için Teşekkür Eder <strong>{{config('app.name')}} </strong> ile Sağlıklı Günler Dileriz. </p></marquee>
        @else
            <marquee behavior="" direction=""><p>Kariyerinizin Bu Bilgileri Görmeye Yetkisi Bulunmamaktadır..</p></marquee>
            <marquee behavior="" direction="left"><p class="text-info text-center">Anlayışınız için Teşekkür Eder <strong>{{config('app.name')}} </strong> ile Sağlıklı Günler Dileriz. </p></marquee>
        @endif
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
