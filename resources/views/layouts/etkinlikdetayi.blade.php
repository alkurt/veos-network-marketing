@extends('layouts.main')
@section("content")
    <div class="table-responsive" style="margin-top: 140px;background-color: ghostwhite">
        <a class="mx-3" style="color: black " href="{{route('home')}}">{{config('app.name')}}</a>><a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()"><strong>{{$etkinlik->title}}</strong></a>
        <div class="row">
            <div class="col-md-12">
                <div class="row mt-5">
                    <div class="col-md-3">
                        <img class="" style="height: 100px;width: 100px;margin-left: 100px;border-radius: 100px;width:100px;height:100px;}" src="{{$etkinlik->url}}" alt="">
                    </div>
                    <div class="col-md-8">
                        <strong class="text-secondary">Mekan</strong>
                        <p class="text-dark"> {{$etkinlik->mekan}}</p>
                        <strong class="text-secondary">Etkinlik İçeriği </strong>
                        <p class="" > {!! $etkinlik->etkinlikler_icerik !!}</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
