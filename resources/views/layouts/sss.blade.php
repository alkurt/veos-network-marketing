@extends('layouts.main')
@section("content")
    <div class="container-fluid" style="margin-top: 140px;background-color: ghostwhite">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h4 class="text-center mb-5" STYLE="font-family: 'Harlow Solid Italic'">SIK SORULAN SORULAR</h4>
        <div class="row">
            @forelse($soru as $s)
                <div class="col-md-12" >

                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed text-dark" type="button" data-toggle="collapse" data-target="#{{$s->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                    {{$s->soru_basligi}}
                                </button>
                            </h2>
                        </div>
                        <div id="{{$s->id}}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                {!! $s->soru_cevabi !!}
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <marquee scrollamount="3" behavior="" direction="down"><p class="text-center text-danger">Herhangi bir
                        S.S.S bulunmamaktadır. </p></marquee>
            @endforelse
        </div>
    </div>
@endsection

@section("js")

@endsection

@section("css")
@endsection
