@extends('layouts.main')
@section("content")
    <div class=" " style="padding: 30px;margin-top: 110px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h3 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">KULLANICILAR EDİT</h3><p class="text-center">K.Numarası: <strong> {{$tip->id}}</strong> / İsim Soyisim : <strong>{{$tip->name}} {{$tip->surname}}</strong>  @if($tip->sponsor_id==null)/ Sponsor Numarası: <strong>Şirket (Sponsoru Yok)</strong> @endif</p>
        <br>
        <br>
            <div class="col-md-12">
               <div class="row">
                   <div class="form-group col-md-4">
                       <h4 class="mb-3 text-center" style="font-family: 'Harlow Solid Italic'"> Bayi / Alışveriş Müşteri</h4>
                       {!!Form::model($tip, ['route' => ['admin-users.update', $tip->id], "method" =>  "put","files" => true])!!}
                       {!! Form::select('kariyer_id', ['17' => 'Alışveriş Müşteri', '16' => 'Standart'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20','weight'=>'25']); !!}

                       <div class="mt-5 mx-5">
                           {!! Form::bsSubmit("Güncelle") !!}
                           {!! Form::close() !!}
                       </div>
                   </div>
                   @if($tip->kariyer_id == 17)
                      <div class="col-md-4">
                          <h4 class="mb-2 text-center" style="font-family: 'Harlow Solid Italic'"> Bayi Kariyer Atlatma</h4>
                          <p class="text-danger">Kullanıcı Alışveriş Müşterisi !! <strong class="text-success">Manuel Kariyer Atlatmak İçin Bayi Yapmalısınız...</strong></p>
                      </div>
                       @else
                       <div class="form-group col-md-4">
                           <h4 class="mb-3 text-center" style="font-family: 'Harlow Solid Italic'"> Bayi Kariyer Atlatma</h4>
                           {!!Form::model($tip, ['route' => ['admin-users.update', $tip->id], "method" =>  "put","files" => true])!!}
                           {!! Form::select('kariyer_id', ['1'=>'Müşteri','2'=>'Kullanıcı','3'=>'Başlangıç','4'=>'Girişimci Adayı','5'=>'Girişimci','6' => 'Danışman', '7' => 'Temsilci','8'=>'Yarı Asistan','9'=>'Asistan','10'=>'Öncü Lider','11'=>'Lider','12'=>'Müdür Yardımcısı','13'=>'Müdür','14'=>'Genel Müdür Yardımcısı','15'=>'Genel Müdür'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20','weight'=>'25']); !!}

                           <div class="mt-5 mx-5">
                               {!! Form::bsSubmit("Güncelle") !!}
                               {!! Form::close() !!}
                           </div>
                       </div>
                       @endif
                   <div class="form-group col-md-4 mt-3">
                       {!!Form::model($tip, ['route' => ['admin-users.update', $tip->id], "method" =>  "put","files" => true])!!}
                       {!! Form::bsText("sponsor_id","Sponsor Numarasını Değiştir",null,['maxlength'=>'5','type'=>'number']) !!}

                       <div class="mt-5 mx-5">
                           {!! Form::bsSubmit("Güncelle") !!}
                           {!! Form::close() !!}
                       </div>
                   </div>
               </div>
            </div>

    </div>
@endsection

@section("js")
@endsection
@section("css")
@endsection
