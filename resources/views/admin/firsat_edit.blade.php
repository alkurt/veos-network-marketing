@extends('layouts.main')

@section('content')
    <div class="container-fluid" style="margin-top: 150px">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
        <!-- Modal -->
        <a class="" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="col-md-12 mt-3">
            <div class="form-group">
                <form action="{{ route('firsat.update',$firsat->id) }}" method="POST" >
                    @csrf
                    @method('PUT')
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Hakkımızda</small>
                                    <textarea name="hakkimizda"   id="" cols="90" rows="12">{{$firsat->hakkimizda}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Başarı Hikayesi</small>
                                    <textarea name="b_hikayesi" id="" cols="90" rows="12">{{$firsat->b_hikayesi}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Profesyenel Pazarlama</small>
                                    <textarea name="p_pazarlama" id="" cols="90" rows="12">{{$firsat->p_pazarlama}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>İş Modeli</small>
                                    <textarea name="ismodeli" cols="90" rows="12">{{$firsat->ismodeli}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Yeni Ürünler</small>
                                    <textarea name="y_urunler"  cols="90" rows="12">{{$firsat->y_urunler}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Neden Bizim Network</small>
                                    <textarea name="nedennetwork"  cols="90" rows="12">{{$firsat->nedennetwork}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Ödeme Planı</small>
                                    <textarea name="odeme_plani"  cols="90" rows="12">{{$firsat->odeme_plani}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>Misyon ve Vizyon</small>
                                    <textarea name="misyon"  cols="90" rows="12">{{$firsat->misyon}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Kaydet " class="btn btn-info w-50 mt-5 mx-5">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')

@endsection
