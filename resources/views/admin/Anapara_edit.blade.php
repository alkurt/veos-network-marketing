@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h3 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">Ana Pay Cirosu Güncelleme Ekranı</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!!Form::model($anapay, ['route' => ['anapara.update', $anapay->id], "method" =>  "put","files" => true])!!}
                    {!! Form::bsText("city","İl Ciro %") !!}
                    {!! Form::bsText("region","Bölge Ciro %") !!}
                    {!! Form::bsText("turkey","Türkiye Ciro %") !!}
                    {!! Form::bsText("world","Dünya Ciro %") !!}
                    {!! Form::bsSubmit("Güncelle") !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section("js")
@endsection
@section("css")
@endsection
