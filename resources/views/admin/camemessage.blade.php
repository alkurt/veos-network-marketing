@extends('layouts.main')
@section('content')

    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>  <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">GELEN MESAJLAR</h4>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>İsim</th>
                            <th>Soyisim </th>
                            <th>Mail Adresi</th>
                            <th>Mesajı</th>
                            <th>Kayıt Tarihi</th>
                            <th>Okundu</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($message as $b)
                            <tr>
                                <td>{{$b->name}}</td>
                                <td>{{$b->surname}}</td>
                                <td>{{$b->email}}</td>
                                <td>{!! $b->message !!}</td>
                                <td>{{$b->created_at}}</td>
                                <td> <a href="{{route('camemessagesil',$b->id)}}" class="btn btn-success" data-method="delete"
                                        data-confirm="Mesajını Okuduğunuzdan Emin Misiniz?"><i class="fa fa-check"></i></a></td>
                            </tr>
                            @empty
                                <p>Gelen Bir Mesaj Bulunmamaktadır.</p>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
