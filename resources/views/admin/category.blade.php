@extends('layouts.main')

@section('content')

    <div class="container product_section_container" style="padding: 30px;margin-top: 140px">
        <a class="mx-3 " style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a><h4 class="mb-3 text-center" style="font-family: 'Harlow Solid Italic'">KATEGORİ EKLE/DÜZENLE</h4>
        <div class="col-md-12">
                <a href="/admin-category/create" class="btn btn-danger mt-3 mb-3">
                    <i class="fa fa-plus"></i>
                    Yeni Kategori Ekle
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kategori İsmi</th>
                        <th>Kategori Url</th>
                        <th>Oluşturma Tarihi</th>
                        <th>Düzenle</th>
                        <th>Silme</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->category_name }}</td>
                            <td>{{ $category->slug }}</td>
                            <td>{{ $category->created_at }}</td>
                            <td>
                                <a href="/admin-category/{{$category->id}}/edit" class="btn btn-primary"><i
                                            class="fa fa-edit"></i>Düzenle</a>
                            </td>
                            <td>
                                <a href="/admin-category/{{$category->id}}" class="btn btn-danger" data-method="delete"
                                   data-confirm="Emin misin?"><i class="fa fa-trash"></i> Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {!! $categories->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection

