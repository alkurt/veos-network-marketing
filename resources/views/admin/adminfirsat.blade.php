@extends('layouts.main')
@section('content')

    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row" style="margin-bottom: 30px;">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            <div class="col-md-12">
                <h4 class="text-center " style="font-family: 'Harlow Solid Italic'">Başlıkların Sayfa İçerikleri  </h4>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Başlıktaki Sayfaların İçeriklerini Buradan Kontrol Edebilirsiniz!!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if($firsatsayi == 0)
                    <a href="{{route('firsat.create')}}" class="btn btn-danger mb-5">
                        <i class="fa fa-plus"></i>
                        İçerikleri Ekle
                    </a>
                @else
                @endif
                <button type="button" class="btn btn-outline-warning mx-3 mb-5" data-toggle="modal" data-target="#exampleModal">
                    Bilgilendirme
                </button>
                <div class="col-md-9">
                    @if($firsatsayi >  0)
                        @foreach($firsat as $f)
                            <a href="{{route('firsat.edit',$f->id)}}" class="btn btn-info w-25"  data-method="post"
                            ><i class="fa fa-edit"> Düzenle / İçerik Ekle</i></a>
                        @endforeach
                    @endif
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Hakkımızda</th>
                            <th>Başarı Hikayemiz</th>
                            <th>Profesyonel Pazarlama</th>
                            <th>İş Modeli</th>
                            <th>Yenilikçi Ürünler</th>
                            <th>Neden Veos Network</th>
                            <th>Ödeme Planı</th>
                            <th>Misyon ve Vizyon</th>
                            <th>Kayıt Tarihi</th>
                            <th>Düzenleme Tarihi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($firsat as $f)
                            <tr>
                                <td>{!! $f->hakkimizda !!}</td>
                                <td>{!! $f->b_hikayesi !!}</td>
                                <td>{!! $f->p_pazarlama !!}</td>
                                <td>{!! $f->ismodeli !!}</td>
                                <td>{!! $f->y_urunler !!}</td>
                                <td>{!! $f->nedennetwork !!}</td>
                                <td>{!! $f->odeme_plani !!}</td>
                                <td>{!! $f->misyon !!}</td>
                                <td>{{$f->created_at}}</td>
                                <td>{{$f->updated_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
    <script type='text/javascript'>
        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
@endsection
@section('css')
@endsection
