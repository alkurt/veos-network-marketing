@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 170px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="row">
            <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">Ödeme Talebi Durum Ekranı</h3>
            <div class="col-md-12">
                <div class="form-group">
                    {!!Form::model($odeme_talebi, ['route' => ['adminodeme.store', $odeme_talebi->id], "method" =>  "put","files" => true])!!}
                    {!! Form::select('status', ['onaylandı' => 'Onaylandı', 'beklemede' => 'Beklemede','onaylanmadı'=>'Onaylanmadı'], null, ['placeholder' => 'Durum Seçiniz']); !!}
                    {!! Form::bsSubmit("Güncelle") !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection
@section("css")
@endsection
