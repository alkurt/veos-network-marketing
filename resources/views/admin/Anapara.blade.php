@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>  <h4 class="text-center" style="font-family: 'Harlow Solid Italic'">ANA PAY CİROSU DÜZENLE </h4>
        <div class="row" style="margin-bottom: 30px;">

        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>İl Payı %</th>
                            <th>Bölge Payı %</th>
                            <th>Türkiye Payı %</th>
                            <th>Dünya Payı %</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Düzenle</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($anapay as $b)
                            <tr>
                                <td>{{$b->city}}</td>
                                <td>{{$b->region}}</td>
                                <td>{{$b->turkey}}</td>
                                <td>{{$b->world}}</td>
                                <td>{{$b->created_at}}</td>
                                <td class="text-center">
                                    <a href="{{route('anapara.edit',$b->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
