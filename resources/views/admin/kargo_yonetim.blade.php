@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>  <h4 class="text-center" style="font-family: 'Harlow Solid Italic'">KARGO YÖNETİM EDİT </h4>
        <div class="row" style="margin-bottom: 30px;">

        </div>
        <div class="row">
            <a href="{{route('kargoyonetim.create')}}" class="btn btn-danger mb-5 mx-3">
                <i class="fa fa-plus"></i>
                Yeni Kargo Ekle
            </a>
            <div class="col-md-12">

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Firma Adı</th>
                            <th>Sabit Fiyat</th>
                            <th>Kargo Durumu</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Düzenle</th>
                            <th>Silme</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kargoYonetim as $b)
                            <tr>
                                <td>{{$b->kargo_adi}}</td>
                                <td>{{$b->kargo_fiyati}}</td>
                                <td>{{$b->kargo_durumu}}</td>
                                <td>{{$b->created_at}}</td>
                                <td class="text-center">
                                    <a href="{{route('kargoyonetim.edit',$b->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                </td>
                                <td> <a href="{{route('ksil',$b->id)}}" class="btn btn-danger" data-method="delete"
                                        data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
