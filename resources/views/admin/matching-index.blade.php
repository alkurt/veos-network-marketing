@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a><h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">MATCHİNG EDİT</h4>
        <div class="row" style="margin-bottom: 30px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyer</th>
                            <th>Derinlik</th>
                            <th>Yüzde Oranı %</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Edit</th>
                            {{--                            <th class="text-center">Silme</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($matchings as  $matching)
                                <tr>
                                    <td>{{$matching->kariyer->kariyername}}</td>
                                    <td>{{$matching->derinlik}}</td>
                                    <td>{{$matching->yuzde}}</td>
                                    <td>{{$matching->created_at}}</td>
                                    <td class="text-center">
                                        <a href="{{route('matching.edit',$matching->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <div class="text-center">Henüz bir matching primi kazanamadınız.</div>
                        @endforelse
                        </tbody>
                    </table>
                    {{ $matchings->links() }}
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
