@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">LİDER ÇIKARMA EDİT</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyer İsmi</th>
                            <th>Tutar</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kariyer as $b)
                            <tr>
                                <td>{{$b->kariyername}}</td>
                                <td>{{$b->lidercikarma->tutar}}</td>
                                <td>{{$b->lidercikarma->created_at}}</td>
                                <td class="text-center">
                                    <a href="{{route('lidercikarma.edit',$b->lidercikarma->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
