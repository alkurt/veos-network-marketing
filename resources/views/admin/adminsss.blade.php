@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row" style="margin-bottom: 30px;">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        </div>
        <h4 class="text-center" style="font-family: 'Harlow Solid Italic'">S.S.S YÖNETİMİ</h4>
        <div class="row">
            <div class="col-md-12">
                <a href="{{route('sss.create')}}" class="btn btn-danger mb-5 mx-3">
                    <i class="fa fa-plus"></i>
                    S.S.S Ekle
                </a>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>S.S.S Başlığı</th>
                            <th>S.S.S Cevabı</th>
                            <th>S.S.S Sıra Numarası</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Edit</th>
                            <th class="text-center">Silme</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($soru as $s)
                            <tr>
                                <td>{{$s->soru_basligi}}</td>
                                <td>{!! $s->soru_cevabi !!}</td>
                                <td>{{$s->sira_no}}</td>
                                <td>{{$s->created_at}}</td>
                                <td class="text-center">
                                    <a href="{{route('sss.edit',$s->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <a href="{{route('sssil',$s->id)}}" class="btn btn-danger float-right" data-method="delete"
                                       data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
