@extends('layouts.main')
@section('title','Ödeme Yapma || VeosNet Network&Marketing')
@section("content")
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="alert alert-info">Talep edilen tutar kullanıcının toplam bakiyesinden fazla ise sistem otomatik olarak kullanıcının toplam bakiyesi üzerinden işlem yapmaktadır.</p>
                    <p class="alert alert-warning">Kullanıcının talep ettiği tutarın %20 'si kasaya aktarılır. Talep edilen tutarın %80' i kullanıcıya gönderilir.</p>
                    <p class="alert alert-warning">Kullanıcıdan kesilen ücretler Kontrol Menü deki Kesinti Kazancı sayfasından görüntülenebilir.</p>
                    <p class=" alert alert-warning">Toplam Bakiye Kullanıcının cüzdanındaki toplam bakiyedir. </p>
                    <p class="alert alert-warning">Ödenen bakiye sizin kullanıcıya şimdiye kadar yaptığınız ödemedir.</p>
                    <p class="alert alert-warning">Bu İşlem Sanal Bir işlemdir kullanıcıya eft yaptıktan sonra cüzdanından oranın düşürülmesiyle gerçekleşir.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>

                </div>
            </div>
        </div>
    </div>
    <div class="container " style="padding: 30px;margin-top: 130px">
        <div class="row">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            <div class="col-md-12">
                <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">ÖDEME YAPMA EKRANI</h4>
               <div class="row">
                   <div class="col-md-6 col-sm-6 form-group">
                       {!!Form::model($talepler, ['route' => ['adminodeme.update', $talepler->id], "method" =>  "put","files" => true])!!}
                       {!! Form::bsText("user_id","Kullanıcı Numarası",null,["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("name","İsim",$talepler->user->name, ["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("surname","Soyisim",$talepler->user->surname, ["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("iban","İban Numarası",$talepler->user->detail->phone, ["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("toplam_bakiye","Toplam Bakiyesi",$cuzdan->toplam_bakiye,["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("odeme_talebi","Ödeme Talebi",$talepler->odeme_talebi,["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("kesinti","",$talepler->odeme_talebi > $cuzdan->toplam_bakiye ? $cuzdan->toplam_bakiye *20/100 : $talepler->odeme_talebi*20/100,["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("kesintili_odenecek"," Ödenecek Tutar",($talepler->odeme_talebi > $cuzdan->toplam_bakiye ? $cuzdan->toplam_bakiye *80/100 : $talepler->odeme_talebi*80/100),["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                       {!! Form::bsText("odenen_bakiye","Ödeme Yapılan Bakiyesi",$cuzdan->odenen_bakiye,["class" => "form-control",'rows'=>'2','cols'=>'5','disabled'=>'disabled']) !!}
                   </div>
                   <div class="col-md-6 col-sm-6">
                       <div class="form-group mx-5"> <!-- Button trigger modal -->
                           <br>
                           <button type="button" class="btn btn-outline-warning mb-5 mx-5" data-toggle="modal" data-target="#exampleModal">
                               Bilgilendirme
                           </button>
                          <div class="mx-5 table-responsive w-75">
                              <input class="form-control" type="number" name ="odenecek_bakiye" value="@if($talepler->odeme_talebi > $cuzdan->toplam_bakiye){{$cuzdan->toplam_bakiye *80/100}}@else{{($talepler->odeme_talebi*80)/100}}@endif" max="{{($talepler->odeme_talebi*80)/100}}">
                              <input class="form-control" type="hidden" name ="kesinti" value="{{$talepler->odeme_talebi > $cuzdan->toplam_bakiye ? $cuzdan->toplam_bakiye *20/100 : $talepler->odeme_talebi*20/100}}" >
                          </div>
                           <div class="mt-3 table-responsive">
                               {!! Form::bsSubmit("Ödeme Yap") !!}
                               {!! Form::close() !!}
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
@endsection
@section("js")
@endsection
@section("css")
@endsection
