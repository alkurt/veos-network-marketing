@extends('layouts.main')
@section('title','Admin - Kariyer Düzenleme Ekranı ||VeosNet Network&Marketing')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 140px">
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-4" style="font-family: 'Harlow Solid Italic'">KARİYER YÖNETİMİ </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyeri</th>
                            <th>Paket Alt Limiti</th>
                            <th>Sponsor %</th>
                            <th>Alışveriş</th>
                            <th>Binary</th>
                            <th>İl Ciro</th>
                            <th>TR Ciro</th>
                            <th>Dünya Ciro</th>
                            <th>Araba Ciro</th>
                            <th>Bölge Ciro</th>
                            <th>Bayi Tazminatı</th>
                            <th>Lider Primi</th>
                            <th>Kayıt Tarihi</th>
                            <th >Düzenle</th>
                            <th >Silme</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kariyers as $k)
                            <tr>
                                <td>{{ $k->kariyername }}</td>
                                <td>@if( $k->Kariyerb->kariyer_id <= 5 )

                                        {{number_format($k->Kariyerb->tutar)}} ₺
                                    @elseif( $k->Kariyerb->kariyer_id > 5)
                                        {{$k->Kariyerb->tutar}} PV
                                    @endif
                                </td>
                                <td>{{$k->Kariyerb->sponsor}}</td>
                                <td>{{ $k->Kariyerb->indirim}}</td>
                                <td>{{ $k->Kariyerb->binary }}</td>
                                <td>{{ $k->Kariyerb->il_ciro }}</td>
                                <td>{{ $k->Kariyerb->tr_ciro }}</td>
                                <td>{{ $k->Kariyerb->dunya_ciro }}</td>
                                <td>{{ $k->Kariyerb->araba_ciro }}</td>
                                <td>{{ $k->Kariyerb->bolge_ciro }}</td>
                                <td>{{ $k->Kariyerb->tazminati }} ₺</td>
                                <td>{{ $k->Kariyerb->lider_primi }} ₺</td>
                                <td>{{ $k->created_at }}</td>
                                <td class="text-center">
                                    <a href="{{route('kariyer.edit',$k->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <a href="{{route('kariyer.destroy',$k->id)}}" class="btn btn-danger float-right" data-method="delete"
                                       data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection

