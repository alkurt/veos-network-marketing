@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 170px">
        <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">Kariyer Bilgisi Güncelleme Ekranı</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!!Form::model($kariyer->kariyerb, ['route' => ['kariyer.update', $kariyer->kariyerb->id], "method" =>  "put","files" => true])!!}
                    {!! Form::bsText("kariyername","",$kariyer->kariyername) !!}
                    {!! Form::bsText("tutar","Paket Alt Limiti") !!}
                    {!! Form::bsText("sponsor","Sponsor Oranı") !!}
                    {!! Form::bsText("indirim","İndirim Oranı") !!}
                    {!! Form::bsText("binary","Binary Oranı") !!}
                    {!! Form::bsText("il_ciro","İL Ciro") !!}
                    {!! Form::bsText("tr_ciro","Türkiye Ciro") !!}
                    {!! Form::bsText("dunya_ciro","Dünya Ciro") !!}
                    {!! Form::bsText("araba_ciro","") !!}
                    {!! Form::bsText("bolge_ciro","Bölge Ciro") !!}
                    {!! Form::bsText("tazminati","Bayi Tazminatı") !!}
                    {!! Form::bsText("lider_primi","Lider Primi") !!}
                    {!! Form::bsSubmit("Güncelle") !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection
@section("css")
@endsection
