@extends('layouts.main')
@section('content')

    <div class="container-fluid" style="padding: 30px;margin-top: 110px">
        <div class="row" style="margin-bottom: 30px;">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            <div class="col-md-12">
                <h4 class="text-center " style="font-family: 'Harlow Solid Italic'"> Alt İçerikler Yönetimi  </h4>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Anasayfa, kategori ve ürün detayları alt kısmında yer alan 4 bilgilendirme alanını kapsar!!</p>

                        <img style="height: 200px;width: 200px" src="{{asset('assets/new images/alt.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if($firsatsayi == 0)
                    <a href="{{route('alt-alan.create')}}" class="btn btn-outline-info mb-5">
                        <i class="fa fa-plus"></i>
                        Alan İçerikleri Ekle
                    </a>
                @else
                @endif
                <button type="button" class="btn btn-outline-warning mx-3 mb-5" data-toggle="modal" data-target="#exampleModal">
                    Bilgilendirme
                </button>
                <div class="col-md-9">
                    @if($firsatsayi >  0)
                        @foreach($firsat as $f)
                            <a href="{{route('firsat.edit',$f->id)}}" class="btn btn-info w-25"  data-method="post"
                            ><i class="fa fa-edit"> Düzenle / İçerik Ekle</i></a>
                        @endforeach
                    @endif
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>1.Alan</th>
                            <th>2.Alan </th>
                            <th> 3.Alan</th>
                            <th>4.Alan</th>

                            <th>Kayıt Tarihi</th>
                            <th>Düzenleme Tarihi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($firsat as $f)
                            <tr>
                                <td>{!! $f->bir !!}</td>
                                <td>{!! $f->iki !!}</td>
                                <td>{!! $f->uc !!}</td>
                                <td>{!! $f->dort !!}</td>
                                <td>{{$f->created_at}}</td>
                                <td>{{$f->updated_at}}</td>
                                <td> </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    <br>
                    <br>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
    <script type='text/javascript'>
        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
@endsection
@section('css')
@endsection
