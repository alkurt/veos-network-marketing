@extends('layouts.main')
@section('content')
    <div class="container" style="margin-top: 170px">
        <div class="row justify-content-center">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            <div class="col-md-8">
                <div class="card mt-5 mb-5">
                    <div class="card-header"><strong>Logo Güncelle || Durum: @if($logo->status==1) Aktif @else Pasif @endif</strong></div>
                    <div class="card-body">

                        <form class="form-check" action="{{ route('logo.update',$logo->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            Logo İsmi
                            <br>
                            <input type="text"  name="name" value="{{$logo->name}}" class="form-control" required>
                            <br>
                            Logo Fotoğraf
                            <br>
                            <input type="file" value="" class="form-control-file" name="cover">
                            <br><br>
                            <p> Aktif <input name="status" value="1" type="radio"></p>
                            <p>Pasif  <input name="status" value="0" type="radio"></p>
                            <button class="btn btn-primary" type="submit"> Kaydet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
