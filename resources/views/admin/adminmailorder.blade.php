@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <a class="mx-3 mb-5" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3 mb-5" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">MAİL ORDER TAHSİLAT YAPMA EKRANI</h4>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-success">Onayla Butonuna Tıklamanız Ödemeyi Aldım, Onayla ve Siparişini Tamamla Demektir.</p>
                                <p class="text-danger">Onaylamamanız Durumunda İşlem Gerçekleşmeyecektir ve Kayıt Silinecektir.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th style="width: 20%">Kullanıcı Adı Soyadı</th>
                            <th style="width: 10%">Kullanıcı Kariyer</th>
                            <th style="width: 5%">Sipariş Numarası </th>
                            <th class="bg-info" >Sipariş Tutarı</th>
                            <th style="width: 30%">Adres</th>
                            <th style="width: 8%">Telefon</th>
                            <th style="width: 8%">Kart Sahibi</th>
                            <th style="width: 8%">Kart Numarası</th>
                            <th style="width: 8%">S.K.T Ay</th>
                            <th style="width: 8%">S.K.T Yıl</th>
                            <th style="width: 8%">Kart Cvc</th>
                            <th style="width: 10%">İşlem Tarihi</th>
                            <th class="text-center" style="width: 15%">Onayla</th>
                            <th>Onaylama</th>
                            <th> <button type="button" class="btn btn-outline-warning mx-2" style="font-size: x-small;width: 5%" data-toggle="modal" data-target="#exampleModal">
                                    ?
                                </button></th>
                            {{--                            <th class="text-center">Silme</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($morder as $beklenen)
                            <tr>
                                <td>{{$beklenen->user->name ." ".$beklenen->user->surname}}</td>
                                <td>{{$beklenen->user->kariyer->kariyername}}</td>
                                <td>{{$beklenen->order_no}} </td>
                                <td class="bg-warning">{{$beklenen->order_price}} ₺</td>
                                <td>{{$beklenen->address}}</td>
                                <td>{{$beklenen->m_phone}}</td>
                                <td>{{$beklenen->cardname}}</td>
                                <td>{{$beklenen->cardnu}}</td>
                                <td>{{$beklenen->cardmonth}}</td>
                                <td>{{$beklenen->cardyear}}</td>
                                <td>{{$beklenen->cardcvc}}</td>
                                <td>{{($beklenen->created_at)->format('d-m-y H:i')}}</td>
                                <td class="text-center">
                                    <a href="{{route('adminmail.edit',$beklenen->id)}}" class="btn btn-success " onclick="return confirm('Ödemenin yapıldığına emin misiniz?');"><i class="fa fa-check"></i></a>
                                </td>
                                <td> <a href="{{route('adminmail.destroy',$beklenen->id)}}" class="btn btn-danger" data-method="delete"
                                        data-confirm="Onaylamak İstemediğinizden Emin misiniz?"><i class="fa fa-user-times"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $morder->links() }}
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
