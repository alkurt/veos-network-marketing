@extends('layouts.main')
@section('content')
    <div class=" mb-5" style="margin-top: 170px; margin-bottom: 10px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')"">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8 mb-5 col-sm-12 text-center">
                <form action="{{route('user.excel.import.store')}}" class="form-control mt-5" method="post" enctype="multipart/form-data">
                    <div class="alert-box alert alert-info text-center">
                        <span style="font-size: 18px;font-weight: bold"> Excel Kullanıcı Ekleme Formu </span>
                    </div>
                    @csrf
                    <div class="form-group-sm">
                        <input type="file" class="form-control-file form" name="users" required>
                    </div>
                    <button type="submit" class="btn btn-outline-success w-25 mt-2">Gönder</button>
                </form>
            </div>
        </div>
    </div>
@endsection
