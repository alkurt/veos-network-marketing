@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 170px">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>URL alanına int istenilen resmin "Resim adresini kopyala diyerek url'ini url alanına ekleyerek yükleyebilirsiniz.  </p>
                                <img class="w-50 h-50" src="{{asset('assets/new images/sda.png')}}" alt="">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"><strong>Yeni Haber Ekle</strong><button type="button" class="btn btn-outline-warning mx-4" data-toggle="modal" data-target="#exampleModal">
                            Bilgilendirme
                        </button></div>
                    <div class="card-body">
                <div class="form-group">
                        <form action="{{ route('haberler.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Fotoğraf Url</label>
                                <input type="text" value="{{old('url')}}" name="url" class="form-control w-50">
                            </div>
                            <div class="form-group">
                                <label>Haber Başlığı</label>
                                <input type="text" value="{{old('haber_basligi')}}" name="haber_basligi" class="form-control w-50">
                            </div>
                            <label for="">Haber İçeriği</label>
                            <div class="form-group">
                                <textarea value="{{old('haber_icerik')}}" name="haber_icerik" id="" cols="80" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value=" Kaydet " class="btn btn-primary">
                            </div>

                        </form>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type='text/javascript'>
        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
    @endsection
