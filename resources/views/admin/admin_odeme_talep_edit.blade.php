@extends('layouts.main')
@section('title,VeosNet Network&Marketing')
@section('content')

    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ödeme Talep Bilgisi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Burada Gelen Talep Tutarı Kullanıcının Cüzdan Tutarının %20 Kesintili Halinden Size Talepte Bulunmuştur.</p>
                            <p>Ör:Toplam Bakiyesi 100 ₺ ise size 80 ₺ den fazla talepte bulunamaz.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
                <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">BAYİDEN GELEN ÖDEME TALEPLERİ</h4><button  type="button" class=" btn btn-outline-warning mb-2" data-toggle="modal" data-target="#exampleModal">
                    Bilgilendirme
                </button>
                <div class="table-responsive">
                    <table class="table table-hover"><!-- Button trigger modal -->
                        <thead>
                        <tr>
                            <th>Talep Nu</th>
                            <th>Kullancı Numarası</th>
                            <th>İsim Soyisim</th>
                            <th>İletişim</th>
                            <th>İban Numarası</th>
                            <th>Talep Tutarı</th>
                            <th>Kayıt Tarihi</th>
                            <th>Ödeme Yap</th>
                            <th>Kaydı Sil</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($odemetalebi as $b)
                            <tr>
                                <td>{{$b->id}}</td>
                                <td>{{$b->user_id}}</td>
                                <td>{{$b->user->name}} {{$b->user->surname}}</td>
                                <td>{{$b->user->telefon}} {{$b->user->email}}</td>
                                <td>{{$b->user->detail->phone}}</td>
                                <td>{{$b->odeme_talebi}} ₺</td>
                                <td>{{$b->created_at}}</td>
                                <td class="justify-content-center">
                                    <a class="mx-4 fa fa-check btn btn-success" href="{{route('adminodeme.edit',$b->id)}}"></a>
                                </td>
                                <td> <a href="{{route('odemetalebisil',$b->id)}}" class="btn btn-danger " data-method="delete"
                                        data-confirm="Ödeme talebi silinecektir ve ödeme onaylanmayacaktır"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            @empty
                            <p>Gelen Bir Ödeme Talebi Bulunmamaktadır.</p>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
