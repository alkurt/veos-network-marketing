@extends('layouts.main')
@section("content")
    <div class="container"style="margin-top: 130px" >
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="{{asset('css/menu.css')}}">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
                            <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'" >TÜM BAYİ EŞLEŞMELERİ</h4>
                            <table class="table table-bordered table-hover" id="binary-table">
                                <thead>
                                <tr>
                                    <th>Eşleşen ID</th>
                                    <th>Ad Soyad</th>
                                    <th>Devreden CV</th>
                                    <th>Eşleşilen CV</th>
                                    <th>Eşleşen ID</th>
                                    <th>Ad Soyad</th>
                                    <th>Devreden CV</th>
                                    <th class="text-center">Kayıt Tarih</th>
                                </tr>
                                </thead>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("css")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@endsection

@section("js")
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#binary-table').DataTable({
                language:{
                    "sDecimal":        ",",
                    "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
                    "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "sInfoEmpty":      "Kayıt yok",
                    "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
                    "sLoadingRecords": "Yükleniyor...",
                    "sProcessing":     "İşleniyor...",
                    "sSearch":         "Ara:",
                    "sZeroRecords":    "Eşleşen kayıt bulunamadı",
                    "oPaginate": {
                        "sFirst":    "İlk",
                        "sLast":     "Son",
                        "sNext":     "Sonraki",
                        "sPrevious": "Önceki"
                    },
                    "oAria": {
                        "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
                        "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                    },
                    "select": {
                        "rows": {
                            "_": "%d kayıt seçildi",
                            "0": "",
                            "1": "1 kayıt seçildi"
                        }
                    }
                },
                processing: true,
                serverSide: true,
                order:[['5','desc']],
                ajax: '{!! route('admineslesme_datatable') !!}',
                columns: [
                    { data: 'eslesen_kullanici_id', name: 'eslesen_kullanici_id'},
                    { data: 'ad', name: 'ad' },
                    { data: 'devreden1', name: 'devreden1' },
                    { data: 'eslesen_cv', name: 'eslesen_cv' },
                    { data: 'eslesen_id', name: 'eslesen_id' },
                    { data: 'eslesen_ad', name: 'eslesen_ad' },
                    { data: 'devreden2', name: 'devreden2' },
                    { data: 'created_at', name:'created_at',searchable:true,orderable: true},
                ]
            });
        })
    </script>
@endsection
