@extends('layouts.main')
@section('content')

    <div class="container product_section_container" style="padding: 30px;margin-top: 120px">
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                          <p> Satış türü, paket veya ürün satışı için yapılmıştır kullancı ilk giriş yaptığında sizin eklediğiniz paket tipindeki ürünü alarak bayi konumuna gelir</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>

                        </div>
                    </div>
                </div>
            </div>
                   <div class="col-md-12 col-sm-12">
                       <script language="javascript" src="http://ir.sitekodlari.com/dovizkurlari6.js"></script>
                       <br><button type="button" class="btn btn-outline-warning " data-toggle="modal" data-target="#exampleModal" style="font-size: small">
                           Bilgilendirme
                       </button>
                       <h4 class="text-center mx-5 mb-4" style="font-family: 'Harlow Solid Italic'" > ÜRÜN KAYIT EKLEME  </h4>
                {!!Form::open(["url" => "/admin-products",'files' => 'true', "method" => "post"]) !!}
                {!! Form::bsFile("img[]","Ürün Görüntü") !!} <!-- Button trigger modal -->
                {!! Form::bsSelect("category_id","Kategori",null,$categoriess,"Lütfen Bir Kategori Seçiniz") !!}
                {!! Form::label( 'Satış Türü');!!}
                {!! Form::select('turu', ['paket' => 'Paket', 'urun' => 'Ürün'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20',]); !!}
                <br>
                {!! Form::bsText("product_name","Ürün İsmi") !!}
                {!! Form::bsText("code","Ürün Kodu") !!}
                {!! Form::bsText("miktar","Ürün Miktarı") !!}
                {!! Form::bsText("alisfiyati","Ürün Alış TL Fiyatı (₺)") !!}
                {!! Form::bsText("product_price","Ürün TL Fiyatı (₺)") !!}
                {!! Form::bsText("dolar","Ürün Dolar Fiyatı ($)") !!}
                {!! Form::bsText("euro","Ürün Euro Fiyatı (€)") !!}
                {!! Form::bsText("cv"," CV") !!}
                {!! Form::bsText("pv"," PV") !!}
                {!! Form::bsTextArea("product_detail","Ürün Detaylar",null,["class" => "summernote"]) !!}
                {!! Form::label('', 'Vitrinde görünmesini ister misiniz?')!!}
                {!! Form::select('vitrin', ['1' => 'Evet', '0' => 'Hayır'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20']) !!}
                       <br>
                 {!! Form::label('', 'Fırsat Ürünlerde Görünmesini İster Misiniz?')!!}
                 {!! Form::select('firsat', ['1' => 'Evet', '0' => 'Hayır'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20']) !!}
                       <br>
                 {!! Form::bsSubmit("Kaydet") !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('js')

    <script>
        $('.summernote').summernote({
            tabsize: 2,
            height: 100
        });
    </script>
@endsection
