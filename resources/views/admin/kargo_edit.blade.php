@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h3 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">Kargo Bilgisi Güncelleme Ekranı</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!!Form::model($kargoYonetim, ['route' => ['kargoyonetim.update', $kargoYonetim->id], "method" =>  "put","files" => true])!!}
                    {!! Form::bsText("kargo_adi","") !!}
                    {!! Form::bsText("kargo_fiyati","") !!}
                    <label for="">Kargo Durumu</label><br>
                    {!! Form::select('kargo_durumu', ['aktif' => 'Aktif', 'pasif' => 'Pasif'], null, ['placeholder' => 'Durum Seçiniz']); !!}
                    {!! Form::bsSubmit("Güncelle") !!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection
@section("css")
@endsection
