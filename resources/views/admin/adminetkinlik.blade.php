@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="{{route('etkinlikler.create')}}" class="btn btn-danger mb-5">
                    <i class="fa fa-plus"></i>
                    Yeni Etkinlik Ekle
                </a>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Etkinlik Adı</th>
                            <th>Fotoğraf Url</th>
                            <th>Mekan İsmi</th>
                            <th>Etkinlik İçeriği</th>
                            <th>Fotoğraf</th>
                            <th>Kayıt Tarihi</th>
                            <th>Silme</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($etkinlikler as $book)
                            <tr>
                                <td>{{$book->title }}</td>
                                <td>{{$book->url}}</td>
                                <td>{{$book->mekan}}</td>
                                <td>{!! $book->etkinlikler_icerik !!}</td>
                                <td>{{$book->created_at}}</td>

                                <td> <a href="/etkinlikler/{{$book->id}}" class="btn btn-danger" data-method="delete"
                                        data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
