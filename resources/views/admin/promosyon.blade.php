@extends('layouts.main')
@section('content')
    <div class="container-fluid" style="padding: 30px;margin-top: 100px">
        <div class="row">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">BAYİ PROMOSYON YÖNETİMİ</h4>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyer İsmi</th>
                            <th>Yüzde</th>
                            <th>Ürün Adedi</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Düzenle</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kariyers as $k)
                         @foreach($k->bayipromosyon as $t)
                            <tr>
                                <td>{{$t->kariyer->kariyername}}</td>
                                <td>{{$t->yuzde}}</td>
                                <td>{{$t->urun_adet}}</td>
                                <td>{{$t->created_at}}</td>
                                <td class="text-center">
                                    <a href="{{route('bayipromosyon.edit',$t->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
