@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a><h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">BAYİ AKTİFLİK EDİT</h4>
        <div class="row" style="margin-bottom: 30px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyer İsmi</th>
                            <th>Minimum Bayi</th>
                            <th>Max Alışveriş</th>
                            <th>Toplam Alısveris</th>
                            <th>Yüzde</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kariyers as $kariyer)
                                <tr>
                                    <td>{{$kariyer->kariyername}}</td>
                                    <td>{{$kariyer->aktiflik->min_bayi}}</td>
                                    <td>{{$kariyer->aktiflik->max_alisveris}}</td>
                                    <td>{{$kariyer->aktiflik->toplam_alisveris}}</td>
                                    <td>{{$kariyer->aktiflik->yuzde}}</td>
                                    <td>{{$kariyer->aktiflik->created_at}}</td>
                                    <td class="text-center">
                                        <a href="{{route('bayiaktiflik.edit',$kariyer->aktiflik->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
