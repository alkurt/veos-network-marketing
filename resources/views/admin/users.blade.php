@extends('layouts.main')

@section('content')
    <div class="container product_section_container" style="padding: 30px;margin-top: 170px">
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Alışveriş Müşteri sadece alışveriş yapabilen kullanıcıdır.</p>
                        <p>Standart ise bayi olarak kaydolmuş ancak daha paket almamış olan kullanıcıdır.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive"> <button type="button" class="btn btn-outline-warning mb-4" style="font-size: small" data-toggle="modal" data-target="#exampleModal">
                        Bilgilendirme
                    </button>
                <table class="table table-hover" id="users-table">
                    <thead>
                    <tr>
                        <th>Nu</th>
                        <th>İsim</th>
                        <th>Kariyer</th>
                        <th>Pv</th>
                        <th>Cv</th>
                        <th>Alt Bayi Sayısı</th>
                        <th>Sponsor Numarası</th>
                        <th>Toplam Alışveriş Tutarı</th>
                        <th>Oluşturma Tarihi</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center">
{{--                                <a href="{{route('admin-users.edit',$user->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>--}}
                            </td>
                            <td class="text-center">
{{--                                <a href="/admin-users/{{$user->id}}" class="btn btn-danger" data-method="delete"--}}
{{--                                   data-confirm="Emin misiniz?"><i class="fa fa-trash"></i></a>--}}
                            </td>
                        </tr>

                    </tbody>
                </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#users-table').DataTable({
                language:{
                    "sDecimal":        ",",
                    "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
                    "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "sInfoEmpty":      "Kayıt yok",
                    "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
                    "sLoadingRecords": "Yükleniyor...",
                    "sProcessing":     "İşleniyor...",
                    "sSearch":         "Ara:",
                    "sZeroRecords":    "Eşleşen kayıt bulunamadı",
                    "oPaginate": {
                        "sFirst":    "İlk",
                        "sLast":     "Son",
                        "sNext":     "Sonraki",
                        "sPrevious": "Önceki"
                    },
                    "oAria": {
                        "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
                        "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                    },
                    "select": {
                        "rows": {
                            "_": "%d kayıt seçildi",
                            "0": "",
                            "1": "1 kayıt seçildi"
                        }
                    }
                },
                processing: true,
                serverSide: true,
                order:[['5','desc']],
                ajax: '{!! route('admin-users.datatable') !!}',
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'ad', name: 'ad' },
                    { data: 'kariyer', name: 'kariyer' },
                    { data: 'ara_pv', name: 'ara_pv' },
                    { data: 'cv', name: 'cv' },
                    { data: 'alt_bayi', name: 'alt_bayi' },
                    { data: 'sponsor_id', name: 'sponsor_id' },
                    {data:'toplam_alisveris', name:'toplam_alisveris'},
                    { data: 'created_at', name: 'created_at' },
                    { data: 'buttons', name:'buttons',searchable:false,orderable: false},
                ]
            });
        })
    </script>
@endsection

