@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 170px">
        <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">SPONSOR DERİNLİK EKLEME ve  GÜNCELLEME</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <form method="POST" action="@isset($odemetalebi) {{route('adminodeme.update')}} @endisset" >
                        @csrf
                        @isset($odemetalebi)
                            @method('put')
                        @endisset
                        <div class="form-group">
                            <label class="mx-2">Kullanıcı Numarası</label>
                            <input class="w-25 form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id" type="text" value="@isset($odemetalebi) {{$odemetalebi->user_id}} @else {{old('kariyer')}} @endisset" disabled>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">İsim </label>
                            <input class="w-25 form-control{{ $errors->has('') ? ' is-invalid' : '' }}" name="" maxlength="1" type="tel" value="@isset($odemetalebi) {{$odemetalebi->user->name}} @else{{old('derinlik')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Soyisim </label>
                            <input class="w-25 form-control{{ $errors->has('') ? ' is-invalid' : '' }}" name="" type="tel"  maxlength="2" value="@isset($odemetalebi) {{$odemetalebi->user->surname}} @else{{old('yuzde')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">İletişim </label>
                            <input class="w-25 form-control{{ $errors->has('') ? ' is-invalid' : '' }}" name="" type="tel"  maxlength="2" value="@isset($odemetalebi) {{$odemetalebi->telefon}} @else{{old('yuzde')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">İban Numarası </label>
                            <input class="w-25 form-control{{ $errors->has('') ? ' is-invalid' : '' }}" name="" type="tel"  maxlength="2" value="@isset($odemetalebi) {{$odemetalebi->user->detail->phone}} @else{{old('yuzde')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Talep Tutarı </label>
                            <input class="w-25 form-control{{ $errors->has('odeme_talebi') ? ' is-invalid' : '' }}" name="odeme_talebi" type="tel"  maxlength="2" value="@isset($odemetalebi) {{$odemetalebi->yuzde}} @else{{old('odeme_talebi')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success mx-2" type="submit">Kaydet</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection

@section("css")
@endsection
