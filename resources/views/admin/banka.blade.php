@extends('layouts.main')
@section('content')

    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a><h4 class="text-center" style="font-family: 'Harlow Solid Italic'">BANKA HESAPLARI</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="{{route('banka.create')}}" class="btn btn-danger mb-5 mx-3">
                    <i class="fa fa-plus"></i>
                    Yeni Hesap Ekle
                </a>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Hesap Sahibi</th>
                            <th>İban Nu:</th>
                            <th>Hesap Nu:</th>
                            <th>Banka Adı</th>
                            <th>Şube Kodu</th>
                            <th>Şube Adı</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Silme</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($banka as $b)
                            <tr>
                                <td>{{$b->hesap_sahibi}}</td>
                                <td>{{$b->iban_no}}</td>
                                <td>{{$b->hesap_no}}</td>
                                <td>{{$b->banka_adi}}</td>
                                <td>{{$b->sube_kodu}}</td>
                                <td>{{$b->sube_adi}}</td>
                                <td>{{$b->created_at}}</td>

                                <td>
                                    <a href="{{route('banka.destroy',$b->id)}}" class="btn btn-danger float-right" data-method="delete"
                                       data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection

