@extends('layouts.main')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <div class="container" style="margin-top: 170px">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div style="background: black" class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="color: white"  href="{{route('menu')}}"></a> <a style="color: #EFEFEF"> KONTROL MENÜ</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body ">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <span class="fa fa-play text-dark"></span><a
                                                        href="{{route("aktif")}}"> Aktif-Pasif Kullanıcılar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-car text-dark"> </span><a
                                                        href="{{route("adminaraciro")}}"> Araba Cirosu Alacaklar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-money text-dark"></span><a
                                                        href="{{route('tazminat')}}"> Tazminat Alacaklar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-dark"></span><a
                                                        href="{{route("adminilciro")}}"> İL Cirosu Alacaklar</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-dark"></span><a
                                                        href="{{route("adminturkeyciro")}}"> Türkiye Cirosu Alacaklar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-dark"></span><a
                                                        href="{{route("admindunyaciro")}}"> Dünya Cirosu Alacaklar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-dark"></span><a
                                                        href="{{route("adminbolgeciro")}}"> Bölge Cirosu Alacaklar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-server text-dark"></span><a href="{{route("kontrolmenu")}}">
                                                    Kontrol Menü</a>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <h4 class="text-center " STYLE="font-family: 'Harlow Solid Italic'">TAZMİNAT ŞARTINI SAĞLAYAN KULLANICILAR</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mt-5">
                            <tr>
                                <th>Kullanıcı Numarası</th>
                                <th>İsim Soyisim </th>
                                <th>İletişim</th>
                                <th>Kariyeri</th>
                                <th>Tazminat Bedeli</th>
                                <th>Öde ve Sil</th>

                            </tr>
                            @foreach($tazminat as $t)
                                    <tr>
                                        <td>{{$t->user->id}}</td>
                                        <td>{{$t->user->name}} {{$t->user->surname}}</td>
                                        <td>{{$t->user->telefon}} / {{$t->user->email}}</td>
                                        <td>{{$t->user->kariyer->kariyername}}</td>
                                        <td> {{$t->tazminat}} ₺</td>
                                        <td class="text-center"> <a href="{{route('tazminat-ode',$t->id)}}" class="btn btn-success" data-method="delete"
                                                data-confirm="Emin Misiniz?"><i class="fa fa-check"></i></a>
                                        </td>
                                    </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection

@section("css")
@endsection
