@extends('layouts.main')

@section('content')
    <div class="container product_section_container" style="padding: 30px;margin-top: 170px">
        <div class="row">
            <div class="col-md-12">
                {!!Form::model($category, ['route' => ['admin-category.update', $category->id], "method" =>  "put","files" => true])!!}
                {!! Form::bsText("category_name","Kategori İsmi") !!}
                {!! Form::bsSubmit("Güncelle") !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection
