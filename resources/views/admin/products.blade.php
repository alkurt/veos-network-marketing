@extends('layouts.main')
@section('content')

    <div class="container product_section_container" style="padding: 30px;margin-top: 130px">
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <a href="/admin-products/create" class="btn btn-danger mb-5 mx-3">
                        <i class="fa fa-plus"></i>
                        Yeni Ürün Ekle
                    </a>
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Nu</th>
                        <th>Görüntü</th>
                        <th>Kategori</th>
                        <th>Ürün İsmi</th>
                        <th>Ürün Kodu</th>
                        <th>Ürün Miktarı</th>
                        <th>Ürün Türü</th>
                        <th>Ürün Alış Fiyatı</th>
                        <th>Ürün TL Fiyatı</th>
                        <th>Ürün Dolar Fiyatı </th>
                        <th>Ürün Euro Fiyatı </th>
                        <th>CV</th>
                        <th>PV</th>
                        <th>Ürün Detayları</th>
                        <th>Durumu</th>
                        <th>Vitrin</th>
                        <th>Fırsat</th>
                        <th>Oluşturma Tarihi</th>
                        <th>Edit</th>
                        <th>Silme</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{!! $product->thumbs !!}</td>
                            <td> {{ $product->categories->category_name }}  </td>
                            <td>{{ $product->product_name }}</td>
                            <td>{{ $product->code }}</td>
                            <td>{{ $product->miktar }}</td>
                            <td>{{$product->turu}}</td>
                            <td>{{ number_format($product->alisfiyati) }} ₺</td>
                            <td>{{ number_format($product->product_price) }} ₺</td>
                            <td>{{ number_format($product->dolar) }} $</td>
                            <td>{{ number_format($product->euro) }} €</td>
                            <td>{{ $product->cv }}</td>
                            <td>{{ $product->pv }}</td>
                            <td>{!! str_limit($product->product_detail, 30) !!}</td>
                            <td>@if($product->status) Aktif @else Pasif @endif</td>
                            <td>@if($product->vitrin==0)
                            <p>Hayır</p>
                                    @else
                                <p>Evet</p>
                                    @endif
                            </td>
                            <td>@if($product->firsat==0)
                            <p>Hayır</p>
                                    @else
                                <p>Evet</p>
                                    @endif
                            </td>
                            <td>{{$product->created_at->format('d-m-y H:i')}}</td>
                            <td>
                                <a href="{{route('admin-products.edit',$product->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                            </td>
                            <td> <a href="/admin-products/{{$product->id}}" class="btn btn-danger float-right" data-method="delete"
                                    data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {!! $products->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection

