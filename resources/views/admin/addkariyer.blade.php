@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 170px">
        <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">Kariyer Ekleme Ekranı</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                {!!Form::open(['url' => route('kariyer.store'),'files' => 'true', "method" => "post"]) !!}
                {!! Form::bsText("kariyername","Kariyer İsmi") !!}
                {!! Form::bsText("tutar","Paket Alt Limiti") !!}
                {!! Form::bsText("sponsor","Sponsor %") !!}
                {!! Form::bsText("indirim"," Alışveriş") !!}
                {!! Form::bsText("binary"," Binary") !!}
                {!! Form::bsText("il_ciro","") !!}
                {!! Form::bsText("tr_ciro","Türkiye Ciro") !!}
                {!! Form::bsText("dunya_ciro","Dünya Ciro") !!}
                {!! Form::bsText("araba_ciro","") !!}
                {!! Form::bsText("bolge_ciro","") !!}
                {!! Form::bsText("lider_primi","") !!}
                {!! Form::bsText("derinlik_birinci_derinlik"," Birinci Derinlik") !!}
                {!! Form::bsSubmit("Kaydet") !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
