@extends('layouts.main')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <div class="container" style="margin-top: 140px">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-9 col-sm-12">
                    <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-5 mt-3" STYLE="font-family: 'Harlow Solid Italic'">YÖNETİCİ KAZANÇ ÖZETİ</h4>
                   <p class="mb-5">Ödenecek Prim Tutarı Toplamı : {{$cuzdan}} ₺</p>
                    <div class="table-responsive">
                        <div class="table table-bordered table-hover" >
                            <table class="table table-bordered table-hover" id="admin-table">
                                <thead>
                                <tr>
                                    <th>Kazanç Sahibi Numarası</th>
                                    <th>Ad Soyad</th>
                                    <th>Kazanç Türü</th>
                                    <th>Kazanç Getiren Kullanıcı</th>
                                    <th>Ad Soyad</th>
                                    <th>Kazanç Miktarı</th>
                                    <th>İşlem Tarihi</th>
                                </tr>
                          </thead>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#admin-table').DataTable({
                language:{
                    "sDecimal":        ",",
                    "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
                    "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "sInfoEmpty":      "Kayıt yok",
                    "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
                    "sLoadingRecords": "Yükleniyor...",
                    "sProcessing":     "İşleniyor...",
                    "sSearch":         "Ara:",
                    "sZeroRecords":    "Eşleşen kayıt bulunamadı",
                    "oPaginate": {
                        "sFirst":    "İlk",
                        "sLast":     "Son",
                        "sNext":     "Sonraki",
                        "sPrevious": "Önceki"
                    },
                    "oAria": {
                        "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
                        "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                    },
                    "select": {
                        "rows": {
                            "_": "%d kayıt seçildi",
                            "0": "",
                            "1": "1 kayıt seçildi"
                        }
                    }
                },
                processing: true,
                serverSide: true,
                order:[['6','desc']],
                ajax: '{!! route('admin-datatable') !!}',
                columns: [
                    { data: 'user_id', name: 'user_id'},
                    { data: 'ad', name: 'ad' },
                    { data: 'kazanc_turu', name: 'kazanc_turu' },
                    { data: 'islem_sahibi_id', name: 'islem_sahibi_id' },
                    { data: 'islem_sahibi_ad', name: 'islem_sahibi_ad' },
                    { data: 'kazanc_miktari', name: 'kazanc_miktari' },
                    { data: 'created_at', name:'created_at',searchable:true,orderable: true},
                ]
            });
        })
    </script>
@endsection

@section("css")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@endsection
