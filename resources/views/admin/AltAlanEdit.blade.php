@extends('layouts.main')

@section('content')
    <div class="container-fluid" style="margin-top: 150px">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
        <!-- Modal -->
        <a class="" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="col-md-12 mt-3">
            <div class="form-group">
                <form action="{{ route('alt-alan.update',$firsat->id) }}" method="POST" >
                    @csrf
                    @method('PUT')
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>1.Alan</small>
                                    <textarea name="bir"   id="" cols="90" rows="12">{{$firsat->bir}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>2.Alan</small>
                                    <textarea name="iki" id="" cols="90" rows="12">{{$firsat->iki}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>3.Alan</small>
                                    <textarea name="uc" id="" cols="90" rows="12">{{$firsat->uc}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>4.Alan</small>
                                    <textarea name="dort" cols="90" rows="12">{{$firsat->dort}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Kaydet " class="btn btn-info w-50 mt-5 mx-5">
                    </div>
                </form>
            </div>
        </div>


    </div>
@endsection
@section('js')

@endsection
