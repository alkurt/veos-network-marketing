@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">SPONSOR DERİNLİK GÜNCELLEME</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <form method="POST" action="@isset($kariyer_derinlik) {{route('sponsorderinlik.update',['kariyer_id'=>$kariyer_derinlik->kariyer_id,'derinlik'=>$kariyer_derinlik->derinlik])}} @else{{route('sponsorderinlik.store')}} @endisset" >
                        @csrf
                        @isset($kariyer_derinlik)
                            @method('put')
                            @endisset
                        <div class="form-group">
                            <label class="mx-2">Kariyer</label>
                            <input class="w-25 form-control{{ $errors->has('kariyer') ? ' is-invalid' : '' }}" name="kariyer" type="text" value="@isset($kariyer_derinlik){{$kariyer_derinlik->kariyer->kariyername}}@else {{old('kariyer')}} @endisset" disabled>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Derinlik: </label>
                            <input class="w-25 form-control{{ $errors->has('derinlik') ? ' is-invalid' : '' }}" name="derinlik" maxlength="1" type="number" value="@isset($kariyer_derinlik){{$kariyer_derinlik->derinlik}}@else{{old('derinlik')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Yüzde Oranı % </label>
                            <input class="w-25 form-control{{ $errors->has('yuzde') ? ' is-invalid' : '' }}" name="yuzde"  type="number" step="0.01"  maxlength="2" value="@isset($kariyer_derinlik){{$kariyer_derinlik->yuzde}}@else{{old('yuzde')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success mx-2" type="submit">Kaydet</button>
                            <a class="btn btn-danger" href="{{route('sponsorderinlik.index')}}">Geri</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection

@section("css")
@endsection
