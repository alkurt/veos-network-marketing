@extends('layouts.main')
@section("content")
    <div class="container-fluid"style="margin-top: 150px">
        <a class="mx-3" style="color: black " href="{{route('kontrolmenu')}}">Kontrolmenu</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
        <?php

        use App\Kariyer;
        use App\User;
        $kariyerler = Kariyer::where('id','<=',16)->get();
        $musteri1 = User::where('kariyer_id',1)->count();
        $musteri2 = User::where('kariyer_id',2)->count();
        $musteri3 = User::where('kariyer_id',3)->count();
        $musteri4 = User::where('kariyer_id',4)->count();
        $musteri5 = User::where('kariyer_id',5)->count();
        $musteri6 = User::where('kariyer_id',6)->count();
        $musteri7 = User::where('kariyer_id',7)->count();
        $musteri8 = User::where('kariyer_id',8)->count();
        $musteri9 = User::where('kariyer_id',9)->count();
        $musteri10 = User::where('kariyer_id',10)->count();
        $musteri11 = User::where('kariyer_id',11)->count();
        $musteri12 = User::where('kariyer_id',12)->count();
        $musteri13 = User::where('kariyer_id',13)->count();
        $musteri14 = User::where('kariyer_id',14)->count();
        $musteri15 = User::where('kariyer_id',15)->count();
        $dataPoints = array(
	array("y" => $musteri1, "label" => $kariyerler[0]->kariyername ),
	array("y" => $musteri2, "label" => $kariyerler[1]->kariyername ),
	array("y" => $musteri3, "label" => $kariyerler[2]->kariyername ),
	array("y" => $musteri4, "label" => $kariyerler[3]->kariyername ),
	array("y" => $musteri5, "label" => $kariyerler[4]->kariyername ),
	array("y" => $musteri6, "label" => $kariyerler[5]->kariyername ),
	array("y" => $musteri7, "label" => $kariyerler[6]->kariyername ),
	array("y" => $musteri8, "label" => $kariyerler[7]->kariyername ),
	array("y" => $musteri9, "label" => $kariyerler[8]->kariyername ),
	array("y" => $musteri10, "label" => $kariyerler[9]->kariyername ),
	array("y" => $musteri11, "label" => $kariyerler[10]->kariyername ),
	array("y" => $musteri12, "label" => $kariyerler[11]->kariyername ),
	array("y" => $musteri13, "label" => $kariyerler[12]->kariyername ),
	array("y" => $musteri14, "label" => $kariyerler[13]->kariyername ),
	array("y" => $musteri15, "label" => $kariyerler[14]->kariyername ),

        );
        ?>
    </div>
@endsection

@section("js")
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Mevcut Kariyer Raporu"
                },
                axisY: {
                    title: "Toplam Kişi Sayıları"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## Kişi",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
@endsection

@section("css")
    <style>
        #timeToRender {
            position:absolute;
            top: 10px;
            font-size: 20px;
            font-weight: bold;
            background-color: #d85757;
            padding: 0px 4px;
            color: #ffffff;
        }
    </style>
@endsection
