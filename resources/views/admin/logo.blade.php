@extends('layouts.main')

@section('content')
    <div class="container-fluid" style="margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-5 mt-5">
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Logo Ekleme Bilgisi</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Yüklenen Logolardan sadece bir tanesini aktif olarak kullanabilirsiniz!!</p>
                                    <p>Aksi Durumda İlk Aktif Olan Logo Baz Alınacaktır!!</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-header "> <a style="font-size: small" href="{{ route('logo.create') }}" class="btn btn-primary">Logo Ekle</a><button type="button" style="font-size: x-small" class="btn btn-outline-warning mx-2" data-toggle="modal" data-target="#exampleModal">
                            ?
                        </button></div>
                    <div class="card-body">

                        <table class="table">
                            <tr>
                                <th style="font-size: small">Logo Görüntüsü</th>
                                <th style="font-size: small">Logo İsmi</th>
                                <th style="font-size: small">Yüklenen Logo</th>
                                <th class="text-center" style="font-size: small">Düzenle</th>
                                <th style="font-size: small">Durumu</th>
                            </tr>
                            @foreach($logo as $logo)
                                <tr>
                                    <td> <img style="height: 35px;width: 35px" src="{{\Illuminate\Support\Facades\Storage::url($logo->cover)}}"
                                              alt=""/></td>
                                    <td>{{ $logo->name }}</td>
                                    <td><a href="{{ route('logo.download', $logo->uuid) }}">{{ $logo->uuid }}</a></td>
                                    <td class="text-center">
                                        <a href="{{route('logo.edit',$logo->id)}}" class="btn btn-outline-info " style="font-size: small"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td> @if($logo->status==1) <span class="text-success">Aktif</span>@else <span class="text-danger">Pasif</span>@endif </td>
                                </tr>
                            @endforeach
                        </table>
                        <br /><br />

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
