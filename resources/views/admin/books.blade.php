@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 170px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="row justify-content-center mt-5 mb-5">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><strong>SÖZLEŞME EKLE</strong></div>
                    <div class="card-body">
                        <a href="{{ route('books.create') }}" class="btn btn-primary">Yeni Sözleşme Ekle</a>
                        <br /><br />
                        <table class="table">
                            <tr>
                                <th>Sözleşme İsmi</th>
                                <th>Yüklenen Sözleşmeler</th>
                                <th>Silme</th>
                            </tr>

                            @forelse ($books as $book)

                                <tr>
                                    <td>{{ $book->title }}</td>
                                    <td><a href="{{ route('books.download', $book->uuid) }}">{{ $book->uuid }} .pdf</a></td>
                                    <td> <a href="{{route('booksil',$book->id)}}" class="btn btn-danger" data-method="delete"
                                            data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Yüklenmiş Herhangi Bir Sözleşme Bulunmuyor...</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
