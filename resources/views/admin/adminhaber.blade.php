@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 140px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <div class="row justify-content-center">
            <div class="col-md-8 mb-5 mt-5">
                <div class="card">
                    <div class="card-header " style="background-color: dodgerblue"><strong style="color: white">HABER EKLE</strong></div>
                    <div class="card-body">
                        <a href="{{ route('haberler.create') }}" class="btn btn-danger">Yeni Haber Ekle</a>
                        <br /><br />
                        <table class="table table-responsive">
                            <tr>
                                <th>Haber Başlığı</th>
                                <th> Haber İçeriği</th>
                                <th>Url</th>
                                <th>Silme</th>
                            </tr>
                            @forelse ($haberler as $book)
                                <tr>

                                    <td>{{ $book->haber_basligi }}</td>
                                    <td>{!! $book->haber_icerik !!}</td>
                                    <td>{{$book->url}}</td>
                                    <td> <a href="{{route('haberlersilme',$book->id)}}" class="btn btn-danger" data-method="delete"
                                            data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Yüklenmiş Herhangi Bir Haber Bulunmuyor...</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
