@extends('layouts.main')
@section('content')
    <div class="container-fluid" style="padding: 30px;margin-top: 100px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">BAYİ İNDİRİM FARK YÖNETİMİ</h4>
        <div class="row" style="margin-bottom: 30px;">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyer İsmi</th>
                            <th>Karşı Kariyer İsmi </th>
                            <th>Fark Oranı</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Düzenle</th>
                            {{--                            <th class="text-center">Silme</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kariyers as $kariyer)
                            @forelse($kariyer->bayiindirimfark as $bfark)
                                <tr>
                                    <td>{{$bfark->kariyer->kariyername}}</td>
                                    <td>{{\App\Kariyer::where('id',$bfark->karsı_kariyer_id)->first()->kariyername}} </td>
                                    <td>{{$bfark->fark}}</td>
                                    <td>{{$bfark->created_at}}</td>
                                    <td class="text-center">
                                        <a href="{{route('bayiindirimfark.edit',$bfark->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @empty
                                @continue
                            @endforelse
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
