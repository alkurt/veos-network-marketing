@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 170px">
        <div class="row justify-content-center">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            <div class="col-md-8">
                <div class="card mt-5 mb-5">
                    <div class="card-header"><strong>Yeni Katalog Ekle</strong></div>
                    <div class="card-body">

                        <form action="{{ route('katalog.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            Katalog İsmi
                            <br>
                            <input type="text" name="title" class="form-control" required>
                            <br>
                            Katalog Pdf(pdf)
                            <br>
                            <input type="file" name="cover">
                            <br><br>
                            <input type="submit" value=" Kaydet " class="btn btn-primary">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
