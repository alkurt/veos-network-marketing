@extends('layouts.main')

@section('content')
    <div class="container-fluid" style="margin-top: 150px">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bilgilendirme</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <button style="font-size: small" type="button" class="btn btn-outline-warning mx-4" data-toggle="modal" data-target="#exampleModal">Bilgilendirme</button>
        <div class="col-md-12 mt-3">
            <div class="form-group">
                <form action="{{ route('alt-alan.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>1.Alan</small>
                                    <textarea name="bir" id="" cols="90" rows="12"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>2.Alan </small>
                                    <textarea name="iki" id="" cols="90" rows="12"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small> 3.Alan</small>
                                    <textarea name="uc" id="" cols="90" rows="12"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <small>4.Alan</small>
                                    <textarea name="dort" id="" cols="90" rows="12"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Kaydet " class="btn btn-info w-50 mt-5 mx-5">
                    </div>
                </form>
            </div>
        </div>


    </div>
@endsection
@section('js')

@endsection
