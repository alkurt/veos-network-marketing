@extends('layouts.main')
@section('content')
    <div class="container" style="padding: 30px;margin-top: 130px">
        <div class="row">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
                <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'"> KULLANICILARIN CÜZDAN BAKİYESİ</h4>
                <div class="table-responsive">
                    <table class="table table-hover table-responsive">
                        <thead>
                        <tr>
                            <th>İsim Soyisim</th>
                            <th>İban Numarası</th>
                            <th>Kullanıcı Numarası </th>
                            <th>Toplam Bakiyesi</th>
                            <th>Ödeme Yapılan Bakiyesi</th>
                            <th>Kayıt Tarihi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $u)
                            <tr>
                                <th>{{$u->name}} {{$u->surname}}</th>
                                <td>{{$u->detail->phone}}</td>
                                <td>{{$u->id}}</td>
                                <td>{{$u->cuzdan->toplam_bakiye}}</td>
                                <td>{{$u->cuzdan->odenen_bakiye}}</td>
                                <td>{{$u->created_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
