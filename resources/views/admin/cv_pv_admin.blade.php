@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h3 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">KULLANICI CV-PV EKLEME</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!!Form::model($user, ['route' => ['cv_pv.update', $user->id], "method" =>  "put","files" => true])!!}
                    {!! Form::bsText("eklenecek_pv","") !!}
                    {!! Form::bsText("eklenecek_cv","") !!}
                    <div class="mt-5 mx-5">
                        {!! Form::bsSubmit("Ekle") !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection
@section("css")
@endsection
