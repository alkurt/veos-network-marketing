@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">BAYİ PROMOSYON EDİT</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <form method="POST" action="@isset($promosyon) {{route('bayipromosyon.update',$promosyon->id)}} @endisset" >
                        @csrf
                        @isset($promosyon)
                            @method('put')
                        @endisset
                        <div class="form-group">
                            <label class="mx-2">Kariyer İsmi</label>
                            <input class="w-25 form-control{{ $errors->has('kariyer') ? ' is-invalid' : '' }}" name="kariyer_id" type="text" value="@isset($promosyon) {{$promosyon->kariyer->kariyername}} @else {{old('kariyer_id')}} @endisset" disabled>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Ürün Adedi </label>
                            <input class="w-25 form-control{{ $errors->has('urun_adet') ? ' is-invalid' : '' }}" name="urun_adet" maxlength="1" type="tel" value="@isset($promosyon) {{$promosyon->urun_adet}} @else{{old('urun_adet')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Yüzde Oranı % </label>
                            <input class="w-25 form-control{{ $errors->has('yuzde') ? ' is-invalid' : '' }}" name="yuzde" type="number" maxlength="2" step="0.01" value="@isset($promosyon){{$promosyon->yuzde}}@else{{old('yuzde')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success mx-2" type="submit">Kaydet</button>
                            <a class="btn btn-danger" href="{{route('bayipromosyon.index')}}">Geri</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("js")
@endsection
@section("css")
@endsection
