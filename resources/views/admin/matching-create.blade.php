@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3 mb-5" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center" style="font-family: 'Harlow Solid Italic'"> MATCHİNG GÜNCELLEME EKRANI</h4>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <form method="POST" action="@isset($kariyer_matching) {{route('matching.update',$kariyer_matching->id)}} @else{{route('matching.store')}} @endisset" >
                        @csrf
                        @isset($kariyer_matching)
                            @method('put')
                        @endisset
                        <div class="form-group">
                            <label class="mx-2">Kariyer</label>
                            <select class="w-25 form-control" name="kariyer_id" type="text"  >
                                @foreach($kariyerler as $a)
                                <option name="kariyer_id" class="w-25 form-control{{ $errors->has('kariyer_id') ? ' is-invalid' : '' }}" value="{{$a->id}}" @if($kariyer_matching->kariyer_id ==$a->id) selected @endif>{{$a->kariyername}} </option>
                                @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Derinlik: </label>
                            <input class="w-25 form-control{{ $errors->has('derinlik') ? ' is-invalid' : '' }}" name="derinlik" maxlength="1" type="tel" value="@isset($kariyer_matching){{$kariyer_matching->derinlik}}@else{{old('derinlik')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Yüzde Oranı % </label>
                            <input class="w-25 form-control{{ $errors->has('yuzde') ? ' is-invalid' : '' }}" name="yuzde" type="number" step="0.01"  min="0" maxlength="2" value="@isset($kariyer_matching){{$kariyer_matching->yuzde}}@else{{old('yuzde')}} @endisset" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success mx-2" type="submit">Kaydet</button>
                            <a class="btn btn-danger" href="{{route('matching.index')}}">Geri</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection

@section("css")
@endsection
