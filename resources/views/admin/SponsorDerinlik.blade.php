@extends('layouts.main')
@section('content')
    <div class="container-fluid" style="padding: 30px;margin-top: 100px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">SPONSOR DERİNLİK YÖNETİMİ </h4>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Kariyer</th>
                            <th>Derinlik</th>
                            <th>Yüzde Oranı %</th>
                            <th>Kayıt Tarihi</th>
                            <th class="text-center" >Düzenle</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kariyers as $kariyer)
                           @forelse($kariyer->derinliks as $derinlikler)
                               <tr>
                                   <td>{{$kariyer->kariyername}}</td>
                                   <td>{{$derinlikler->derinlik}}</td>
                                   <td>{{$derinlikler->yuzde}}</td>
                                   <td>{{$derinlikler->created_at}}</td>
                                   <td class="text-center">
                                       <a href="{{route('sponsorderinlik.edit',['kariyer_id'=>$kariyer->id,'derinlik'=>$derinlikler->derinlik])}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>
                                   </td>
                               </tr>
                               @empty

                               @endforelse
                        @endforeach
                        </tbody>
                    </table>
                    {{ $kariyers->links() }}
                </div>
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
@endsection
