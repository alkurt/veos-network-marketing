@extends('layouts.main')

@section('content')
    <div class="container-fluid" style="margin-top: 130px">
        <div class="row justify-content-center">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
            <div class="col-md-8">
                <div class="card mt-5 mb-5">
                    <div class="card-header"><strong>Yeni Logo Ekle</strong></div>
                    <div class="card-body">

                        <form action="{{ route('logo.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            Logo İsmi
                            <br>
                            <input type="text" name="name" class="form-control" required>
                            <br>
                            Logo Fotoğraf
                            <br>
                            <input type="file" name="cover">
                            <br><br>
                            <button class="btn btn-primary" type="submit"> Kaydet</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
