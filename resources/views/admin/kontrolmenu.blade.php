@extends("layouts.main")
@section('content')
    <div class="container-fluid" style="margin-top:80px;background-image: url('https://cutewallpaper.org/21/white-hd-wallpaper/Abstract-Wallpaper-White.jpg')">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="{{asset('css/menu.css')}}">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="{{asset('css/mback.css')}}">
        <!------ Include the above in your HEAD tag ---------->
        <!-- Modal -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document "  style="margin-top: 80px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Kullanıcıya Ödeme Yap</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Kapat</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                 <div class="row">
                     <div class="col-md-3">
                         <ul id="menu-v">
                             <li>  <h5 class=" text-white  text-center">KONTROL MENÜ</h5></li>
                             <li><a  href="{{route("aktif")}}"> Aktif-Pasif Kullanıcılar</a></li>
                             <li><a  href="{{route('adminaraciro')}}"> Araç Cirosu Alacaklar</a></li>
                             <li><a  href="{{route('adminilciro')}}"> İL Cirosu Alacaklar </a></li>
                             <li><a  href="{{route('adminturkeyciro')}}"> Türkiye Cirosu Alacaklar </a></li>
                             <li><a  href="{{route('admindunyaciro')}}"> Dünya Cirosu Alacaklar </a></li>
                             <li><a  href="{{route('adminbolgeciro')}}"> Bölge Cirosu Alacaklar </a></li>
                             <li><a  href="{{route('tazminat')}}"> Tazminat Alacaklar Listesi </a></li>
                             <li class="mt-3 mb-3">  <h5 class="text-white  text-center" >Düzenleme Kontrol İşlemleri </h5></li>
                             <li><a href="{{ url('/admin-category') }}"> Kategori</a></li>
                             <li><a href="{{ url('/admin-products') }}"> Ürün Ekle / Düzenle </a></li>
                             <li><a href="{{route('kariyer.index')}}"> Kariyer Düzenle</a></li>
                             <li><a href="{{route('alisverisderinlikdagilimi.index')}}"> Alışveriş Derinlik Düzenle</a></li>
                             <li><a href="{{route('sponsorderinlik.index')}}"> Sponsor Derinlik Düzenle</a></li>
                             <li><a href="{{route('sponsorfark.index')}}">Sponsor Fark Düzenle</a></li>
                             <li> <a href="{{route('matching.index')}}"> Matching Primi Düzenle</a></li>
                             <li><a href="{{route('bayiaktiflik.index')}}">Bayi Aktiftik Düzenle</a></li>
                             <li><a href="{{route('bayiindirimfark.index')}}">Bayi İndirim Fark Düzenle</a></li>
                             <li><a href="{{route('lidercikarma.index')}}">Lider Çıkarma Düzenle</a></li>
                             <li><a href="{{route('bayipromosyon.index')}}">Bayi Promosyon Düzenle</a></li>
                             <li><a href="{{route('anapara.index')}}">Ana Pay Düzenle</a></li>
                             <li class="mt-3 mb-3">  <h5 class="text-white  text-center" >Düzenleme Kontrol İşlemleri </h5></li>
                             <li><a  href="{{route('kariyer-raporu')}}">KARİYER RAPORU</a></li>
                             <li><a  href="{{route('books.index')}}"> Sözleşme Yükle</a></li>
                             <li><a  href="{{route('firsat.index')}}"> Sayfaların İçerik Yönetimi</a></li>
                             <li><a  href="{{route('alt-alan.index')}}">Alt Bilgi Alan Yönetimi</a></li>
                             <li><a  href="{{route('banka.index')}}"> Banka Hesapları</a></li>
                             <li><a  href="{{route('kargoyonetim.index')}}"> Kargo Firmaları</a></li>
                             <li><a  href="{{route('sss.index')}}"> S.S.S Yönetimi</a></li>
                             <li><a  href="{{route('haberler.index')}}"> Haber Yönetimi</a></li>
                             <li><a  href="{{route('etkinlikler.index')}}"> Etkinlikler Yönetimi</a></li>
                             <li><a  href="{{route('slider.index')}}"> Slider Yönetimi</a></li>
                             <li><a  href="{{route('logo.index')}}"> Logo Yönetimi</a></li>
                             <li><a  href="{{route('iletisim.index')}}"> İletişim Yönetimi</a></li>
                             <li><a  href="{{route('duyurular.index')}}"> Duyurular Yönetimi</a></li>
                             <li><a  href="{{route('firsat.index')}}"> Fırsat Bilgi Yönetimi</a></li>
                             <li><a  href="{{route('katalog.index')}}"> E-Katalog Yönetimi</a></li>
                             <li><a  href="{{route('user.excel.import')}}"> Excel Dosyası Yükle</a></li>
                         </ul>
                     </div>
                     <div class="col-md-9">
                         <div class="row">
                             <div class="col-md-12">
                                 <div class="table-responsive">
                                     <div class="row my-5">
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('admin-users.index')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-eye fa-5x"></i><br/>
                                                 <br> KULLANICI SAYISI <br><br>
                                                 <strong style="color: black">{{$users}}</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('aktif')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-thumbs-up  fa-5x"></i><br/>
                                                 <br>AKTİF <br><br>
                                                 <strong style="color: black">{{$aktif}}</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('users.passive')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-thumbs-down fa-5x"></i><br/>
                                                 <br>PASİF <br><br>
                                                 <strong style="color: black">{{$pasif}}</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('adminekip')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-list  fa-5x"></i><br/>
                                                 <br>EKİP LİSTESİ
                                             </a>
                                         </div>
                                     </div>
                                     <div class="row my-5">
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="#" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-money text-success  fa-5x"></i><br>
                                                 <br>Ödenecek Prim <br>Tutarı <br><br>
                                                 <strong style="color: black">{{number_format($primodeme,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('admin.giderler')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-money text-success  fa-5x"></i><br/>
                                                 <br>Ödenen Prim <br>Tutarı<br><br>
                                                 <strong style="color: black">{{number_format($giderler,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('admin.giderler')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-money text-success  fa-5x"></i><br/>
                                                 <br>Kesinti <br>Kazancı<br><br>
                                                 <strong style="color: black">{{number_format($kesinti,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="#" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-money text-success  fa-5x"></i><br/>
                                                 <br>TOPLAM <br>KASA<br><br>
                                                 <strong style="color: black">{{number_format($gelirler,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="#" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-shopping-basket text-success  fa-5x"></i><br>
                                                 <br>Ürün Alış <br>Toplam Tutarı <br><br>
                                                 <strong style="color: black">{{number_format($alisfiyati,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="#" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-shopping-cart text-success  fa-5x"></i><br/>
                                                 <br>Ürün Satış <br>Toplam Tutarı <br><br>
                                                 <strong style="color: black">{{number_format($satisfiyati,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="#" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-shopping-bag text-success  fa-5x"></i><br/>
                                                 <br>ÜRÜN <br>KÂR KAZANCI <br><br>
                                                 <strong style="color: black">{{number_format($urunkar,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5">
                                             <a href="#" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-database text-success fa-5x"></i><br/>
                                                 <br>GENEL KÂR <br><br>
                                                 <strong style="color: black">{{number_format($net_kasa,2,',','.')}} ₺</strong>
                                             </a>
                                         </div>

                                     </div>
                                     <div class="row my-5">
                                         @if($eft == 0)
                                             <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5  ">
                                                 <a href="{{route('efthavalebekleme.index')}}" class="btn btn-sq-lg mt-5 ">
                                                     <i class="fa fa-money text-success  fa-5x" style="margin-top: -30px"></i><br/>
                                                     <br>EFT / HAVALEME <br> ÖDEME BİLDİRİMLERİ
                                                 </a>
                                             </div>
                                         @else
                                             <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5 ">
                                                 <p style="background-color: orangered;color: white;margin-left: 160px;border-radius:30px;width:30px;height:30px">{{$eft}}</p>
                                                 <a href="{{route('efthavalebekleme.index')}}" class="btn btn-sq-lg ">
                                                     <i class="fa fa-money text-success  fa-5x" style="margin-top: -30px"></i><br/>
                                                     <br>EFT / HAVALEME <br> ÖDEME BİLDİRİMLERİ
                                                 </a>
                                             </div>
                                         @endif
                                                     <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5 ">
                                                         <a href="{{route('admineslesme')}}" class="btn btn-sq-lg ">
                                                             <i class="fa fa-code-fork text-dark  fa-5x"></i><br/>
                                                             <br>TÜM BAYİ <br> EŞLEŞMELERİ
                                                         </a>
                                                     </div>
                                             @if($odeme == 0)
                                                 <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5 ">
                                                     <a href="{{route('odemetalebi')}}" class="btn btn-sq-lg mt-3">
                                                         <i class="fa fa-history  text-dark fa-5x"></i><br/>
                                                         <br>ÖDEME TALEPLERİ
                                                     </a>
                                                 </div>
                                             @else
                                                 <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5 ">
                                                     <p  style="background-color: orangered;color: white;margin-left: 170px;border-radius:30px;width:30px;height:30px">{{$odeme}}</p>
                                                     <a href="{{route('odemetalebi')}}" class="btn btn-sq-lg ">
                                                         <i class="fa fa-history  fa-5x" style="margin-top: -30px"></i><br/>
                                                         <br>ÖDEME TALEPLERİ
                                                     </a>
                                                 </div>
                                             @endif
                                             <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5">
                                                 <a href="{{route('adminbinary')}}" class="btn btn-sq-lg  zoom">
                                                     <i class="fa fa-money text-success fa-5x"></i><br/>
                                                     <br> KAZANÇ <br> ÖZETİ
                                                 </a>
                                             </div>
                                     </div>
                                     <div class="row my-5">

                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5">
                                             <a href="{{route('admin.giderler')}}" class="btn btn-sq-lg  zoom">
                                                 <i class="fa fa-folder-open  fa-5x"></i><br/>
                                                 <br>YAPILAN <br> ÖDEMELER
                                             </a>
                                         </div>


                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('binarykontrol')}}" class="btn btn-sq-lg  zoom mt-5">
                                                 <i class="fa fa-tree fa-5x"></i><br/>
                                                 <br>SOY AĞACI <br> İŞLEMLERİ
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('admingelenpv')}}" class="btn btn-sq-lg  zoom mt-5">
                                                 <i class="fa fa-feed fa-5x"></i><br/>
                                                 <br>PV / CV <br> GELİRLERİ
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback ">
                                             <a href="{{route('adminodemes')}}" class="btn btn-sq-lg  zoom mt-5">
                                                 <i class="fa fa-unlock-alt fa-5x"></i><br/>
                                                 <br>CÜZDAN <br>KONTROL
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback  mt-5">
                                             @if($gelenmesaj == 1)
                                                 <p style="background-color: orangered;margin-bottom: 12px;color: white;margin-left: 140px;border-radius:30px;width:30px;height:30px">{{$gelenmesaj}}</p>
                                                 <a class="btn btn-sq-lg " href="{{route('camemessage')}}">  <i class="fa fa-commenting-o text-warning  fa-5x" style="margin-top: -45px"></i><br/>
                                                     <br>İletişimden Gelen<br> Mesajlar </a>
                                             @else
                                                 <a class="btn btn-sq-lg " href="{{route('camemessage')}}">  <i class="fa fa-commenting-o text-warning  fa-5x"></i><br/>
                                                     <br>İletişimden Gelen<br> Mesajlar </a>
                                             @endif
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback">
                                             <a href="{{route('sosyalmedya.index')}}" class="btn btn-sq-lg    zoom mt-5">
                                                 <i class="fa fa-snowflake-o text-warning fa-5x"></i><br/>
                                                 <br>SOSYAL MEDYA
                                             </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback  mt-5">
                                             <a class="btn btn-sq-lg  zoom" href="{{ url('/admin-orders') }}"> <i class="fa fa-pencil-square-o text-success  fa-5x"></i><br/>
                                                 <br>Tüm Siparişlerin<br> Takibi </a>
                                         </div>
                                         <div class="col-md-3 text-center mb-2 col-xs-12 mback mt-5 ">
                                             @if($alinan == 0)
                                                 <a class="btn btn-sq-lg" href="{{ route('siparisalindi') }}"> <i class=" fa fa-bullhorn text-dark  fa-5x"></i><br/>
                                                     <br>Bekleyen Siparişimiz<br>Yok <i class="fa fa-check text-success"></i>  </a>
                                             @else
                                                 <p style="background-color: orangered;margin-top: 1px;color: white;margin-left: 150px;border-radius:30px;width:30px;height:30px">{{$alinan}}</p>
                                                 <a class="btn btn-sq-lg" href="{{ route('siparisalindi') }}"> <img style="margin-top: -50px;height: 80px;width: 80px;border-radius: 100px;width: 80px;height:80px"
                                                                                                                    src="https://i.gifer.com/UBLm.gif" alt=""><br/>
                                                     <br>Yeni Gelen<br>Sipariş Var </a>
                                             @endif
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <!------ Include the above in your HEAD tag ---------->
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/button.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('js')
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
@endsection
