@extends('layouts.main')
@section("content")
    <div class="container"style="margin-top: 160px" >
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a> <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'" >EKİP LİSTESİ EKRANI</h4>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Kullanıcı Numarası</th>
                                    <th>İsim Soyisim </th>
                                    <th>Konumu</th>
                                    <th>Sponsor Numarası</th>
                                    <th>Durumu</th>
                                    <th>Kariyeri</th>
                                    <th>Kolu</th>
                                    <th>Mevcut Pv</th>
                                    <th>Mevcut Cv</th>
                                    <th class="text-center">Kayıt Tarih</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}&nbsp;{{$user->surname}}</td>
                                        <td class="text-center">
                                            @if($user->konum == null)
                                                <p>Boş</p>
                                                @else
                                                {{$user->konum}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->sponsor_id == null)
                                            <p>Şirket Hesabı</p>
                                            @else
                                            {{$user->sponsor_id}}</td>
                                        @endif
                                        <td>
                                            @if($user->status == true)
                                                <p>Aktif</p>
                                                @elseif($user->status == false)
                                                    <p>Pasif</p>
                                                @endif

                                        </td>
                                        <td>{{$user->kariyer->kariyername}}</td>
                                        <td>
                                           @for($i=1; $i<=4; $i++)
                                                @if($user->konum == $i)
                                                    <p>Sol</p>
                                                @endif
                                               @for($k=1; $k<=9; $k++)
                                                   @if($user->konum == (($i).'.'.$k))
                                                            <p>Sol</p>
                                                        @endif
                                                            @endfor
                                            @endfor
                                               @for($i=5; $i<=8; $i++)
                                                   @if($user->konum == $i)
                                                       <p>Sağ</p>
                                                   @endif
                                                       @for($k=1; $k<=9; $k++)
                                                           @if($user->konum == (($i).'.'.$k))
                                                               <p>Sağ</p>
                                                           @endif
                                                           @endfor
                                               @endfor
                                               @if($user->konum == null)
                                                   <p>Boş</p>
                                               @endif
                                        </td>
                                        <td>{{$user->ara_pv}}</td>
                                        <td>{{$user->cv}}</td>
                                        <td>{{$user->created_at}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
