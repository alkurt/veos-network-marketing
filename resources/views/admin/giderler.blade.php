@extends('layouts.main')
@section('content')
    <div class="container product_section_container" style="padding: 30px;margin-top: 130px">
        <div class="row" style="margin-bottom: 30px;">
            <div class="col-md-12">
                <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
                <h4 class="text-center"style="font-family: 'Harlow Solid Italic'"> YAPILAN ÖDEMELER</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Ad Soyad/K.Numarası</th>
                            <th>Ödeme Yapılan Tutar</th>
                            <th>%20 Kesinti Tutarı</th>
                            <th>Ödeme Tarihi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($giderler as $gider)
                            <tr>
                                <td>{{ \App\User::find($gider->user_id)->FullName .' ('.$gider->user_id.')'}}</td>
                                <td>{{ $gider->amount }} ₺</td>
                                <td>{{ $gider->kesinti }} ₺</td>
                                <td>{{$gider->created_at->format('d-m-y H:i')}}</td>
                            </tr>
                        @empty
                                    <marquee scrollamount="4" behavior="" direction=""> <p class="text-info">Yapılan Ödeme Bulunmamaktadır...</p></marquee>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ $giderler->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
@section('css')

@endsection

<script>

</script>
