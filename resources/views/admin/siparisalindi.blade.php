@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 40px;margin-top: 150px">
        <div class="col-md-12">
            <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>  <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'">YENİ GELEN SİPARİŞLER </h4>
          @if($sgeldi == 0)
              @else
                <img style="margin-top: -20px;height: 80px;width: 80px;border-radius: 100px;width: 80px;height:80px" src="https://i.gifer.com/UBLm.gif" alt="">
            @endif
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead >
                    <tr>
                        <th>Sipariş Nu</th>
                        <th>Sipariş Nu</th>
                        <th>İsim Soyisim</th>
                        <th>İletişim</th>
                        <th>Ödeme şekli</th>
                        <th>Tutar</th>
                        <th>Durum</th>
                        <th>Sipariş Tarihi</th>
                        <th>Takip Durumu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($orders) == 0)
                        <tr><td colspan="7">Kayıt bulunamadı</td></tr>
                    @endif
                    @foreach($orders as $order)
                        <tr>
                            <td>PN-{{$order->id}}</td>
                            <td>{{ $order->order_no }}</td>
                            <td>{{ $order->baskets->user->name }} {{ $order->baskets->user->surname }}</td>
                            <td>{{ $order->m_phone }}</td>
                            <td>{{ $order->payment_method}}</td>
                            <td>{{ $order->order_price}} ₺</td>
                            <td>{{ $order->status }} </td>
                            <td>{{ $order->created_at }}</td>
                            <td class="text-center"><a href="/admin-orders/{{$order->id}}/edit" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$orders->links()}}
            </div>
        </div>
    </div>
@endsection

