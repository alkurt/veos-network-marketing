@extends('layouts.main')

@section('title', 'Admin - Aktif Bayiler Listesi || VeosNet Network&Marketing')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <div class="container" style="margin-top: 170px">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div style="background: black" class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="color: white"  href="{{route('menu')}}"></a> <a style="color: #EFEFEF"> KONTROL MENÜ</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body ">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <span class="fa fa-play text-dark"></span><a
                                                        href="{{route("menu")}}"> Aktif Kullanıcılar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-car text-dark"> </span><a
                                                        href="{{route("kazanc")}}"> Araç Primi Alanlar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-money text-dark"></span><a
                                                        href="{{route('gelirlerim')}}"> Tazminat Alacaklar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-dark"></span><a
                                                        href="{{route("cuzdan")}}"> Cüzdanım</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="glyphicon glyphicon-stats text-dark"></span><a
                                                        href="{{route('kazancozeti')}}">Kazanç Özetim</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-server text-dark"></span><a href="{{route("hesap")}}">
                                                    Hesap Menü</a>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <h4 class="text-center mt-5" STYLE="font-family: 'Harlow Solid Italic'">AKTİF ve PASİF KULLANICILAR</h4>
                   <div class="table-responsive">
                       <table class="table table-hover" id="users-table">
                           <thead>
                           <tr>
                               <th>Nu</th>
                               <th>İsim</th>
                               <th>Kariyer</th>
                               <th>Pv</th>
                               <th>Cv</th>
                               <th>Alt Bayi Sayısı</th>
                               <th>Oluşturma Tarihi</th>
                               <th>İşlemler</th>
                           </tr>
                           </thead>
                           <tbody>

                           <tr>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td class="text-center">
                                   {{--                                <a href="{{route('admin-users.edit',$user->id)}}" class="btn btn-primary "><i class="fa fa-edit"></i></a>--}}
                               </td>
                               <td class="text-center">
                                   {{--                                <a href="/admin-users/{{$user->id}}" class="btn btn-danger" data-method="delete"--}}
                                   {{--                                   data-confirm="Emin misiniz?"><i class="fa fa-trash"></i></a>--}}
                               </td>
                           </tr>

                           </tbody>
                       </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("css")
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@endsection

@section('js')
    <script src="{{asset("js/laravel-delete.js")}}"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#users-table').DataTable({
                language:{
                    "sDecimal":        ",",
                    "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
                    "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "sInfoEmpty":      "Kayıt yok",
                    "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
                    "sLoadingRecords": "Yükleniyor...",
                    "sProcessing":     "İşleniyor...",
                    "sSearch":         "Ara:",
                    "sZeroRecords":    "Eşleşen kayıt bulunamadı",
                    "oPaginate": {
                        "sFirst":    "İlk",
                        "sLast":     "Son",
                        "sNext":     "Sonraki",
                        "sPrevious": "Önceki"
                    },
                    "oAria": {
                        "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
                        "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                    },
                    "select": {
                        "rows": {
                            "_": "%d kayıt seçildi",
                            "0": "",
                            "1": "1 kayıt seçildi"
                        }
                    }
                },
                processing: true,
                serverSide: true,
                order:[['5','desc']],
                ajax: '{!! route('admin-users.active.datatable') !!}',
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'ad', name: 'ad' },
                    { data: 'kariyer', name: 'kariyer' },
                    { data: 'ara_pv', name: 'ara_pv' },
                    { data: 'cv', name: 'cv' },
                    { data: 'alt_bayi', name: 'alt_bayi' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'buttons', name:'buttons',searchable:false,orderable: false},
                ]
            });
        })
    </script>
@endsection