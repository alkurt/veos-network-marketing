@extends('layouts.main')

@section('content')
    <div class="container product_section_container" style="padding: 30px;margin-top: 170px">
        <div class="row">
            <div class="col-md-12">
                {!!Form::model($products, ['route' => ['admin-products.update', $products->id], "method" =>  "put","files" => true])!!}
                {!! Form::bsFile("img[]","Ürün Görüntü") !!}
                {!! Form::bsSelect("category_id","Kategori",null,$categoriess,"Lütfen Bir Kategori Seçiniz!") !!}
                {!! Form::label('', 'Satış Türü');!!}
                {!! Form::select('turu', ['paket' => 'Paket', 'urun' => 'Ürün'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20']); !!}
                {!! Form::bsText("product_name","Ürün İsmi") !!}
                {!! Form::bsText("code","Ürün Kodu") !!}
                {!! Form::bsText("miktar","Ürün Miktarı") !!}
                {!! Form::bsText("alisfiyati","Ürün Alış TL Fiyatı") !!}
                {!! Form::bsText("product_price","Ürün TL Fiyatı") !!}
                {!! Form::bsText("dolar","Ürün Dolar Fiyatı") !!}
                {!! Form::bsText("euro","Ürün Euro Fiyatı") !!}
                {!! Form::bsText("cv","CV") !!}
                {!! Form::bsText("pv","PV") !!}
                {!! Form::bsTextArea("product_detail","Ürün Detayları",null,["class" => "summernote"]) !!}
                {!! Form::label('', 'Vitrinde görünmesini ister misiniz?')!!}
                {!! Form::select('vitrin', ['1' => 'Evet', '0' => 'Hayır'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20']) !!}
                <br>
                {!! Form::label('', 'Fırsat Ürünlerde Görünmesini İster Misiniz?')!!}
                {!! Form::select('firsat', ['1' => 'Evet', '0' => 'Hayır'], null, ["class" => "form-control",'rows'=>'5','cols'=>'20']) !!}
                <br>
                {!! Form::bsSubmit("Güncelle") !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.summernote').summernote({
            tabsize: 2,
            height: 100
        });
    </script>
@endsection
