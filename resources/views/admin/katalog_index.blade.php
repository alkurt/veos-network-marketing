@extends('layouts.main')

@section('content')
    <div class="container" style="margin-top: 170px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-5 mt-5">
                    <div class="card-header "><strong>KATALOG EKLE</strong></div>
                    <div class="card-body">
                        <a href="{{ route('katalog.create') }}" class="btn btn-primary">Yeni Katalog Ekle</a>
                        <br /><br />
                        <table class="table">
                            <tr>
                                <th>Katalog İsmi</th>
                                <th>Yüklenen Kataloglar</th>
                                <th>Silme</th>
                            </tr>
                            @forelse ($katalog as $katalog)
                                <tr>
                                    <td>{{ $katalog->title }}</td>
                                    <td><a href="{{ route('katalog.download', $katalog->uuid) }}">{{ $katalog->uuid }} .pdf</a></td>
                                    <td> <a href="{{route('katalogsil',$katalog->id)}}" class="btn btn-danger" data-method="delete"
                                            data-confirm="Emin Misiniz?"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Yüklenmiş Herhangi Bir Katalog Bulunmuyor...</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
