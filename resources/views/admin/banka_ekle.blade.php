@extends('layouts.main')
@section("content")
    <div class="container " style="padding: 30px;margin-top: 130px">
        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
        <h3 class="text-center" style="font-family: 'Harlow Solid Italic'">Banka Hesabı Ekleme Ekranı</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <form method="POST" action="{{route('banka.store')}}" >
                        @csrf
                        <div class="form-group">
                            <label class="mx-2">Hesap Sahibi :</label>
                            <input class="w-25 text-uppercase form-control{{ $errors->has('hesap_sahibi') ? ' is-invalid' : '' }}" name="hesap_sahibi" type="text" value="{{old('hesap_sahibi')}}">
                        </div>
                        <div class="form-group">
                            <label class="mx-2">İban Numarası :</label>
                            <input class="w-25 form-control{{ $errors->has('iban_no') ? ' is-invalid' : '' }}" name="iban_no" type="tel" value="{{old('iban_no')}}" maxlength="50" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Hesap Numarası :</label>
                            <input class="w-25 form-control{{ $errors->has('hesap_no') ? ' is-invalid' : '' }}" name="hesap_no" type="tel" value="{{old('hesap_no')}}" maxlength="50" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Şube Kodu:</label>
                            <input class="w-25 form-control{{ $errors->has('sube_kodu') ? ' is-invalid' : '' }}" name="sube_kodu" type="tel" value="{{old('sube_kodu')}}" maxlength="50" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'>
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Şube Adı :</label>
                            <input class="w-25 form-control{{ $errors->has('sube_adi') ? ' is-invalid' : '' }}" name="sube_adi" type="tel" value="{{old('sube_adi')}}">
                        </div>
                        <div class="form-group">
                            <label class="mx-2">Banka Adı :</label>
                            <input class="w-25 form-control{{ $errors->has('banka_adi') ? ' is-invalid' : '' }}" name="banka_adi" type="tel" value="{{old('banka_adi')}}" >
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success mx-2" type="submit">Kaydet</button>
                            <a class="btn btn-danger" href="{{route('banka.index')}}">Geri</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection

@section("css")
@endsection
