@extends('layouts/main')

@section('content')


    @if($order->user_id === Auth::id())

        <!-- Checkout Content -->
        <div class="container-fluid no-padding checkout-content" style="margin-top: 170px;">
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <!-- Order Summary -->
                    <div class="col-sm-12 locations text-left">
                        <div class="section-padding"></div>
                        <a class="mx-3" style="color: black " href="{{route('home')}}">Anasayfa</a> > <a class="mx-3" style="color: gray" title="Geri" onclick="window.history.back()">Geri Git</a>
                        <h2 class="text-center" style="font-family: 'Harlow Solid Italic'">Sipariş Detayları (PN-{{ $order->id }}) <br><br></h2>
                        <table class="table table-bordererd table-hover">
                            <tr>
                                <th>Fotoğraflar</th>
                                <th>Ürün adı</th>
                                <th>Birim fiyatı</th>
                                <th>Miktar</th>
                                <th>Toplam Tutar</th>
                            </tr>
                            @foreach($order->baskets->basket_products as $basket_product)
                                <tr>
                                    <td>
                                        <a href="{{ route('product', $basket_product->product->slug) }}">
                                            @foreach($basket_product->product->images as $image)
                                                <img class="img-responsive" style="width: 50px;"
                                                     src="/uploads/{{ $image->name }}">
                                            @endforeach
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('product', $basket_product->product->slug) }}">
                                            {{ $basket_product->product->product_name }}
                                        </a>
                                    </td>
                                    <td>{{ number_format($basket_product->price,2) }} ₺</td>
                                    <td>{{ $basket_product->quantity }}</td>
                                    <td>{{ $basket_product->price * $basket_product->quantity }} ₺</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="4" class="text-right">TOPLAM (Kdv Dahil)</th>
                                <td colspan="2">{{ $order->order_price }} ₺</td>
                            </tr>

                            <tr>
                                <th colspan="4" class="text-right">Durum</th>
                                <td colspan="2">{{ $order->status }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div><!-- Container /- -->
            <div class="section-padding"></div>
        </div><!-- Checkout Content /- -->
    @else
        <div class="container-fluid no-padding checkout-content" style="margin-top: 40px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 order-summary">
                        <div class="alert alert-danger text-center">
                            <h2>Kayıt bulunamadı!</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endif


@endsection
