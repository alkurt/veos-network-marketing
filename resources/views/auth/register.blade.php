@extends('layouts.app')
@section('content')
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <div class="container" >
            <div class="modal fade"style="
                                background: rgba(0,0,0,.8);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.5);
                                border-radius: 10px;

                          " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"style="
                                background: rgba(0,0,0,.8);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.5);
                                border-radius: 10px;
                                color: white;
                            " id="exampleModalLabel">&nbsp;Bilgilendirme&nbsp;</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           <p>Lütfen Aktif Kullandığınız e-Posta Adresinizi Yazınız.</p>
                            <p>Şifre Sıfırlamada ve Firma Bilgilendirmede İletişim Halinde Olmanız Amaçlı Güncel e-Postanızı Yazmanızı Tavsiye Ederiz</p>
                            <strong style="font-size: large;font-family: 'Harlow Solid Italic';margin-left: 300px">VeosNet</strong>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row justify-content-center ">
                    <div class="col-md-8 col-sm-12" style="
                                background: rgba(0,0,0,.8);
                                box-sizing: border-box;
                                box-shadow: 0 15px 25px rgba(0,0,0,.5);
                                border-radius: 10px;
                                color:white;
                            ">
                         <h4 class="mt-2 ">VeosNet<button type="button" class="btn btn-outline-warning size-sm mx-2 " style="font-size: x-small" data-toggle="modal" data-target="#exampleModal">?</button></h4>
                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" accept-charset="UTF-8">
                            @csrf
                            <div class="row" >
                              <div class="col-md-6 col-sm-6">
                                  <div class="form-group text-center mt-2">
                                      <div class="form-check form-check-inline">
                                          <label class="form-check-label text-white mx-3" for="Bayi">Bayi</label>
                                          <input class="form-check-input" type="radio" name="type" id="Bayi" value="b" checked>
                                      </div>
                                      <div class="form-check form-check-inline">
                                          <label class="form-check-label text-white mx-3" for="Muşteri">Müşteri</label>
                                          <input class="form-check-input" type="radio" name="type" id="Muşteri" value="m">

                                      </div>
                                  </div>
                                  <div class="form-group row align-items-center">
                                      <label class="col-md-4 col-form-label text-md-right"  for="sponsor_id">Sponsor Numaranız</label>
                                      <div class="col-md-6 col-sm-12">
                                          <input id="sponsor_id" type="text"  onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'
                                                 class="form-control {{ $errors->has('sponsor_id') ? ' is-invalid' : '' }}"
                                                 name="sponsor_id" value="{{old('sponsor_id')}}" maxlength="5" minlength="5"  required>

                                          @if ($errors->has('sponsor_id'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sponsor_id') }}</strong>
                                    </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row" >
                                      <label for="kimlik" class="col-md-4 col-form-label text-md-right">{{ __('Tc Kimlik Nu*') }}</label>

                                      <div class="col-md-6 col-sm-12" >
                                          <input id="kimlik" type="text" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'
                                                 class="form-control{{$errors->has('kimlik') ? ' is-invalid' : '' }}"
                                                 name="kimlik" value="{{old('kimlik')}}" maxlength="11" minlength="11" required>

                                          @if ($errors->has('kimlik'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('kimlik') }}</strong>
                                    </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label for="name" class="col-md-4 col-sm-12 col-form-label text-md-right">{{ __('İsim*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <input id="name" type="text"
                                                 class="form-control  {{$errors->has('name') ? ' is-invalid' : '' }}"
                                                 name="name" value="{{old('name')}}" style="text-transform: lowercase;" required  >

                                          @if ($errors->has('name'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{$errors->first('name')}}</strong>
                                    </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Soyisim*') }}</label>

                                      <div class="col-md-6 col-sm-12 ">
                                          <input id="surname" type="text"
                                                 class="form-control  {{$errors->has('surname') ? ' is-invalid' : '' }}"
                                                 name="surname" value="{{old('surname')}}" style="text-transform: lowercase;" required>

                                          @if ($errors->has('surname'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row ">
                                      <label for="dogum" class="col-md-4 col-sm-12 col-form-label text-md-right zipcode-number">{{ __('Doğum Yılı*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <input id="dogum" type="text" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'
                                                 class="form-control{{ $errors->has('dogum') ? ' is-invalid' : '' }}"
                                                 name="dogum" value="{{ old('dogum') }}" placeholder="19 - -" maxlength="4" minlength="4"  required autofocus>

                                          @if ($errors->has('dogum'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dogum') }}</strong>
                                    </span>
                                          @endif

                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6 col-sm-6">
                                  <div class="form-group row ">
                                      <label for="ulke" class="col-md-4 col-form-label text-md-right">{{ __('Ülkeniz*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <select  type="text"
                                                   class="form-control {{ $errors->has('ulke') ? ' is-invalid' : '' }}"
                                                   name="ulke" value="{{ old('ulke') }}" required autofocus>
                                              <option value="Turkey">Turkey</option>
                                              <option disabled value="Almanya">Almanya</option>
                                              <option disabled value="Azerbaycan">Azerbaycan</option>
                                              <option disabled value="Avusturya">Avusturya</option>
                                              <option disabled value="Fransa">Fransa</option>
                                              <option disabled value="Hollanda">Hollanda</option>
                                              <option disabled value="İsviçre">İsviçre</option>
                                              <option disabled value="Kırgızistan">Kırgızistan</option>
                                              <option disabled value="Rusya">Rusya</option>
                                              <option disabled value="Belçika">Belçika</option>
                                              <option disabled value="Bulgaristan">Bulgaristan</option>
                                              <option disabled value="Danimarka">Danimarka</option>
                                              <option disabled value="Gürcistan">Gürcistan</option>
                                              <option disabled  value="İtalya">İtalya</option>
                                              <option disabled value="Kazakistan">Kazakistan</option>
                                              <option disabled value="Özbekistan">Özbekistan</option>
                                          </select>
                                          @if ($errors->has('ulke'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ulke') }}</strong>
                                    </span>
                                          @endif

                                      </div>
                                  </div>
                                  <div class="form-group row ">
                                      <label for="iller" class="col-md-4 col-form-label text-md-right">{{ __('Şehir*') }}</label>
                                      <div class="col-md-6 col-sm-12">
                                          <select  type="text"
                                                   class="form-control {{ $errors->has('iller') ? ' is-invalid' : '' }}"
                                                   name="iller" value="{{ old('iller') }}" required autofocus>
                                              <option value="1">Adana</option>
                                              <option value="2">Adıyaman</option>
                                              <option value="3">Afyonkarahisar</option>
                                              <option value="4">Ağrı</option>
                                              <option value="5">Amasya</option>
                                              <option value="6">Ankara</option>
                                              <option value="7">Antalya</option>
                                              <option value="8">Artvin</option>
                                              <option value="9">Aydın</option>
                                              <option value="10">Balıkesir</option>
                                              <option value="11">Bilecik</option>
                                              <option value="12">Bingöl</option>
                                              <option value="13">Bitlis</option>
                                              <option value="14">Bolu</option>
                                              <option value="15">Burdur</option>
                                              <option value="16">Bursa</option>
                                              <option value="17">Çanakkale</option>
                                              <option value="18">Çankırı</option>
                                              <option value="19">Çorum</option>
                                              <option value="20">Denizli</option>
                                              <option value="21">Diyarbakır</option>
                                              <option value="22">Edirne</option>
                                              <option value="23">Elazığ</option>
                                              <option value="24">Erzincan</option>
                                              <option value="25">Erzurum</option>
                                              <option value="26">Eskişehir</option>
                                              <option value="27">Gaziantep</option>
                                              <option value="28">Giresun</option>
                                              <option value="29">Gümüşhane</option>
                                              <option value="30">Hakkâri</option>
                                              <option value="31">Hatay</option>
                                              <option value="32">Isparta</option>
                                              <option value="33">Mersin</option>
                                              <option value="34">İstanbul</option>
                                              <option value="35">İzmir</option>
                                              <option value="36">Kars</option>
                                              <option value="37">Kastamonu</option>
                                              <option value="38">Kayseri</option>
                                              <option value="39">Kırklareli</option>
                                              <option value="40">Kırşehir</option>
                                              <option value="41">Kocaeli</option>
                                              <option value="42">Konya</option>
                                              <option value="43">Kütahya</option>
                                              <option value="44">Malatya</option>
                                              <option value="45">Manisa</option>
                                              <option value="46">Kahramanmaraş</option>
                                              <option value="47">Mardin</option>
                                              <option value="48">Muğla</option>
                                              <option value="49">Muş</option>
                                              <option value="50">Nevşehir</option>
                                              <option value="51">Niğde</option>
                                              <option value="52">Ordu</option>
                                              <option value="53">Rize</option>
                                              <option value="54">Sakarya</option>
                                              <option value="55">Samsun</option>
                                              <option value="56">Siirt</option>
                                              <option value="57">Sinop</option>
                                              <option value="58">Sivas</option>
                                              <option value="59">Tekirdağ</option>
                                              <option value="60">Tokat</option>
                                              <option value="61">Trabzon</option>
                                              <option value="62">Tunceli</option>
                                              <option value="63">Şanlıurfa</option>
                                              <option value="64">Uşak</option>
                                              <option value="65">Van</option>
                                              <option value="66">Yozgat</option>
                                              <option value="67">Zonguldak</option>
                                              <option value="68">Aksaray</option>
                                              <option value="69">Bayburt</option>
                                              <option value="70">Karaman</option>
                                              <option value="71">Kırıkkale</option>
                                              <option value="72">Batman</option>
                                              <option value="73">Şırnak</option>
                                              <option value="74">Bartın</option>
                                              <option value="75">Ardahan</option>
                                              <option value="76">Iğdır</option>
                                              <option value="77">Yalova</option>
                                              <option value="78">Karabük</option>
                                              <option value="79">Kilis</option>
                                              <option value="80">Osmaniye</option>
                                              <option value="81">Düzce</option>
                                          </select>
                                          @if ($errors->has('ulke'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ulke') }}</strong>
                                    </span>
                                          @endif

                                      </div>
                                  </div>
                                  <div class="form-group row align-items-center">
                                      <label for="telefon" class="col-md-4 col-sm-12 col-form-label text-md-right">{{ __('Cep Numaranız*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <input id="telefon" type="tel" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'
                                                 class="form-control{{ $errors->has('telefon') ? ' is-invalid' : '' }}"
                                                 name="telefon" value="{{ old('telefon') }}" placeholder="0(5--) (---) (---)" maxlength="11" minlength="11" required autofocus>

                                          @if ($errors->has('telefon'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefon') }}</strong>
                                    </span>
                                          @endif

                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label for="email" class="col-md-4 col-sm-12 col-form-label text-md-right">{{ __('e-mail*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <input id="mail" type="email"
                                                 class="form-control{{ $errors->has('mail') ? ' is-invalid' : '' }}"
                                                 name="email" value="{{old('mail')}}" required autofocus>

                                          @if ($errors->has('mail'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mail') }}</strong>
                                    </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <label for="password"
                                             class="col-md-4 col-form-label text-md-right">{{ __('Şifreniz*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <input id="password" type="password"
                                                 class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                 name="password" minlength="6"  required>

                                          @if ($errors->has('password'))
                                              <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row  align-items-center">
                                      <label for="password-confirm"
                                             class="col-md-4 col-form-label text-md-right">{{ __('Şifrenizi Doğrulayın*') }}</label>

                                      <div class="col-md-6 col-sm-12">
                                          <input id="password-confirm" type="password" class="form-control"
                                                 name="password_confirmation" required>
                                      </div>
                                  </div>
                              </div>
                                <div class=" col-md-6 col-sm-12">
                                    <input required  id="sozlesme" type="checkbox"> <a  style="color: white; font-weight: bold" href="{{route("bayi")}}">Üyelik Sözleşmesi</a><label for="sozlesme"><strong class="text-white">'ni okudum ve kabul ediyorum.</strong></label>
                                </div>
                                <div class="col-md-4 col-sm-12 mb-4 mx-4 ">
                                    <button type="submit" class="btn btn-lg w-50 btn-outline-light text-secondary font-weight-bold" style="float:right">Kaydol</button>
                                </div>
                          </div>
                        </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
    @endsection
