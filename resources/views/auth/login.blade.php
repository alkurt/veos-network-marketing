@extends('layouts.app')
@section('content')
        <div class="box mb-3" style="margin-top: 60px">

        <h2>VeosNet</h2>
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        <div class="form-group form-check-inline text-center col-sm-12">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="type" id="bayi" value="b" checked>
                                <label  class="form-check-label text-white mx-3" for="bayi">Bayi</label>
                            </div>
                            <div class="form-check form-check-inline ">
                                <input class="form-check-input" type="radio" name="type" id="musteri" value="m">
                                <label class="form-check-label text-white mx-3" for="musteri">Müşteri</label>
                            </div>
                        </div>

                        @csrf
                        <div class="form-group row inputBox">
                                <input id="id" type="text" onkeypress='return event.keyCode >= 48 && event.keyCode <= 57'  class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ old('id') }}" minlength="5" maxlength="5" required autofocus>
                            <label for="id" >Kullanıcı Numaranız...</label>
                                @if ($errors->has('id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        <div class="form-group row inputBox">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <label for="password" >Şifreniz...</label>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg w-50 btn-outline-light text-secondary font-weight-bold"style="float:right">Giriş Yap
                            </button>
                           <div class="row">
                               <a class="btn btn-link text-white" href="{{ route('password.request') }}">
                                   <p>Şifremi Hatırlamıyorum?</p>
                               </a>
                           </div>
                        </div>
                    </form>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $(".number").keydown(function (e) {
                //  backspace, delete, tab, escape, enter and vb tuşlara izin vermek için.
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    //  Ctrl+A Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                    //  Ctrl+C Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                    //  Ctrl+X Tuş kobinasyonuna izin vermek için.
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                    // home, end, left, right gibi tuşlara izin vermek için.
                    (e.keyCode >= 35 && e.keyCode <= 39)) {

                    return;
                }
                // Basılan Tuş takımının Sayısal bir değer taşıdığından emin olmak için.
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
    @endsection
