@extends('layouts.main')
@section("content")
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('css/menu.css')}}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <div class="container"style="margin-top: 170px" >
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3 col-sm-12  ">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div style="background: #1c7430" class="panel-heading">
                                <h4 class="panel-title">

                                    <a class="fa fa-step-backward" style="color: white"  href="{{route('menu')}}"></a> <a style="color: #EFEFEF"> PRİM MENÜ</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body ">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <span class="glyphicon glyphicon-eye-open text-primary"></span><a
                                                        href="{{route('menu')}}">Genel Bakış</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-users text-success"> </span>
                                                <a href="{{route("kazanc")}}"> Ekibim</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-money text-info"></span>
                                                <a href="{{route('gelirlerim')}}" > Gelirlerim</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-google-wallet text-success"></span><a
                                                        href="{{route("cuzdan")}}"> Cüzdanım</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-safari text-success"></span><a
                                                        href="{{route("isteklerim")}}"> İsteklerim</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="glyphicon glyphicon-stats text-success"></span><a
                                                        href="{{route("kazancozeti")}}">Kazanç Özetim</a>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fa fa-server text-success"></span><a href="{{route("hesap")}}">
                                                    Hesap Menü</a>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <h4 class="text-center mb-5" style="font-family: 'Harlow Solid Italic'" >EKİP LİSTESİ EKRANI</h4>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th></th>
                                    <th>Yatıran Kişi </th>
                                    <th class="text-center">Yatırılan Tutar</th>
                                    <th class="text-center">Yatırılma Tarihi</th>
                                </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section("customJs")
@endsection

@section("customCss")
@endsection
