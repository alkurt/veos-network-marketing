@extends('layouts.main')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/categories_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/styles/categories_responsive.css')}}">
@endsection
@section('meta')
    <meta type="keywords" content="Ürünler,Arama">
@endsection
@section('title','VeosNet Kategori Ürün Sayfası || VeosNet Network&Marketing')
@section('content')
    <div class="container product_section_container">
        <div class="row">
            <div class="col product_section clearfix">
                <!-- Breadcrumbs -->

                <!-- Sidebar -->
                <!-- Main Content -->
                <div class="main_content">
                    <!-- Products -->
                    <div class="products_iso">
                        <div class="row">
                            <div class="col">
                                @if(isset($details))
                                    <p class="mt-3" style="font-size: 16px"><b> {{ $query }} </b><small class="text-secondary">araması için bulunan ürünler</small></p>
                                    <h4 style="font-family: 'Harlow Solid Italic'" class="text-center" >ÜRÜN DETAYLARI</h4>
                                    <div class="product-grid"
                                         data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
                                        @foreach($details as $product)
                                            <a href="/product/{{$product->slug}}">
                                                <div class="product-item {{$product->id}} ">
                                                    <div class="product discount product_filter">
                                                        <div class="product_image"style="margin-top: -22px;margin-left: 7px" >
                                                            {!! $product->thumbs !!}
                                                        </div>
                                                        <div class="product_info">
                                                            <strong class="product_name"style="margin-top: -2px"><a class="text-secondary mt-2" href="/product/{{$product->slug}}">{{ $product->product_name }}</a></strong><br>
                                                            <small><a class="text-secondary mb-1" href="/product/{{$product->slug}}">{{ $product->code }}</a><i class="mx-1 fa fa-search"></i></small>
                                                            <div class="row mb-1">
                                                                <div class="col-md-4 col-sm-4 product_price" style="font-size: 12px">{{ number_format($product->dolar) }} <small>$</small></div>
                                                                <div class="col-md-4 col-sm-4 product_price" style="font-size:12px">{{ number_format($product->kisiself) }} <small>₺</small></div>
                                                                <div class="col-md-4 col-sm-4 product_price " style="font-size: 12px">{{ number_format($product->euro) }} <small>€</small> </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="add_to_cart_button red_button"><a href="{{ route('basket.create', ['id' => $product->id]) }}">Sepete Ekle</a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </a>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Benefit -->
    <div class="benefit">
        <div class="container">
            <div class="row ">
                <div class=" item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-truck"></i></span>
                    <p>300 ₺ ve Üzeri Alışverişlerde</p> <p class="text-dark">Ücretsiz Kargo</p>
                </div>
                <div class="item mx-3"><span class="icon feature_box_col_two"><i class="fa fa-database"></i></span>
                    <h6>VeosNet </h6>
                    <p>Kazanıyor, Kazandırıyor...</p>
                </div>
                <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-mail-forward"></i></span>
                    <h6>Geri Dönüşlerinizi Bekliyoruz</h6>
                    <p>Bizim İçin Değerlisiniz... </p>
                </div>
                <div class="mx-3 item"><span class="icon feature_box_col_two"><i class="fa fa-hourglass-1"></i></span>
                    <p>Açılış Saatimiz 09:00</p>
                    <p>Kapanış Saatimiz 18:00</p>
                </div>
            </div>
        </div>
    </div>
    <style>
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {}
        a,
        a:hover,
        a:focus,
        a:active {
            text-decoration: none;
            outline: none;
        }

        a,
        a:active,
        a:focus {
            color: #333;
            text-decoration: none;
            transition-timing-function: ease-in-out;
            -ms-transition-timing-function: ease-in-out;
            -moz-transition-timing-function: ease-in-out;
            -webkit-transition-timing-function: ease-in-out;
            -o-transition-timing-function: ease-in-out;
            transition-duration: .2s;
            -ms-transition-duration: .2s;
            -moz-transition-duration: .2s;
            -webkit-transition-duration: .2s;
            -o-transition-duration: .2s;
        }

        ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        img {
            max-width: 100%;
            height: auto;
        }
        span, a, a:hover {
            display: inline-block;
            text-decoration: none;
            color: inherit;
        }
        .section-head {
            margin-bottom: 60px;
        }
        .section-head h4 {
            position: relative;
            padding:0;
            color:#f91942;
            line-height: 1;
            letter-spacing:0.3px;
            font-size: 34px;
            font-weight: 700;
            text-align:center;
            text-transform:none;
            margin-bottom:30px;
        }
        .section-head h4:before {
            content: '';
            width: 60px;
            height: 3px;
            background: #f91942;
            position: absolute;
            left: 0px;
            bottom: -10px;
            right:0;
            margin:0 auto;
        }
        .section-head h4 span {
            font-weight: 700;
            padding-bottom: 5px;
            color:#2f2f2f
        }
        p.service_text{
            color:#cccccc !important;
            font-size:16px;
            line-height:28px;
            text-align:center;
        }
        .section-head p, p.awesome_line{
            color:#818181;
            font-size:16px;
            line-height:28px;
            text-align:center;
        }

        .extra-text {
            font-size:34px;
            font-weight: 700;
            color:#2f2f2f;
            margin-bottom: 25px;
            position:relative;
            text-transform: none;
        }
        .extra-text::before {
            content: '';
            width: 60px;
            height: 3px;
            background: #f91942;
            position: absolute;
            left: 0px;
            bottom: -10px;
            right: 0;
            margin: 0 auto;
        }
        .extra-text span {
            font-weight: 700;
            color:#f91942;
        }
        .item {
            background: #fff;
            text-align: center;
            padding: 30px 25px;
            -webkit-box-shadow:0 0px 25px rgba(0, 0, 0, 0.07);
            box-shadow:0 0px 25px rgba(0, 0, 0, 0.07);
            border-radius: 20px;
            border:5px solid rgba(0, 0, 0, 0.07);
            margin-bottom: 30px;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover{
            background:#f91942;
            box-shadow:0 8px 20px 0px rgba(0, 0, 0, 0.2);
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover .item, .item:hover span.icon{
            background:#fff;
            border-radius:10px;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item:hover h6, .item:hover p{
            color:#fff;
            -webkit-transition: all .5s ease 0;
            transition: all .5s ease 0;
            transition: all 0.5s ease 0s;
        }
        .item .icon {
            font-size: 40px;
            margin-bottom:25px;
            color: #f91942;
            width: 90px;
            height: 90px;
            line-height: 96px;
            border-radius: 50px;
        }
        .item .feature_box_col_one{
            background:rgba(247, 198, 5, 0.20);
            color:#f91942
        }
        .item .feature_box_col_two{
            background:rgba(255, 77, 28, 0.15);
            color:#f91942
        }
        .item .feature_box_col_three{
            background:rgba(0, 147, 38, 0.15);
            color:#f91942
        }
        .item .feature_box_col_four{
            background:rgba(0, 108, 255, 0.15);
            color:#f91942
        }
        .item .feature_box_col_five{
            background:rgba(146, 39, 255, 0.15);
            color:#f91942
        }
        .item .feature_box_col_six{
            background:rgba(23, 39, 246, 0.15);
            color:#f91942
        }
        .item p{
            font-size:15px;
            line-height:26px;
        }
        .item h6 {
            margin-bottom:20px;
            color:#2f2f2f;
        }
        .mission p {
            margin-bottom: 10px;
            font-size: 15px;
            line-height: 28px;
            font-weight: 500;
        }
        .mission i {
            display: inline-block;
            width: 50px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            background: #f91942;
            border-radius: 50%;
            color: #fff;
            font-size: 25px;
        }
        .mission .small-text {
            margin-left: 10px;
            font-size: 13px;
            color: #666;
        }
        .skills {
            padding-top:0px;
        }
        .skills .prog-item {
            margin-bottom: 25px;
        }
        .skills .prog-item:last-child {
            margin-bottom: 0;
        }
        .skills .prog-item p {
            font-weight: 500;
            font-size: 15px;
            margin-bottom: 10px;
        }
        .skills .prog-item .skills-progress {
            width: 100%;
            height: 10px;
            background: #e0e0e0;
            border-radius:20px;
            position: relative;
        }
        .skills .prog-item .skills-progress span {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            background: #f91942;
            width: 10%;
            border-radius: 10px;
            -webkit-transition: all 1s;
            transition: all 1s;
        }
        .skills .prog-item .skills-progress span:after {
            content: attr(data-value);
            position: absolute;
            top: -5px;
            right: 0;
            font-size: 10px;
            font-weight:600;
            color: #fff;
            background:rgba(0, 0, 0, 0.9);
            padding: 3px 7px;
            border-radius: 30px;
        }
    </style>
@endsection
@section('js')

    <script>
        $('.add_to_cart_button').find('a').click(function (event) {
            event.preventDefault();
            var quantity = $(this).parent().prev().find('input').val();
            var max= $(this).parent().prev().find('input').attr('max');
            max = parseInt(max);
            quantity = parseInt(quantity);
            if(quantity > max) {
                Swal.fire({
                    icon: 'error',
                    title: 'Uyarı',
                    text: 'Girdiğiniz ürün miktarı ürün stoğunu aşmaktadır.',
                    footer: '<span class="alert alert-primary w-auto font-weight-bold"> Ürün stok durumu :'+ max +' ürün mevcut</span>'
                })
            }else{
                $.ajax({
                    type: "POST",
                    url: $(this).attr('href'),
                    data: {quantity: quantity}
                    , success: function (data) {
                        console.log(data);
                        $('#checkout_items').html(data.cartCount);
                    }
                });
                return false; //for good measure
            }
        });
    </script>
    <script src="{{asset('assets/plugins/jquery-ui-1.12.1.custom/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/js/categories_custom.js')}}"></script>
@endsection

