<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Haberler extends Model
{
    public $fillable=['haber_basligi','haber_icerik','url'];
    public function getImageAttribute()
    {
        return $this->image;
    }
}
