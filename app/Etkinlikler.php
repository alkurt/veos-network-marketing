<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etkinlikler extends Model
{
    protected $table= "etkinliklers";
    public function images()
    {
        return $this->morphMany("App\Images","imageable");
    }
    protected $appends = ["thumbs"];
    public function getThumbsAttribute()
    {
        $images = asset("uploads/thumb_".$this->images()->first()->name);
        return '<img src="' .$images.'" class="img-thumbnail" width="100" />';
    }
}
