<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banka extends Model
{
    protected $fillable=['banka_adi','hesap_sahibi','sube_kodu','sube_adi','hesap_no','iban_no'];
}
