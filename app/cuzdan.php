<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cuzdan extends Model
{
    protected $fillable=['user_id','cuzdan_brut','odenen_bakiye','toplam_bakiye','arac_bakiye'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
