<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilCreate extends Model
{
    //
    protected $fillable=['status','cover','user_id','uuid'];
}
