<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SponsorDerinlik extends Model
{
    //
    protected $fillable =['derinlik','yuzde','kariyer_id'];
    public function kariyer(){
        return $this->belongsTo(Kariyer::class);
    }
    public function getYuzdeAttribute(){
        return number_format( $this->attributes['yuzde'],2);
    }
}
