<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iller extends Model
{
    //
    protected $fillable =['name','bolge_id'];

    public function Bolge(){
       return $this->belongsTo(Bolge::class);
    }
    public function Users(){
        return $this->hasMany(User::class);
    }
}
