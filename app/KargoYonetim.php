<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KargoYonetim extends Model
{
  protected $fillable=['kargo_adi','kargo_fiyati','kargo_durumu'];
}
