<?php

namespace App;

use App\Notifications\LarashopAdminResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;


    class User extends Authenticatable
    {
        use Notifiable;

        /**
         * Send a password reset email to the user
         */
//        public function sendPasswordResetNotification($token)
//        {
//            $this->notify(new LarashopAdminResetPassword($token));
//        }
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        public $incrementing = false;
        protected $fillable = [
           'bolge_id','iller_id','ulke','id','status','durum','name', 'surname','kimlik','dogum','telefon', 'email', 'password','sponsor_id','root_id','konum','kariyer_id','pv','ara_pv','cv','user_id'
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];
        protected static function boot()
        {
            parent::boot();
            static::addGlobalScope('status', function (Builder $builder) {
                $builder->where('status',  1);
            });
        }

        public function Konumbul($konum){
            return $this->konum ==$konum;
        }

        public function roles()
        {
            return $this->belongsToMany("App\Role","role_user");
        }
        public function kariyer()
        {
            return $this->belongsTo(Kariyer::class);
        }

        public function orders(){
            return $this->hasMany(Order::class);
        }

        public function eft(){
            return $this->hasMany(EftHavaleBekleme::class);
        }
    public function mailorder(){
        return $this->hasMany(MailOrder::class);
    }
        public function odeme_talebi(){
            return $this->hasMany(OdemeTalebi::class);
        }
        public function baskets(){
            return $this->hasMany(Basket::class);
        }
        public function cuzdan(){
            return $this->hasOne(cuzdan::class);
        }
        public function nakitindirim(){
            return $this->hasOne(Nakitindirim::class);
        }
        public function aylikkariyer(){
            return $this->hasMany(AylikKariyerModel::class);
        }
        public function tazminat(){
            return $this->hasOne(Tazminat::class);
        }
        public function gider(){
           return  $this->hasOne(Gider::class);
        }

        public function iller(){
            return $this->belongsTo(Iller::class);
        }
        public function Bolge(){
            return $this->belongsTo(Bolge::class);
        }
        public function isItAuthorized($authorization)
        {
            foreach ($this->roles()->get() as $role) {
                    if ($role->name == $authorization)
                {
                    return true;
                    break;
                }
            }
            return false;
        }
        public function detail()
        {
            return $this->hasOne('App\UserDetail')->withDefault();
        }

        public function binary(){
            return $this->hasMany(EslesmeBinary::class,'kullanici_id','id');
        }
        public function gelirs(){
            return $this->hasMany(Gelir::class);
        }
    public function gelenpv(){
        return $this->hasMany(Gelenpv::class);
    }
        public function getFullNameAttribute(){
            return $this->name.' '.$this->surname;
        }
        public function Il(){
            return $this->belongsTo(Iller::class);
        }


        public function getIlCiroAttribute(){
            $il_ciro = 0;
            $il = Iller::find($this->iller_id);
            foreach ($il->users as $user){
                $aylik_kasa = $user->orders()->whereMonth('created_at',date('m'))->get();
                foreach ($aylik_kasa as $order){
                    $il_ciro += $order->order_price;
                }
            }
            return $il_ciro;
        }
        public function getBolgeCiroAttribute(){
            $bolge_ciro = 0;
            $bolge = Bolge::find($this->bolge_id);
            foreach ($bolge->users as $user){
                $aylik_kasa = $user->orders()->whereMonth('created_at',date('m'))->get();
                foreach ($user->orders as $order){
                    $bolge_ciro += $order->order_price;
                }
            }
            return $bolge_ciro;
        }
        public function getTurkiyeCiroAttribute(){
            $ulke_ciro = 0;
           $users = User::where('ulke','Turkey')->get();
            foreach ($users as $user){
                $aylik_kasa = $user->orders()->whereMonth('created_at',date('m'))->get();
                foreach ($user->orders as $order){
                    $ulke_ciro += $order->order_price;
                }
            }
            return $ulke_ciro;
        }

    }
