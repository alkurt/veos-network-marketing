<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nakitindirim extends Model
{
    public $fillable = ['user_id','indirim_tutar','status'];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
