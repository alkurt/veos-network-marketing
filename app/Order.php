<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = "orders";

    protected $guarded = [];

    public function baskets()
    {
        return $this->belongsTo('App\Basket', 'basket_id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function eft(){
        return $this->belongsTo(EftHavaleBekleme::class,'order_no','id');
    }
}
