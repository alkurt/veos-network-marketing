<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiderCikarma extends Model
{
    protected $fillable = ['kariyer_id','tutar'];

    public function user(){
        return $this->belongsTo(Kariyer::class);
    }
}
