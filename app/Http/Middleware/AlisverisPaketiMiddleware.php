<?php

namespace App\Http\Middleware;

use Closure;

class AlisverisPaketiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->kariyer_id == 17){
            session()->flash('error','Yetkiniz yok');
            return back();
        }
        return $next($request);
    }
}
