<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UyelikRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'dogum'=>'required|string|max:4|min:4',
            'kimlik' => 'required|integer|unique:users',
            'telefon' => 'required|string|max:11|min:11',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Bu Alan Gereklidir',
            'surname.required'=>'Bu Alan Gereklidir',
            'dogum.required'=>'Bilgileriniz Doğrulanamadı',
            'kimlik.unique' => '  Bu :attribute ile kayıt daha önce alınmış.',
            'telefon.required'=>'Lütfen 11  haneli numaranızı giriniz.',
            'email.unique'=>'Bu email adresi ile daha önce kayıt yapılmış.',
            'password.confirmed'=>'Girilen şifre tekrarıyla uyuşmuyor',
        ];
    }
}
