<?php

namespace App\Http\Controllers;

use App\Category;
use App\cuzdan;
use App\KargoYonetim;
use App\Kasa;
use App\OdemeTalebi;
use App\User;
use Illuminate\Http\Request;

class OdemeTalebiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view('admin.adminodeme', compact( 'categoryMenu','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $musteri = auth()->user()->cuzdan->toplam_bakiye;
        $status=OdemeTalebi::where('user_id',auth()->user()->id)->get();
        return view("layouts.musteri_odeme_talebi",compact('categoryMenu','musteri','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages=[
            'odeme_talebi.required' => 'Bu alan gereklidir. ',
            'odeme_talebi.numeric' => 'Girilen rakam harici değer içeremez. ',

        ];
        $this->validate($request,
            [
                "odeme_talebi" => "required",
                "odeme_talebi" => "numeric",

            ],$messages);
        $this->validate($request,$messages);
        $input = $request->only( 'odeme_talebi');
        if ($request->odeme_talebi < 0)
        {
            Session()->flash('error',"Ödeme Talebi Sıfırın Altında Olamaz. ");
            return redirect(route('odemetalebi.create'));
        }
            if (auth()->user()->cuzdan->toplam_bakiye <= 0)
            {
                Session()->flash('error','Talep İçin Cüzdan Bakiyeniz Yetersiz!!.');
                return redirect(route('odemetalebi.create'));
            }
            if ($request->odeme_talebi > auth()->user()->cuzdan->toplam_bakiye)
            {
                Session()->flash('error','Bakiye Tutarınızdan Fazla Tutarda Talepte Bulunamazsınız.');
                return redirect(route('odemetalebi.create'));
            }

            //--------------------------------------------
        if ( OdemeTalebi::where('user_id',auth()->user()->id)->where('status',1)->exists())
        {
            Session()->flash('error','Beklemede Olan Mevcut Bir Talebiniz Var Lütfen Onaylandıktan Sonra Tekrar Deneyiniz !! ');
            return redirect(route('odemetalebi.create'));
        }
            else {
                $o_acan=auth()->user();
                $o_acan->odeme_talebi()->create($input);
                Session()->flash('success', 'Talebiniz Başarıyla Alınmıştır.');
                return redirect(route('menu'));
            }

            Session()->flash('error','Talep Oluşturulamadı.');
            return redirect(route('odemetalebi.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OdemeTalebi  $odemeTalebi
     * @return \Illuminate\Http\Response
     */
    public function show(OdemeTalebi $odemeTalebi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OdemeTalebi  $odemeTalebi
     * @return \Illuminate\Http\Response
     */
    public function edit(OdemeTalebi $odemeTalebi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OdemeTalebi  $odemeTalebi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OdemeTalebi $odemeTalebi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OdemeTalebi  $odemeTalebi
     * @return \Illuminate\Http\Response
     */
    public function destroy(OdemeTalebi $odemeTalebi)
    {
        //
    }
}
