<?php

namespace App\Http\Controllers;

use App\Tazminat;
use Illuminate\Http\Request;

class TazminatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tazminat  $tazminat
     * @return \Illuminate\Http\Response
     */
    public function show(Tazminat $tazminat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tazminat  $tazminat
     * @return \Illuminate\Http\Response
     */
    public function edit(Tazminat $tazminat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tazminat  $tazminat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tazminat $tazminat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tazminat  $tazminat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tazminat $tazminat)
    {
        //
    }
}
