<?php

namespace App\Http\Controllers;

use App\Category;
use App\KargoYonetim;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("contact",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            "name" => "required",
            "surname" => "required",
            "email" => "required",
            "message" => "required",
        ];
        $messages=[
            'name.required' => 'Bu alan gereklidir. ',
            'surname.required' => 'Bu alan gereklidir.',
            'email.required' => 'Bu alan gereklidir.',
            'message.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'name','surname', 'email','message');
        if($Message=Message::create($input))
        {
            Session()->flash('success','Mesajınız Başarıyla Alınmıştır');
            return redirect(route('contact'));
        }
        else{
            Session()->flash('error','Mesajınız Alınamadı !!.');
            return redirect(route('contact'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $Message=Message::find($id);
        return view('contact',compact('categoryMenu','Message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
