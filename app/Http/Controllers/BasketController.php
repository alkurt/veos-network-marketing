<?php

namespace App\Http\Controllers;

use App\Basket;
use App\BasketProduct;
use App\Category;
use App\EftHavaleBekleme;
use App\Kariyer;
use App\Kariyerb;
use App\Order;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use http\Client\Curl\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $urunler=BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get();
        $oturum=\auth()->user()->kariyer->kariyerb->indirim;
        $urn = false ;
        $paket = false ;

       foreach ($urunler as $urun){
            if( $urun->product->turu == 'paket'){
                $paket = true ;
            }
            if ($urun->product->turu == 'urun')
            {
                $urn =true ;
            }
            if ($urn==true && $paket==true)
            {
                session()->flash('error','Sepetinizde Sadece Bayi Ürünü Veya Diğer Ürün Türü Bulundurabilirsiniz.');
                Cart::destroy();
                session()->forget('active_basket_id');
                session()->forget('order_no');
                return back();
            }
        }
        if ($user->kariyer_id >=1 && $user->kariyer_id <=4)
        {
            $kariyer=Kariyer::where('id','>',\auth()->user()->kariyer_id)->where('id','<=', 5)->with('kariyerb')->get();
            $toplam=0;
            //user'ın satın almalarını topluyoruz
            $orders = $user->baskets;
            foreach ($orders as $order)
            {
                $bCount=$order->basket_products->count();
                if ($bCount != 0)
                {
                    $toplam += ($order->product_price()*$order->basket_product_qty())/$bCount;
                }
            }
            return view('basket', compact('categoryMenu','oturum','toplam','kariyer','user'));
        }
        else
        {
            return view('basket', compact('categoryMenu','oturum','urunler'));
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quantity = request('quantity');
        if ($quantity<1){
          abort(400);
        }
        $product = Product::find(request('id'));
        $cartItem = Cart::add($product->id, $product->product_name, $quantity, $product->kisiself, ['slug' => $product->slug]);

        if (Auth::check()) {
            $active_basket_id = session('active_basket_id');
            if (!isset($active_basket_id)) {
                $active_basket = Basket::create([
                    'user_id' => Auth::id()
                ]);
                $active_basket_id = $active_basket->id;
                session()->put('active_basket_id', $active_basket_id);
            }
            BasketProduct::updateOrCreate(
                ['basket_id' => $active_basket_id, 'product_id' => $product->id],
                ['quantity' => $cartItem->qty, 'price' => $product->kisiself, 'status' => 'Siparişleriniz alındı.']
            );
        }
        return response()->json(['cartCount' => Cart::count()]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($rowid)
    {
        //
        if (Auth::check()) {
            $active_basket_id = session('active_basket_id');
            $cartItem = Cart::get($rowid);

            if (request('quantity') == 0)
                BasketProduct::where('basket_id', $active_basket_id)->where('product_id', $cartItem->id)
                    ->delete();
            else
                BasketProduct::where('basket_id', $active_basket_id)->where('product_id', $cartItem->id)
                    ->update(['quantity' => request('quantity')]);
        }

        Cart::update($rowid, request('quantity'));


        return response()->json(['success' => true]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        if (Auth::check()) {
            $active_basket_id = session('active_basket_id');
            BasketProduct::where('basket_id', $active_basket_id)->delete();
        }

        Cart::destroy();

        return redirect()->route('basket');
    }
}
