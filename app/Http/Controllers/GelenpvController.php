<?php

namespace App\Http\Controllers;

use App\Category;
use App\Gelenpv;
use App\Gelir;
use App\KargoYonetim;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class GelenpvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view('layouts.gelenpv_kullanici', compact( 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gelenpv  $gelenpv
     * @return \Illuminate\Http\Response
     */
    public function show(Gelenpv $gelenpv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gelenpv  $gelenpv
     * @return \Illuminate\Http\Response
     */
    public function edit(Gelenpv $gelenpv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gelenpv  $gelenpv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gelenpv $gelenpv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gelenpv  $gelenpv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gelenpv $gelenpv)
    {
        //
    }
    public function gelenpv_datatable()
    {
        $data = Gelenpv::where('user_id',\auth()->user()->id)->get();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad =User::find($row->user_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('islem_sahibi_ad',function ($row){
                $ad2 =User::find($row->islem_sahibi_id);
                $ad2 = $ad2->name." ".$ad2->surname;
                return $ad2;
            }) ->addColumn('puan_miktari',function ($row){
                $ad =$row->puan_miktari;
                return $ad;
            })
            ->make(true);
    }
}
