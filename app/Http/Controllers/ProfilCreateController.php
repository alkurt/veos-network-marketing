<?php

namespace App\Http\Controllers;

use App\ProfilCreate;
use Illuminate\Http\Request;

class ProfilCreateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->hasFile('cover')) {

            $file =$request->cover;
            $rand = rand(1, 999999);
            $image_name = $rand.'.'.$file->extension();
            $new=$request->file('cover')->store('public/profil_creates');
            $pdf = new ProfilCreate();
            $user = auth()->user()->id;
            $pdf->image_name = $image_name;
            $pdf->user_id = $user;
            $pdf->cover = $new;
            $pdf->uuid = $rand;
            $pdf->save();
        }
        session()->flash('success','Profil Fotoğrafınız Yüklendi');
        return back();

    }




    /**
     * Display the specified resource.
     *
     * @param  \App\ProfilCreate  $profilCreate
     * @return \Illuminate\Http\Response
     */
    public function show(ProfilCreate $profilCreate)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfilCreate  $profilCreate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfilCreate $profilCreate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfilCreate  $profilCreate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfilCreate $profilCreate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfilCreate  $profilCreate
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $profilCreate = ProfilCreate::find($id);
        $profilCreate->delete();
        session()->flash('success','Profil Fotoğrafnız Silindi');
        return back();
    }
}
