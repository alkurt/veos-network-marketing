<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\OrderShipped;
use App\Mail\UserCreated;
use App\Order;
use App\ProfilCreate;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;



class UserDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $userDetails = UserDetail::where('user_id',Auth::id())->get();
        $varmi = ProfilCreate::where('user_id',\auth()->user()->id)->exists();
        $profil_create = ProfilCreate::where('user_id',\auth()->user()->id)->first();
        return view('profile',compact('userDetails','categoryMenu','varmi','profil_create'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $userDetails = UserDetail::where('user_id',$id)->first();
        return view('profile-edit',compact('userDetails','categoryMenu'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
            $user = Auth::user();
            $rules = [
                "phone" => ["required","numeric"],
                "m_phone" => ["required","numeric","min:11", Rule::unique('user_details')->ignore($user->id,'user_id')],
                "address" => "required",
                "city" => "required|alpha",
                "country" => "required|alpha",
                "zipcode" => "required|numeric",
            ];
            $messages = [
                'phone.required' => 'Bu alan boş geçilemez! ',
                'm_phone.unique' => 'Kayıtlı bir telefon numarası girdiniz! ',
                'phone.numeric' => 'Bu alan sadece sayısal değer içerir! ',
                'm_phone.required' => 'Bu Alan Boş Geçilemez!!',
                'm_phone.numeric' => 'Bu alan sadece sayısal değer içerir! ',
                'address.required' => 'Bu Alan Boş Geçilemez!!',
                'city.required' => 'Bu Alan Boş Geçilemez!!',
                'city.alpha' => 'Bu Alanı boşluksuz ve sayısal değer içermeden giriniz!',
                'country.alpha' => 'Bu Alanı boşluksuz ve sayısal değer içermeden giriniz!',
                'country.required' => 'Bu Alan Boş Geçilemez!!',
                'zipcode.required' => 'Bu Alan Boş Geçilemez!!',
                'zipcode.numeric' => 'Bu alan sadece sayısal değer içerir!',

            ];

        $this->validate($request,$rules,$messages);
        $input = $request->only('phone','m_phone','address','city','country','zipcode');
        $userDetail = User::find(Auth::id())->detail;
        $userDetail->update($input);
        Session::flash("status", 1);
        return redirect('/basket/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
