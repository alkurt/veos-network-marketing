<?php

namespace App\Http\Controllers;

use App\cuzdan;
use App\Http\Requests\UyelikRequest;
use App\Kariyerb;
use App\SponsorDerinlik;
use App\User;
use App\UserDetail;
use Cassandra\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use function App\Http\Controllers\Auth\barcodeNumberExists;
use function App\Http\Requests\UyelikRequest\strto;
use function App\Http\Requests\UyelikRequest\tcno_dogrula;

class uyelikController extends Controller
{
    public function generateBarcodeNumber() {
        function barcodeNumberExists2($number) {
            // query the database and return a boolean
            // for instance, it might look like this in Laravel
            return User::where('id',$number)->exists();
        }
        $number = mt_rand(10000, 99999); // better than rand()

        // call the same function if the barcode exists already
        if (barcodeNumberExists2($number)) {
            return generateBarcodeNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($konum,$sps)
    {
        $user = auth()->user();
        if ($user->id == '10001'){
            return view("layouts.kayityapma")->with('konum',$konum)->with('sps',$sps);
        }else{
            if ($user->id != (int) $sps) {
                $count = User::where('root_id', $user->id)->count();
                $case = (float)$count / (float)8;
                if ($case >= (float)$konum) {
                    return view("layouts.kayityapma")->with('konum', $konum)->with('sps', $sps);
                } else {
                    session()->flash('error', 'Ağacınız bu işlem için hazır değil');
                    return back();
                }
            }else {return view("layouts.kayityapma")->with('konum', $konum)->with('sps', $sps);}
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(UyelikRequest $request)
    {
        $user = auth()->user();
        if ($user->id != '10001'){
            if ($user->id != (int) $request->root_id){
                $count=User::where('root_id',$user->id)->count();
                $case = (float) $count/ (float) 8;
                if ($case >= (float) $request->konum){

                }else{
                    session()->flash('error','Ağacınız bu işlem için hazır değil');
                    return back();
                }
            }
        }
        if($user->kariyer_id == 16)
        {
            \session()->flash('error','Mevcut Bayiniz Bulunamadı,Bayi Olduktan Sonra Bayi Eklemesi Yapabilirsiniz!!');
            return back();
        }
        function st2($to, $str) {
            if($to == 'lower') {
                return mb_strtolower(str_replace(array('I','Ğ','Ü','Ş','İ','Ö','Ç'), array('ı','ğ','ü','ş','i','ö','ç'), $str), 'utf-8');
            }
            elseif($to == 'upper') {
                return mb_strtoupper(str_replace(array('ı','ğ','ü','ş','i','ö','ç'), array('I','Ğ','Ü','Ş','İ','Ö','Ç'), $str), 'utf-8');
            }
            else { trigger_error('Lütfen geçerli bir strto() parametresi giriniz.', E_USER_ERROR); }
        }

            $data =$request->only('iller','bolge','ulke','name','surname','kimlik','dogum','email','root_id','konum','password','telefon','id','type');
            if($data['type'] == 'b') {
                $data['kariyer_id']=16;
            } elseif($data['type'] == 'm') {
                $data['kariyer_id']=17;
            }
            if ($data['iller']=="34"||$data['iller']=="22"||$data['iller']=='39'||$data['iller']=='59'||$data['iller']=='41'||$data['iller']=='77'||$data['iller']=='54'||$data['iller']=='11'||$data['iller']=='16'||$data['iller']=='10'||$data['iller']=='17')
            {
                $data['bolge']=1;
            }
            elseif ($data['iller']=="68"||$data['iller']=="06"||$data['iller']=='18'||$data['iller']=='26'||$data['iller']=='70'||$data['iller']=='71'||$data['iller']=='40'||$data['iller']=='42'||$data['iller']=='50'||$data['iller']=='51'||$data['iller']=='58'||$data['iller']=='66'||$data['iller']=='38')
            {
                $data['bolge']=2;

            }
            elseif ($data['iller']=="35"||$data['iller']=="45"||$data['iller']=='9'||$data['iller']=='20'||$data['iller']=='43'||$data['iller']=='3'||$data['iller']=='64'||$data['iller']=='48')
            {
                $data['bolge']=3;

            }
            elseif ($data['iller']=='01'||$data['iller']=='80'||$data['iller']=='7'||$data['iller']=='15'||$data['iller']=='31'||$data['iller']=='32'||$data['iller']=='33'||$data['iller']=='46')
            {
                $data['bolge']=4;

            }
            elseif ($data['iller']=="53"||$data['iller']=="61"||$data['iller']=='8'||$data['iller']=='60'||$data['iller']=='19'||$data['iller']=='5'||$data['iller']=='55'||$data['iller']=='67'||$data['iller']=='14'||$data['iller']=='81'||$data['iller']=='78'||$data['iller']=='74'||$data['iller']=='37'||$data['iller']=='69'||$data['iller']=='28'||$data['iller']=='29'||$data['iller']=='52'||$data['iller']=='57')
            {
                $data['bolge']=5;

            }
            elseif ($data['iller']=="4"||$data['iller']=="75"||$data['iller']=='12'||$data['iller']=='13'||$data['iller']=='23'||$data['iller']=='24'||$data['iller']=='25'||$data['iller']=='76'||$data['iller']=='30'||$data['iller']=='36'||$data['iller']=='44'||$data['iller']=='49'||$data['iller']=='62'||$data['iller']=='65'||$data['iller']=='73')
            {
                $data['bolge']=6;

            }
            elseif ($data['iller']=="2"||$data['iller']=="72"||$data['iller']=='21'||$data['iller']=='27'||$data['iller']=='79'||$data['iller']=='47'||$data['iller']=='56'||$data['iller']=='63')
            {
                $data['bolge']=7;
            }
            $create = User::create([
                'id'=> $this->generateBarcodeNumber(),
                'name'=>strtoupper($data['name']),
                'surname' =>strtoupper($data['surname']),
                'kariyer_id'=>$data['kariyer_id'],
                'kimlik' => $data['kimlik'],
                'dogum' => $data['dogum'],
                'ulke' => $data['ulke'],
                'iller_id' => $data['iller'],
                'bolge_id' => $data['bolge'],
                'telefon' => $data['telefon'],
                'email' => $data['email'],
                'root_id' => $data['root_id'],
                'sponsor_id' => auth()->user()->id,
                'konum'=>$data['konum'],
                'password' => Hash::make($data['password']),
            ]);
            if ($create){
                Session()->flash('success','Kayıt Eklendi');
                $input = [];
                $input["user_id"] = $create->id;
                $user_detail = UserDetail::create($input);
                $user_detail->m_phone = $data['telefon'];
                $user_detail -> city = $create->iller->name;
                $user_detail -> country = $create->ulke;
                $user_detail->save();

                $create->roles()->attach(2);
                $create->cuzdan()->create();

                if(auth()->user()->id == '10001')
                {return redirect(route('binarykontrol')); }
                else
                return redirect(route('kazanc'));
            }
            else{
                Session()->flash('error','Kayıt Eklenemedi');
                if (auth()->user()->id=='10001')
                { return redirect(route('binarykontrol'));}
                else
                return redirect(route('kazanc'));
            }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
