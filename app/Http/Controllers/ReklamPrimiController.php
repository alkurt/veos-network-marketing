<?php

namespace App\Http\Controllers;

use App\ReklamPrimi;
use Illuminate\Http\Request;

class ReklamPrimiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReklamPrimi  $reklamPrimi
     * @return \Illuminate\Http\Response
     */
    public function show(ReklamPrimi $reklamPrimi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReklamPrimi  $reklamPrimi
     * @return \Illuminate\Http\Response
     */
    public function edit(ReklamPrimi $reklamPrimi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReklamPrimi  $reklamPrimi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReklamPrimi $reklamPrimi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReklamPrimi  $reklamPrimi
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReklamPrimi $reklamPrimi)
    {
        //
    }
}
