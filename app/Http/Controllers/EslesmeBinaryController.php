<?php

namespace App\Http\Controllers;

use App\EslesmeBinary;
use Illuminate\Http\Request;

class EslesmeBinaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EslesmeBinary  $eslesmeBinary
     * @return \Illuminate\Http\Response
     */
    public function show(EslesmeBinary $eslesmeBinary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EslesmeBinary  $eslesmeBinary
     * @return \Illuminate\Http\Response
     */
    public function edit(EslesmeBinary $eslesmeBinary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EslesmeBinary  $eslesmeBinary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EslesmeBinary $eslesmeBinary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EslesmeBinary  $eslesmeBinary
     * @return \Illuminate\Http\Response
     */
    public function destroy(EslesmeBinary $eslesmeBinary)
    {
        //
    }
}
