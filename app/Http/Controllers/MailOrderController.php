<?php

namespace App\Http\Controllers;

use App\AlisverisDerinlikDagilimi;
use App\AnaPara;
use App\BasketProduct;
use App\Category;
use App\EftHavaleBekleme;
use App\KargoYonetim;
use App\Kariyerb;
use App\MailOrder;
use App\Order;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MailOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       // $categoryMenu = Category::orderBy('category_name', 'asc')->get();
       // return view("layouts.MailOrder",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      /*  $rules=[
            "cardnu" => "required",
            "cardcvc" => "required",
            "cardname" => "required",
            "cardmonth" => "required",
            "cardyear" => "required",
        ];
        $messages=[
            'cardnu.required' => 'Bu Bilgiler Gereklidir',
            'cardcvc.required' => 'Bu Bilgiler Gereklidir',
            'cardname.required' => 'Bu Bilgiler Gereklidir',
            'cardyear.required' => 'Bu Bilgiler Gereklidir',
            'cardmonth.required' => 'Bu Bilgiler Gereklidir',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'cardnu','cardcvc','cardname','cardyear','cardmonth');
        if($mailorder=MailOrder::create($input))
        {
            Session()->flash('success','Ödemeniz Başarıyla Tamamlandı');
            return redirect(route('home'));
        }
        else{
            Session()->flash('error','Ödemeniz Alınamadı');
            return redirect(route('home'));
        }
      */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MailOrder  $mailOrder
     * @return \Illuminate\Http\Response
     */
    public function show(MailOrder $mailOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MailOrder  $mailOrder
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //


    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MailOrder  $mailOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            "cardnu" => "required",
            "cardname" => "required",
            "cardmonth" => "required",
            "cardyear" => "required",
            "cardcvc" => "required",
        ];
        $messages=[
            'cardnu.required' => 'Bu alan gereklidir. ',
            'cardname.required' => 'Bu alan gereklidir.',
            'cardmonth.required' => 'Bu alan gereklidir.',
            'cardyear.required' => 'Bu alan gereklidir.',
            'cardcvc.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'cardname','cardcvc', 'cardnu','cardmonth','cardyear');
        $mailorder= MailOrder::find($id);
       $mailorder->update($input);
        \session()->flash('success', 'Ödeme İşleminiz Başarıyla Tamamlanmıştır');

        return redirect(route('home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MailOrder  $mailOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mailorder=MailOrder::find($id);
        $mailorder->delete();
        session()->flash('success','Ödeme Onaylanmadı ve Başarıyla Silindi');
        return back();
    }
}
