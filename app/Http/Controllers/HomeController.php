<?php

namespace App\Http\Controllers;

use App\AltAlan;
use App\Banka;
use App\Book;
use App\Category;
use App\EftHavaleBekleme;
use App\Etkinlikler;
use App\Firsat;
use App\Haberler;
use App\Iletisim;
use App\Kariyer;
use App\Kariyerb;
use App\Katalog;
use App\Logo;
use App\Product;
use App\Slider;
use App\sorular;
use App\SosyalMedya;
use App\User;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $products = Product::orderBy('created_at','desc')->where('status',1)->where('turu','urun')->where('vitrin',true)->get();
        $f_products = Product::orderBy('created_at','desc')->where('status',1)->where('firsat',true)->get();
        $firsat=AltAlan::first();
        $slider=Slider::all();
        $varmi = Logo::where('status',1)->exists();
        $logo=Logo::where('status',1)->first();
        // $f_products = $f_products[rand(0,(count($f_products)-1))];
       //  $oturum=\auth()->user()->kariyer->kariyerb->indirim;
        return view('index', compact( 'products','categoryMenu','varmi','logo','f_products','firsat','slider'));
    }
    public function category($slug){
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = AltAlan::first();
        $category = Category::where("slug",$slug)->first();
        $products = Product::with('categories')->where('status',1)->where('category_id',$category->id)->get();
        return view('category-details', compact('category','products','categoryMenu','firsat'));
    }
    public function product($slug){
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $product = Product::where("slug",$slug)->first();
        $firsat = AltAlan::first();
        $bcrumb = $product->categories()->distinct()->get();
        return view('product-detail', compact('product','bcrumb','categoryMenu','firsat'));
    }

    public function contact(){
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $iletisim=Iletisim::all();
        $medya=SosyalMedya::all();
        $user = auth()->user();
        return view('contact', compact('categoryMenu','iletisim','medya','user'));
    }
    public function oturum()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.pmenu' ,compact('categoryMenu'));
    }
    public function bilgilendirme()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $tutar = Kariyerb::pluck('tutar');
        $name = Kariyer::pluck('kariyername');
        return view('layouts.bilgilendirme' ,compact('categoryMenu','tutar','name'));
    }
    public function kullanici_katalog()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $kullanici_katalog = Katalog::all();
        return view('layouts.kullanici_katalog' ,compact('categoryMenu','kullanici_katalog'));
    }
    public function hesap()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.hesabim' ,compact('categoryMenu'));
    }
    public function hk()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.hakkimizda' ,compact('categoryMenu','firsat'));
    }
    public function bayi()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.bayi' ,compact('categoryMenu'));
    }
    function musteri_eft_durum()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $eft=EftHavaleBekleme::where('user_id',auth()->user()->id)->get();
        return view('layouts.musteri_eft_durum' ,compact('categoryMenu','eft'));
    }
    public function dogrudansatis()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.dogrudansatis' ,compact('categoryMenu'));
    }
    public function gizlilik()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.gizlilik' ,compact('categoryMenu'));
    }
    public function mesafelisatis()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.mesafelisatis' ,compact('categoryMenu'));
    }
    public function ismodeli()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.ismodeli' ,compact('categoryMenu','firsat'));
    }
    public function yeniurunler ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.yeniurunler' ,compact('categoryMenu','firsat'));
    }
    public function nedenveos ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.nedenveos' ,compact('categoryMenu','firsat'));
    }
    public function ppazarlama ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.ppazarlama' ,compact('categoryMenu','firsat'));
    }
    public function temsilcilik()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.temsilcilik' ,compact('categoryMenu'));
    }
    public function kariyerler()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.kariyerler' ,compact('categoryMenu'));
    }
    public function etkinlikler()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $etkin=Etkinlikler::all();
        return view('layouts.etkinlikler' ,compact('categoryMenu','etkin'));
    }
    public function misyon()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.misyon' ,compact('categoryMenu','firsat'));
    }
    public function iade()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.iade' ,compact('categoryMenu'));
    }
    public function kargo()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('kargo' ,compact('categoryMenu'));
    }
    public function odemeplanı()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.odemeplani' ,compact('categoryMenu','firsat'));
    }
    public function basari()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.basari' ,compact('categoryMenu','firsat'));
    }
    public function mailorder()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.MailOrder' ,compact('categoryMenu'));
    }
    public function profesyonelpazarlama()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $firsat = Firsat::all();
        return view('layouts.profesyonelpazarlama' ,compact('categoryMenu','firsat'));
    }
    public function cartman()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $kisi=auth()->user();
        $banka= Banka::all();
        return view('layouts.cartmon',compact('categoryMenu','banka','kisi'));
    }

    public function tarimbelge()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.tarimbelge',compact('categoryMenu'));
    }
    public function gelirlerim()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.gelirlerim',compact('categoryMenu'));
    }
    public function kazancozeti()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.kazancozeti',compact('categoryMenu'));
    }

    public function sss()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $soru=sorular::all();
        return view('layouts.sss',compact('categoryMenu','soru'));
    }
    public function haber2()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $haber=Haberler::all();
        return view('layouts.haberler',compact('categoryMenu','haber'));
    }
    public function kampanyalar()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.kampanyalar',compact('categoryMenu'));
    }
    public function banka()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $banka=Banka::all();
        return view('layouts.bankamusteri',compact('categoryMenu','banka'));
    }
    public function sozlesmemusteri()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $sozlesme=Book::all();
        return view('layouts.sozlesmemusteri',compact('categoryMenu','sozlesme'));
    }
    public function download($uuid)
    {
        $sozlesme = Book::where('uuid', $uuid)->firstOrFail();
        $pathToFile = storage_path('app/books/' . $sozlesme->cover);
        return response()->download($pathToFile);
    }
    public function paratika ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('paratika.app',compact('categoryMenu'));
    }
    public function il_ciro ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=\auth()->user()->kariyer_id;
        return view('layouts.il_kullanici',compact('categoryMenu','users'));
    }
    public function tr_ciro()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=\auth()->user()->kariyer_id;
        return view('layouts.tr_kullanici',compact('categoryMenu','users'));
    }
    public function dunya_ciro ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=\auth()->user()->kariyer_id;
        return view('layouts.dunya_kullanici',compact('categoryMenu','users'));
    }
    public function araba_ciro ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=\auth()->user()->kariyer_id;
        return view('layouts.araba_kullanici',compact('categoryMenu','users'));
    }
    public function bolge_ciro ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=\auth()->user()->kariyer_id;
        return view('layouts.bolge_kullanici',compact('categoryMenu','users'));
    }




}
