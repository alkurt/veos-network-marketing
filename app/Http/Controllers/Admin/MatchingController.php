<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Kariyer;
use App\Matching;
use App\SponsorDerinlik;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MatchingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers=Kariyer::where('id','>=',1)->where('id','<=',15)->with('matchings')->paginate(10);
        $matchings = Matching::paginate(15);

        return view('admin.matching-index',compact('kariyers','matchings','categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            "kariyer" => "required",
            "derinlik" => "required",
            "yuzde" => "required",
        ];
        $messages=[
            'kariyer.required' => 'Bu alan gereklidir. ',
            'derinlik.required' => 'Bu alan gereklidir.',
            'yuzde.required' => 'Bu alan gereklidir.',

        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'derinlik','yuzde', 'kariyer');
        if($derinlik=Matching::create($input))

        {
            Session()->flash('success','Kayıt Oluşturuldu.');
            return redirect(route('matching.index'));
        }
        else
        {
            Session()->flash('error','Kayıt Oluşturulamadı.');
            return redirect(route('matching.create'));
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Matching  $matching
     * @return \Illuminate\Http\Response
     */
    public function show(Matching $matching)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Matching  $matching
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyer_matching = Matching::find($id);
        $kariyerler = Kariyer::where('id','>=',1)->where('id','<=',15)->get();
        return view('admin.matching-create',compact('categoryMenu','kariyerler'))->with('kariyer_matching',$kariyer_matching);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Matching  $matching
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Matching::find($id)->update(['derinlik'=>$request->derinlik,'yuzde'=>$request->yuzde,'kariyer_id'=>$request->kariyer_id]);
        session()->flash('success','Matching bilgileri güncellendi');
        return redirect(route('matching.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Matching  $matching
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matching $matching)
    {
        //
    }
}
