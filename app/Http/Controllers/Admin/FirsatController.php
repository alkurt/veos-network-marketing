<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Etkinlikler;
use App\Firsat;
use App\KargoYonetim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class FirsatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $firsat = Firsat::all();
        $firsatsayi = Firsat::count();
        return view('admin.adminfirsat', compact('firsat', 'categoryMenu','firsatsayi'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.adminfirsat_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->only('hakkimizda','b_hikayesi','p_pazarlama','ismodeli','y_urunler','nedennetwork','odeme_plani','misyon');
        if (Firsat::create($input))
        {
            session()->flash('success','İçerikler Kaydı Eklendi');
            return redirect()->route('firsat.index');
        }
        else
        {
            session()->flash('error','İçerikler Kaydı Eklenemedi');
            return redirect()->route('firsat.edit');
        }



    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Firsat  $firsat
     * @return \Illuminate\Http\Response
     */
    public function show(Firsat $firsat)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Firsat  $firsat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $firsat=Firsat::find($id);
        return view('admin.firsat_edit',compact('categoryMenu','firsat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Firsat  $firsat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //

        $input['hakkimizda'] = $request->hakkimizda;
        $input['b_hikayesi'] = $request->b_hikayesi;
        $input['p_pazarlama'] = $request->p_pazarlama;
        $input['ismodeli'] = $request->ismodeli;
        $input['y_urunler'] = $request->y_urunler;
        $input['nedennetwork'] = $request->nedennetwork;
        $input['odeme_plani'] = $request->odeme_plani;
        $input['misyon'] = $request->misyon;
        $firsat= Firsat::find($id);
        $firsat->update($input);
        session()->flash('success','İşlem Başarılı');
        return redirect()->route('firsat.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Firsat  $firsat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Firsat $firsat)
    {
        $firsat->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
}
