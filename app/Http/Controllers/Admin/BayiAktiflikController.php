<?php

namespace App\Http\Controllers\Admin;

use App\BayiAktiflik;
use App\Category;
use App\Kariyer;
use App\SponsorDerinlik;
use App\SponsorFark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BayiAktiflikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers = Kariyer::where('id','>=',4)->where('id','<=',15)->with('aktiflik')->get();
        return view('admin.BayiAktiflik-index', compact('kariyers', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            "min_bayi" => "required",
            "max_alisveris" => "required",
            "toplam_alisveris" => "required",
            "yuzde" => "required",
        ];
        $messages=[
            'min_bayi.required' => 'Bu alan gereklidir. ',
            'max_alisveris.required' => 'Bu alan gereklidir.',
            'toplam_alisveris.required' => 'Bu alan gereklidir.',
            'yuzde.required' => 'Bu alan gereklidir.',

        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'min_bayi','max_alisveris', 'toplam_alisveris','yuzde');
        if($aktiflik=BayiAktiflik::create($input))
        {
            Session()->flash('success','Kayıt Oluşturuldu.');
            return redirect(route('bayiaktiflik.index'));
        }
        else
        {
            Session()->flash('error','Kayıt Oluşturulamadı.');
            return redirect(route('bayiaktiflik.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BayiAktiflik  $bayiAktiflik
     * @return \Illuminate\Http\Response
     */
    public function show(BayiAktiflik $bayiAktiflik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BayiAktiflik  $bayiAktiflik
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $aktiflik = BayiAktiflik::where('id',$id)->first();
        return view('admin.BayiAktiflik-edit',compact('categoryMenu','aktiflik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BayiAktiflik  $bayiAktiflik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $aktiflik=BayiAktiflik::where('id',$id)->first();
        $aktiflik->update(['min_bayi'=>$request->min_bayi,'toplam_alisveris'=>$request->toplam_alisveris,'yuzde'=>$request->yuzde,'max_alisveris'=>$request->max_alisveris]);
        session()->flash('success','Bayi Aktiflik güncellendi');
        return redirect(route('bayiaktiflik.index'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BayiAktiflik  $bayiAktiflik
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bayi=BayiAktiflik::find($id);
        $bayi->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();

    }
}
