<?php

namespace App\Http\Controllers\Admin;

use App\AltAlan;
use App\Category;
use App\Firsat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AltAlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $firsat = AltAlan::all();
        $firsatsayi = AltAlan::count();
        return view('admin.AltAlan',compact('categoryMenu','firsat','firsatsayi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.AltAlanCreate",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->only('bir','iki','uc','dort');
        if (AltAlan::create($input))
        {
            session()->flash('success','İçerikler Kaydı Eklendi');
            return redirect()->route('alt-alan.index');
        }
        else
        {
            session()->flash('error','İçerikler Kaydı Eklenemedi');
            return redirect()->route('alt-alan.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AltAlan  $altAlan
     * @return \Illuminate\Http\Response
     */
    public function show(AltAlan $altAlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AltAlan  $altAlan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $firsat=AltAlan::find($id);
        return view('admin.AltAlanEdit',compact('categoryMenu','firsat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AltAlan  $altAlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input['bir'] = $request->bir;
        $input['iki'] = $request->iki;
        $input['uc'] = $request->uc;
        $input['dort'] = $request->dort;
        $firsat= AltAlan::find($id);
        $firsat->update($input);
        session()->flash('success','İşlem Başarılı');
        return redirect()->route('alt-alan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AltAlan  $altAlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(AltAlan $altAlan)
    {
        //
        $altAlan->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
}
