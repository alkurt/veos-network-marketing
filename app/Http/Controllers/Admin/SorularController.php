<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\KargoYonetim;
use App\Product;
use App\sorular;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SorularController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $soru = sorular::all();
        return view('admin.adminsss', compact('soru', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.adminsss_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            "soru_basligi" => "required",
            "soru_cevabi" => "required",
            "sira_no" => "required",
        ];
        $messages=[
            'soru_basligi.required' => 'Bu alan gereklidir. ',
            'soru_cevabi.required' => 'Bu alan gereklidir.',
            'sira_no.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'soru_basligi','soru_cevabi', 'sira_no');
        if($soru=sorular::create($input))
        {
            Session()->flash('success','s.s.s Eklendi');
            return redirect(route('sss.index'));
        }
        else{
            Session()->flash('error','s.s.s Eklenemedi!');
            return redirect(route('sss.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sorular  $sorular
     * @return \Illuminate\Http\Response
     */
    public function show(sorular $sorular)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sorular  $sorular
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $sorular = sorular::find($id);
        return view("admin.sss_edit",compact('categoryMenu','sorular'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $sorular
     * @param  \App\sorular  $sorular
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            "soru_basligi" => "required",
            "soru_cevabi" => "required",
            "sira_no" => "required",
        ];
        $messages=[
            'soru_basligi.required' => 'Bu alan gereklidir. ',
            'soru_cevabi.required' => 'Bu alan gereklidir.',
            'sira_no.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'soru_basligi','soru_cevabi','sira_no');
        $soru = sorular::find($id);
        $soru->update($input);
      return redirect()->route('sss.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sorular  $sorular
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=sorular::find($id);
        $user->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
}
