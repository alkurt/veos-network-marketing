<?php

namespace App\Http\Controllers\Admin;

use App\AdminMail;
use App\AlisverisDerinlikDagilimi;
use App\BasketProduct;
use App\Category;
use App\Kariyerb;
use App\MailOrder;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $morder= MailOrder::where('durum',0)->paginate(10);
        return view('admin.adminmailorder',compact('categoryMenu','morder'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminMail  $adminMail
     * @return \Illuminate\Http\Response
     */
    public function show(AdminMail $adminMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminMail  $adminMail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $islem=MailOrder::find($id);
        $islem->durum=1;
        $islem->save();

        $token   = session('_token');
        $orderNo = session('order_no');
        $user = $islem->user;
        $order                   = [];
        $order['name']           = $user->name.' '.$user->surname;
        $order['address']        = $user->detail->address;
        $order['phone']          = $user->detail->phone;
        $order['m_phone']        = $user->detail->m_phone;
        $order['basket_id']      = $islem->basket_id;
        $order['user_id']        = $user->id;
        $order['installments']   = 1;
        $order['status']         = "Siparişiniz Alındı";
        $order['payment_method'] = $islem->payment_method;
        $order['order_price']    = $islem->order_price;
        $order['token']          = $islem->token;
        $order['order_no']       = $islem->order_no;

        $eskikariyer=$user->kariyer_id;
        Order::create($order);
        //sponsor fark primi
        function sponsor_fark_primi($ekleyen,$eklenen){
            if(User::where('id',$ekleyen->sponsor_id)->exists()){
                $sponsor=User::where('id',$ekleyen->sponsor_id)->first();
                if ($sponsor->kariyer_id > $ekleyen->kariyer_id && $ekleyen->kariyer_id <=4){
                    //$sponsor_sponsor_primi=$sponsor->kariyer->kariyerb->sponsor;
                    //$ekleyen_sponsor_primi=$ekleyen->kariyer->kariyerb->sponsor;
                    $eklenen_tutar=Kariyerb::find($eklenen->kariyer_id);
                    $eklenen_tutar=$eklenen_tutar->tutar;
                    //$fark = $sponsor_sponsor_primi - $ekleyen_sponsor_primi;
                    $fark=$sponsor->kariyer->sponsorfarks()->where('karsı_kariyer_id',$ekleyen->kariyer_id)->first();
                    $fark=$fark->fark;
                    $tutar=(($eklenen_tutar*$fark)/100);
                    $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                    $sponsor->gelirs()->create(['islem_sahibi_id'=>$ekleyen->id,'kazanc_turu'=>'Sponsor Fark Primi','kazanc_miktari'=>$tutar]);
                }
            }
        }
        //perakende gelir 4 8 14 19 24 = % oranları
        if ($user->kariyer_id == 17)
        {
            $alisverisi_yapan=$user;
            $sponsor = User::where('id',$alisverisi_yapan->sponsor_id)->first(); // kariyer id 17 olacak
            if ($sponsor->kariyer_id == 1)
            {
                $tutar = ($order['order_price']*4)/100;
                $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                $sponsor->gelirs()->create(['islem_sahibi_id'=>$alisverisi_yapan->id,'kazanc_turu'=>'Müşteri Alışveriş','kazanc_miktari'=> $tutar]);
            }
            if ($sponsor->kariyer_id == 2)
            {
                $tutar = ($order['order_price']*8)/100;
                $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                $sponsor->gelirs()->create(['islem_sahibi_id'=>$alisverisi_yapan->id,'kazanc_turu'=>'Müşteri Alışveriş','kazanc_miktari'=> $tutar]);
            }
            if ($sponsor->kariyer_id == 3)
            {
                $tutar = ($order['order_price']*14)/100;
                $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                $sponsor->gelirs()->create(['islem_sahibi_id'=>$alisverisi_yapan->id,'kazanc_turu'=>'Müşteri Alışveriş','kazanc_miktari'=> $tutar]);
            }
            if ($sponsor->kariyer_id == 4)
            {
                $tutar = ($order['order_price']*19)/100;
                $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                $sponsor->gelirs()->create(['islem_sahibi_id'=>$alisverisi_yapan->id,'kazanc_turu'=>'Müşteri Alışveriş','kazanc_miktari'=> $tutar]);
            }
            if ($sponsor->kariyer_id == 5)
            {
                $tutar = ($order['order_price']*24)/100;
                $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                $sponsor->gelirs()->create(['islem_sahibi_id'=>$alisverisi_yapan->id,'kazanc_turu'=>'Müşteri Alışveriş','kazanc_miktari'=> $tutar]);
            }

            $bs=BasketProduct::where('basket_id',$order['basket_id'])->get();

            $urun_pv=0;
            $urun_cv=0;
            $urun_adet=0;
            $pv=0;
            $cv=0;
            foreach ($bs as $basket_products){
                $urun_pv =$basket_products->product->pv;
                $urun_cv =$basket_products->product->cv;
                $urun_adet = $basket_products->quantity;
                $pv +=$urun_pv * $urun_adet;
                $cv +=$urun_cv * $urun_adet;
            }
            function pv_dagıtma($user,$pv,$cv){
                if(User::where('id',$user->sponsor_id)->exists()) {
                    $user2 = User::find($user->sponsor_id);
                    $user2->update(['ara_pv' => $user2->ara_pv += $pv ]);
                    $user2->update(['pv' => $user2->pv += $pv ]);
                    $user2->update(['cv' => $user2->cv += $cv ]);
                    pv_dagıtma($user2,$pv,$cv);
                }
            }
            pv_dagıtma($user,$pv,$cv);

            return back();
        }else{

            /*  Paket alışındaki sponsor ve sponsor fark primi dağıtımlar.  */

            $urunler=BasketProduct::where('basket_id',$islem->basket_id)->with('product')->get();
            $isPaket = false;
            $isUrun= false;
            foreach ($urunler as $urun){
                if($urun->product->turu=='paket'){
                    $isPaket = true;
                }
                else{
                    $isUrun = true;
                }
            }
            /*   Son  */
            //sponsor fark primi sonu
            function sponsor_primi($eklenen_kullanici)  //sponsor primi başlangıcı
            {
                $ekleyen_kullanici=User::where('id',$eklenen_kullanici->sponsor_id)->first() ; // üstüm
                $ekleyen_sponsor_yuzdesi = $ekleyen_kullanici->kariyer->kariyerb->sponsor;
                $eklenen_paket_tutari=Kariyerb::find($eklenen_kullanici->kariyer_id);
                $paket_tutari=$eklenen_paket_tutari->tutar;
                $ekleyen_kullanici->cuzdan()->increment('toplam_bakiye',($paket_tutari * $ekleyen_sponsor_yuzdesi)/100); // gelirs eklenicek
                $ekleyen_kullanici->gelirs()->create(['islem_sahibi_id'=>$eklenen_kullanici->id,'kazanc_turu'=>'Sponsor Primi','kazanc_miktari'=>($paket_tutari * $ekleyen_sponsor_yuzdesi)/100]);
            }
            //sponsor primi sonu
            function siparis_fark_primi($siparis_veren,$tutar){ //bayi fark primi
                if(User::where('id',$siparis_veren->sponsor_id)->exists()){
                    $sponsor=User::where('id',$siparis_veren->sponsor_id)->first();
                    $siparis_veren_indirim=$siparis_veren->kariyer->kariyerb->indirim;
                    $sponsor_indirim_yuzdesi=$sponsor->kariyer->kariyerb->indirim;
                    if ($sponsor_indirim_yuzdesi > $siparis_veren_indirim){
                        $tutar=$tutar * ($sponsor->kariyer->bayiindirimfark()->where('karsı_kariyer_id',$siparis_veren->kariyer_id)->first()->fark) /100;
                        $sponsor->cuzdan()->increment('toplam_bakiye',$tutar);
                        $sponsor->gelirs()->create(['islem_sahibi_id'=>$siparis_veren->id,'kazanc_turu'=>'Bayi Fark Primi','kazanc_miktari'=>$tutar]);
                    }
                }
            } //bayii fark derinlik
            function kariyer_atlat($user){
                $toplam=0;
                //user'ın satın almalarını topluyoruz
                $orders = $user->orders;

                foreach ($orders->baskets as $basket){
                    foreach ($basket as $order) {
                        foreach ($order->basket_products as $basket_product) {
                            if ($basket_product->product->turu == 'paket') {
//                            $bCount=$order->basket_products->count();
//                            $toplam += ($order->product_price()*$order->basket_product_qty())/$bCount;
                                $toplam += ($basket_product->quantity * $basket_product->price);
                            }
                        }
                    }
                }
                if($toplam>=300 && $toplam<=599){
                    if ($user->kariyer_id != 1){
                        $user->kariyer_id=1;
                        $user->save();
                        $user->aylikkariyer()->create();
                    }
                }
                elseif ($toplam>=600 && $toplam<=1499){
                    if ($user->kariyer_id != 2){
                        $user->update(['kariyer_id'=>2]);
                        $user->aylikkariyer()->create();
                    }
                }
                elseif ($toplam>=1500 && $toplam<=2499){
                    if ($user->kariyer_id != 3){
                        $user->kariyer_id=3;
                        $user->save();
                        $user->aylikkariyer()->create();
                    }
                }
                elseif ($toplam>=2500 && $toplam<=4999){
                    if ($user->kariyer_id != 4){
                        $user->update(['kariyer_id'=>4]);
                        $user->aylikkariyer()->create();
                    }
                }
                elseif ($toplam >=5000){
                    if ($user->kariyer_id !=5){
                        $user->update(['kariyer_id'=>5]);
                        $user->aylikkariyer()->create();
                    }
                }
            }
            $bs=BasketProduct::where('basket_id',$order['basket_id'])->get();
            $urun_pv=0;
            $urun_cv=0;
            $urun_adet=0;
            $pv=0;
            $cv=0;
            foreach ($bs as $basket_products){
                $urun_pv =$basket_products->product->pv;
                $urun_cv =$basket_products->product->cv;
                $urun_adet = $basket_products->quantity;
                $pv +=$urun_pv * $urun_adet;
                $cv +=$urun_cv * $urun_adet;
            }
            //   $pv =($bs->product->pv);
            //  $cv =($bs->product->cv);
            //    $adet = $bs->quantity;

            /*  $user->update(['ara_pv'=>$user->ara_pv +=$pv,'cv'=>$user->cv +=$cv ,'pv'=>$user->pv +$pv]);
            */
            if($user->kariyer_id <=5 || $user->kariyer_id == 16) {
                kariyer_atlat($user);
            }
            $yenikariyer=User::find($islem->user->id)->kariyer_id;
            //;\auth()->user()->kariyer_id;
            $yeni=User::find($islem->user->id);
            function alisveris_derinlik_dagilimi($alisveris_yapan_kisi,$user,$deep,$tutar){
                if (User::where('id',$user->sponsor_id)->exists()){
                    for ($i=1;$i<=8;$i++){
                        if ($deep==$i){
                            $derinlik= User::where('id',$user->sponsor_id)->first();
                            $yuzde=AlisverisDerinlikDagilimi::where('derinlik',$deep)->first()->yuzde;
                            $derinlik->cuzdan()->increment('toplam_bakiye',(($tutar * $yuzde)/100));
                            $derinlik->gelirs()->create(['islem_sahibi_id'=>$alisveris_yapan_kisi->id,'kazanc_turu'=>'Alışveriş Derinlik Primi','kazanc_miktari'=>$tutar * $yuzde /100]);
                            alisveris_derinlik_dagilimi($alisveris_yapan_kisi,$derinlik,$deep+1,$tutar);
                        }
                    }
                }
            }
            //Sponsorluk derinlik kazancu
            function sponsor_derinlik_kazancı($ekleyen_kisi,$user,$deep){
                if (User::where('id',$user->sponsor_id)->exists()){
                    $kariyer_id=User::where('id',$user->sponsor_id)->first()->kariyer->id;
                    if ($kariyer_id >= 5 && $kariyer_id <=15){
                        if ($deep==1){
                            $derinlik= User::where('id',$user->sponsor_id)->first();
                            $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',1)->first()->yuzde;
                            $derinlik->cuzdan()->increment('toplam_bakiye',(($ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde)/100));
                            $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                            sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                        }elseif ($deep==2){
                            $derinlik= User::where('id',$user->sponsor_id)->first();
                            $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',2)->first()->yuzde;
                            $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                            $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                            sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                        }elseif ($deep==3){
                            $derinlik= User::where('id',$user->sponsor_id)->first();
                            $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',3)->first()->yuzde;
                            $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                            $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                            sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                        }elseif ($deep==4){
                            if ($kariyer_id>=6) {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',4)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }
                        }elseif ($deep==5){
                            if ($kariyer_id>=7){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',5)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }
                        }elseif($deep==6){
                            if ($kariyer_id>=8){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',6)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }
                        }
                        elseif($deep==7){
                            if ($kariyer_id>=8){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',7)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }
                        }else{
                            if ($kariyer_id >8){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->derinliks()->where('derinlik',7)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Sponsor Derinlik Primi','kazanc_miktari'=>$ekleyen_kisi->kariyer->kariyerb->tutar * $yuzde /100]);
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                            }
                        }
                    }else{
                        $derinlik= User::where('id',$user->sponsor_id)->first();
                        sponsor_derinlik_kazancı($ekleyen_kisi,$derinlik,$deep+1);
                    }
                }
            }
            //sponsor derinlik sonu
            if ($isPaket){
                if ($yeni->orders->count() ==1 && $eskikariyer==16 && $yenikariyer !=16){
                    sponsor_fark_primi(User::where('id',$user->sponsor_id)->first(),$yeni);
                    sponsor_derinlik_kazancı($yeni,User::where('id',$user->sponsor_id)->first(),1);
                    sponsor_primi($yeni);
                }
            }if ($isUrun){
                if($user->kariyer_id>=1 && $user->kariyer_id<=15)
                {
                    siparis_fark_primi($user,$order['order_price']);
                    //Alisveris Derinlik Dagılımı ~~~START
                    alisveris_derinlik_dagilimi($yeni,$yeni,1,$order['order_price']);
                    // ~~~END
                }
            }
            function pv_dagıtma($user,$pv,$cv){
                if(User::where('id',$user->sponsor_id)->exists()) {
                    $user2 = User::find($user->sponsor_id);
                    $user2->update(['ara_pv' => $user2->ara_pv += $pv]);
                    $user2->update(['pv' => $user2->pv += $pv ]);
                    $user2->update(['cv' => $user2->cv += $cv ]);
                    $user2->gelenpv()->create(['puan_turu'=>'PV','puan_miktari'=> $pv ,'islem_sahibi_id'=>$user->id]);
                    $user2->gelenpv()->create(['puan_turu'=>'CV','puan_miktari'=> $cv ,'islem_sahibi_id'=>$user->id]);
                    if($user2->kariyer_id <=5 ){
                        kariyer_atlat($user2);
                    }
                    pv_dagıtma($user2,$pv,$cv);
                }
            }
            //meb
            pv_dagıtma($user,$pv,$cv);

            session()->flash('success','İşlem Başarılı');
            $islem->delete();
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminMail  $adminMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminMail $adminMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminMail  $adminMail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mailorder=MailOrder::find($id);
        $mailorder->delete();
        session()->flash('success','Ödeme Onaylanmadı ve Başarıyla Silindi');
        return back();
    }
}
