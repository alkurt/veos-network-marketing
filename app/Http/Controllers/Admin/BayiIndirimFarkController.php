<?php

namespace App\Http\Controllers\Admin;

use App\BayiIndirimFark;
use App\Category;
use App\Kariyer;
use App\SponsorFark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BayiIndirimFarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers = Kariyer::where('id','>=',2)->where('id','<=',5)->with('bayiindirimfark')->get();
        return view('admin.Bayi-Fark-indirim', compact('kariyers', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BayiIndirimFark  $bayiIndirimFark
     * @return \Illuminate\Http\Response
     */
    public function show(BayiIndirimFark $bayiIndirimFark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BayiIndirimFark  $bayiIndirimFark
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $bfark = BayiIndirimFark::where('id',$id)->first();
        return view('admin.Bayi-Fark-indirim-Edit',compact('categoryMenu','bfark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BayiIndirimFark  $bayiIndirimFark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $kariyer=BayiIndirimFark::where('id',$id)->first();
        $kariyer->update(['fark'=>$request->fark]);
        session()->flash('success','Bayi İndirim Fark bilgileri güncellendi');
        return redirect(route('bayiindirimfark.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BayiIndirimFark  $bayiIndirimFark
     * @return \Illuminate\Http\Response
     */
    public function destroy(BayiIndirimFark $bayiIndirimFark)
    {
        //
    }
}
