<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Kariyer;
use App\SponsorDerinlik;
use App\SponsorFark;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class SponsorFarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers = Kariyer::where('id','>=',2)->where('id','<=',5)->with('sponsorfarks')->paginate(1);
        return view('admin.S-fark-index', compact('kariyers', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.s-fark-edit",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            "fark" => "required",
        ];
        $messages=[
            'fark.required' => 'Bu alan gereklidir. ',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'fark');
        if($sfark=SponsorFark::create($input))
        {
            Session()->flash('success','Kayıt Oluşturuldu.');
            return redirect(route('sponsorfark.index'));
        }
        else
        {
            Session()->flash('error','Kayıt Oluşturulamadı.');
            return redirect(route('sponsorfark.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SponsorFark  $sponsorFark
     * @return \Illuminate\Http\Response
     */
    public function show(SponsorFark $sponsorFark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SponsorFark  $sponsorFark
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $sfark = SponsorFark::where('id',$id)->first();
        return view('admin.s-fark-edit',compact('categoryMenu','sfark'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SponsorFark  $sponsorFark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $kariyer=SponsorFark::where('id',$id)->first();
        $kariyer->update(['fark'=>$request->fark]);
        session()->flash('success','SP Fark Bilgileri Güncellendi');
        return redirect(route('sponsorfark.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SponsorFark  $sponsorFark
     * @return \Illuminate\Http\Response
     */
    public function destroy(SponsorFark $sponsorFark)
    {
        //
    }
}
