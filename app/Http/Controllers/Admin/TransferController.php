<?php

namespace App\Http\Controllers\Admin;

use App\AdminOdeme;
use App\Category;
use App\cuzdan;
use App\EftHavaleBekleme;
use App\EslesmeBinary;
use App\Gelenpv;
use App\Gelir;
use App\Gider;
use App\KargoYonetim;
use App\Kariyer;
use App\Kariyerb;
use App\Kasa;
use App\Message;
use App\OdemeTalebi;
use App\Order;
use App\Product;
use App\Tazminat;
use App\Transfer;
use App\User;
use http\Exception\BadConversionException;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Gd\Commands\BackupCommand;
use Yajra\DataTables\DataTables;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//
    }
    public function transferet($tid,Request $request)
    {
        if (User::where('id',$request->transfer)->exists()){
            $user =User::find($tid);
            $user->sponsor_id=$request->transfer;
            $user->root_id=$request->transfer;
            $user->konum=$request->konumlandir;
            $user->save();

            session()->flash('success','Kullanıcı Başarıyla Transfer Edildi');
            return back();
        }else{
            session()->flash('error','Transfer Etmek İstediğiniz Kullanıcı Bulunamadı.');
            return back();
        }

    }
    public function konumlandir($kdid,Request $request)
    {
        $userk=User::find($kdid);
        $userk->konum=$request->konumlandir;
        $userk->save();
        return back();

    }
    public function adminbinary()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $eslesmeler=\auth()->user()->binary;
        $cuzdan=cuzdan::sum('toplam_bakiye');
        return view("admin.adminkazancozeti", compact("categoryMenu",'eslesmeler','cuzdan'));
    }
    public function odemetalebi()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $odemetalebi = OdemeTalebi::where('status',1)->get();
        return view('admin.admin_odeme_talep_edit', compact('odemetalebi', 'categoryMenu'));
    }
    public function admingelenpv()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view('admin.admingelenpv', compact('categoryMenu'));
    }
    public function odemetalebidurum()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $odemetalebi = OdemeTalebi::all();
        return view('admin.admin_odeme_talep_edit', compact('odemetalebi', 'categoryMenu'));
    }
    public function odemetalebisil($id)
    {
        $userk= OdemeTalebi::find($id);
        $userk->delete();
        session()->flash('success','Başarıyla Silindi');
        return redirect()->route('odemetalebi');
    }
    public function admin_datatable()
    {
        $data = Gelir::all();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad =User::find($row->user_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('islem_sahibi_ad',function ($row){
                $ad2 =User::find($row->islem_sahibi_id);
                $ad2 = $ad2->name." ".$ad2->surname;
                return $ad2;
            })
            ->make(true);
    }
    public function adminagac()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $dallar = User::where('root_id',auth()->user()->id)->get();
        $kisi=User::find(auth()->user()->id);
        return view("admin.adminagac", compact("categoryMenu"))->with('dallar',$dallar)->with('kisi',$kisi);
    }
    public function admin_pv_ekleme(Request $req,$id){
        $this->validate(
            $req,
            ['pv'=>'required|numeric'],
            [
                'pv.required'=>'Pv değeri boş geçilemez.',
                'pv.numeric'=>'Pv alanına sadece rakam girebilirsiniz.'
            ]
        );
        $user=User::find($id);
        $user->ara_pv +=$req->pv;
        $user->pv +=$req->pv;
        $user->save();
        function pv_dagıtma($user,$pv){
            if(User::where('id',$user->sponsor_id)->exists()) {
                $user2 = User::find($user->sponsor_id);
                $user2->update(['ara_pv' => $user2->ara_pv += $pv ]);
                $user2->update(['pv' => $user2->pv += $pv ]);
                pv_dagıtma($user2,$pv);
            }
        }
        pv_dagıtma($user,$req->pv);

        session()->flash('success','Pv Ekleme İşlemi Başarılı.');
        return back();
    }
    public function admin_cv_ekleme(Request $req,$id){
        $this->validate(
            $req,
            ['cv'=>'required|numeric'],
            [
                'cv.required'=>'Cv değeri boş geçilemez.',
                'cv.numeric'=>'Cv alanına sadece rakam girebilirsiniz.'
            ]
        );
        $user=User::find($id);
        $user->cv +=$req->cv;
        $user->save();

        function pv_dagıtma($user,$cv){
            if(User::where('id',$user->sponsor_id)->exists()) {
                $user2 = User::find($user->sponsor_id);
                $user2->update(['cv' => $user2->cv += $cv ]);
                pv_dagıtma($user2,$cv);
            }
        }
        pv_dagıtma($user,$req->cv);

        session()->flash('success','Cv Ekleme İşlemi Başarılı.');
        return back();
    }
    public function gonderme($kid)
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $dallar = User::where('root_id',$kid)->get();
        $kisi = User::find(($kid));
        return view('admin.adminagac',compact('categoryMenu'))->with('dallar', $dallar)->with('kisi', $kisi);
    }
    public function adminekip()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users= User::all();
        return view('admin.adminekip',compact('categoryMenu','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Transfer $transfer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfer $transfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfer $transfer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy2($id)
    {
        $users=User::find($id);
        $users->delete();
        session()->flash('success','Kullanıcı Silme Başarılı');
        return back();
    }
    public function iskonum($ikid,Request $request)
    {
        $konumkontrol = User::where('root_id',auth()->user()->id)->where('konum',$request->iskonum)->exists();

        $altbayi = User::find($ikid);


        if (($altbayi->user_id != null) && ( $altbayi->kariyer_id < 5 || $altbayi->kariyer_id == 16))
        {
            \session()->flash('error','Alt Bayinizin Kariyeri Girişimci Olmadan Konumlandırma Yapamazsınız');
            return back();
        }
        if(\auth()->user()->kariyer_id == 16 )
        {
            \session()->flash('error','Mevcut Bir Bayiniz Bulunamadı, Bayi Olduktan Sonra Konumlandırmanızı Yapabilirsiniz!!');
            return back();
        }

        if (!$konumkontrol){
            $bayi=User::find($ikid);
            $bayi->update(['konum'=>$request->iskonum]);
            Session()->flash('success','Konumlandırma Başarılı');
            if (auth()->user()->id ==10001)
            {
                return redirect(route('binarykontrol'));
            }
            else
                return redirect(route('kazanc'));
        }
        else
        {
            Session()->flash('error','Girmiş olduğunuz konuma kayıt yapılamıyor,lütfen kontrol ediniz');
            if (auth()->user()->id ==10001)
            {
                return redirect(route('binarykontrol'));
            }
            else
                return redirect(route('kazanc'));
        }
    }
    public function admingelenpv_datatable()
    {
        $data = Gelenpv::all();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad =User::find($row->user_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('islem_sahibi_ad',function ($row){
                $ad2 =User::find($row->islem_sahibi_id);
                $ad2 = $ad2->name." ".$ad2->surname;
                return $ad2;
            }) ->addColumn('puan_miktari',function ($row){
                $ad =$row->puan_miktari;
                return $ad;
            })
            ->make(true);
    }
    public function kontrolmenu()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=User::count();
        $aktif=User::where('durum',true)->count();
        $pasif=User::where('durum',false)->count();
        $cuzdan=Order::sum('order_price');
        $gelirler= Kasa::sum('toplam_kasa');
        $primodeme= cuzdan::sum('toplam_bakiye');
        $giderler = Gider::sum('amount');
        $alisfiyati = Product::sum('alisfiyati');
        $satisfiyati = Product::sum('product_price');
        $eft = EftHavaleBekleme::where('durum',0)->count();
        $alinan = Order::where('status', "Siparişiniz Alındı")->with('baskets')->count();
        $odeme = OdemeTalebi::where('status',1)->count();
        $urunkar = $satisfiyati-$alisfiyati;
        $gelenmesaj= Message::where('status',1)->count();
        $kesinti = Gider::sum('kesinti');
        $net_kasa = $gelirler- $primodeme + $urunkar-$giderler;
        return view('admin.kontrolmenu',compact('categoryMenu','alinan','gelenmesaj','eft','odeme','urunkar','alisfiyati','satisfiyati','users','kesinti','aktif','pasif','cuzdan','gelirler','net_kasa','giderler','primodeme'));
    }
    public function destroy($id)
    {
        $usera= User::find($id);
        $usera->delete();
        $usera->save();
        return back();
    }

    public function aktif()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('admin.aktif',compact('categoryMenu'));
    }
    public function pasif()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();

        return view('admin.pasive-users',compact('categoryMenu'));
    }
    public function araciro ()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users=User::all();
        return view('admin.aracciro',compact('categoryMenu','users'));
    }
    public function tazminat()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $tazminat=Tazminat::where('tazminat','>',0)->with('user')->get();
        return view('admin.tazminat',compact('categoryMenu','tazminat'));
    }
    public function tazminatode($id)
    {
        $tazminat=Tazminat::where('id',$id)->with('user')->first();
       // $tazminat->user()->delete();
        $kisi = User::where('id',$tazminat->user_id)->get();
        foreach ($kisi as $kisi)
        {
            $tutar =$tazminat->tazminat;
            $kisi->cuzdan()->increment('toplam_bakiye',$tutar);
            $kisi->gelirs()->create(['kazanc_turu'=>'Bayi Çıkış Tazminatı','kazanc_miktari'=> $tutar,'islem_sahibi_id'=>$tazminat->user_id]);
            $tazminat->save();
            $tazminat->delete();
            session()->flash('success','Tazminat Kullanıcının Cüzdanına Yatırıldı');
            return back();
        }
    }
    public function ilciro()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $il_ciro = Gelir::where('kazanc_turu','İl Ciro')->whereMonth('created_at',date('m'))->paginate(20);
        return view('admin.ilciro',compact('categoryMenu','il_ciro'));
    }
    public function turkeyciro()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $turkiye_ciro = Gelir::where('kazanc_turu','Türkiye Ciro')->whereMonth('created_at',date('m'))->paginate(20);
        return view('admin.turkeyciro',compact('categoryMenu','turkiye_ciro'));
    }
    public function dunyaciro()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $dunya_ciro = Gelir::where('kazanc_turu','Dünya Ciro')->whereMonth('created_at',date('m'))->paginate(20);
        return view('admin.dunya_ciro',compact('categoryMenu','dunya_ciro'));
    }
    public function bolgeciro()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $bolge_ciro = Gelir::where('kazanc_turu','Bölge Ciro')->whereMonth('created_at',date('m'))->paginate(20);
        return view('admin.bolge_ciro',compact('categoryMenu','bolge_ciro'));
    }
    public function giderler(){
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $giderler = Gider::orderBy('id','desc')->paginate(25);
        return view('admin.giderler',compact('categoryMenu'))->with('giderler',$giderler);
    }
    public function admineslesme_datatable()
    {
        $data = EslesmeBinary::all();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad =User::find($row->eslesen_kullanici_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('eslesen_ad',function ($row){
                $ad =User::find($row->eslesen_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('devreden1',function ($row){
                $ad =User::find($row->eslesen_kullanici_id);
                $ad = $ad->cv;
                return $ad;
            })->addColumn('devreden2',function ($row){
                $ad =User::find($row->eslesen_id);
                $ad = $ad->cv;
                return $ad;
            })
            ->make(true);
    }
    public function admineslesme()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view("admin.admineslesme", compact("categoryMenu"));
    }
}
