<?php

namespace App\Http\Controllers;

use App\Kampanyalar;
use Illuminate\Http\Request;

class KampanyalarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kampanyalar  $kampanyalar
     * @return \Illuminate\Http\Response
     */
    public function show(Kampanyalar $kampanyalar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kampanyalar  $kampanyalar
     * @return \Illuminate\Http\Response
     */
    public function edit(Kampanyalar $kampanyalar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kampanyalar  $kampanyalar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kampanyalar $kampanyalar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kampanyalar  $kampanyalar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kampanyalar $kampanyalar)
    {
        //
    }
}
