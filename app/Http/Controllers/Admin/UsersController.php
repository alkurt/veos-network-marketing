<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\EslesmeBinary;
use App\KargoYonetim;
use App\Order;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users = User::orderBy('id','desc')->paginate(5);
        return view('admin.users', compact('users','categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $tip=User::find($id);
        return view('admin.users_edit',compact('categoryMenu','tip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules=[
            "sponsor_id" => "numeric"
        ];
        $messages=[
            'sponsor_id.numeric' => 'Sponsor Numarası Sayısal Değer İçermelidir . '
        ];

          $this->validate($request,$rules,$messages);
        $input = $request->only( 'kariyer_id','sponsor_id');
        $tip= User::find($id);
        $tip->unsetEventDispatcher($input);
        $tip->update($input);
        session()->flash('success','Güncelleme Başarılı');
        return redirect()->route('admin-users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bayi_sponsor_count = User::where('sponsor_id',$id)->count();
        $bayi_root_count = User::where('root_id',$id)->count();
        if ($bayi_root_count >0 || $bayi_sponsor_count >0){
            Session::flash("error", "Kullanıcıyı silebilmeniz için altında başka bir kullanıcı olmaması gerekiyor.");
        }else{
            User::find($id)->update(['status'=>0]);
            Session::flash("status", 1);
        }
        return redirect()->route('admin-users.index');
    }

    public function users_datatable()
    {
        $data = User::where('id','!=',10001)->get();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad = '<a href="'.route('kid',$row->id).'" class="text-dark" style="text-decoration:none;font-weight:bold;">' .$row->name." ".$row->surname .' </a>';
                return $ad;
            })->addColumn('id',function ($row){
                $ad = '<a href="'.route('kid',$row->id).'" class="text-dark" style="text-decoration:none;font-weight:bold;">' .$row->id .' </a>';
                return $ad;
            })->addColumn('kariyer',function ($row){
              $ad= $row->kariyer->kariyername;
                return $ad;
            })->addColumn('alt_bayi',function ($row){
              $count= User::where('sponsor_id',$row->id)->count();
                return $count;
            })->addColumn('sponsor_id',function ($row){
                $ad= $row->sponsor_id;
                if ($ad== null)
                {
                    return 'Şirket';
                }
                else
                { return $ad;}

            })->addColumn('toplam_alisveris',function ($row){
                $ad= Order::where('user_id',$row->id)->sum('order_price');
                return $ad.'₺';
            })->addColumn('buttons',function ($row){
              $btn= '<div class="d-flex" >
                    <a href="'.route('kid',$row->id).'" class="btn btn-success mr-2"><i class="fa fa-tree"></i></a>
                    <a style="" href="admin-users/'.$row->id.'/edit" class="btn btn-primary mr-1"><i class="fa fa-edit"></i></a>                 
                     <a href="/admin-users/'.$row->id.'" class="btn btn-danger ml-auto text" data-method="delete"
                              data-confirm="Emin misiniz?"><i class="fa fa-trash"></i></a>
                </div>';
              return $btn;
            })->rawColumns(['buttons','ad','id'])
            ->make(true);
    }

    public function users_active_datatable()
    {
        $data = User::where('id','!=',10001)->where('durum',1)->get();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad = '<a href="'.route('kid',$row->id).'" class="text-dark" style="text-decoration:none;font-weight:bold;">' .$row->name." ".$row->surname .' </a>';
                return $ad;
            })->addColumn('id',function ($row){
                $ad = '<a href="'.route('kid',$row->id).'" class="text-dark" style="text-decoration:none;font-weight:bold;">' .$row->id .' </a>';
                return $ad;
            })->addColumn('kariyer',function ($row){
                $ad= $row->kariyer->kariyername;
                return $ad;
            })->addColumn('alt_bayi',function ($row){
                $count= User::where('sponsor_id',$row->id)->count();
                return $count;
            })->addColumn('buttons',function ($row){
                $btn= '<div class="d-flex" >
                    <a href="'.route('kid',$row->id).'" class="btn btn-success mr-2"><i class="fa fa-tree"></i></a>
                    <a style="" href="admin-users/'.$row->id.'/edit" class="btn btn-primary mr-1"><i class="fa fa-edit"></i></a>                 
                     <a href="/admin-users/'.$row->id.'" class="btn btn-danger ml-auto text" data-method="delete"
                              data-confirm="Emin misiniz?"><i class="fa fa-trash"></i></a>
                </div>';
                return $btn;
            })->rawColumns(['buttons','ad','id'])
            ->make(true);
    }
    public function users_passive_datatable()
    {
        $data = User::where('id','!=',10001)->where('durum',0)->get();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad = '<a href="'.route('kid',$row->id).'" class="text-dark" style="text-decoration:none;font-weight:bold;">' .$row->name." ".$row->surname .' </a>';
                return $ad;
            })->addColumn('id',function ($row){
                $ad = '<a href="'.route('kid',$row->id).'" class="text-dark" style="text-decoration:none;font-weight:bold;">' .$row->id .' </a>';
                return $ad;
            })->addColumn('kariyer',function ($row){
                $ad= $row->kariyer->kariyername;
                return $ad;
            })->addColumn('alt_bayi',function ($row){
                $count= User::where('sponsor_id',$row->id)->count();
                return $count;
            })->addColumn('buttons',function ($row){
                $btn= '<div class="d-flex" >
                    <a href="'.route('kid',$row->id).'" class="btn btn-success mr-2"><i class="fa fa-tree"></i></a>
                    <a style="" href="admin-users/'.$row->id.'/edit" class="btn btn-primary mr-1"><i class="fa fa-edit"></i></a>                 
                     <a href="/admin-users/'.$row->id.'" class="btn btn-danger ml-auto text" data-method="delete"
                              data-confirm="Emin misiniz?"><i class="fa fa-trash"></i></a>
                </div>';
                return $btn;
            })->rawColumns(['buttons','ad','id'])
            ->make(true);
    }
}
