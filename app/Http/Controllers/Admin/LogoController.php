<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Logo;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $varmi = Logo::where('status',1)->exists();
        $logo = Logo::all();
        return view('admin.logo', compact('logo','categoryMenu','varmi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('admin.logocreate',compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->hasFile('cover')) {

            $file =$request->cover;
            $rand = rand(1, 999999);
            $image_name = $rand.'.'.$file->extension();
            //Storage::disk('local')->put('public/sliders/'.$image_name,$image,'public');
            //Storage::put('books/'.time(),$request->file('cover'));
            $new=$request->file('cover')->store('public/logos');
            $pdf = new Logo();
            $pdf->name = $request->name;
            $pdf->image_name = $image_name;
            $pdf->cover =$new;
            $pdf->uuid = $rand;
            $pdf->save();
        }
        session()->flash('success','Logo Başarıyla Eklendi');
        return redirect()->route('logo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Logo  $logo
     * @return \Illuminate\Http\Response
     */
    public function show(Logo $logo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Logo  $logo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $logo = Logo::find($id);
        return view("admin.logoguncel", compact( 'categoryMenu','logo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Logo  $logo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logo $logo)
    {
        //
        $logo->name = $request->name;
        $logo->status = $request->status;
        if ($request->hasFile('cover')) {
            $file =$request->cover;
            $rand = rand(1, 999999);
            $image_name = $rand.'.'.$file->extension();
            Storage::delete($logo->cover);
            $new=$request->file('cover')->store('public/logos');
            $logo->image_name = $image_name;
            $logo->cover =$new;
            $logo->uuid = $rand;
        }
        $logo->save();
        session()->flash('success','Logo Başarıyla Güncellendi');
        return redirect()->route('logo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Logo  $logo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $logo=Logo::find($id);
        $logo->delete();
        session()->flash('success','Logo Görüntüsü Başarıyla Silindi');
        return back();
    }
    public function download($uuid)
    {
        session()->flash('success','Yüklü'); return back();
    }
}
