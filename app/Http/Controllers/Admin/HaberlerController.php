<?php

namespace App\Http\Controllers\Admin;


use App\Book;
use App\Category;
use App\Haberler;
use App\Iletisim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;

class HaberlerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $haberler = Haberler::all();
        return view('admin.adminhaber', compact('haberler', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.adminhaber_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules=[
            "haber_basligi" => "required",
            "haber_icerik" => "required",
            "url" => "required",
        ];
        $messages=[
            'haber_basligi.required' => 'Bu alan gereklidir. ',
            'haber_icerik.required' => 'Bu alan gereklidir.',
            'url.required' => 'Bu alan gereklidir.',


        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'haber_basligi','haber_icerik', 'url');

        if($haberler=Haberler::create($input))
        {
            Session()->flash('success','Haber Oluşturuldu.');
            return redirect(route('haberler.index'));
        }
        else
        {
            Session()->flash('error','Haber Oluşturulamadı.');
            return redirect(route('haberler.create'));
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Haberler  $haberler
     * @return \Illuminate\Http\Response
     */
    public function show(Haberler $haberler)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Haberler  $haberler
     * @return \Illuminate\Http\Response
     */
    public function edit(Haberler $haberler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Haberler  $haberler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Haberler $haberler)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Haberler  $haberler
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $haberler=Haberler::find($id);
        $haberler->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }

}
