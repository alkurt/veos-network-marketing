<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\Category;
use App\Etkinlikler;
use App\Haberler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EtkinliklerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $etkinlikler = Etkinlikler::all();
        return view('admin.adminetkinlik', compact('etkinlikler', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.adminetkinlik_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $pdf = new Etkinlikler();
            $pdf->title = $request->title;
            $pdf->url=$request->url;
            $pdf->mekan=$request->mekan;
            $pdf->etkinlikler_icerik = $request->etkinlikler_icerik;
            $pdf->save();
            session()->flash('success','Etkinlik Oluşturuldu');
        return redirect()->route('etkinlikler.index');
    }
    public function download($uuid)
    {
        $book = Etkinlikler::where('uuid', $uuid)->firstOrFail();
        $pathToFile = storage_path('app/books/' . $book->cover);
        return response()->download($pathToFile);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Etkinlikler  $etkinlikler
     * @return \Illuminate\Http\Response
     */
    public function show(Etkinlikler $etkinlikler)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Etkinlikler  $etkinlikler
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $etkinlik=Etkinlikler::find($id);
        return view('layouts.etkinlikdetayi',compact('categoryMenu','etkinlik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Etkinlikler  $etkinlikler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Etkinlikler $etkinlikler)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Etkinlikler  $etkinlikler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Etkinlikler $etkinlikler)
    {
     $etkinlikler->delete();
     session()->flash('success','Kayıt Başarıyla Silindi');
     return back();
    }
}
