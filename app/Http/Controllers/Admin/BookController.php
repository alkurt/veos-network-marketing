<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\User;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('admin.books', compact('books','categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('admin.books_create',compact('categoryMenu'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('cover')) {

            $file =$request->cover;
            $rand = rand(1, 999999);
          //Storage::put('books/'.time(),$request->file('cover'));
            $new=$request->file('cover')->store('books');
            $pdf = new Book();
            $pdf->title = $request->title;
            $pdf->cover =$new;
            $pdf->uuid = $rand;
            $pdf->save();
        }
        session()->flash('success','Sözleşme Eklendi');
        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a=Book::find($id);
        $a->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
    public function download($uuid)
    {
        $book = Book::where('uuid',$uuid)->firstOrFail();
        $pathToFile = storage_path($book->cover);
        return Storage::download($book->cover);
    }
}
