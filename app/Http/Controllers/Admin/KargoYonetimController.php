<?php

namespace App\Http\Controllers\Admin;

use App\Banka;
use App\Category;
use App\KargoYonetim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KargoYonetimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kargoYonetim = KargoYonetim::all();
        return view('admin.kargo_yonetim', compact('kargoYonetim', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.kargoyonetim_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            "kargo_adi" => "required",
            "kargo_fiyati" => "required",
            "kargo_durumu" => "required",
        ];
        $messages=[
            'kargo_adi.required' => 'Bu alan gereklidir. ',
            'kargo_fiyati.required' => 'Bu alan gereklidir.',
            'kargo_durumu.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'kargo_adi','kargo_fiyati', 'kargo_durumu');
        if($kargoYonetim=KargoYonetim::create($input))
        {
            Session()->flash('success','Kargo Kaydı Oluşturuldu.');
            return redirect(route('kargoyonetim.index'));
        }
        else{
            Session()->flash('error','Kargo Kaydı Oluşturulamadı.');
            return redirect(route('kargoyonetim.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KargoYonetim  $kargoYonetim
     * @return \Illuminate\Http\Response
     */
    public function show(KargoYonetim $kargoYonetim)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KargoYonetim  $kargoYonetim
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kargoYonetim=KargoYonetim::find($id);
        return view('admin.kargo_edit',compact('categoryMenu','kargoYonetim'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KargoYonetim  $kargoYonetim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id )
    {
        //
        $rules=[
            "kargo_adi" => "required",
            "kargo_fiyati" => "required",
            "kargo_durumu" => "required",
        ];
        $messages=[
            'kargo_adi.required' => 'Bu alan gereklidir. ',
            'kargo_fiyati.required' => 'Bu alan gereklidir.',
            'kargo_durumu.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'kargo_adi','kargo_fiyati', 'kargo_durumu');
        $kargoYonetim= KargoYonetim::find($id);
        $kargoYonetim->update($input);
        session()->flash('success','Güncelleme Başarılı');
        return redirect()->route('kargoyonetim.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KargoYonetim  $kargoYonetim
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kargoYonetim = KargoYonetim::find($id);
    $kargoYonetim->delete();
    session()->flash('success','Silme Başarılı');
    return back();

    }
}
