<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Duyurular;
use App\Iletisim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DuyurularController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $duyuru = Duyurular::all();
        return view('admin.adminduyuru', compact('duyuru', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.adminduyuru_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            "duyuru_basligi" => "required",
            "duyuru_icerigi" => "required",

        ];
        $messages=[
            'duyuru_basligi.required' => 'Bu alan gereklidir. ',
            'duyuru_icerigi.required' => 'Bu alan gereklidir.',

        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'duyuru_basligi','duyuru_icerigi');
        if($duyuru=Duyurular::create($input))

        {
            Session()->flash('success','Duyurular Oluşturuldu.');
            return redirect(route('duyurular.index'));
        }
        else
        {
            Session()->flash('error','Duyurular Oluşturulamadı.');
            return redirect(route('duyurular.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Duyurular  $duyurular
     * @return \Illuminate\Http\Response
     */
    public function show(Duyurular $duyurular)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Duyurular  $duyurular
     * @return \Illuminate\Http\Response
     */
    public function edit(Duyurular $duyurular)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view('admin.duyuru_edit',compact('categoryMenu','duyurular'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Duyurular  $duyurular
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $rules=[
            "duyuru_basligi" => "required",
            "duyuru_icerigi" => "required",

        ];
        $messages=[
            'duyuru_basligi.required' => 'Bu alan gereklidir. ',
            'duyuru_icerigi.required' => 'Bu alan gereklidir.',

        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'duyuru_basligi','duyuru_icerigi');
        $duyuru =Duyurular::find($id);
        $duyuru->update($input);
        session()->flash('success','Güncelleme Başarılı');
        return redirect()->route('duyurular.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Duyurular  $duyurular
     * @return \Illuminate\Http\Response
     */
    public function destroy(Duyurular $duyurular)
    {
       $duyurular->delete();
       session()->flash('success','Kayıt Başarıyla Silindi');
       return back();
    }
}
