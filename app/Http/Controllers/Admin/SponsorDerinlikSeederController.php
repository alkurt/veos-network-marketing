<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Iletisim;
use App\Kariyer;
use App\SponsorDerinlik;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SponsorDerinlikSeederController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers = Kariyer::where('id','>=',5)->where('id','<=',15)->with('derinliks')->paginate(1);
        return view('admin.SponsorDerinlik', compact('kariyers', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.SponsorDerinlik_create",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            "kariyer" => "required",
            "derinlik" => "required",
            "yuzde" => "required",
        ];
        $messages=[
            'kariyer.required' => 'Bu alan gereklidir. ',
            'derinlik.required' => 'Bu alan gereklidir.',
            'yuzde.required' => 'Bu alan gereklidir.',

        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'derinlik','yuzde', 'kariyer');
        if($derinlik=SponsorDerinlik::create($input))

        {
            Session()->flash('success','Kayıt Oluşturuldu.');
            return redirect(route('sponsorderinlik.index'));
        }
        else
        {
            Session()->flash('error','Kayıt Oluşturulamadı.');
            return redirect(route('sponsorderinlik.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$derinlik)
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyer = Kariyer::where('id',$id)->with('derinliks')->first();
        $kariyer_derinlik= $kariyer->derinliks()->where('derinlik',$derinlik)->first();
        return view('admin.SponsorDerinlik_create',compact('categoryMenu'))->with('kariyer_derinlik',$kariyer_derinlik);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$derinlik)
    {
        $kariyer=Kariyer::where('id',$id)->with('derinliks')->first();
        $kariyer->derinliks()->where('kariyer_id',$id)->where('derinlik',$derinlik)->update(['derinlik'=>$request->derinlik,'yuzde'=>$request->yuzde]);
        session()->flash('success','Derinlik bilgileri güncellendi');
        return redirect(route('sponsorderinlik.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $derinlik=SponsorDerinlik::find($id);
        $derinlik->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
}
