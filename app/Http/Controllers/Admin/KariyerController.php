<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Kariyer;
use App\Kariyerb;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class KariyerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers = Kariyer::where('id','>=',1)->where('id','<=',15)->with('kariyerb')->get();
        return view('admin.kariyeredit', compact('kariyers', 'categoryMenu'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.addkariyer",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            "kariyername" => "required",
            "tutar" => "required",
            "indirim" => "required",
            "binary" => "required",
            "sponsor" => "required",
            'il_ciro'=>'required',
            'tr_ciro'=>'required',
            'dunya_ciro'=>'required',
            'araba_ciro'=>'required',
            'tazminati'=>'required',
            'lider_primi'=>'required',

        ];
        $messages=[
            'kariyername.required' => 'Bu alan gereklidir. ',
            'tutar.required' => 'Bu alan gereklidir.',
            'indirim.required' => 'Bu alan gereklidir.',
            'binary.required' => 'Bu alan gereklidir.',
            'sponsor.required' => 'Bu alan gereklidir.',
            'il_ciro'=>'Bu alan gereklidir.',
            'tr_ciro'=>'Bu alan gereklidir.',
            'dunya_ciro'=>'Bu alan gereklidir.',
            'araba_ciro'=>'Bu alan gereklidir.',
            'tazminati'=>'Bu alan gereklidir.',
            'lider_primi'=>'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'kariyername','tutar', 'indirim', 'sponsor', 'binary','il_cio','tr_ciro','araba_ciro','dunya_ciro','bolge_ciro','tazminati','lider_primi');
        if($kariyer=Kariyer::create($input)){
         Session()->flash('success','Kariyer Oluşturuldu.');
         $kariyer->Kariyerb()->create($input);
         return redirect(route('kariyer.index'));
        }
        else{
            Session()->flash('error','Kariyer Oluşturulamadı.');
            return redirect(route('kariyer.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kariyer  $kariyer
     * @return \Illuminate\Http\Response
     */
    public function show(Kariyer $kariyer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kariyer  $kariyer
     * @return \Illuminate\Http\Response
     */
    public function edit(Kariyer $kariyer)
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
      return view('admin.kariyer_edit',compact('kariyer','categoryMenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kariyer  $kariyer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        {
            $rules = [

                "tutar" => "required",
                "indirim" => "required",
                "binary" => "required",
                "sponsor" => "required",
                'il_ciro'=>'required',
                'tr_ciro'=>'required',
                'dunya_ciro'=>'required',
                'araba_ciro'=>'required',
                'bolge_ciro'=>'required',
                'tazminati'=>'required',
                'lider_primi'=>'required',
            ];
            $messages = [

                'tutar.required' => 'Bu alan gereklidir.',
                'indirim.required' => 'Bu alan gereklidir.',
                'binary.required' => 'Bu alan gereklidir.',
                'sponsor.required' => 'Bu alan gereklidir.',
                'il_ciro'=>'Bu alan gereklidir.',
                'tr_ciro'=>'Bu alan gereklidir.',
                'dunya_ciro'=>'Bu alan gereklidir.',
                'araba_ciro'=>'Bu alan gereklidir.',
                'bolge_ciro'=>'Bu alan gereklidir.',
                'tazminati'=>'Bu alan gereklidir.',
                'lider_primi'=>'Bu alan gereklidir.',
            ];
            $this->validate($request, $rules, $messages);
            $input = $request->only('tutar', 'indirim', 'sponsor', 'binary','il_cio','tr_ciro','araba_ciro','dunya_ciro','bolge_ciro','lider_primi','tazminati','lider_primi');
            $kariyer = Kariyer::find($id);
            $kariyer->Kariyerb()->update($input);
            $kariyer->update(['kariyername'=>$request->kariyername]);
            return redirect()->route('kariyer.index');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kariyer  $kariyer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kariyer $kariyer)
    {
        $kariyer->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
}
