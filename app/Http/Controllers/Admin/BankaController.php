<?php

namespace App\Http\Controllers\Admin;

use App\Banka;
use App\Category;
use App\Kariyer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $banka = Banka::all();
        return view('admin.banka', compact('banka', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view("admin.banka_ekle",compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            "banka_adi" => "required",
            "hesap_sahibi" => "required",
            "sube_kodu" => "required",
            "sube_adi" => "required",
            "hesap_no" => "required",
            "iban_no" => "required"

        ];
        $messages=[
            'banka_adi.required' => 'Bu alan gereklidir. ',
            'hesap_sahibi.required' => 'Bu alan gereklidir.',
            'sube_kodu.required' => 'Bu alan gereklidir.',
            'sube_adi.required' => 'Bu alan gereklidir.',
            'hesap_no.required' => 'Bu alan gereklidir.',
            'iban_no.required' => 'Bu alan gereklidir.'


        ];
        $this->validate($request,$rules,$messages);

        $input = $request->only( 'banka_adi','hesap_sahibi', 'sube_kodu', 'sube_adi', 'hesap_no','iban_no');
        if($banka=Banka::create($input)){
            Session()->flash('success','Hesap Oluşturuldu.');
            return redirect(route('banka.index'));
        }
        else{
            Session()->flash('error','Hesap Oluşturulamadı.');
            return redirect(route('banka.index'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banka  $banka
     * @return \Illuminate\Http\Response
     */
    public function show(Banka $banka)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banka  $banka
     * @return \Illuminate\Http\Response
     */
    public function edit(Banka $banka)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banka  $banka
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banka $banka)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banka  $banka
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banka $banka)
    {
        $banka->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
}
