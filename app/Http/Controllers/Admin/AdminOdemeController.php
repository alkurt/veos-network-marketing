<?php

namespace App\Http\Controllers\Admin;

use App\AdminOdeme;
use App\Category;
use App\cuzdan;
use App\EslesmeBinary;
use App\Gider;
use App\Iletisim;
use App\KargoYonetim;
use App\Kasa;
use App\OdemeTalebi;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class AdminOdemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $users=User::all();
        $adminOdeme=cuzdan::all();
        return view('admin.adminodeme', compact( 'categoryMenu','adminOdeme','users'));
    }
   /* public function binary_datatable()
    {
        $data = User::where('kullanici_id',\auth()->user()->id)->get();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad =User::find($row->eslesen_kullanici_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('eslesen_ad',function ($row){
                $ad =User::find($row->eslesen_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })
            ->make(true);
    }
*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminOdeme  $adminOdeme
     * @return \Illuminate\Http\Response
     */
    public function show(AdminOdeme $adminOdeme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminOdeme  $adminOdeme
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $talepler=OdemeTalebi::find($id);
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $cuzdan=$talepler->user->cuzdan;
        return view('admin.OdemeYapma',compact('categoryMenu','talepler','cuzdan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminOdeme  $adminOdeme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $messages=[
            'toplam_bakiye.numeric' => 'Girilen rakam harici değer içeremez. ',
            'odenen_bakiye.numeric' => 'Girilen rakam harici değer içeremez. ',
        ];
        $this->validate($request,
            [
                "odenen_bakiye" => "numeric",
                "toplam_bakiye" => "numeric",
            ],$messages);
        $this->validate($request,$messages);
        $input = $request->only( 'odenecek_bakiye','kesinti');
        $talep = OdemeTalebi::find($id);
        $user = $talep->user;
        $cuzdan = $user->cuzdan;
        $cuzdan->toplam_bakiye -=$input['odenecek_bakiye'];
        $cuzdan->toplam_bakiye -=$input['kesinti'];
        $cuzdan->odenen_bakiye +=$input['odenecek_bakiye'];

        if ($cuzdan->save()) {
            $kesinti = $request->kesinti ;
            $kasa = Kasa::find(1);
            $kasa->toplam_kasa += $kesinti;
            $kasa->save();
            Session()->flash('success', 'Ödeme Başarıyla Yapıldı.');
            $talep->status=0;
            $talep->save();
            $gider = new Gider();
            $gider->user_id = $user->id;
            $gider->amount = $input['odenecek_bakiye'];
            $gider->kesinti = $input['kesinti'];
            $gider->save();
            return redirect(route('odemetalebi'));
        }
        else
        {
            Session()->flash('error','Ödeme Yapılamadı!!.');
            return redirect(route('adminodeme.index'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminOdeme  $adminOdeme
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminOdeme $adminOdeme)
    {
        //
    }
}
