<?php

namespace App\Http\Controllers\Admin;

use App\BayiAktiflik;
use App\BayiPromosyon;
use App\Category;
use App\Kariyer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BayiPromosyonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyers= Kariyer::where('id','>=',9)->where('id','<=',15)->get();
        return view('admin.promosyon', compact( 'categoryMenu','kariyers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\BayiPromosyon  $bayiPromosyon
     * @return \Illuminate\Http\Response
     */
    public function show(BayiPromosyon $bayiPromosyon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BayiPromosyon  $bayiPromosyon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $promosyon = BayiPromosyon::where('id',$id)->first();
        return view('admin.promosyon_edit',compact('categoryMenu','promosyon'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BayiPromosyon  $bayiPromosyon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $promosyon=BayiPromosyon::where('id',$id)->first();
        $promosyon->update(['urun_adet'=>$request->urun_adet,'yuzde'=>$request->yuzde]);
        session()->flash('success','Bayi Promosyon güncellendi');
        return redirect(route('bayipromosyon.index'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BayiPromosyon  $bayiPromosyon
     * @return \Illuminate\Http\Response
     */
    public function destroy(BayiPromosyon $bayiPromosyon)
    {
        //
    }
}
