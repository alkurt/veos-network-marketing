<?php

namespace App\Http\Controllers\Admin;

use App\AlisverisDerinlikDagilimi;
use App\BayiIndirimFark;
use App\Category;
use App\Kariyer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlisverisDerinlikDagilimiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $alisveris_derinlik=AlisverisDerinlikDagilimi::all();
        return view('admin.Alisveris_Derinlik_index', compact( 'categoryMenu','alisveris_derinlik'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AlisverisDerinlikDagilimi  $alisverisDerinlikDagilimi
     * @return \Illuminate\Http\Response
     */
    public function show(AlisverisDerinlikDagilimi $alisverisDerinlikDagilimi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AlisverisDerinlikDagilimi  $alisverisDerinlikDagilimi
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $alisveris_derinlik = AlisverisDerinlikDagilimi::where('id',$id)->first();
        return view('admin.Alisveris_Derinlik_Edit',compact('categoryMenu','alisveris_derinlik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AlisverisDerinlikDagilimi  $alisverisDerinlikDagilimi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $alisveris_derinlik=AlisverisDerinlikDagilimi::where('id',$id)->first();
        $alisveris_derinlik->update(['yuzde'=>$request->yuzde]);
        session()->flash('success','Alışveriş Derinlik Bilgileri Güncellendi');
        return redirect(route('alisverisderinlikdagilimi.index'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AlisverisDerinlikDagilimi  $alisverisDerinlikDagilimi
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlisverisDerinlikDagilimi $alisverisDerinlikDagilimi)
    {
        //
    }
}
