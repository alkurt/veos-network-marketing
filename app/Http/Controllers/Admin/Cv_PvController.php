<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Cv_PvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $user=User::find($id);
        return view('admin.cv_pv_admin',compact('categoryMenu','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules=[
            "cv" => "numeric",
            "pv" => "numeric",
        ];
        $messages=[
            'cv.numeric' => 'Bu alan sayısal değer gereklidir. ',
            'pv.numeric' => 'Bu alan sayısal değer gereklidir. ',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'cv','pv');
        $user= User::find($id);
        $user->ara_pv +=$input['eklenecek_pv'];
        $user->cv +=$input['eklenecek_cv'];
        if ($user->save()) {
            Session()->flash('success', 'Ekleme Başarılı.');
            return redirect(route('adminekip'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
