<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\Category;
use App\Katalog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class KatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $katalog = Katalog::all();
        return view('admin.katalog_index', compact('katalog','categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('admin.katalog_create',compact('categoryMenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('cover')) {

            $file =$request->cover;
            $rand = rand(1, 999999);
            //Storage::put('books/'.time(),$request->file('cover'));
            $new=$request->file('cover')->store('katalogs');
            $pdf = new Katalog();
            $pdf->title = $request->title;
            $pdf->cover =$new;
            $pdf->uuid = $rand;
            $pdf->save();
        }
        session()->flash('success','E-Katalog Eklendi');
        return redirect()->route('katalog.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Katalog  $katalog
     * @return \Illuminate\Http\Response
     */
    public function show(Katalog $katalog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Katalog  $katalog
     * @return \Illuminate\Http\Response
     */
    public function edit(Katalog $katalog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Katalog  $katalog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Katalog $katalog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Katalog  $katalog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $katalog=Katalog::find($id);
        $katalog->delete();
        session()->flash('success','Kayıt Başarıyla Silindi');
        return back();
    }
    public function download($uuid)
    {
        $katalog = Katalog::where('uuid',$uuid)->firstOrFail();
        $pathToFile = storage_path($katalog->cover);
        return Storage::download($katalog->cover);
    }
}
