<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Kariyerb;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class KariyerbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view('layouts.addkariyer',compact('categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[


            "tutar" => "required",
            "sponsor" => "required",
            "indirim" => "required|numeric",
            "binary" => "required|numeric",
            'il_ciro'=>'required',
            'tr_ciro'=>'required',
            'dunya_ciro'=>'required',
            'araba_ciro'=>'required',
            'bolge_ciro'=>'required',
            'tazminati'=>'required',
        ];
        $messages=[

            'tutar.required' => 'Bu Alan Gereklidir.',
            'sponsor.required' => 'Bu Alan Gereklidir.',
            'indirim.required' => 'Bu Alan Gereklidir.',
            'binary.required' => ' Bu Alan Gereklidir.',
            'il_ciro'=>'Bu alan gereklidir.',
            'tr_ciro'=>'Bu alan gereklidir.',
            'dunya_ciro'=>'Bu alan gereklidir.',
            'araba_ciro'=>'Bu alan gereklidir.',
            'bolge_ciro'=>'Bu alan gereklidir.',
            'tazminati'=>'Bu alan gereklidir.',

        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kariyerb  $kariyerb
     * @return \Illuminate\Http\Response
     */
    public function show(Kariyerb $kariyerb)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kariyerb  $kariyerb
     * @return \Illuminate\Http\Response
     */
    public function edit(Kariyerb $kariyerb)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kariyerb  $kariyerb
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,
            [

                "sponsor" => "required|numeric",
                "tutar" => "required|numeric", //paket Alt Limiti
                "indirim" => "required",
                "binary" => "required",
                'il_ciro'=>'required',
                'tr_ciro'=>'required',
                'dunya_ciro'=>'required',
                'araba_ciro'=>'required',
                'bolge_ciro'=>'required',
                'tazminati'=>'required',
                'lider_primi'=>'required',

            ]);
        $input = $request->only('kariyername','tutar', 'indirim', 'binary', 'sponsor','il_ciro','tr_ciro','dunya_ciro','araba_ciro','bolge_ciro','tazminati','lider_primi');

        $kariyerbs = Kariyerb::find($id);
        $kariyerbs->kariyer->update(['kariyername'=>$request->kariyername]);
        $kariyerbs->update($input);
        return redirect()->route('kariyeredit.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kariyerb  $kariyerb
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kariyerb $kariyerb)
    {
        //
    }
}
