<?php

namespace App\Http\Controllers\Admin;

use App\AnaPara;
use App\Category;
use App\KargoYonetim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnaParaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $anapay = AnaPara::all();
        return view('admin.Anapara', compact('anapay', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AnaPara  $anaPara
     * @return \Illuminate\Http\Response
     */
    public function show(AnaPara $anaPara)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AnaPara  $anaPara
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $anapay=AnaPara::find($id);
        return view('admin.Anapara_edit',compact('categoryMenu','anapay'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnaPara  $anaPara
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules=[
            "region" => "required",
            "city" => "required",
            "world" => "required",
            "city" => "required",
        ];
        $messages=[
            'city.required' => 'Bu alan gereklidir. ',
            'region.required' => 'Bu alan gereklidir.',
            'world.required' => 'Bu alan gereklidir.',
            'city.required' => 'Bu alan gereklidir.',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'world','city', 'region','turkey');
        $anapay= AnaPara::find($id);
        $anapay->update($input);
        session()->flash('success','Güncelleme Başarılı');
        return redirect()->route('anapara.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AnaPara  $anaPara
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnaPara $anaPara)
    {
        //
    }
    public function kariyerraporu()
    {
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        return view('admin.kariyerraporu',compact('categoryMenu'));
    }
}
