<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\KargoYonetim;
use App\Kariyer;
use App\LiderCikarma;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LiderCikarmaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $kariyer= Kariyer::where('id','>=',8)->where('id','<=',15)->get();
        return view('admin.lider-cikarma-edit', compact('kariyer', 'categoryMenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LiderCikarma  $liderCikarma
     * @return \Illuminate\Http\Response
     */
    public function show(LiderCikarma $liderCikarma)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LiderCikarma  $liderCikarma
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $lider_cikarma=LiderCikarma::find($id);
        return view('admin.lider-cikarma-create',compact('categoryMenu','lider_cikarma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LiderCikarma  $liderCikarma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules=[
            "tutar" => "required",

        ];
        $messages=[
            'tutar.required' => 'Bu alan gereklidir. ',
        ];
        $this->validate($request,$rules,$messages);
        $input = $request->only( 'tutar');
        $lider_cikarma= LiderCikarma::find($id);
        $lider_cikarma->update($input);
        session()->flash('success','Güncelleme Başarılı');
        return redirect()->route('lidercikarma.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LiderCikarma  $liderCikarma
     * @return \Illuminate\Http\Response
     */
    public function destroy(LiderCikarma $liderCikarma)
    {
        //
    }
}
