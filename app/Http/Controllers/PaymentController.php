<?php

namespace App\Http\Controllers;

use App\AnaPara;
use App\Banka;
use App\Basket;
use App\BasketProduct;
use App\Category;
use App\cuzdan;
use App\EftHavaleBekleme;
use App\Kariyer;
use App\Kariyerb;
use App\MailOrder;
use App\Services\PaymentService;
use App\Order;
use App\SponsorDerinlik;
use App\User;
use App\UserDetail;
use Gloudemans\Shoppingcart\Facades\Cart;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use function Composer\Autoload\includeFile;

class PaymentController extends Controller
{
    //

    public function paytrodemeekrani(){
        $bilgiler=UserDetail::find(Auth::id());
        return view('iframe_ornek')->with('bilgiler',$bilgiler);
    }
    public function index()
    {
        $user1= Kariyerb::find(1);
        $user2= Kariyerb::find(2);
        $user3= Kariyerb::find(3);
        $user4= Kariyerb::find(4);
        $user5= Kariyerb::find(5);

        $urunler=BasketProduct::where('basket_id',session('active_basket_id'))->with('product')->get();
        $kisi_indirim_kontrol=\auth()->user()->id;
        $eft_kontrol = EftHavaleBekleme::where('user_id',$kisi_indirim_kontrol)->where('durum',false)->first();
        $price = 0;
        $a = Auth::user()->orders;
        $toplam = $a->sum('order_price');
        $urn = false;
        $paket = false;
        foreach ($urunler as $urun){
            if(auth()->user()->kariyer_id == 16 && $urun->product->turu=='urun'){
                if (\auth()->user()->orders->count() ==0){
                    \session()->flash('error','Kariyer sahibi olmadan ürün alamazsınız, lütfen paket alımından sonra deneyiniz.');
                    Cart::destroy();

                    session()->forget('active_basket_id');
                    session()->forget('order_no');
                    return back();
                }
            }
            elseif (auth()->user()->kariyer_id == 17 && $urun->product->turu=='paket' ){
                \session()->flash('error','Üyelik tipiniz bu alışverişe uygun değil.');
                return back();
                Cart::destroy();

                session()->forget('active_basket_id');
                session()->forget('order_no');
            }

                if ($eft_kontrol)
                {
                    session()->flash('error', "Üzgünüz!! Bir Önceki Siparişiniz Onaylandıktan Sonra Alışverişe Devam Edebilirsiniz...");
                    return back();
                }
                if ($urun->product->turu == 'paket')
            {
                $paket = true;
            }

            // sıkıntı burda sadece tek ürünü alıyo urunlerinkini alması gerek

            $price += $urun->price * $urun->quantity;
        }

                $kisi=\auth()->user()->kariyer_id;
                if ($kisi==16)         // toplam = şimdiye kadar yaptığı alışveriş tutarı // price = sepetteki ürün fiyatı * adet
                {
                    if ($paket == true && $toplam > $user1->tutar && $price < $user1->tutar)
                    {
                        session()->flash('error', "Müşteri Bayi Olmak İçin Sepetinizi $user1->tutar ₺'ye Tamamlamanız Gerekmektedir.");
                        return back();
                    }
                }
                //-------------------------------------
                if ($kisi==1)
                {
                    $r1 = $user2->tutar-$user1->tutar;

                    if ($paket == true && $toplam <= $user2->tutar-$user1->tutar && $price < ($user2->tutar-$user1->tutar)) {
                        session()->flash('error', "Kullanıcı Bayi Olmak İçin Sepetinizi $r1 ₺'ye Tamamlamanız Gerekmektedir.");
                        return back();
                    }
                }
                //-----------------------------------
                if ($kisi==2)
                {
                    $r2 =$user3->tutar-$user2->tutar;
                    if ($paket == true && $toplam <= ($user3->tutar)-($user2->tutar) && $price < ($user3->tutar)-($user2->tutar))
                    {
                        session()->flash('error', "Başlangıç Bayi Olmak İçin Sepetinizi $r2 ₺'ye Tamamlamanız Gerekmektedir.");
                        return back();
                    }
                }
                //-------------------------------
                if ($kisi==3)
                {
                   $r3= $user4->tutar-$user3->tutar;
                    if ($paket == true && $toplam >= ($user4->tutar)-($user3->tutar) && $price < ($user4->tutar)-($user3->tutar))
                    {
                        session()->flash('error', "Girişimci Adayı Bayi Olmak İçin Sepetinizi $r3 ₺'ye Tamamlamanız Gerekmektedir.");
                        return back();
                    }
                }
                //---------------------------
                if ($kisi==4)
                {
                    $r4 = $user5->tutar-$user4->tutar;
                    if ($paket == true && $toplam <= ($user5->tutar)-($user4->tutar) && $price < ($user5->tutar)-($user4->tutar)) {
                        session()->flash('error', "Girişimci Bayi Olmak İçin $r4 Olması Gerekmektedir.");
                        return back();
                    }
                }


        $data         = [];
        $categoryMenu = Category::orderBy('category_name', 'asc')->get();
        $user         = auth()->user();
        $user_detail  = $user->detail;
        $order        = Cart::subtotal();

        $random       = rand(1, 100000).date('YmdHi');
        $data["user_detail"]  = $user_detail;
        $data["user"]         = $user;
        $data["order"]        = $order;
        $data["categoryMenu"] = $categoryMenu;

        session()->put('order_no', $random);

        $form                   = [];
        $form['sessionOrderNo'] = session('order_no');
        $form['orderPrice']     = $order;
        $form['basketID']       = session('active_basket_id');
        $form['route']          = 'pay';
        $form['userID']         = $user->id;
        $form['name']           = $user->name;
        $form['surname']        = $user->surname;
        $form['phone']          = $user->detail->phone;
        $form['email']          = $user->email;
        $form['city']           = $user->detail->city;
        $form['country']        = $user->detail->country;
        $form['zipcode']        = $user->detail->zipcode;
        $form['address']        = $user->detail->address;

        $cuzdan = $user->cuzdan;

        return view('payment',compact('cuzdan'))->with($data);
    }
    public function pay(Request $request)
    {
        $req = $request->only('type');

        $token   = session('_token');
        $orderNo = session('order_no');
        $user = Auth()->user();
        $order                   = [];
        $order['name']           = $user->name.' '.$user->surname;
        $order['address']        = $user->detail->address;
        $order['phone']          = $user->detail->phone;
        $order['m_phone']        = $user->detail->m_phone;
        $order['basket_id']      = session('active_basket_id');
        $order['user_id']        = Auth::id();
        $order['installments']   = 1;
        $order['status']         = "Siparişiniz Alındı";
        $order['payment_method'] = $req['type'];
//        $indirimli_fiyat=((float)Cart::subtotal());
//        $indirimli_fiyat=$indirimli_fiyat - (($indirimli_fiyat * $user->kariyer->Kariyerb->indirim)/100);

        $order['order_price']= Cart::subtotal();
        if ($user->nakitindirim()->exists()){
            $paket=false;
            if($user->nakitindirim->status)
           {
               $basket_products=Basket::find($order['basket_id'])->basket_products;
               foreach ($basket_products as $bsp){
                   if ($bsp->product->turu == 'paket'){
                       $paket =true;
                   }
               }
               if (!$paket) {
                   $order['order_price'] =$order['order_price']- $user->nakitindirim->indirim_tutar;
               }
           }
        }
        if($user->kariyer_id >=9 && $user->kariyer_id <= 15){ //Bayi Promosyon Oranı
            if ($user->kariyer->id ==9 || $user->kariyer->id==10){
                $urunler=BasketProduct::where('basket_id',$order['basket_id'])->with('product')->get();
                $urun_sayisi=$urunler->count();
                $indirim_yuzdesi = $user->kariyer->bayipromosyon()->where('urun_adet',$urun_sayisi)->first()->yuzde;
                $order['order_price'] -=$order['order_price'] * $indirim_yuzdesi/ 100;
            }else{
                $indirim_yuzdesi = $user->kariyer->bayipromosyon()->first()->yuzde;

                $order['order_price'] -=$order['order_price'] * $indirim_yuzdesi/ 100;
            }
        }
        $order['token']          = $token;
        $order['order_no']       = session('order_no');

        if ($order['order_price']<0){
            $order['order_price']=1;
        }
        elseif($order['payment_method'] == "Havale/Eft"){

            $eft = EftHavaleBekleme::create($order);
            \session()->flash('success','Eft talebiniz alınmıştır. Ödemeniz onaylandıktan sonra işleminiz gerçekleştirelecektir.');
            Cart::destroy();

            session()->forget('active_basket_id');
            session()->forget('order_no');

            return redirect(route('bankahesapbilgileri.kisiyeozel',$eft ));
        }
        elseif($order['payment_method'] == "mailorder"){
            $categoryMenu = Category::orderBy('category_name', 'asc')->get();
            $order['cardnu']            = null;
            $order['cardname']          = null;
            $order['cardcvc']           = null;
            $order['cardmonth']         = null;
            $order['cardyear']          = null;
            $mailorder = MailOrder::create($order);
            \session()->flash('success','veosnet.com ile MailOrder Güvenli Ödeme Ekranına Yönlendirildiniz');
            Cart::destroy();

            session()->forget('active_basket_id');
            session()->forget('order_no');

            return view('layouts.MailOrder',compact('mailorder','categoryMenu'));
        }
        else{
            $client = new Client();
            $res = $client->request('post', 'https://posservice.elekse.com/api/pay/TrustedPay', [
                'headers' => ['Content-Type' => 'application/json'],
                'verify' => false,
                'body'=>'{
                "Config" :  {
                    "MERCHANT" : "veosnet.com",
                    "MERCHANT_KEY" : "wIe4+f2qFtXG8egx9y4mLERiVVsLqC5PndQHfXT9705tCJ2kaM2zrQ==",
                    "BACK_URL" : "http://veosnet.com/guzzle-sonuc/'.$order['order_no'].'",
                    "PRICES_CURRENCY" : "TR",
                    "ORDER_REF_NUMBER" : "'.$order['order_no'].'",
                    "ORDER_AMOUNT" : "'.$order['order_price'].'"
                },
                "CreditCard" : {
                    "INSTALLMENT_NUMBER" : "1"
                },
                "Customer" : {
                    "FIRST_NAME" : "'.$user->name.'",
                     "LAST_NAME" : "'.$user->surname.'",
                     "MAIL" : "'.$user->email.'",
                     "PHONE" : "'.$user->detail->m_phone.'",
                     "CITY" : "'.$user->detail->city.'",
                     "STATE" : "'.$user->detail->country.'",
                     "ADDRESS" : "'.$user->detail->address.'"
                }
             }'
            ]);
            if   ($res->getStatusCode()==200){
                $a= json_decode($res->getBody()) ;

                return redirect($a->URL_3DS);
            }else{
                Session::flash('success',"Bilinmeyen bir hata meydana geldi.");
                return redirect(route('home'));
            }
        }
        //return redirect()->route('orders');
    }
}
