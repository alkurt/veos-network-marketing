<?php

namespace App\Http\Controllers;

use App\Banka;
use App\BolgeCıro;
use App\Category;
use App\cuzdan;
use App\Duyurular;
use App\EftHavaleBekleme;
use App\EslesmeBinary;
use App\Order;
use App\ProfilCreate;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class PrimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view("prim_menu.ekipagaci", compact("categoryMenu"));

    }
    public function genelbakis()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        return view("prim_menu.genelbakis", compact("categoryMenu"));
    }
    public function isteklerim()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $users= User::where('sponsor_id',Auth::id())->where('konum',null)->get();
        $agacim= User::where('root_id', auth()->user()->id)->where('konum','!=', null)->count();
        return view('layouts.istekler',compact('users','categoryMenu','agacim'))->with('idCount',count($users));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $cuzdan=\auth()->user()->cuzdan;
        return view("layouts.prim_menu.cuzdan", compact("categoryMenu",'cuzdan'));
    }
    public function binary()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $eslesmeler=\auth()->user()->binary;
        return view("auth.binary", compact("categoryMenu",'eslesmeler'));
    }
    public function binary_datatable()
    {
        $data = EslesmeBinary::where('kullanici_id',\auth()->user()->id)->get();
        return Datatables::of($data)
            ->addColumn('ad',function ($row){
                $ad =User::find($row->eslesen_kullanici_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('eslesen_ad',function ($row){
                $ad =User::find($row->eslesen_id);
                $ad = $ad->name." ".$ad->surname;
                return $ad;
            })->addColumn('devreden1',function ($row){
                $ad =User::find($row->eslesen_kullanici_id);
                $ad = $ad->cv;
                return $ad;
            })->addColumn('devreden2',function ($row){
                $ad =User::find($row->eslesen_id);
                $ad = $ad->cv;
                return $ad;
            })
            ->make(true);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $dallar = User::where('root_id',auth()->user()->id)->get();
        $kisi=User::find(auth()->user()->id);
        return view("layouts.prim_menu.kazanc", compact("categoryMenu"))->with('dallar',$dallar)->with('kisi',$kisi);
    }
    public function gonderme($kid)
    {
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $dallar = User::where('root_id',$kid)->get();
        $kisi = User::find(($kid));
return view('layouts.prim_menu.kazanc',compact('categoryMenu'))->with('dallar', $dallar)->with('kisi', $kisi);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $usera= User::find($id);
       $usera->delete();
       $usera->save();
       return back();

    }
public function menu()
{
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    $orders = Order::where('user_id',auth()->user()->id)->get();
    $duyuru=Duyurular::all();
    $altkisi = User::where('sponsor_id',Auth::id())->get();
    $profil_creates = ProfilCreate::where('user_id',\auth()->user()->id)->first();
    $profil = ProfilCreate::where('user_id',\auth()->user()->id)->count();
    $solpv=0;
    $sagpv=0;
    $solcv=0;
    $sagcv=0;
    foreach ($altkisi as $a)
    {
        for ($i=1; $i<=4; $i++)
        {
            if ( $a->konum == $i)
                {
                    $solpv += $a->ara_pv;
                }
            for ($k=1; $k<=9; $k++)
            {
                if ($a->konum == (($i).'.'.$k))
                {
                 $solpv+= $a->ara_pv;
                }
            }
        }
        for ($i=5; $i<=8; $i++)
        {
            if ( $a->konum == $i)
            {
                $sagpv += $a->ara_pv;
            }
            for ($k=1; $k<=9; $k++)
            {
                if ($a->konum == (($i).'.'.$k))
                {
                    $sagpv += $a->ara_pv;
                }
            }
        }
        //------------------------------------------------------------
        for ($i=1; $i<=4; $i++)
        {
            $t = $a->cv;
            if ( $a->konum == $i)
            {
                $solcv += $t;
            }
            for ($k=1; $k<=9; $k++)
            {
                if ($a->konum == (($i).'.'.$k))
                {
                    $solcv+= $t;
                }
            }
        }
        for ($i=5; $i<=8; $i++)
        {
            if ( $a->konum == $i)
            {
                $sagcv += $t;
            }
            for ($k=1; $k<=9; $k++)
            {
                if ($a->konum == (($i).'.'.$k))
                {
                    $sagcv += $t;
                }
            }
        }
    }
    return view("menu.menu",compact("categoryMenu",'duyuru','solpv','profil_creates','profil','sagpv','solcv','sagcv'))->with('orderCount',count($orders));
}
public function bilgilendirme()
{
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    return view('layouts.konumbilgi',compact('categoryMenu'));
}
public function ekiplistesi()
{
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    $users= User::where('sponsor_id',Auth::id())->get();
    return view('layouts.ekiplistesi',compact('categoryMenu','users'));
}
public function ekiptakip()
{
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    $users= User::where('sponsor_id',Auth::id())->get();
    return view('layouts.ekiptakip',compact('categoryMenu','users'));
}
public function farkli_il_kayit_index(){
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    return view('farkli_il_kayit',compact('categoryMenu'));
}
    public function farkli_il_kayit_store(Request $request){
        $user = auth()->user();
        $data = $request->only('iller','bolge','ulke','root_id','password','id','type','sponsor_id');
        if($data['type'] == 'b') {
            $data['kariyer_id']=16;
        } elseif($data['type'] == 'm') {
            $data['kariyer_id']=17;
        }
        if ($data['iller']=="34"||$data['iller']=="22"||$data['iller']=='39'||$data['iller']=='59'||$data['iller']=='41'||$data['iller']=='77'||$data['iller']=='54'||$data['iller']=='11'||$data['iller']=='16'||$data['iller']=='10'||$data['iller']=='17')
        {
            $data['bolge']=1;
        }
        elseif ($data['iller']=="68"||$data['iller']=="06"||$data['iller']=='18'||$data['iller']=='26'||$data['iller']=='70'||$data['iller']=='71'||$data['iller']=='40'||$data['iller']=='42'||$data['iller']=='50'||$data['iller']=='51'||$data['iller']=='58'||$data['iller']=='66'||$data['iller']=='38')
        {
            $data['bolge']=2;

        }
        elseif ($data['iller']=="35"||$data['iller']=="45"||$data['iller']=='9'||$data['iller']=='20'||$data['iller']=='43'||$data['iller']=='3'||$data['iller']=='64'||$data['iller']=='48')
        {
            $data['bolge']=3;

        }
        elseif ($data['iller']=='01'||$data['iller']=='80'||$data['iller']=='7'||$data['iller']=='15'||$data['iller']=='31'||$data['iller']=='32'||$data['iller']=='33'||$data['iller']=='46')
        {
            $data['bolge']=4;

        }
        elseif ($data['iller']=="53"||$data['iller']=="61"||$data['iller']=='8'||$data['iller']=='60'||$data['iller']=='19'||$data['iller']=='5'||$data['iller']=='55'||$data['iller']=='67'||$data['iller']=='14'||$data['iller']=='81'||$data['iller']=='78'||$data['iller']=='74'||$data['iller']=='37'||$data['iller']=='69'||$data['iller']=='28'||$data['iller']=='29'||$data['iller']=='52'||$data['iller']=='57')
        {
            $data['bolge']=5;

        }
        elseif ($data['iller']=="4"||$data['iller']=="75"||$data['iller']=='12'||$data['iller']=='13'||$data['iller']=='23'||$data['iller']=='24'||$data['iller']=='25'||$data['iller']=='76'||$data['iller']=='30'||$data['iller']=='36'||$data['iller']=='44'||$data['iller']=='49'||$data['iller']=='62'||$data['iller']=='65'||$data['iller']=='73')
        {
            $data['bolge']=6;

        }
        elseif ($data['iller']=="2"||$data['iller']=="72"||$data['iller']=='21'||$data['iller']=='27'||$data['iller']=='79'||$data['iller']=='47'||$data['iller']=='56'||$data['iller']=='63')
        {
            $data['bolge']=7;
        }
        $kontrol=User::where('user_id',\auth()->user()->id)->where('iller_id', $request->iller)->exists();
        if ($kontrol)
        {
        session()->flash('error','Aynı İlden Sadece Bir Bayiye Sahip Olabilirsiniz. ');
        return back();
        }
        $create = User::create([
            'id'=> $this->generateBarcodeNumber(),
            'name'=>$user->name,
            'surname' =>$user->surname,
            'kariyer_id'=>$data['kariyer_id'],
            'kimlik' => $user->kimlik,
            'dogum' => $user->dogum,
            'ulke' => $data['ulke'],
            'iller_id' => $data['iller'],
            'bolge_id' => $data['bolge'],
            'telefon' => $user->telefon,
            'email' => $user->email,
            'root_id'=>\auth()->user()->id,
            'sponsor_id' => \auth()->user()->id,
            'user_id'=> $user->id,
            'password' => Hash::make($data['password']),
        ]);
        if ($create){
            Session()->flash('success','Kayıt Eklendi');
            $input = [];
            $input["user_id"] = $create->id;
            $user_detail =UserDetail::create($input);
            $user_detail->m_phone = $create->m_phone;
            $user_detail -> city = $create->iller->name;
            $user_detail -> country = $create->ulke;
            $user_detail->save();
            $create->roles()->attach(2);
            $create->cuzdan()->create();

            if(auth()->user()->id == '10001')
            {return redirect(route('binarykontrol')); }
            else
                return redirect(route('kazanc'));
        }
        else{
            Session()->flash('error','Kayıt Eklenemedi');
            if (auth()->user()->id=='10001')
            { return redirect(route('binarykontrol'));}
            else
                return redirect(route('kazanc'));
        }

    }

    private function generateBarcodeNumber()
    {
        function barcodeNumberExists2($number) {
            // query the database and return a boolean
            // for instance, it might look like this in Laravel
            return User::where('id',$number)->exists();
        }
        $number = mt_rand(10000, 99999); // better than rand()

        // call the same function if the barcode exists already
        if (barcodeNumberExists2($number)) {
            return generateBarcodeNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }
    public function alt_bayiliklerim(){
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    $users=User::where('user_id', Auth::user()->id)->get();
    return view('layouts.alt-bayiklerim',compact('categoryMenu','users'));
}

public function bankahesapbilgileri(){
    $categoryMenu = Category::orderBy('category_name','asc')->get();
    $banka= Banka::all();
    return view('auth.bankaHesapBilgileri',compact('categoryMenu','banka'));
}
    public function bankahesapbilgilerikisiyeozel($eft){
$eft = EftHavaleBekleme::find($eft);
        $categoryMenu = Category::orderBy('category_name','asc')->get();
        $banka= Banka::all();
        return view('auth.bankaHesapBilgileriKisiyeOzel',compact('categoryMenu','banka','eft'));
    }

}
