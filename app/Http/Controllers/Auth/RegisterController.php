<?php

namespace App\Http\Controllers\Auth;

use App\Category;
use App\Transfer;
use App\User;
use App\Http\Controllers\Controller;
use App\UserDetail;
use Illuminate\Support\Facades\Auth;
use function App\Http\Controllers\Auth\barcodeNumberExists;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected  $redirectTo ='/home';



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'id.integer'=>'Bu :attribute ',
            'kimlik.unique' => '  Bu :attribute ile kayıt daha önce alınmış.',
            'kimlik.string' => '   Lütfen :attribute bilgilerinizi doğru giriniz!! ',
            'sponsor_id.required' => ' Bu :attribute gereklidir.',
            'sponsor_id.string' => ' Bu :attribute gereklidir.',
            'sponsor_id.exists' => ' Bu sponsor numarası bulunmamaktadır.',
            'dogum'=>'Bilgilerinizi Tekrar Kontrol Ediniz'
        ];
        return Validator::make($data, [
            'id'=>'integer|max:5|min:5',
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'kimlik' => 'required|string|max:11|min:11|unique:users',
            'dogum'=>'required|string|max:4|min:4',
            'telefon' => 'required|string|max:11|min:11',
            'email' => 'string|email|max:255|unique:users',
            'type'=>'required',
            'sponsor_id' => 'required|string|max:11|exists:users,id',
            'password' => 'required|string|min:6|confirmed',
        ],$messages);
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
   public function generateBarcodeNumber() {
        function barcodeNumberExists($number) {
           // query the database and return a boolean
           // for instance, it might look like this in Laravel
           return User::where('id',$number)->exists();
       }
        $number = mt_rand(10000, 99999); // better than rand()

        // call the same function if the barcode exists already
        if (barcodeNumberExists($number)) {
            return generateBarcodeNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }
    protected function create(array $data)
    {
        $data['id']=intval( $this->generateBarcodeNumber());
        if($data['type'] == 'b') {
            $data['kariyer_id']=16;
        } elseif($data['type'] == 'm') {
            $data['kariyer_id']=17;
        }
        if ($data['iller']=="34"||$data['iller']=="22"||$data['iller']=='39'||$data['iller']=='59'||$data['iller']=='41'||$data['iller']=='77'||$data['iller']=='54'||$data['iller']=='11'||$data['iller']=='16'||$data['iller']=='10'||$data['iller']=='17')
        {
            $data['bolge']=1;
        }
        elseif ($data['iller']=="68"||$data['iller']=="6"||$data['iller']=='18'||$data['iller']=='26'||$data['iller']=='70'||$data['iller']=='71'||$data['iller']=='40'||$data['iller']=='42'||$data['iller']=='50'||$data['iller']=='51'||$data['iller']=='58'||$data['iller']=='66'||$data['iller']=='38')
        {
            $data['bolge']=2;

        }
        elseif ($data['iller']=="35"||$data['iller']=="45"||$data['iller']=='9'||$data['iller']=='20'||$data['iller']=='43'||$data['iller']=='3'||$data['iller']=='64'||$data['iller']=='48')
        {
            $data['bolge']=3;

        }
        elseif ($data['iller']=='01'||$data['iller']=='80'||$data['iller']=='7'||$data['iller']=='15'||$data['iller']=='31'||$data['iller']=='32'||$data['iller']=='33'||$data['iller']=='46')
        {
            $data['bolge']=4;

        }
        elseif ($data['iller']=="53"||$data['iller']=="61"||$data['iller']=='8'||$data['iller']=='60'||$data['iller']=='19'||$data['iller']=='5'||$data['iller']=='55'||$data['iller']=='67'||$data['iller']=='14'||$data['iller']=='81'||$data['iller']=='78'||$data['iller']=='74'||$data['iller']=='37'||$data['iller']=='69'||$data['iller']=='28'||$data['iller']=='29'||$data['iller']=='52'||$data['iller']=='57')
        {
            $data['bolge']=5;

        }
        elseif ($data['iller']=="4"||$data['iller']=="75"||$data['iller']=='12'||$data['iller']=='13'||$data['iller']=='23'||$data['iller']=='24'||$data['iller']=='25'||$data['iller']=='76'||$data['iller']=='30'||$data['iller']=='36'||$data['iller']=='44'||$data['iller']=='49'||$data['iller']=='62'||$data['iller']=='65'||$data['iller']=='73')
        {
            $data['bolge']=6;

        }
        elseif ($data['iller']=="2"||$data['iller']=="72"||$data['iller']=='21'||$data['iller']=='27'||$data['iller']=='79'||$data['iller']=='47'||$data['iller']=='56'||$data['iller']=='63')
        {
            $data['bolge']=7;
        }

        $create = User::create([
            'id'=>$data['id'],
            'name' => strtoupper($data['name']),
            'surname' => strtoupper($data['surname']),
            'kariyer_id'=>$data['kariyer_id'],
            'kimlik' => $data['kimlik'],
            'dogum' => $data['dogum'],
            'telefon' => $data['telefon'],
            'email' => $data['email'],
            'ulke' => $data['ulke'],
            'iller_id' => $data['iller'],
            'bolge_id' => $data['bolge'],
            'sponsor_id' => $data['sponsor_id'],
            'root_id' => $data['sponsor_id'],
            'password' => Hash::make($data['password']),
        ]);
        $create->roles()->attach(2);
        $input = [];
        $input["user_id"] = $create->id;
        $user_detail = UserDetail::create($input);
        $user_detail->m_phone = $data['telefon'];
        $user_detail -> city = $create->iller->name;
        $user_detail -> country = $create->ulke;
        $user_detail->save();
        $create->cuzdan()->create([]);
        return $create;


    }

}
