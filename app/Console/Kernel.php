<?php

namespace App\Console;

use App\AnaPara;
use App\cuzdan;
use App\EslesmeBinary;
use App\Firsat;
use App\Iller;
use App\Kariyerb;
use App\Kasa;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use function App\Http\Controllers\girisimci;
use function App\Http\Controllers\sponsor_derinlik_kazancı;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        //aylık kişi eklemeyle kariyer atlama işlemi
        $schedule->call(function ()
        {
            $user = User::where('kariyer_id','<=',4)->get(); // 5 den küçük eşit olmalı
            foreach ($user as $u)
            {
                $musteri = User::where('sponsor_id', $u->id)->where('kariyer_id', 1)->whereMonth('created_at',Carbon::now()->month)->count(); //bu ay içinde sorgusu bakılacak bu ay içinde eklediği sayısını getir
                if ($u->kariyer_id == 1) {
                    if ($musteri >= 4) //müşteri için 4 kisi şartı
                    {
                            $u->update(['kariyer_id', 2]);
                            $u->aylikkariyer()->create();
                    }
                }
                $musteri = User::where('sponsor_id', $u->id)->where('kariyer_id' , 2)->whereMonth('created_at',Carbon::now()->month)->count();;
                if ($u->kariyer_id == 2) {
                    if ($musteri >= 3) //müşteri için 4 kisi şartı
                    {
                        $u->update(['kariyer_id'=>3]);
                        $u->aylikkariyer()->create();
                    }
                }
                $musteri = User::where('sponsor_id', $u->id)->where('kariyer_id' , 3)->whereMonth('created_at',Carbon::now()->month)->count();;
                if ($u->kariyer_id == 3) {
                    if ($musteri >= 3) //müşteri için 4 kisi şartı
                    {
                        $u->update(['kariyer_id' , 4]);

                        $u->aylikkariyer()->create();
                    }
                }
                $musteri = User::where('sponsor_id', $u->id)->where('kariyer_id' , 4)->whereMonth('created_at',Carbon::now()->month)->count();;
                if ($u->kariyer_id == 4) {
                    if ($musteri >= 3) //müşteri için 4 kisi şartı
                    {
                        $u->update(['kariyer_id' , 5]);

                        $u->aylikkariyer()->create();
                    }
                }
            }
        })->hourly();

        //aylık kişi eklemeyle kariyer atlama işlemi
        $schedule->call(function ()
        {
            function girisim($us)                    // danışmanın(5) 6 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){                          //else ise kariyeri aynı kalsın atlamasın
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 1000) //
                            {
                                $toplam += 1000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 1000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 2500)
                    {
                        $us->update(['kariyer_id'=> 6]);

                        $us->aylikkariyer()->create();
                    }
                }
            } // 6 danışman oldu
            function danisman($us)                    // danışmanın(6) 7 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 2000) //
                            {
                                $toplam += 2000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 2000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 5000)
                    {
                        $us->update(['kariyer_id'=> 7]);

                        $us->aylikkariyer()->create();
                    }
                }
            }  // 7 temsilci oldu
            function temsilci($us)                    // 7 idi  8 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 4000) //
                            {
                                $toplam += 4000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 4000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 10000)
                    {
                        $us->update(['kariyer_id'=> 8]);
                        $us->update(['ara_pv' => 0]);
                        $us->aylikkariyer()->create();
                    }
                }
            }    // 8 y.asistan
            function asistan_kariyer($us)             // 8 idi  9 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 12000) //
                            {
                                $toplam += 12000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 12000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 30000)
                    {
                        $us->update(['kariyer_id'=>9]);
                        $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            }  // 9 asistan oldu
            function oncu_lider($us)                  // 9 idi  10 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 30000) //
                            {
                                $toplam += 30000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 30000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 75000)
                    {
                        $us->update(['kariyer_id'=>10]);
                            $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            } // 10 öncü lider
            function lider($us)                       // 10 idi  11 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 60000) //
                            {
                                $toplam += 60000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 60000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 150000)
                    {
                        $us->update(['kariyer_id'=>11]);
                            $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            }  //11 lider
            function mudur_yardimcisi($us)           // 11 idi  12 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 100000) //
                            {
                                $toplam += 100000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 100000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 250000)
                    {
                        $us->update(['kariyer_id'=>12]);
                            $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            }  // 12 m.yardimcisi
            function mudur($us)                      // 12 idi  13 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 200000) //
                            {
                                $toplam += 200000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 200000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 500000)
                    {
                        $us->update(['kariyer_id'=>13]);
                            $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            } // 13 müdür
            function g_mudur_yardimcisi($us)         // 13 idi  14 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 300000) //
                            {
                                $toplam += 300000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 300000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 750000)
                    {
                        $us->update(['kariyer_id'=>14]);
                            $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            } // 14 gmüdüry
            function g_mudur($us)// 14 idi  15 olması şartı
            {
                $toplam=0;
                $altindaki=User::where('sponsor_id',$us->id)->get();
                $kol=count($altindaki);
                if($kol>=3){
                    foreach ($altindaki as $altkisi)
                    {
                        for($i=1;$i<=8;$i++)
                        {
                            if ($altkisi->konum == $i && $altkisi->ara_pv >= 400000) //
                            {
                                $toplam += 400000;
                            }
                            elseif ($altkisi->konum== $i && $altkisi->ara_pv < 400000)
                            {
                                $toplam += $altkisi->ara_pv;
                            }
//                        for ($k=1;$k<=7; $k++)
//                        {
//                            if ($altkisi->konum == (($i).'.'.$k) && $altkisi->pv >= 1000) //
//                            {
//                                $toplam += 1000;
//                            }
//                            elseif ($altkisi->konum== (($i).'.'.$k) && $altkisi->pv < 1000)
//                            {
//                                $toplam += $altkisi->pv;
//                            }
//                        }
                        }
                    }
                    if ($toplam >= 1000000)
                    {
                        $us->update(['kariyer_id'=>15]);
                            $us->update(['ara_pv'=>0]);
                        $us->aylikkariyer()->create();
                    }
                }
            }
            $users = User::all();
            foreach ($users as $us)
            {
                if ($us->kariyer_id == 5)
                {
                    girisim($us);
                }
                elseif($us->kariyer_id == 6)
                {
                    danisman($us);
                }
                elseif($us->kariyer_id == 7)
                {
                    temsilci($us);
                }
                elseif ($us->kariyer_id == 8)
                {
                    asistan_kariyer($us);
                }
                elseif ($us->kariyer_id == 9)
                {
                    oncu_lider($us);
                }
                elseif ($us->kariyer_id == 10)
                {
                    lider($us);
                }
                elseif ($us->kariyer_id == 11)
                {
                    mudur_yardimcisi($us);
                }
                elseif ($us->kariyer_id == 12)
                {
                    mudur($us);
                }
                elseif ($us->kariyer_id == 13)
                {
                    g_mudur_yardimcisi($us);
                }
                elseif ($us->kariyer_id == 14)
                {
                    g_mudur($us);
                }
            }
        })->hourly();
        //aylık kişi eklemeyle kariyer atlama işlemi

        //aylık pasif yapma işlemi başlangıç
        $schedule->call(function () {
            $users = User::with('kariyer')->withCount('orders')->whereHas('roles',function($q){$q->where('name','!=','Admin');})->where('id','!=','10001')->get();
            foreach ($users as $u){
                if ($u->kariyer->id == 1){
                    if ($u->orders_count < 1){
                        $u->update(['pv'=>0,'cv'=>0,'ara_pv'=>0,'durum'=>false]);
                    }else{
                        if ($u->durum == false){
                            $u->update(['durum'=>true]);
                        }
                    }
                }elseif ($u->kariyer->id == 2){
                    if ($u->orders_count <2){
                        $u->update(['pv'=>0,'cv'=>0,'ara_pv'=>0,'durum'=>false]);
                    }else{
                        if ($u->durum == false){
                            $u->update(['durum'=>true]);
                        }
                    }
                }else{
                    if ($u->orders_count <3){
                        $u->update(['pv'=>0,'cv'=>0,'ara_pv'=>0,'durum'=>false]);
                    }
                    else{
                        if ($u->durum == false){
                            $u->update(['durum'=>true]);
                        }
                    }
                }
            }
        }) /*->monthlyOn(1,'00:01');*/ ->hourly();
        //aylik pasiy ypma işemi sonu

        // Aylık binary eşleşme başlangıcı
        $schedule->call(function () {
            function matching_primi($ekleyen_kisi,$user,$deep,$tutar){
                if (User::where('id',$user->sponsor_id)->exists()){
                    $kariyer = User::where('id',$user->sponsor_id)->first()->kariyer;
                    $kariyer_id=$kariyer->id;
                    if ($kariyer->matchings()->exists()){
                        if ($deep==1){
                            $derinlik= User::where('id',$user->sponsor_id)->first();
                            $yuzde=$derinlik->kariyer->matchings()->where('derinlik',1)->first()->yuzde;
                            $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                            $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                            matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                        }elseif ($deep==2){
                            if ($kariyer_id>=9) {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }elseif ($deep==3){
                            if ($kariyer_id>=10){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }elseif($deep==4){
                            if ($kariyer_id>=11){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        } elseif($deep==5){
                            if ($kariyer_id>=12){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }elseif($deep==6){
                            if ($kariyer_id>=13){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }elseif($deep==7){
                            if ($kariyer_id>=14){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }elseif($deep==8){
                            if ($kariyer_id>=15){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $yuzde=$derinlik->kariyer->matchings()->where('derinlik',$deep)->first()->yuzde;
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*$yuzde/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*$yuzde/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }
                        else{
                            if ($kariyer_id >=8){
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                $derinlik->cuzdan()->increment('toplam_bakiye',$tutar*1/100);
                                $derinlik->gelirs()->create(['islem_sahibi_id'=>$ekleyen_kisi->id,'kazanc_turu'=>'Matching Primi','kazanc_miktari'=>$tutar*1/100]);
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }else {
                                $derinlik= User::where('id',$user->sponsor_id)->first();
                                matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                            }
                        }
                    }else{
                        $derinlik= User::where('id',$user->sponsor_id)->first();
                        matching_primi($ekleyen_kisi,$derinlik,$deep+1,$tutar);
                    }
                }
            }
            function kazanc_hesaplama($sagcv,$solcv,$sol,$sag,$user){
                $binary = Kariyerb::pluck('binary');
                $kariyer = Kariyerb::pluck('tutar');
                if ($solcv >= $kariyer[0] && $solcv<=$kariyer[1]-1){
                    $sol->cv = $solcv-$kariyer[0];
                    $sol->save();
                    $sag->cv = $sagcv-$kariyer[0];
                    $sag->save();
                    $kazanc = ($kariyer[0] * $binary[0])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    if ($solcv<$sagcv) {
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[0],'eslesen_id'=>$sag->id]);
                    }
                    elseif ($sagcv <= $solcv){
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sag->id,'eslesen_cv'=>$kariyer[0],'eslesen_id'=>$sol->id]);
                    }
                }elseif($solcv>=$kariyer[1] && $solcv<=$kariyer[2]-1){
                    $sol->cv = $solcv-$kariyer[1];
                    $sol->save();
                    $sag->cv = $sagcv-$kariyer[1];
                    $sag->save();
                    $kazanc = ($kariyer[1]*$binary[1])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    if ($solcv<$sagcv) {
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[1],'eslesen_id'=>$sag->id]);
                    }
                    elseif ($sagcv<$solcv){
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sag->id,'eslesen_cv'=>$kariyer[1],'eslesen_id'=>$sol->id]);
                    }
                }elseif ($solcv>=$kariyer[2] && $solcv<=$kariyer[3]-1){
                    $sol->cv = $solcv-$kariyer[2];
                    $sol->save();
                    $sag->cv = $sagcv-$kariyer[2];
                    $sag->save();
                    $kazanc = ($kariyer[2]*$binary[2])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    if ($solcv<$sagcv) {
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[2],'eslesen_id'=>$sag->id]);
                    }
                    elseif ($sagcv<$solcv){
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sag->id,'eslesen_cv'=>$kariyer[2],'eslesen_id'=>$sol->id]);
                    }
                }elseif ($solcv >= $kariyer[3] && $solcv<=$kariyer[4]-1){
                    $sol->cv = $solcv-$kariyer[3];
                    $sol->save();
                    $sag->cv = $sagcv-$kariyer[3];
                    $sag->save();
                    $kazanc = ($kariyer[3]*$binary[3])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    if ($solcv<$sagcv) {
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[3],'eslesen_id'=>$sag->id]);
                    }
                    elseif ($sagcv<$solcv){
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sag->id,'eslesen_cv'=>$kariyer[3],'eslesen_id'=>$sol->id]);
                    }
                }elseif ($solcv>=$kariyer[4]){
                    $sol->cv = $solcv-$kariyer[4];
                    $sol->save();
                    $sag->cv = $sagcv-$kariyer[4];
                    $sag->save();
                    $kazanc = ($kariyer[4]*$binary[4])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    if ($solcv<$sagcv) {
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[4],'eslesen_id'=>$sag->id]);
                    }
                    elseif ($sagcv<$solcv){
                        EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sag->id,'eslesen_cv'=>$kariyer[4],'eslesen_id'=>$sol->id]);
                    }
                }
            }
            function kazanc_tek_hesaplama($solcv,$sol,$user){
                $binary = Kariyerb::pluck('binary');
                $kariyer = Kariyerb::pluck('tutar');
                $kazanc=0;
                if ($solcv>=$kariyer[0] && $solcv<=$kariyer[1]-1){
                    $sol->cv = $solcv-$kariyer[0];
                    $sol->save();
                    $kazanc = ($kariyer[0]*$binary[0])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[0]]);
                    //kisinin cuzdanına ekleme yapcam. Daha sonra aynı işlemleri diğeri elseifler için daha sonra sagdaki kücükse ya da eşitse için yapıcam
                    //bu işlemin olabilidğinde burdan alıp farklı fonskiyon haline geitrmeye çalışacağım. ki görüntü sade olsun
                }elseif($solcv>=$kariyer[1] && $solcv<=$kariyer[2]-1){
                    $sol->cv = $solcv-$kariyer[1];
                    $sol->save();
                    $kazanc = ($kariyer[1]*$binary[1])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[1]]);
                }elseif ($solcv>=$kariyer[2] && $solcv<=$kariyer[3]-1){
                    $sol->cv = $solcv-$kariyer[2];
                    $sol->save();
                    $kazanc = ($kariyer[2]*$binary[2])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[2]]);
                }elseif ($solcv >=$kariyer[3] && $solcv<=$kariyer[4]-1){
                    $sol->cv = $solcv-$kariyer[3] ;
                    $sol->save();
                    $kazanc = ($kariyer[3] *$binary[3])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[3]]);
                }elseif ($solcv>=$kariyer[4]){
                    $sol->cv = $solcv-$kariyer[4];
                    $sol->save();
                    $kazanc = ($kariyer[4] *$binary[4])/100;
                    $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                    $user->gelirs()->create(['islem_sahibi_id'=>$user->id,'kazanc_turu'=>'Binary Eşleşme','kazanc_miktari'=>$kazanc]);
                    matching_primi($user,$user,1,$kazanc);
                    EslesmeBinary::create(['kullanici_id'=>$user->id,'eslesen_kullanici_id'=>$sol->id,'eslesen_cv'=>$kariyer[4] ]);
                }
            }

            $users = User::where('kariyer_id','<',16)->where('kariyer_id','>=',1)->get();
            foreach ($users as $user){
                $kisilerim = User::where('root_id',$user->id)->get();
                foreach ($kisilerim as $kisim){
                    for ($i=1;$i<=8;$i++){
                        //2,3,4 lerburda
                        if($i==1 || $i==3 || $i==5 ||$i==7){
                            if ($kisim->konum ==$i){
                                $sol=$kisim;
                                $solcv=$sol->cv;
                                if (User::where('konum',$i+1)->where('root_id',$user->id)->exists()){
                                    $sag=User::where('konum',$i+1)->where('root_id',$user->id)->first();
                                    $sagcv=$sag->cv;
                                    if ($solcv<$sagcv) {
                                        kazanc_hesaplama($sagcv,$solcv,$sol,$sag,$user);
                                    }
                                    elseif ($sagcv<$solcv){
                                        kazanc_hesaplama($solcv,$sagcv,$sag,$sol,$user);
                                    }
                                    else{
                                        kazanc_hesaplama($sagcv,$solcv,$sol,$sag,$user);
                                    }
                                }
                                /*else{
                                        kazanc_tek_hesaplama($solcv,$sol,$user); //tek kalınca bu
                                }*/
                            }
                        }
                        for ($k=1;$k<=9;$k++){
                            if($i==1 || $i==3 || $i==5 ||$i==7 ){
                                if ($kisim->konum ==$i.'.'.$k){
                                    $sol=$kisim;
                                    $solcv=$sol->cv;
                                    if (User::where('konum',(($i+1).'.'.$k))->where('root_id',$user->id)->exists()){
                                        $sag=User::where('konum',(($i+1).'.'.$k))->where('root_id',$user->id)->first();
                                        $sagcv=$sag->cv;
                                        if ($solcv<$sagcv) {
                                            kazanc_hesaplama($sagcv,$solcv,$sol,$sag,$user);
                                        }
                                        elseif ($sagcv<$solcv){
                                            kazanc_hesaplama($sagcv,$solcv,$sol,$sag,$user);
                                        }
                                        else{
                                            kazanc_hesaplama($sagcv,$solcv,$sol,$sag,$user);
                                        }
                                    }else{
                                            kazanc_tek_hesaplama($solcv,$sol,$user);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }) /*->monthlyOn(1,'00:05');*/->everyMinute();
        //aylık binary eşleşme sonu

        //aylık sponsor fark primi dağıtımı
        $schedule->call(function (){
           $users= User::all();
            function sponsor_fark_primi($eklenen){
                if (User::where('id',$eklenen->sponsor_id)->exists()){
                    $ekleyen =User::where('id',$eklenen->sponsor_id)->first();
                    if(User::where('id',$ekleyen->sponsor_id)->exists()){
                        $sponsor=User::where('id',$ekleyen->sponsor_id)->first();
                        if ($sponsor->kariyer_id > $ekleyen->kariyer_id && $ekleyen->kariyer_id <=4){
//                            $sponsor_sponsor_primi=$sponsor->kariyer->kariyerb->sponsor;
//                            $ekleyen_sponsor_primi=$ekleyen->kariyer->kariyerb->sponsor;
                            $eklenen_tutar=$eklenen->kariyer->kariyerb->tutar;
                            $fark=$sponsor->kariyer->sponsorfarks()->where('karsı_kariyer_id',$ekleyen->kariyer_id)->first();
//                            $fark = $sponsor_sponsor_primi - $ekleyen_sponsor_primi;
//                            $fark=(($eklenen_tutar*$fark)/100);
                            $fark=$fark->fark;
                            $kazanc=$eklenen_tutar*$fark/100;
                            $sponsor->cuzdan()->increment('toplam_bakiye',$kazanc);
                            $sponsor->gelirs()->create(['islem_sahibi_id'=>$ekleyen->id,'kazanc_turu'=>'Sponsor Fark Primi','kazanc_miktari'=>$kazanc]);
                        }
                    }
                }
            }
            foreach ($users as $u){
                sponsor_fark_primi($u);
            }
        }) /*->monthlyOn(1,'00:10'); */  ->hourly();
        //sponsor fark primi sonu

        $schedule->call(function (){  // İl Ciro
             //asistandan itibaren olacak asistan k_id=9 ay sonu ayrılan %3 payın dağıtılması aylık olacak
            $users = User::where('kariyer_id', '>=', 9)->where('kariyer_id', '<=', 15)->get();
            $anapay = AnaPara::find(1);
            $kasa = (Kasa::find(1)->toplam_kasa * $anapay->turkey) / 100;
            $kariyer_tutar = Kariyerb::pluck('tutar');
            $asistan_sayisi = User::where('ara_pv','>=',$kariyer_tutar[8])->where('ara_pv','<',$kariyer_tutar[9])->count();
            $oncu_lider_sayisi = User::where('ara_pv','>=',$kariyer_tutar[9])->where('ara_pv','<',$kariyer_tutar[10])->count();
            $lider_sayisi = User::where('ara_pv','>=',$kariyer_tutar[10])->where('ara_pv','<',$kariyer_tutar[11])->count();
            $mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[11])->where('ara_pv','<',$kariyer_tutar[12])->count();
            $mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[12])->where('ara_pv','<',$kariyer_tutar[13])->count();
            $genel_mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[13])->where('ara_pv','<',$kariyer_tutar[14])->count();
            $genel_mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[14])->count();

            if ($asistan_sayisi == 0) {
                $asistan_ucretleri = 0;
            } else {
                $asistan_ucretleri = $kasa / $asistan_sayisi;
            }
            // $asistan_ucretleri = 0 ? $asistan_sayisi == 0 : $kasa /$asistan_sayisi;

            if ($oncu_lider_sayisi == 0) {
                $oncu_lider_ucretleri = 0;
            } else {
                $oncu_lider_ucretleri = $kasa / $oncu_lider_sayisi;
            }
            if ($lider_sayisi == 0) {
                $lider_ucretleri = 0;
            } else {
                $lider_ucretleri = $kasa / $lider_sayisi;
            }
            // $lider_ucretleri = 0 ? $lider_sayisi == 0 : $kasa /$lider_sayisi;
            if ($mudur_yardimcisi_sayisi == 0) {
                $mudur_yardimcisi_ucretleri = 0;
            } else {
                $mudur_yardimcisi_ucretleri = $kasa / $mudur_yardimcisi_sayisi;
            }
            // $mudur_yardimcisi_ucretleri = 0 ? $mudur_yardimcisi_sayisi == 0 : $kasa /$mudur_yardimcisi_sayisi;
            if ($mudur_sayisi == 0) {
                $mudur_ucretleri = 0;
            } else {
                $mudur_ucretleri = $kasa / $mudur_sayisi;
            }
            //  $mudur_ucretleri = 0 ? $mudur_sayisi == 0 : $kasa /$mudur_sayisi;
            if ($genel_mudur_yardimcisi_sayisi == 0) {
                $genel_mudur_yardimcisi_ucreti = 0;
            } else {
                $genel_mudur_yardimcisi_ucreti = $kasa / $genel_mudur_yardimcisi_sayisi;
            }
            // $genel_mudur_yardimcisi_ucreti = 0 ? $genel_mudur_yardimcisi_sayisi == 0 : $kasa /$genel_mudur_yardimcisi_sayisi;
            if ($genel_mudur_sayisi == 0) {
                $genel_mudur_ucreti = 0;
            } else {
                $genel_mudur_ucreti = $kasa / $genel_mudur_sayisi;
            }
            foreach ($users as $kullanici) {
                $user_pv = $kullanici->ara_pv;
                $kariyer = Kariyerb::where('tutar','<=',$user_pv)->where('kariyer_id','>',5)->Orderby('tutar','desc')->first();
                $kisi = Kariyerb::find($kariyer->id);
                if ($kisi->kariyer_id == 9) {
                    $asistan_parasi = (($kisi->kariyer->kariyerb->il_ciro) * $asistan_ucretleri) / 100;            // 9 10 11 12 13 14 15
                    $kullanici->cuzdan()->increment('toplam_bakiye', $asistan_parasi);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $asistan_parasi, 'islem_sahibi_id' => $kullanici->id]);// 5 7 10 15 17 20  26
                }
                if ($kisi->kariyer_id == 10) {
                    $oncu_lider = ($oncu_lider_ucretleri * ($kisi->kariyer->kariyerb->il_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $oncu_lider);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $oncu_lider, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 11) {
                    $lider = ($lider_ucretleri * ($kisi->kariyer->kariyerb->il_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $lider);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $lider, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 12) {
                    $m_yardimcisi = ($mudur_yardimcisi_ucretleri * ($kisi->kariyer->kariyerb->il_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $m_yardimcisi);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $m_yardimcisi, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 13) {
                    $mudur = ($mudur_ucretleri * ($kisi->kariyer->kariyerb->il_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $mudur);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $mudur, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 14) {
                    $g_mudur_yardimcisi = ($genel_mudur_yardimcisi_ucreti * ($kisi->kariyer->kariyerb->il_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $g_mudur_yardimcisi);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $g_mudur_yardimcisi, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 15) {
                    $g_mudur = ($genel_mudur_ucreti * ($kisi->kariyer->kariyerb->il_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $g_mudur);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'İL Ciro', 'kazanc_miktari' => $g_mudur, 'islem_sahibi_id' => $kullanici->id]);
                }
            }

        })->hourly(); /*->monthlyOn(1,'02:10'); */

        $schedule->call(function () {        //Türkiye Ciro
            $anapay = AnaPara::find(1);
            $kasa = (Kasa::find(1)->toplam_kasa * $anapay->turkey) / 100; //ör toplam tutarın %3 ayrılacak buraya kasa toplam paranın %3 ü olacak
            $users = User::where('kariyer_id', '>=', 11)->where('kariyer_id', '<=', 15)->get();
            $kariyer_tutar = Kariyerb::pluck('tutar');
            $lider_sayisi = User::where('ara_pv','>=',$kariyer_tutar[10])->where('ara_pv','<',$kariyer_tutar[11])->count();
            $mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[11])->where('ara_pv','<',$kariyer_tutar[12])->count();
            $mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[12])->where('ara_pv','<',$kariyer_tutar[13])->count();
            $genel_mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[13])->where('ara_pv','<',$kariyer_tutar[14])->count();
            $genel_mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[14])->count();

            // $asistan_ucretleri = 0 ? $asistan_sayisi == 0 : $kasa /$asistan_sayisi;

            if ($lider_sayisi == 0) {
                $lider_ucretleri = 0;
            } else {
                $lider_ucretleri = $kasa / $lider_sayisi;
            }
            // $lider_ucretleri = 0 ? $lider_sayisi == 0 : $kasa /$lider_sayisi;
            if ($mudur_yardimcisi_sayisi == 0) {
                $mudur_yardimcisi_ucretleri = 0;
            } else {
                $mudur_yardimcisi_ucretleri = $kasa / $mudur_yardimcisi_sayisi;
            }
            // $mudur_yardimcisi_ucretleri = 0 ? $mudur_yardimcisi_sayisi == 0 : $kasa /$mudur_yardimcisi_sayisi;
            if ($mudur_sayisi == 0) {
                $mudur_ucretleri = 0;
            } else {
                $mudur_ucretleri = $kasa / $mudur_sayisi;
            }
            //  $mudur_ucretleri = 0 ? $mudur_sayisi == 0 : $kasa /$mudur_sayisi;
            if ($genel_mudur_yardimcisi_sayisi == 0) {
                $genel_mudur_yardimcisi_ucreti = 0;
            } else {
                $genel_mudur_yardimcisi_ucreti = $kasa / $genel_mudur_yardimcisi_sayisi;
            }
            // $genel_mudur_yardimcisi_ucreti = 0 ? $genel_mudur_yardimcisi_sayisi == 0 : $kasa /$genel_mudur_yardimcisi_sayisi;
            if ($genel_mudur_sayisi == 0) {
                $genel_mudur_ucreti = 0;
            } else {
                $genel_mudur_ucreti = $kasa / $genel_mudur_sayisi;
            }
            foreach ($users as $kullanici) {
                $user_pv = $kullanici->ara_pv;
                $kariyer = Kariyerb::where('tutar','<=',$user_pv)->where('kariyer_id','>',5)->Orderby('tutar','desc')->first();
                $kisi = Kariyerb::find($kariyer->id);
                if ($kisi->kariyer_id == 11) {
                    $lider = ($lider_ucretleri * ($kisi->kariyer->kariyerb->tr_ciro) / 100);
                    $kullanici->cuzdan()->increment('toplam_bakiye', $lider);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Türkiye Ciro', 'kazanc_miktari' => $lider, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 12) {
                    $m_yardimci = ($mudur_yardimcisi_ucretleri * ($kisi->kariyer->kariyerb->tr_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $m_yardimci);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Türkiye Ciro', 'kazanc_miktari' => $m_yardimci, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 13) {
                    $mudur = ($mudur_ucretleri * ($kisi->kariyer->kariyerb->tr_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $mudur);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Türkiye Ciro', 'kazanc_miktari' => $mudur, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 14) {
                    $g_mudur_yardimcisi = ($genel_mudur_yardimcisi_ucreti * ($kisi->kariyer->kariyerb->tr_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $g_mudur_yardimcisi);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Türkiye Ciro', 'kazanc_miktari' => $g_mudur_yardimcisi, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kisi->kariyer_id == 15) {
                    $g_mudur = ($genel_mudur_ucreti * ($kisi->kariyer->kariyerb->tr_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $g_mudur);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Türkiye Ciro', 'kazanc_miktari' => $g_mudur, 'islem_sahibi_id' => $kullanici->id]);
                }
                else
                {
                    $girisimci = User::find($kisi->kariyer_id);
                    $sifir = ($girisimci * ($kisi->kariyer->kariyerb->tr_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $sifir);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Türkiye Ciro', 'kazanc_miktari' => $sifir, 'islem_sahibi_id' => $kullanici->id]);
                }
            }

        })/*->yearly();//->everyThirtyMinutes()*/ ->hourly();
        //türkiye ciro Son------ !!

        //Dünya Ciro Başlangıç
        $schedule->call(function () { //Dünya Ciro
            $anapay = AnaPara::find(1);
            $kasa = (Kasa::find(1)->toplam_kasa * $anapay->world) / 100;
            $kariyer_tutar = Kariyerb::pluck('tutar');
            $users = User::where('kariyer_id', '>=', 12)->where('kariyer_id', '<=', 15)->get();

            $mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[11])->where('ara_pv','<',$kariyer_tutar[12])->count();
            $mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[12])->where('ara_pv','<',$kariyer_tutar[13])->count();
            $genel_mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[13])->where('ara_pv','<',$kariyer_tutar[14])->count();
            $genel_mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[14])->count();

            if ($mudur_yardimcisi_sayisi == 0) {
                $mudur_yardimcisi_ucretleri = 0;
            } else {
                $mudur_yardimcisi_ucretleri = $kasa / $mudur_yardimcisi_sayisi;
            }
            // $mudur_yardimcisi_ucretleri = 0 ? $mudur_yardimcisi_sayisi == 0 : $kasa /$mudur_yardimcisi_sayisi;
            if ($mudur_sayisi == 0) {
                $mudur_ucretleri = 0;
            } else {
                $mudur_ucretleri = $kasa / $mudur_sayisi;
            }
            //  $mudur_ucretleri = 0 ? $mudur_sayisi == 0 : $kasa /$mudur_sayisi;
            if ($genel_mudur_yardimcisi_sayisi == 0) {
                $genel_mudur_yardimcisi_ucreti = 0;
            } else {
                $genel_mudur_yardimcisi_ucreti = $kasa / $genel_mudur_yardimcisi_sayisi;
            }
            // $genel_mudur_yardimcisi_ucreti = 0 ? $genel_mudur_yardimcisi_sayisi == 0 : $kasa /$genel_mudur_yardimcisi_sayisi;
            if ($genel_mudur_sayisi == 0) {
                $genel_mudur_ucreti = 0;
            } else {
                $genel_mudur_ucreti = $kasa / $genel_mudur_sayisi;
            }
            foreach ($users as $kullanici) {
                $user_pv = $kullanici->ara_pv;
                $kariyer = Kariyerb::where('tutar','<=',$user_pv)->where('kariyer_id','>',5)->Orderby('tutar','desc')->first();
                $kisi = Kariyerb::find($kariyer->id);
                if ($kullanici->kariyer_id == 12) {
                    $m_yardimci = ($mudur_yardimcisi_ucretleri * ($kisi->kariyer->kariyerb->dunya_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $m_yardimci);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Dünya Ciro', 'kazanc_miktari' => $m_yardimci, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kullanici->kariyer_id == 13) {
                    $mudur = ($mudur_ucretleri * ($kisi->kariyer->kariyerb->dunya_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $mudur);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Dünya Ciro', 'kazanc_miktari' => $mudur, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kullanici->kariyer_id == 14) {
                    $g_mudur_yardimcisi = ($genel_mudur_yardimcisi_ucreti * ($kisi->kariyer->kariyerb->dunya_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $g_mudur_yardimcisi);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Dünya Ciro', 'kazanc_miktari' => $g_mudur_yardimcisi, 'islem_sahibi_id' => $kullanici->id]);
                }
                if ($kullanici->kariyer_id == 15) {
                    $g_mudur = ($genel_mudur_ucreti * ($kisi->kariyer->kariyerb->dunya_ciro)) / 100;
                    $kullanici->cuzdan()->increment('toplam_bakiye', $g_mudur);
                    $kullanici->gelirs()->create(['kazanc_turu' => 'Dünya Ciro', 'kazanc_miktari' => $g_mudur, 'islem_sahibi_id' => $kullanici->id]);
                }
            }

        })/*->yearly();//*/ ->hourly();
        //Dünya ciro bitiş

        //Araba Prim başlangıç
        $schedule->call(function () { // araba primi

            if (User::where('kariyer_id', '>', 5)->exists())
            {
                $users = User::where('kariyer_id','>',5)->get();
                foreach ($users as $u) {
                    $user_pv = $u->ara_pv;
                    $kariyer = Kariyerb::where('tutar','<=',$user_pv)->where('kariyer_id','>',5)->Orderby('tutar','desc')->first();
                    $kisi = Kariyerb::find($kariyer->id);
                    if ($u->kariyer_id==8)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                    if ($u->kariyer_id==9)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                    if ($u->kariyer_id==10)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                    if ($u->kariyer_id==11)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                    if ($u->kariyer_id==12)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                    if ($u->kariyer_id==13)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                    if ($u->kariyer_id==14)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u>id]);
                    }
                    if ($u->kariyer_id==15)
                    {
                        $kazanc=$kisi->kariyer->kariyerb->araba_ciro;
                        $u->cuzdan()->increment('toplam_bakiye',$kazanc);
                        $u->gelirs()->create(['kazanc_turu'=>'Araba Ciro','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$u->id]);
                    }
                }
            }
            // araba primi aylık olur en az asistan<=ve üstü kariyerler her ay hangi kariyere ulaşırsa o kariyerin primini alır
        })/*->monthlyOn(1,"00:25");//*/ ->hourly();
        //Araba prim bitiş

        //Aylık Bayi Aktiflik Primi Başlangıç
        $schedule->call(function (){
            $users= User::where('kariyer_id','>=',4)->where('kariyer_id','<=',15)->get();
             foreach ($users as $user){
                        $bayilerin_alisveris_toplamı=0;
                        $min_bayi=$user->kariyer->aktiflik->min_bayi;
                        $max_alisveris=$user->kariyer->aktiflik->max_alisveris;
                        $toplam_alisveris=$user->kariyer->aktiflik->toplam_alisveris;
                        if(User::where('sponsor_id',$user->id)->count() >=$min_bayi ){
                            $bayiler = User::where('sponsor_id',$user->id)->get();
                            foreach ($bayiler as $bayi){
                                $toplam=0;
                                $orders = $bayi->baskets;
                                foreach ($orders as $order){
                                    foreach ($order->basket_products as $basket_product){
                                        foreach ($basket_product->product as $product){
                                            if ($product->turu =='urun'){
                                                $toplam += $order->product_price()*$order->basket_product_qty();
                                            }
                                        }
                                    }
                                }
                                if ($toplam >$max_alisveris){
                                    $toplam=$max_alisveris;
                                }
                                $bayilerin_alisveris_toplamı += $toplam;
                            }
                            if ($bayilerin_alisveris_toplamı >$toplam_alisveris){
                                $kazanc=$toplam*$user->kariyer->aktiflik->yuzde/100;
                                $user->cuzdan()->increment('toplam_bakiye',$kazanc);
                                $user->gelirs()->create(['kazanc_turu'=>'Bayi Aktiflik Primi','kazanc_miktari'=> $kazanc,'islem_sahibi_id'=>$user->id]);
                            }
                        }
                    }
        })   /*->monthlyOn(1,"00:20"); */->hourly();
        //Aylık Bayi Aktiflik Primi Sonu

        //Bölge Ciro Başlangıç
        $schedule->call(function () {
            $anapay = AnaPara::find(1);
            $users = User::where('kariyer_id', '>=', 9)->where('kariyer_id', '<=', 15)->get();
            $kariyer_tutar = Kariyerb::pluck('tutar');
            foreach ($users as $user) {
                $kasa = ($user->bolge_ciro * $anapay->region) / 100;
                $asistan_sayisi = User::where('ara_pv','>=',$kariyer_tutar[8])->where('ara_pv','<',$kariyer_tutar[9])->count();
                $oncu_lider_sayisi = User::where('ara_pv','>=',$kariyer_tutar[9])->where('ara_pv','<',$kariyer_tutar[10])->count();
                $lider_sayisi = User::where('ara_pv','>=',$kariyer_tutar[10])->where('ara_pv','<',$kariyer_tutar[11])->count();
                $mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[11])->where('ara_pv','<',$kariyer_tutar[12])->count();
                $mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[12])->where('ara_pv','<',$kariyer_tutar[13])->count();
                $genel_mudur_yardimcisi_sayisi = User::where('ara_pv','>=',$kariyer_tutar[13])->where('ara_pv','<',$kariyer_tutar[14])->count();
                $genel_mudur_sayisi = User::where('ara_pv','>=',$kariyer_tutar[14])->count();
                if ($asistan_sayisi == 0) {
                    $asistan_ucretleri = 0;
                } else {
                    $asistan_ucretleri = $kasa / $asistan_sayisi;

                }
                // $asistan_ucretleri = 0 ? $asistan_sayisi == 0 : $kasa /$asistan_sayisi;

                if ($oncu_lider_sayisi == 0) {
                    $oncu_lider_ucretleri = 0;
                } else {
                    $oncu_lider_ucretleri = $kasa / $oncu_lider_sayisi;
                }

                if ($lider_sayisi == 0) {
                    $lider_ucretleri = 0;
                } else {
                    $lider_ucretleri = $kasa / $lider_sayisi;
                }
                if ($lider_sayisi == 0) {
                    $lider_ucretleri = 0;
                } else {
                    $lider_ucretleri = $kasa / $lider_sayisi;
                }
                // $lider_ucretleri = 0 ? $lider_sayisi == 0 : $kasa /$lider_sayisi;
                if ($mudur_yardimcisi_sayisi == 0) {
                    $mudur_yardimcisi_ucretleri = 0;
                } else {
                    $mudur_yardimcisi_ucretleri = $kasa / $mudur_yardimcisi_sayisi;
                }
                // $mudur_yardimcisi_ucretleri = 0 ? $mudur_yardimcisi_sayisi == 0 : $kasa /$mudur_yardimcisi_sayisi;
                if ($mudur_sayisi == 0) {
                    $mudur_ucretleri = 0;
                } else {
                    $mudur_ucretleri = $kasa / $mudur_sayisi;
                }
                //  $mudur_ucretleri = 0 ? $mudur_sayisi == 0 : $kasa /$mudur_sayisi;
                if ($genel_mudur_yardimcisi_sayisi == 0) {
                    $genel_mudur_yardimcisi_ucreti = 0;
                } else {
                    $genel_mudur_yardimcisi_ucreti = $kasa / $genel_mudur_yardimcisi_sayisi;
                }
                // $genel_mudur_yardimcisi_ucreti = 0 ? $genel_mudur_yardimcisi_sayisi == 0 : $kasa /$genel_mudur_yardimcisi_sayisi;
                if ($genel_mudur_sayisi == 0) {
                    $genel_mudur_ucreti = 0;
                } else {
                    $genel_mudur_ucreti = $kasa / $genel_mudur_sayisi;
                }
                $bolge = $user->bolge->id;
                $il_sayisi = Iller::where('bolge_id', $bolge)->count();
                $alt_bayilikler = User::where('user_id', $user->id)->where('kariyer_id', '>=', 9)->where('kariyer_id', '<=', 15)->get();
                $user_pv = $user->ara_pv;
                $kariyer = Kariyerb::where('tutar','<=', $user_pv)->where('kariyer_id','>',5)->Orderby('tutar', 'desc')->first();
                $kisi = Kariyerb::find($kariyer->id);
                if (count($alt_bayilikler) >= ($il_sayisi / 2)) {
                    $asistan = false;
                    $oncu_lider = false;
                    $lider = false;
                    $mudur_yardimci = false;
                    $mudur = false;
                    $genel_mudur_yardimci = false;
                    $genel_mudur = false;
                    foreach ($alt_bayilikler as $alt_bayi) {
                        if ($alt_bayi->kariyer_id >= 9) $asistan = true;
                        if ($alt_bayi->kariyer_id >= 10) $oncu_lider = true;
                        if ($alt_bayi->kariyer_id >= 11) $lider = true;
                        if ($alt_bayi->kariyer_id >= 12) $mudur_yardimci = true;
                        if ($alt_bayi->kariyer_id >= 13) $mudur = true;
                        if ($alt_bayi->kariyer_id >= 14) $genel_mudur_yardimci = true;
                        if ($alt_bayi->kariyer_id == 15) $genel_mudur = true;
                    }
                    if ($asistan) {
                        $kazanc = $asistan_ucretleri * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($oncu_lider) {
                        $kazanc = $oncu_lider_ucretleri * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($lider) {
                        $kazanc = $lider_ucretleri * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($mudur_yardimci) {
                        $kazanc = $mudur_yardimcisi_ucretleri * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($mudur) {
                        $kazanc = $mudur_ucretleri * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($genel_mudur_yardimci) {
                        $kazanc = $genel_mudur_yardimcisi_ucreti * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($genel_mudur) {
                        $kazanc = $genel_mudur_ucreti * $kisi->kariyer->kariyerb->bolge_ciro / 100;
                        $kazanc = $kazanc * (count($alt_bayilikler) + 1);
                        $user->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $user->gelirs()->create(['kazanc_turu' => 'Bölge Ciro', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    }

                }

            }
        })/*->monthlyOn(1,"00:35");->everyThirtyMinutes()*/ ->hourly();
        //Bölge cirp sonu

        $schedule->call(function (){//tazminat
            $users = User::where('kariyer_id','>=',1)->where('kariyer_id','<=',15)->get();
            foreach ($users as $user){
                if ( (! $user->aylikkariyer()->whereMonth('created_at',Carbon::now()->month)->exists()) &&  ( $user->aylikkariyer()->whereMonth('created_at',Carbon::now()->subMonth()->month)->exists() )){
                    $donen=$user->tazminat()->firstOrNew(array('user_id' => $user->id));
                    $donen->tazminat=$user->kariyer->kariyerb->tazminati;
                    $donen->save();
                }
            }
        })->hourly();  //->monthlyOn(1,"00:40");
    }
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
