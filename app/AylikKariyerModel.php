<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AylikKariyerModel extends Model
{
    //
    protected $fillable = ['user_id'];

    public function aylikkariyer(){
        return $this->belongsTo(User::class);
    }
}
