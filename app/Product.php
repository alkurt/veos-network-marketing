<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";

    protected $guarded = [];


    protected $appends = ["thumbs"];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name'
            ]
        ];
    }

    public function categories()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function images()
    {
        return $this->morphMany("App\Images", "imageable");
    }

    public function getThumbsAttribute()
    {
        $images = asset("uploads/thumb_" . $this->images()->first()->name);
        return '<img src="' . $images . '" class="img-thumbnail" width="100" />';
    }

    public function getKisiselfAttribute()
    {
        if($this->turu =="urun"){
            if (auth()->user()) {
                try {
                    $fiyat = $this->product_price - (($this->product_price * auth()->user()->kariyer->kariyerb->indirim) / 100);
                    return $fiyat;
                } catch (\Exception $exception) {
                    return $this->product_price;
                }
            } else {
                return $this->product_price;
            }
        }else {
            return $this->product_price;
        }
    }

}
