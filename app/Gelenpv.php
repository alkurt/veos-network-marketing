<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gelenpv extends Model
{
    //
    protected $fillable = ['user_id','puan_turu','islem_sahibi_id','puan_miktari'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
