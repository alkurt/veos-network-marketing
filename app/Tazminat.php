<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tazminat extends Model
{
    protected $fillable = ['user_id','tazminat'];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
