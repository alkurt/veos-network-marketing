<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BasketProduct extends Model
{
    //
    protected $table = "basket_products";
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function basket(){
        return $this->belongsTo(Basket::class);
    }
}
