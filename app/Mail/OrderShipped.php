<?php

namespace App\Mail;
use App\BasketProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;
    public $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $urunler=BasketProduct::where('basket_id',$this->order->basket_id)->with('product')->get();
        return $this->view('layouts.mail.OrderCreated')->subject('Sipariş Alındı')->with('urunler',$urunler)->with('id',$this->order->order_no);
    }
}
