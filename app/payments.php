<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payments extends Model
{
    protected $fillable =['STATUS','AMOUNT','CUSTOMER_CC_NAME','COMMISSION',"DATE",'ORDER_REF_NUMBER'];
}
