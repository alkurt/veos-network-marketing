<?php

namespace App\Providers;

use App\cuzdan;
use App\Logo;
use App\Observers\CuzdanObserver;
use App\Observers\OrderObserver;
use App\Observers\ProductObserver;
use App\Observers\PvObserver;
use App\Observers\UserObserver;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(Logo::where('status',1)->exists()){
            $logo=Logo::where('status',1)->first();
            \view()->share('logo', $logo);
        }

        User::observe(UserObserver::class);
        Product::observe(ProductObserver::class);
        Order::observe(OrderObserver::class);
        Schema::defaultStringLength(191);

        // Form Component
        $this->app["form"]->component('bsText', 'form_components.text', ['name','label_name', 'value' => null, 'attributes' => []]);
        $this->app["form"]->component('bsFile', 'form_components.file', ['name','label_name']);
        $this->app["form"]->component('bsPassword', 'form_components.password', ['name','label_name', 'attributes' => []]);
        $this->app["form"]->component('bsSubmit', 'form_components.submit', ['name', 'url' => URL::previous()]);
        $this->app["form"]->component('bsCheckbox', 'form_components.checkbox', ['name', 'label_name', 'elements' => []]);
        $this->app["form"]->component('bsSelect', 'form_components.select', ['name', 'label_name','value','list' => [],"placeholder"]);
        $this->app["form"]->component('bsTextArea', 'form_components.textarea', ['name','label_name', 'value' => null, 'attributes' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
