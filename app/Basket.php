<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Basket extends Model
{
    //

    protected $table = "baskets";

    protected $guarded = [];

    public function order()
    {
        return $this->hasOne('App\Order');
    }
    public function eft()
    {
        return $this->hasOne(EftHavaleBekleme::class);
    }

    public function basket_products()
    {
        return $this->hasMany('App\BasketProduct');
    }

    public static function active_basket_id()
    {
        $active_basket = DB::table('baskets as b')
            ->leftJoin('orders as o', 'o.basket_id','=', 'b.id')
            ->where('b.user_id', Auth::id())
            ->whereNull('o.id')
            ->orderByDesc('b.created_at')
            ->select('b.id')
            ->first();
        if (!is_null($active_basket)) return $active_basket->id;
    }

    public function basket_product_qty()
    {
        return DB::table('basket_products')->where('basket_id', $this->id)->sum('quantity');
    }

    public function product_price()
    {
        return DB::table('basket_products')->where('basket_id', $this->id)->sum('price');
    }
    public function toplam_pv(){
        $basket_products = DB::table('basket_products')->where('basket_id', $this->id)->get();
        foreach ($basket_products as $baskets){
            $baskets->product->pv;
            $baskets->product->cv;
            $baskets->quantity;
        }
    }
    public function toplam_cv(){
        return DB::table('basket_products')->where('basket_id', $this->id)->sum('price');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
