<?php

namespace App\Imports;

use App\Iller;
use App\User;
use App\UserDetail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\ToCollection;


class UsersImport implements WithHeadingRow,ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        function generateBarcodeNumber() {

            $number = mt_rand(10000, 99999); // better than rand()
            if (! User::where('id',$number)->exists()){
                return $number;
            }else{
                return generateBarcodeNumber();
            }
        }
       foreach ($rows as $row){
           if ($row['iller_id']=="34"||$row['iller_id']=="22"||$row['iller_id']=='39'||$row['iller_id']=='59'||$row['iller_id']=='41'||$row['iller_id']=='77'||$row['iller_id']=='54'||$row['iller_id']=='11'||$row['iller_id']=='16'||$row['iller_id']=='10'||$row['iller_id']=='17')
           {
               $data['bolge']=1;
           }
           elseif ($row['iller_id']=="68"||$row['iller_id']=="06"||$row['iller_id']=='18'||$row['iller_id']=='26'||$row['iller_id']=='70'||$row['iller_id']=='71'||$row['iller_id']=='40'||$row['iller_id']=='42'||$row['iller_id']=='50'||$row['iller_id']=='51'||$row['iller_id']=='58'||$row['iller_id']=='66'||$row['iller_id']=='38')
           {
               $data['bolge']=2;

           }
           elseif ($row['iller_id']=="35"||$row['iller_id']=="45"||$row['iller_id']=='9'||$row['iller_id']=='20'||$row['iller_id']=='43'||$row['iller_id']=='3'||$row['iller_id']=='64'||$row['iller_id']=='48')
           {
               $data['bolge']=3;

           }
           elseif ($row['iller_id']=='01'||$row['iller_id']=='80'||$row['iller_id']=='7'||$row['iller_id']=='15'||$row['iller_id']=='31'||$row['iller_id']=='32'||$row['iller_id']=='33'||$row['iller_id']=='46')
           {
               $data['bolge']=4;

           }
           elseif ($row['iller_id']=="53"||$row['iller_id']=="61"||$row['iller_id']=='8'||$row['iller_id']=='60'||$row['iller_id']=='19'||$row['iller_id']=='5'||$row['iller_id']=='55'||$row['iller_id']=='67'||$row['iller_id']=='14'||$row['iller_id']=='81'||$row['iller_id']=='78'||$row['iller_id']=='74'||$row['iller_id']=='37'||$row['iller_id']=='69'||$row['iller_id']=='28'||$row['iller_id']=='29'||$row['iller_id']=='52'||$row['iller_id']=='57')
           {
               $data['bolge']=5;

           }
           elseif ($row['iller_id']=="4"||$row['iller_id']=="75"||$row['iller_id']=='12'||$row['iller_id']=='13'||$row['iller_id']=='23'||$row['iller_id']=='24'||$row['iller_id']=='25'||$row['iller_id']=='76'||$row['iller_id']=='30'||$row['iller_id']=='36'||$row['iller_id']=='44'||$row['iller_id']=='49'||$row['iller_id']=='62'||$row['iller_id']=='65'||$row['iller_id']=='73')
           {
               $data['bolge']=6;

           }
           elseif ($row['iller_id']=="2"||$row['iller_id']=="72"||$row['iller_id']=='21'||$row['iller_id']=='27'||$row['iller_id']=='79'||$row['iller_id']=='47'||$row['iller_id']=='56'||$row['iller_id']=='63')
           {
               $data['bolge']=7;
           }
           $create =  new User([
               'id'=> generateBarcodeNumber(),
               'name'=>strtoupper($row['name']),
               'surname' =>strtoupper($row['surname']),
               'kariyer_id'=>$row['kariyer_id'],
               'kimlik' => $row['kimlik'],
               'dogum' => $row['dogum'],
               'ulke' => $row['ulke'],
               'iller_id' => $row['iller_id'],
               'bolge_id' => $data['bolge'],
               'telefon' => $row['telefon'],
               'email' => $row['email'],
               'root_id' => $row['root_id'],
               'sponsor_id' => $row['sponsor_id'],
               'konum'=>$row['konum'],
               'password' => Hash::make($row['password']),
           ]);
           $create->save();
           $input = [];
           $input["user_id"] = $create->id;
           $user_detail = UserDetail::create($input);
           $user_detail->m_phone = $row['telefon'];
           $user_detail -> city = $create->iller->name;
           $user_detail -> country = $create->ulke;
           $user_detail->save();
           $create->roles()->attach(2);
           $create->cuzdan()->create();
       }
    }

}
