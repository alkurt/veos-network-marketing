<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gelir extends Model
{
    //
    protected $fillable = ['user_id','kazanc_turu','islem_sahibi_id','kazanc_miktari'];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
