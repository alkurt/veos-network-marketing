<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sorular extends Model
{
  protected $fillable=['soru_basligi','soru_cevabi','sira_no'];
  protected $table ='Sorulars';
}
