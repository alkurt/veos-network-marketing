<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OdemeTalebi extends Model
{
    //
    protected $fillable=['user_id','odeme_talebi','status'];
    public function user ()
    {
        return $this->belongsTo(User::class);
    }
}
