<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayiAktiflik extends Model
{
    //
    protected $fillable=['kariyer_id','min_bayi','max_alisveris','toplam_alisveris','yuzde'];

    public function kariyer(){
       return $this->belongsTo(Kariyer::class);
    }
    public function getYuzdeAttribute(){
        return number_format( $this->attributes['yuzde'],2);
    }
}
