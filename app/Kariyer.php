<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kariyer extends Model
{
        protected $fillable=['kariyername'];

    public function Kariyerb(){
        return $this->hasOne(Kariyerb::class);
    }
    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function aktiflik()
    {
        return $this->hasOne(BayiAktiflik::class);
    }
    public function derinliks(){
        return $this->hasMany(SponsorDerinlik::class);
    }
    public function matchings(){
        return $this->hasMany(Matching::class);
    }
    public function sponsorfarks(){
        return $this->hasMany(SponsorFark::class);
    }
    public function bayiindirimfark(){
        return $this->hasMany(BayiIndirimFark::class);
    }
    public function lidercikarma(){
        return $this->hasOne(LiderCikarma::class);
    }
    public function bayipromosyon(){
        return $this->hasMany(BayiPromosyon::class);
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($k) { // before delete() method call this
            $k->Kariyerb()->each(function($kariyer) {
                $kariyer->delete();
             });

        });
    }
}
