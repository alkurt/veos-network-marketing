<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlisverisDerinlikDagilimi extends Model
{
    protected $fillable = ['yuzde','derinlik'];

    public function getYuzdeAttribute(){
        return number_format( $this->attributes['yuzde'],2);
    }
}
