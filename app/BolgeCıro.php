<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BolgeCıro extends Model
{
    protected $fillable = ['bolge_ciro','il_ciro','turkiye_ciro','dunya_ciro'];
}
