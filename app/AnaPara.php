<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnaPara extends Model
{
    //
    protected $fillable = ['city','region','turkey','world'];
    public function getCityAttribute(){
        return number_format( $this->attributes['city'],2);
    }
    public function getRegionAttribute(){
        return number_format( $this->attributes['region'],2);
    }
    public function getTurkeyAttribute(){
        return number_format( $this->attributes['turkey'],2);
    }
    public function getWorldAttribute(){
        return number_format( $this->attributes['world'],2);
    }
}
