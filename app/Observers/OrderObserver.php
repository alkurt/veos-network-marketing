<?php

namespace App\Observers;


use App\Kasa;
use App\Mail\OrderShipped;
use App\Order;
use Illuminate\Support\Facades\Mail;


class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        $user=$order->user;
       // Mail::to($user)->queue(new OrderShipped($order));
        $kasa=Kasa::find(1);
        $kasa->toplam_kasa += $order->order_price;
        $kasa->save();
        foreach ($order->baskets->basket_products as $basket_product){
         $product =$basket_product->product;
         $product->miktar -=$basket_product->quantity;
         $product->save();
        }

    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        //
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
