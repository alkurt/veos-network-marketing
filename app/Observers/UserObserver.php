<?php

namespace App\Observers;

use App\EslesmeBinary;
use App\Gelir;
use App\Mail\UserCreated;
use App\User;
use Illuminate\Support\Facades\Mail;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
//            Mail::to($user)->queue(new UserCreated($user));
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        if ($user->isDirty('pv')){ //pv değeri güncellendiğinde çalışacak fonk.
//            $toplam=0;
//            //user'ın satın almalarını topluyoruz
//            $orders = $user->baskets;
//            foreach ($orders as $order){
//                $toplam += $order->product_price()*$order->basket_product_qty();
//            }
//            if($toplam>=300 && $toplam<=599){
//                if ($user->kariyer_id != 1){
//                    $user->kariyer_id=1;
//                    $user->save();
//                    $user->aylikkariyer()->create();
//                }
//            }
//            elseif ($toplam>=600 && $toplam<=1499){
//                if ($user->kariyer_id != 2){
//                    $user->update(['kariyer_id'=>2]);
//                    $user->aylikkariyer()->create();
//                }
//            }
//            elseif ($toplam>=1500 && $toplam<=2499){
//                if ($user->kariyer_id != 3){
//                    $user->kariyer_id=3;
//                    $user->save();
//                    $user->aylikkariyer()->create();
//                }
//            }
//            elseif ($toplam>=2500 && $toplam<=4999){
//                if ($user->kariyer_id != 4){
//                    $user->update(['kariyer_id'=>4]);
//                    $user->aylikkariyer()->create();
//                }
//            }
//            elseif ($toplam>=5000 ){
//                if ($user->kariyer_id !=5){
//                    $user->update(['kariyer_id'=>5]);
//                    $user->aylikkariyer()->create();
//                }
//            }
        }
        if ($user->isDirty('kariyer_id')){
                if (User::where('id', $user->sponsor_id)->exists())
                {
                    $sponsor = User::where('id', $user->sponsor_id)->first();
                    if(! $sponsor->gelirs()->where('kazanc_turu','Lider Çıkarma primi')->where('user_id',$user->sponsor_id)->exists()) {
                    if ($user->kariyer_id == 8) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Cıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 9) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 10) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 11) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 12) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 13) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 14) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    } elseif ($user->kariyer_id == 15) {
                        $kazanc = $user->kariyer->lidercikarma->tutar;
                        $sponsor->cuzdan()->increment('toplam_bakiye', $kazanc);
                        $sponsor->gelirs()->create(['kazanc_turu' => 'Lider Çıkarma primi', 'kazanc_miktari' => $kazanc, 'islem_sahibi_id' => $user->id]);
                    }
                }
            }
            //Liderlik primi start
    if (! $user->gelirs()->where('kazanc_turu','Lider Primi')->exists())
    {
        if ($user->kariyer_id == 8){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id==9){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id ==10){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id ==11){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id == 12){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id ==13){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id ==14){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }elseif ($user->kariyer_id ==15){
            $kazanc = $user->kariyer->kariyerb->lider_primi;
            $user->cuzdan()->increment('toplam_bakiye',$kazanc);
            $user->gelirs()->create(['kazanc_turu'=>'Lider Primi','kazanc_miktari'=>$kazanc,'islem_sahibi_id'=>$user->id]);
        }
    }
            //Liderlik primi end
        }
    }

    public function updating(User $user)
    {


    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $user->detail()->delete();
        $user->cuzdan()->delete();
        $user->eft()->delete();
        foreach ($user->baskets() as $basket_product){
            $basket_product->delete();
        }
        $user->baskets();
        $user->orders();
        $user->gelirs()->delete();
        $eslemeler = EslesmeBinary::where('eslesen_kullanici_id',$user->id)->orWhere('eslesen_id',$user->id)->get();
        foreach ($eslemeler as $e){
            $e->delete();
        }
        $user->nakitindirim()->delete();
        $user->tazminat()->delete();
        $user->odeme_talebi()->delete();
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
