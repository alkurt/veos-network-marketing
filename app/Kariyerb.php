<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kariyerb extends Model
{
    protected $fillable =['tutar','sponsor','indirim','binary','kariyer_id','il_ciro','tr_ciro','dunya_ciro','araba_ciro','tazminati','lider_primi'];

    public function Kariyer()
    {
        return $this->belongsTo(Kariyer::class);
    }

}
