<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayiPromosyon extends Model
{
    //
    protected $fillable = ['kariyer_id','urun_adet','yuzde'];
    public function kariyer(){
        return $this->belongsTo(Kariyer::class);
    }
    public function getYuzdeAttribute(){
        return number_format( $this->attributes['yuzde'],2);
    }
}
