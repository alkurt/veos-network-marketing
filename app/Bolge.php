<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bolge extends Model
{
    protected $fillable = ['name'];

    public function Illers(){
       return $this->hasMany(Iller::class);
    }
    public function Users(){
        return $this->hasMany(User::class);
    }
}
