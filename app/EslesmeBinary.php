<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EslesmeBinary extends Model
{
    protected $fillable = ['kullanici_id','eslesen_kullanici_id','eslesen_cv','artan_cv','eslesen_id'];
    public function user(){
        return $this->belongsTo(User::class,'kullanici_id','id');
    }
}
