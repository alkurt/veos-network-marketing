<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gider extends Model
{
    protected $fillable = ['user_id','amaount','kesinti'];

    public function user(){
        $this->belongsTo(User::class);
    }
}
