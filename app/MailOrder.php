<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailOrder extends Model
{
    //
    protected $fillable = ['basket_id','user_id','order_price','status','order_no','name','address','phone','m_phone','payment_method','installments','token','durum','cardname','cardmonth','cardyear','cardcvc','cardnu'];
    //protected $table = "eft_havale_beklemes";


    public function baskets()
    {
        return $this->belongsTo('App\Basket', 'basket_id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function order(){
        return $this->belongsTo(Order::class,'id','order_no');
    }
}
