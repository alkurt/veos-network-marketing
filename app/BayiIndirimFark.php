<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayiIndirimFark extends Model
{
    protected $fillable = ['kariyer_id','karsı_kariyer_id','fark'];
    public function kariyer(){
        return $this->belongsTo(Kariyer::class);
    }
    public function getFarkAttribute(){
        return number_format( $this->attributes['fark'],2);
    }
}
