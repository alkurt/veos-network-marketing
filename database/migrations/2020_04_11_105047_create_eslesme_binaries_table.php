<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEslesmeBinariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eslesme_binaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kullanici_id')->nullable();
            $table->string('eslesen_kullanici_id')->nullable();
            $table->bigInteger('eslesen_cv')->nullable();
            $table->bigInteger('artan_cv')->nullable();
            $table->string('eslesen_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eslesme_binaries');
    }
}
