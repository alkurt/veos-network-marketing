<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEftHavaleBeklemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eft_havale_beklemes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basket_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->mediumText('order_price')->nullable();
            $table->string('status',30)->nullable();
            $table->biginteger('order_no')->nullable();

            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('m_phone')->nullable();
            $table->string('payment_method')->nullable();
            $table->integer('installments')->nullable();

            $table->boolean('durum')->default(0);
            $table->string('token')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eft_havale_beklemes');
    }
}
