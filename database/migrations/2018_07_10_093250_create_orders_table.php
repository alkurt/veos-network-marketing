<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basket_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->mediumText('order_price')->nullable();
            $table->string('status',30)->nullable();
            $table->biginteger('order_no')->nullable();

            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('m_phone')->nullable();
            $table->string('payment_method')->nullable();
            $table->integer('installments')->nullable();

            $table->string('token')->nullable();

            $table->timestamps();

            $table->unique('basket_id');
            $table->foreign('basket_id')->references('id')->on('baskets')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
