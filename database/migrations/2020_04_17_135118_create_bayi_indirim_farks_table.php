<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayiIndirimFarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayi_indirim_farks', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('kariyer_id');
            $table->bigInteger('karsı_kariyer_id');
            $table->float('fark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayi_indirim_farks');
    }
}
