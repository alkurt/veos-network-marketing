<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlisverisDerinlikDagilimisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alisveris_derinlik_dagilimis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('derinlik');
            $table->float('yuzde');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alisveris_derinlik_dagilimis');
    }
}
