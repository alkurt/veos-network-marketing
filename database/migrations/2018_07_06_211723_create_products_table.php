<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default(1);
            $table->integer('category_id');
            $table->string('slug',160);
            $table->string('product_name',150);
            $table->string('turu');
            $table->string('code');
            $table->bigInteger('miktar');
            $table->boolean('vitrin')->default(false);
            $table->boolean('firsat')->default(false);
            $table->text('product_detail');
            $table->float('alisfiyati',18,9)->nullable();
            $table->float('product_price',18,9)->nullable();
            $table->float('cv');
            $table->float('pv');
            $table->float('dolar')->nullable();
            $table->float('euro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
