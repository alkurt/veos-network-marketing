<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirsatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firsats', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('hakkimizda')->nullable();
            $table->longText('b_hikayesi')->nullable();
            $table->longText('p_pazarlama')->nullable();
            $table->longText('ismodeli')->nullable();
            $table->longText('y_urunler')->nullable();
            $table->longText('nedennetwork')->nullable();
            $table->longText('odeme_plani')->nullable();
            $table->longText('misyon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firsats');
    }
}
