<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBolgeCırosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bolge_cıros', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('bolge_ciro')->nullaeble();
            $table->bigInteger('il_ciro')->nullaeble();
            $table->bigInteger('dunya_ciro')->nullaeble();
            $table->bigInteger('turkiye_ciro')->nullaeble();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bolge_cıros');
    }
}
