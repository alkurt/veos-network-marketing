<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtkinliklersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etkinliklers', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('etkinlikler_icerik');
            $table->longText('mekan');
            $table->longText('title')->nullable();
            $table->longText('url');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etkinliklers');
    }
}
