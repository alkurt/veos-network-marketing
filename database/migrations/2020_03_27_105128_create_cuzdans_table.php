<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuzdansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuzdans', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->float('cuzdan_brut',20,2)->default(0);
            $table->float('toplam_bakiye',20,2)->default(0);
            $table->float('arac_bakiye',20,2)->default(0);
            $table->float('odenen_bakiye',20,2)->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuzdans');
    }
}
