<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayiPromosyonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayi_promosyons', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('kariyer_id');
            $table->bigInteger('urun_adet')->nullable();
            $table->float('yuzde');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayi_promosyons');
    }
}
