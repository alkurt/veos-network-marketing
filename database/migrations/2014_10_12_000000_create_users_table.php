<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->boolean('status')->default(true);
            $table->boolean('durum')->default(true);
            $table->integer('sponsor_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('root_id')->nullable();
            $table->integer('pv')->default(0);
            $table->integer('ara_pv')->default(0);
            $table->integer('cv')->default(0);
            $table->string('konum')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->string('ulke');
            $table->bigInteger('iller_id');
            $table->bigInteger('bolge_id');
            $table->integer('kariyer_id')->default(16);
            $table->string('dogum');
            $table->string('kimlik');
            $table->string('telefon');
            $table->string('email')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
