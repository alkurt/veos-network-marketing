<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBayiAktifliksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayi_aktifliks', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('kariyer_id');
            $table->bigInteger('min_bayi');
            $table->bigInteger('max_alisveris');
            $table->bigInteger('toplam_alisveris');
            $table->float('yuzde');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayi_aktifliks');
    }
}
