<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKariyerbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kariyerbs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kariyer_id');
            $table->integer('tutar');
            $table->float('sponsor');
            $table->float('indirim');
            $table->float('binary');
            $table->float('il_ciro')->default(0);
            $table->float('tr_ciro')->default(0);
            $table->float('araba_ciro')->default(0);
            $table->float('dunya_ciro')->default(0);
            $table->float('bolge_ciro')->default(0);
            $table->float('lider_primi')->nullable();
            $table->integer('tazminati');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kariyerbs');
    }
}
