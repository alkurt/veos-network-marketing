<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAltAlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alt_alans', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('bir')->nullable();
            $table->longText('iki')->nullable();
            $table->longText('uc')->nullable();
            $table->longText('dort')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alt_alans');
    }
}
