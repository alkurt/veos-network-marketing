<?php

use App\UserDetail;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $usersDetails = [
            ["user_id" => 10001,"address" => "ADMIN ADDRESS","phone" => 12345678900223456789,"m_phone" => 12345567575,"city" => "ADMIN CITY","country" => "ADMIN COUNTRY","zipcode" => 12345],
            ["user_id" => 10002,"address" => "USER ADDRESS","phone" =>  12345723444234211245,"m_phone" => 12345678991,"city" => "USER CITY","country" => "USER COUNTRY","zipcode" => 1234],
            ["user_id" => 23456,"address" => "USER ADDRESS","phone" =>  12345723444234211246,"m_phone" => 12345678992,"city" => "USER CITY","country" => "USER COUNTRY","zipcode" => 1234],
            ["user_id" => 12345,"address" => "USER ADDRESS","phone" =>  12345723444234211244,"m_phone" => 12345678993,"city" => "USER CITY","country" => "USER COUNTRY","zipcode" => 1234],
            ["user_id" => 98765,"address" => "USER ADDRESS","phone" =>  12345723444234211243,"m_phone" => 12345678994,"city" => "USER CITY","country" => "USER COUNTRY","zipcode" => 1234],
            ["user_id" => 65432,"address" => "USER ADDRESS","phone" =>  12345723444234211242,"m_phone" => 12345678995,"city" => "USER CITY","country" => "USER COUNTRY","zipcode" => 1234],
        ];

        foreach($usersDetails as $userDetail){
            UserDetail::create($userDetail);
        }

    }
}
