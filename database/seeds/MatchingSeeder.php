<?php

use Illuminate\Database\Seeder;

class MatchingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kariyers= \App\Kariyer::where('id','>=',8)->get();

        foreach ($kariyers as $kariyer){
            if ($kariyer->id ==8 ){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);

            }elseif ($kariyer->id == 9){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>4]);

            }elseif ($kariyer->id == 10){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>4]);
                $kariyer->matchings()->create(['derinlik'=>3,'yuzde'=>3]);

            }
            elseif ($kariyer->id == 11){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>4]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>3]);
                $kariyer->matchings()->create(['derinlik'=>3,'yuzde'=>3]);
                $kariyer->matchings()->create(['derinlik'=>4,'yuzde'=>2]);
            }elseif ($kariyer->id == 12){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>4]);
                $kariyer->matchings()->create(['derinlik'=>3,'yuzde'=>3]);
                $kariyer->matchings()->create(['derinlik'=>4,'yuzde'=>2]);
                $kariyer->matchings()->create(['derinlik'=>5,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 13){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>4]);
                $kariyer->matchings()->create(['derinlik'=>3,'yuzde'=>3]);
                $kariyer->matchings()->create(['derinlik'=>4,'yuzde'=>2]);
                $kariyer->matchings()->create(['derinlik'=>5,'yuzde'=>1]);
                $kariyer->matchings()->create(['derinlik'=>6,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 14){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>4]);
                $kariyer->matchings()->create(['derinlik'=>3,'yuzde'=>3]);
                $kariyer->matchings()->create(['derinlik'=>4,'yuzde'=>2]);
                $kariyer->matchings()->create(['derinlik'=>5,'yuzde'=>1]);
                $kariyer->matchings()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->matchings()->create(['derinlik'=>7,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 15){
                $kariyer->matchings()->create(['derinlik'=>1,'yuzde'=>5]);
                $kariyer->matchings()->create(['derinlik'=>2,'yuzde'=>4]);
                $kariyer->matchings()->create(['derinlik'=>3,'yuzde'=>3]);
                $kariyer->matchings()->create(['derinlik'=>4,'yuzde'=>2]);
                $kariyer->matchings()->create(['derinlik'=>5,'yuzde'=>1]);
                $kariyer->matchings()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->matchings()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->matchings()->create(['derinlik'=>8,'yuzde'=>1]);
            }
        }
    }
}
