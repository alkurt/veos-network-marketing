<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    public function run()
    {


        $users = [
            ["id"=>10001,"name" => "Admin","surname" => "Admin","kimlik" =>"11111111111","dogum" => "1990","pv"=>0,"cv"=>0,"telefon" =>"999999999",'ulke'=>'TÜRKİYE','bolge_id'=>3,'iller_id'=>1,'kariyer_id'=>1,"email" => "veosnet@veosnet.com","password" => bcrypt("admin")],
            ["id"=>10002,"name" => "User","surname" => "User","kimlik" =>"00000000000","dogum" => "1992","pv"=>0,"cv"=>0,"telefon" =>"999999999",'ulke'=>'TÜRKİYE','bolge_id'=>3,'iller_id'=>20,'kariyer_id'=>1,"email" => "user@user.com","password" => bcrypt("user")],
            ["id"=>23456,"name" => "Veosnet ","surname" => "A.Ş","kimlik" =>"00000000034","dogum" => "1992","pv"=>0,"cv"=>0,"telefon" =>"999999992",'ulke'=>'TÜRKİYE','bolge_id'=>3,'iller_id'=>20,'kariyer_id'=>1,"email" => "user@user.com","password" => bcrypt("user")],
            ["id"=>12345,"name" => "Veosnet ","surname" => "A.Ş","kimlik" =>"00000000021","dogum" => "1992","pv"=>0,"cv"=>0,"telefon" =>"999999990",'ulke'=>'TÜRKİYE','bolge_id'=>3,'iller_id'=>20,'kariyer_id'=>1,"email" => "user@user.com","password" => bcrypt("user")],
            ["id"=>98765,"name" => "Veosnet ","surname" => "A.Ş","kimlik" =>"00000000012","dogum" => "1992","pv"=>0,"cv"=>0,"telefon" =>"999999993",'ulke'=>'TÜRKİYE','bolge_id'=>3,'iller_id'=>20,'kariyer_id'=>1,"email" => "user@user.com","password" => bcrypt("user")],
            ["id"=>65432,"name" => "Veosnet ","surname" => "A.Ş","kimlik" =>"00000000045","dogum" => "1992","pv"=>0,"cv"=>0,"telefon" =>"999999994",'ulke'=>'TÜRKİYE','bolge_id'=>3,'iller_id'=>20,'kariyer_id'=>1,"email" => "user@user.com","password" => bcrypt("user")],
        ];

        foreach($users as $user){
            User::create($user);
        }

        DB::table("role_user")->insert(["role_id" => 1, "user_id" => 10001]);
       // DB::table("role_user")->insert(["role_id" => 1, "user_id" => 3]);
        DB::table("role_user")->insert(["role_id" => 2, "user_id" => 10001]);
        DB::table("role_user")->insert(["role_id" => 2, "user_id" => 10002]);
        DB::table("role_user")->insert(["role_id" => 2, "user_id" => 12345]);
        DB::table("role_user")->insert(["role_id" => 2, "user_id" => 23456]);
        DB::table("role_user")->insert(["role_id" => 2, "user_id" => 98765]);
        DB::table("role_user")->insert(["role_id" => 2, "user_id" => 65432]);
    }
}
