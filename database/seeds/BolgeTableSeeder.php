<?php

use Illuminate\Database\Seeder;

class BolgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            \App\Bolge::create(['name'=>'Marmara']);
            \App\Bolge::create(['name'=> 'Ic Anadolu']);
            \App\Bolge::create(['name'=> 'Ege']);
            \App\Bolge::create(['name'=> 'Akdeniz']);
            \App\Bolge::create(['name'=> 'Karadeniz']);
            \App\Bolge::create(['name'=> 'Dogu Anadolu']);
            \App\Bolge::create(['name'=> 'Guney Dogu Anadolu']);

    }
}
