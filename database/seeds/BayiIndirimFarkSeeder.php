<?php

use Illuminate\Database\Seeder;

class BayiIndirimFarkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kariyers=\App\Kariyer::where('id','<=',5)->where('id','>=',2)->get();
        foreach ($kariyers as $k ){
            for ($i=1;$i<$k->id;$i++){
                $k->bayiindirimfark()->create(['karsı_kariyer_id'=>$i,'fark'=>5]);
            }
        }
    }
}
