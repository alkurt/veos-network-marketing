<?php

use Illuminate\Database\Seeder;
use App\AlisverisDerinlikDagilimi;

class AlisverisDerinikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $k=5;
        for ($i=1 ;$i <=8; $i++){
            AlisverisDerinlikDagilimi::create(['derinlik'=>$i,'yuzde'=>$k]);
            if ($k != 1) $k--;
            else $k=1;
        }
    }
}
