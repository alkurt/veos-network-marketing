<?php

use Illuminate\Database\Seeder;

class BayiAktiflikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kariyers = \App\Kariyer::where('id','<=',15)->where('id','>=',4)->get();
        foreach ($kariyers as $k){
            $k->aktiflik()->create(['min_bayi'=>10,'max_alisveris'=>250,'toplam_alisveris'=>2500,'yuzde'=>10]);
        }
    }
}
