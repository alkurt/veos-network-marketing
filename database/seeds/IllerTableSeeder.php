<?php

use Illuminate\Database\Seeder;

class IllerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iller = ['Adana', 'Adıyaman', 'Afyon', 'Ağrı', 'Amasya', 'Ankara', 'Antalya', 'Artvin',
            'Aydın', 'Balıkesir', 'Bilecik', 'Bingöl', 'Bitlis', 'Bolu', 'Burdur', 'Bursa', 'Çanakkale',
            'Çankırı', 'Çorum', 'Denizli', 'Diyarbakır', 'Edirne', 'Elazığ', 'Erzincan', 'Erzurum', 'Eskişehir',
            'Gaziantep', 'Giresun', 'Gümüşhane', 'Hakkari', 'Hatay', 'Isparta', 'Mersin', 'İstanbul', 'İzmir',
            'Kars', 'Kastamonu', 'Kayseri', 'Kırklareli', 'Kırşehir', 'Kocaeli', 'Konya', 'Kütahya', 'Malatya',
            'Manisa', 'Kahramanmaraş', 'Mardin', 'Muğla', 'Muş', 'Nevşehir', 'Niğde', 'Ordu', 'Rize', 'Sakarya',
            'Samsun', 'Siirt', 'Sinop', 'Sivas', 'Tekirdağ', 'Tokat', 'Trabzon', 'Tunceli', 'Şanlıurfa', 'Uşak',
            'Van', 'Yozgat', 'Zonguldak', 'Aksaray', 'Bayburt', 'Karaman', 'Kırıkkale', 'Batman', 'Şırnak',
            'Bartın', 'Ardahan', 'Iğdır', 'Yalova', 'Karabük', 'Kilis', 'Osmaniye', 'Düzce'];
        $bolge_id=null;
        foreach ($iller as $il){
            $plaka = array_search($il,$iller)+1;

            if ($plaka=="34"||$plaka=="22"||$plaka=='39'||$plaka=='59'||$plaka=='41'||$plaka=='77'||$plaka=='54'||$plaka=='11'||$plaka=='16'||$plaka=='10'||$plaka=='17')
            {
                $bolge_id=1;
            }
            elseif ($plaka=="68"||$plaka=="06"||$plaka=='18'||$plaka=='26'||$plaka=='70'||$plaka=='71'||$plaka=='40'||$plaka=='42'||$plaka=='50'||$plaka=='51'||$plaka=='58'||$plaka=='66'||$plaka=='38')
            {
                $bolge_id=2;

            }
            elseif ($plaka=="35"||$plaka=="45"||$plaka=='9'||$plaka=='20'||$plaka=='43'||$plaka=='3'||$plaka=='64'||$plaka=='48')
            {
                $bolge_id=3;

            }
            elseif ($plaka=='01'||$plaka=='80'||$plaka=='7'||$plaka=='15'||$plaka=='31'||$plaka=='32'||$plaka=='33'||$plaka=='46')
            {
                $bolge_id=4;

            }
            elseif ($plaka=="53"||$plaka=="61"||$plaka=='8'||$plaka=='60'||$plaka=='19'||$plaka=='5'||$plaka=='55'||$plaka=='67'||$plaka=='14'||$plaka=='81'||$plaka=='78'||$plaka=='74'||$plaka=='37'||$plaka=='69'||$plaka=='28'||$plaka=='29'||$plaka=='52'||$plaka=='57')
            {
                $bolge_id=5;

            }
            elseif ($plaka=="4"||$plaka=="75"||$plaka=='12'||$plaka=='13'||$plaka=='23'||$plaka=='24'||$plaka=='25'||$plaka=='76'||$plaka=='30'||$plaka=='36'||$plaka=='44'||$plaka=='49'||$plaka=='62'||$plaka=='65'||$plaka=='73')
            {
                $bolge_id=6;

            }
            elseif ($plaka=="2"||$plaka=="72"||$plaka=='21'||$plaka=='27'||$plaka=='79'||$plaka=='47'||$plaka=='56'||$plaka=='63')
            {
                $bolge_id=7;
            }
            \App\Iller::create(['name'=>$il,'bolge_id'=> $bolge_id ]);
        }

    }
}
