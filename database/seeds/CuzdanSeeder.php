<?php

use Illuminate\Database\Seeder;

class CuzdanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $cuzdan=[
            ['user_id'=>10001,'cuzdan_brut'=>0.00,'toplam_bakiye'=>0.00,'odenen_bakiye'=>0.00],
            ['user_id'=>10002,'cuzdan_brut'=>0.00,'toplam_bakiye'=>0.00,'odenen_bakiye'=>0.00],
            ['user_id'=>12345,'cuzdan_brut'=>0.00,'toplam_bakiye'=>0.00,'odenen_bakiye'=>0.00],
            ['user_id'=>23456,'cuzdan_brut'=>0.00,'toplam_bakiye'=>0.00,'odenen_bakiye'=>0.00],
            ['user_id'=>98765,'cuzdan_brut'=>0.00,'toplam_bakiye'=>0.00,'odenen_bakiye'=>0.00],
            ['user_id'=>65432,'cuzdan_brut'=>0.00,'toplam_bakiye'=>0.00,'odenen_bakiye'=>0.00],
            ];
        foreach($cuzdan as $cuzdan){
            \App\cuzdan::create($cuzdan);
        }
    \App\Kasa::create(['toplam_kasa'=>0]);
    }
}
