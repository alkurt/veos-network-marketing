<?php

use Illuminate\Database\Seeder;

class AnaparaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $anapay = ['city'=>1,'region'=>2,'turkey'=>3,'world'=>3];
            \App\AnaPara::create($anapay);
    }
}
