<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = array(
            array("category_name" => "Temizlik", "slug" => "Temizlik Ürünler"),
            array("category_name" => "Kişisel Bakım", "slug" => "Kişisel Bakım"),
            array("category_name" => "Paketler", "slug" => "bags")
        );

        foreach ($categories as $category)
        {
            Category::create($category);
        }
    }
}
