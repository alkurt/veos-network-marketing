<?php

use Illuminate\Database\Seeder;

class BayiPromosyonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kariyers= \App\Kariyer::where('id','>=',9)->where('id','<=',15)->get();
        foreach ($kariyers as $kariyer){
            if ($kariyer->id ==9 || $kariyer->id==10){
                $kariyer->bayipromosyon()->create(['urun_adet'=>2,'yuzde'=>5]);
                $kariyer->bayipromosyon()->create(['urun_adet'=>3,'yuzde'=>10]);
                $kariyer->bayipromosyon()->create(['urun_adet'=>4,'yuzde'=>30]);
                $kariyer->bayipromosyon()->create(['urun_adet'=>5,'yuzde'=>40]);
            }elseif ($kariyer->id ==11){
                $kariyer->bayipromosyon()->create(['yuzde'=>5]);
            }elseif ($kariyer->id ==12){
                $kariyer->bayipromosyon()->create(['yuzde'=>10]);
            }elseif ($kariyer->id == 13){
                $kariyer->bayipromosyon()->create(['yuzde'=>15]);
            }elseif ($kariyer->id ==14){
                $kariyer->bayipromosyon()->create(['yuzde'=>20]);
            }elseif ($kariyer->id ==15){
                $kariyer->bayipromosyon()->create(['yuzde'=>25]);
            }
        }
    }
}
