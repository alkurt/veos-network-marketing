<?php

use Illuminate\Database\Seeder;

class BolgeCıroTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\BolgeCıro::create(['bolge_ciro'=>3,'il_ciro'=>3,'turkiye_ciro'=>3,'dunya_ciro'=>3]);
    }
}
