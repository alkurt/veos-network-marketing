<?php

use Illuminate\Database\Seeder;

class LiderCikarmaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kariyers= \App\Kariyer::where('id','>=',8)->where('id','<=',15)->get();
        foreach ($kariyers as $kariyer){
           if ($kariyer->id ==8){
               $kariyer->lidercikarma()->create(['tutar'=>500]);
           }elseif ($kariyer->id==9){
               $kariyer->lidercikarma()->create(['tutar'=>1250]);
           }elseif ($kariyer->id ==10){
               $kariyer->lidercikarma()->create(['tutar'=>2500]);
           }elseif ($kariyer->id ==11){
               $kariyer->lidercikarma()->create(['tutar'=>5000]);
           }elseif ($kariyer->id == 12){
               $kariyer->lidercikarma()->create(['tutar'=>10000]);
           }elseif ($kariyer->id ==13){
               $kariyer->lidercikarma()->create(['tutar'=>15000]);
           }elseif ($kariyer->id ==14){
               $kariyer->lidercikarma()->create(['tutar'=>20000]);
           }elseif ($kariyer->id ==15){
               $kariyer->lidercikarma()->create(['tutar'=>30000]);
           }
        }
    }
}
