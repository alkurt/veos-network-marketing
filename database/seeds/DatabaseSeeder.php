<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(UserDetailTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(BasketTableSeeder::class);
        $this->call(BasketProductsTableSeeder::class);
        $this->call(KariyerSeeder::class);
        $this->call(SponsorDerinlikSeeder::class);
        $this->call(CuzdanSeeder::class);
        $this->call(OrderTableSeeder::class);
        $this->call(MatchingSeeder::class);
        $this->call(BayiAktiflikSeeder::class);
        $this->call(SponsorFarkSeeder::class);
        $this->call(BayiIndirimFarkSeeder::class);
        $this->call(LiderCikarmaSeeder::class);
        $this->call(BayiPromosyonSeeder::class);
        $this->call(AlisverisDerinikSeeder::class);
        $this->call(BolgeTableSeeder::class);
        $this->call(IllerTableSeeder::class);
        $this->call(BolgeCıroTableSeeder::class);
        $this->call(AnaparaSeeder::class);
    }
}
