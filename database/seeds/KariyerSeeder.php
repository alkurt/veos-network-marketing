<?php

use Illuminate\Database\Seeder;

class   KariyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $Kariyerler=[
        ['kariyername'=>'Müşteri'],
        ['kariyername'=>'Kullanıcı'],
        ['kariyername'=>'Başlangıç'],
        ['kariyername'=>'Girişimci Adayı'],
        ['kariyername'=>'Girişimci'],
        ['kariyername'=>'Danışman'],
        ['kariyername'=>'Temsilci'],
        ['kariyername'=>'Yarı Asistan'],
        ['kariyername'=>'Asistan'],
        ['kariyername'=>'Öncü Lider'],
        ['kariyername'=>'Lider'],
        ['kariyername'=>'Müdür Yardımcısı'],
        ['kariyername'=>'Müdür'],
        ['kariyername'=>'Genel Müdür Yardımcısı'],
        ['kariyername'=>'Genel Müdür'],
        ['kariyername'=>'Standart'],
        ['kariyername'=>'Alışveriş Müşteri'],
        ];

    $bilgiler=[
      ['kariyer_id'=>1,'sponsor'=>2,'indirim'=>5,'binary'=>2,'tutar'=>300,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>300,'lider_primi'=>0],
      ['kariyer_id'=>2,'sponsor'=>6,'indirim'=>10,'binary'=>5,'tutar'=>600,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>600,'lider_primi'=>0],
      ['kariyer_id'=>3,'sponsor'=>10,'indirim'=>15,'binary'=>10,'tutar'=>1500,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>1500,'lider_primi'=>0],
      ['kariyer_id'=>4,'sponsor'=>15,'indirim'=>20,'binary'=>15,'tutar'=>2500,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>2500,'lider_primi'=>0],
      ['kariyer_id'=>5,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>5000,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>5000,'lider_primi'=>0],
      ['kariyer_id'=>6,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>2500,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>2500,'lider_primi'=>0],
      ['kariyer_id'=>7,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>5000,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>5000,'lider_primi'=>0],
      ['kariyer_id'=>8,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>10000,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>1250,'bolge_ciro'=>0,'tazminati'=>10000,'lider_primi'=>1000],
      ['kariyer_id'=>9,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>30000,'il_ciro'=>5,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>2000,'bolge_ciro'=>5,'tazminati'=>30000,'lider_primi'=>3250],
      ['kariyer_id'=>10,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>75000,'il_ciro'=>7,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>3000,'bolge_ciro'=>7,'tazminati'=>75000,'lider_primi'=>7000],
      ['kariyer_id'=>11,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>150000,'il_ciro'=>10,'tr_ciro'=>5,'dunya_ciro'=>0,'araba_ciro'=>4500,'bolge_ciro'=>10,'tazminati'=>150000,'lider_primi'=>15000],
      ['kariyer_id'=>12,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>250000,'il_ciro'=>15,'tr_ciro'=>10,'dunya_ciro'=>11,'araba_ciro'=>7500,'bolge_ciro'=>15,'tazminati'=>250000,'lider_primi'=>25000],
      ['kariyer_id'=>13,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>500000,'il_ciro'=>17,'tr_ciro'=>15,'dunya_ciro'=>16,'araba_ciro'=>10000,'bolge_ciro'=>17,'tazminati'=>500000,'lider_primi'=>40000],
      ['kariyer_id'=>14,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>750000,'il_ciro'=>20,'tr_ciro'=>30,'dunya_ciro'=>31,'araba_ciro'=>15000,'bolge_ciro'=>20,'tazminati'=>750000,'lider_primi'=>50000],
      ['kariyer_id'=>15,'sponsor'=>20,'indirim'=>25,'binary'=>20,'tutar'=>1000000,'il_ciro'=>26,'tr_ciro'=>40,'dunya_ciro'=>41,'araba_ciro'=>20000,'bolge_ciro'=>26,'tazminati'=>1000000,'lider_primi'=>65000],
      ['kariyer_id'=>16,'sponsor'=>0,'indirim'=>0,'binary'=>0,'tutar'=>0,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>0,'lider_primi'=>0],
      ['kariyer_id'=>17,'sponsor'=>0,'indirim'=>0,'binary'=>0,'tutar'=>0,'il_ciro'=>0,'tr_ciro'=>0,'dunya_ciro'=>0,'araba_ciro'=>0,'bolge_ciro'=>0,'tazminati'=>0,'lider_primi'=>0],


    ];
        foreach ($Kariyerler as $kariyer){
            \App\Kariyer::create($kariyer);
        }
        foreach ($bilgiler as $bilgi)
        {
            \App\Kariyerb::create($bilgi);
        }
    }

}
