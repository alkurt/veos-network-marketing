<?php

use Illuminate\Database\Seeder;

class SponsorDerinlikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $kariyerler = \App\Kariyer::where('id','>=',5)->get();
        foreach ($kariyerler as $kariyer){
            if ($kariyer->id ==5 ){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>20]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
            }elseif ($kariyer->id == 6){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>20]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
            }elseif ($kariyer->id == 7){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>20]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
            }
            elseif ($kariyer->id == 8){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>20]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
            }elseif ($kariyer->id == 9){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>20]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 10){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>20]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>8,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 11){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>21]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>8,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 12){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>22]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>8,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 13){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>23]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>8,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 14){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>24]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>8,'yuzde'=>1]);
            }
            elseif ($kariyer->id == 15){
                $kariyer->derinliks()->create(['derinlik'=>1,'yuzde'=>25]);
                $kariyer->derinliks()->create(['derinlik'=>2,'yuzde'=>5]);
                $kariyer->derinliks()->create(['derinlik'=>3,'yuzde'=>4]);
                $kariyer->derinliks()->create(['derinlik'=>4,'yuzde'=>3]);
                $kariyer->derinliks()->create(['derinlik'=>5,'yuzde'=>2]);
                $kariyer->derinliks()->create(['derinlik'=>6,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>7,'yuzde'=>1]);
                $kariyer->derinliks()->create(['derinlik'=>8,'yuzde'=>1]);
            }
        }

    }
}

