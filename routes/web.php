<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/oturum/pmenu', 'HomeController@oturum')->name('pmenu')->middleware('auth','onyedi');
    Route::get('/oturum/hesabim', 'HomeController@hesap')->name('hesap')->middleware('auth','onyedi');
Route::get('/anasayfa/hakkimizda', 'HomeController@hk')->name('hk');
Route::get('/anasayfa/kariyerler', 'HomeController@kariyerler')->name('kariyerler');
Route::get('/veosnet/etkinlikler', 'HomeController@etkinlikler')->name('etkinlikler');
Route::get('/veosnet/misyonumuz ve vizyonumuz', 'HomeController@misyon')->name('misyon');
Route::get('/veosnet/iade şartları', 'HomeController@iade')->name('iade');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/category/{slug}', 'HomeController@category')->name('category');
Route::get('/product/{slug}', 'HomeController@product')->name('product');
Route::get('/Kurumsal/Bayi Sozlesmesi ve Prosedurler', 'HomeController@bayi')->name('bayi');
Route::get('/Kurumsal/Temsilcilik Sozlesmesi', 'HomeController@temsilcilik')->name('temsilcilik');
Route::get('/Kurumsal/Gizlilik Sozlesmesi','HomeController@gizlilik')->name('gizlilik');
Route::get('/Kurumsal/Dogrudan Satis Sozlesmesi', 'HomeController@dogrudansatis')->name('dogrudansatis');
Route::get('/Kurumsal/Mesafeli Satis Sozlesmesi', 'HomeController@mesafelisatis')->name('mesafelisatis');
Route::get('/Veosnet/İş Modeli', 'HomeController@ismodeli')->name('ismodeli');
Route::get('/Veosnet/Yeni Urunler', 'HomeController@yeniurunler')->name('yeniurunler');
Route::resource('profil-create','ProfilCreateController');
Route::get('dest/{id}','ProfilCreateController@destroy')->name('psil');
Route::get('/Veosnet/Neden Veosnetwork', 'HomeController@nedenveos')->name('nedenveos');
Route::get('/Veosnet/ProfesyonelPazarlama', 'HomeController@ppazarlama')->name('ppazarlama');
Route::get('/PrimMenu/ekip/', 'PrimController@index')->name('ekipagaci')->middleware('auth','onyedi');
Route::get('/VeosNet/cuzdan', 'PrimController@create')->name('cuzdan')->middleware('auth','onyedi');
Route::get('/VeosNet/binary', 'PrimController@binary')->name('binary')->middleware('auth','onyedi');
Route::get('/VeosNet/binary/datatable', 'PrimController@binary_datatable')->name('binary-datatable')->middleware('auth','onyedi');
Route::get('/VeosNet/kazanc', 'PrimController@store')->name('kazanc')->middleware('auth','onyedi');
Route::get('/PrimMenu/{kid}/kazanc', 'PrimController@gonderme')->name('kid')->middleware('auth','onyedi');
Route::get('/view/Kargo', 'HomeController@kargo')->name('kargo');
Route::get('/view/Odemeplanı', 'HomeController@odemeplanı')->name('odemeplanı');
Route::get('/view/basari', 'HomeController@basari')->name('basari');
Route::get('/view/profesyonel_pazarlama', 'HomeController@profesyonelpazarlama')->name('profesyonelpazarlama');
Route::get('/view/genelbakis', 'PrimController@genelbakis')->name('genelbakis')->middleware('auth','onyedi');
Route::get("/view/menu","PrimController@menu")->name('menu')->middleware('auth','onyedi');
Route::get('/view/guvenliodeme','HomeController@cartman')->name('cartman')->middleware('auth','onyedi');
Route::get('/view/TarimOnaylidir','HomeController@tarimbelge')->name('tarimbelge');
Route::get('/view/Kazancozeti','HomeController@kazancozeti')->name('kazancozeti')->middleware('auth','onyedi');
Route::get('/VeosNet/Kazancozeti/datatable','GelirController@kazancozeti_datatable')->name('kazancozeti.datatable')->middleware('auth','onyedi');
Route::get('/VeosNet/gelenpv/datatable','GelenpvController@gelenpv_datatable')->name('gelenpv.datatable')->middleware('auth','onyedi');
Route::get('Veosnet/Pv/Cv Gelirlerim','GelenpvController@index')->name('gelenpvsyf');
Route::get('/VeosNet/gelirlerim','HomeController@gelirlerim')->name('gelirlerim')->middleware('auth','onyedi');
Route::get('VeosNet/S.S.S','HomeController@sss')->name('sss');
Route::resource('mailorder','MailOrderController');
Route::get('Kampanyalar','HomeController@kampanyalar')->name('kampanyalar');
Route::get('haberlerimiz','HomeController@haber2')->name('haberler2');
Route::get('Bilgilendirme','HomeController@bilgilendirme')->name('bilgilendirme21')->middleware('auth','onyedi');
Route::get('hesapbilgileri','HomeController@banka')->name('banka')->middleware('auth','onyedi');
Route::get('VeosNet/Sozlesmeler','HomeController@sozlesmemusteri')->name('sozlesmemusteri');
Route::get('VeosNet/sozlesmelerimiz/{uuid}/download','HomeController@download')->name('sozlesme.download')->middleware('auth');
Route::get('VeosNet/kullaniciilcirosu','HomeController@il_ciro')->name('il_ciro')->middleware('auth','onyedi');
Route::get('VeosNet/kullanicitrciro','HomeController@tr_ciro')->name('tr_ciro')->middleware('auth','onyedi');
Route::get('VeosNet/kullanicidunyaciro','HomeController@dunya_ciro')->name('dunya_ciro')->middleware('auth','onyedi');
Route::get('VeosNet/kullaniciarabaciro','HomeController@araba_ciro')->name('araba_ciro')->middleware('auth','onyedi');
Route::get('VeosNet/kullanicibolgeciro','HomeController@bolge_ciro')->name('bolge_ciro')->middleware('auth','onyedi');
Route::get('veosnet/GuvenliOdeme','HomeController@mailorder')->name('mailorder');
Route::resource('VeosNet/odemetalebi','OdemeTalebiController')->middleware('auth','onyedi');
Route::resource('message','MessageController');
Route::get('/Veosnet/Kataloglar','HomeController@kullanici_katalog')->name('kullanici_katalog');

Route::group(['namespace'=>"Admin"],function (){
    Route::get('/VeosNet/kullanici/katalog/{uuid}/download', 'KatalogController@download')->name('katalog.download');
});
Route::get('VeosNet/EftDurum','HomeController@musteri_eft_durum')->name('musteri_eft_durum')->middleware('auth','onyedi');

Route::get('veosnet/alt-uyelik-ekle','PrimController@farkli_il_kayit_index')->middleware('auth')->name('yeni-alt-bayi');
Route::post('veosnet/alt-uyelik-ekle','PrimController@farkli_il_kayit_store')->middleware('auth')->name('yeni-alt-bayi.store');
Route::get('veosnet/alt-uyeliklerim','PrimController@alt_bayiliklerim')->middleware('auth')->name('alt-bayiliklerim');

Route::get('logout','Auth\LoginController@logout');
    //Route::get('kayit/{konum}','uyelikController@index')->name('kayityapma');
Route::get('kayit/{konum}/{sps}','uyelikController@index')->name('kayityapma')->middleware('auth');
Route::post('kayit','uyelikController@store')->name('kayityapma.kayıt')->middleware('auth');
Route::get('KonumBilgilendirme','PrimController@bilgilendirme')->name('bilgilendirme')->middleware('auth');
Route::get('isteklerim','PrimController@isteklerim')->name('isteklerim')->middleware('auth');
Route::get('ekiptakip','PrimController@ekiptakip')->name('ekiptakip')->middleware('auth');
Route::get('ekiplistesi','PrimController@ekiplistesi')->name('ekiplistesi')->middleware('auth');
Route::get('change-password', 'ChangePasswordController@index')->name('change')->middleware('auth');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password')->middleware('auth');
Route::post('konumlandirin/{ikid}','Admin\TransferController@iskonum')->name('iskonum')->middleware('auth');

Route::post('odeme-cuzdan/{tutar}','GelirController@cuzdanOdeme')->name('cuzdan.odeme')->middleware('auth');
Route::get('bankahesapbilgileri','PrimController@bankahesapbilgileri')->name('bankahesapbilgileri')->middleware('auth');
Route::get('bankahesapbilgileri/{order_no}','PrimController@bankahesapbilgilerikisiyeozel')->name('bankahesapbilgileri.kisiyeozel')->middleware('auth');


Route::group(["middleware" => ["is_thisAdmin","auth"]],function (){
    Route::group(["namespace" => "Admin"], function (){
        Route::resource("admin-users","UsersController");
        Route::get("admin-users/datatable/d","UsersController@users_datatable")->name('admin-users.datatable');
        Route::get("admin-users/datatable/active","UsersController@users_active_datatable")->name('admin-users.active.datatable');
        Route::get("admin-users/datatable/pasive","UsersController@users_passive_datatable")->name('admin-users.passive.datatable');
        Route::resource("admin-category","CategoryController");
        Route::resource("admin-products","ProductController");
        Route::resource("admin-orders","OrderController");
        Route::resource('kariyer','KariyerController');
        Route::resource('transfer','TransferController');
        Route::get('Binarykontrol','TransferController@adminagac')->name('binarykontrol');
        Route::post('admin/admin-pv/{id}/ekle','TransferController@admin_pv_ekleme')->name('admin.pv.ekle');
        Route::post('admin/admin-cv/{id}/ekle','TransferController@admin_cv_ekleme')->name('admin.cv.ekle');
        Route::get('AdminEkiplistesi','TransferController@adminekip')->name('adminekip');
        Route::get('transfer/{id}/sil','TransferController@destroy2')->name('transfersilme');
        Route::post('transfer/{tid}','TransferController@transferet')->name('transfer');
        Route::post('konumlandir/{kdid}','TransferController@konumlandir')->name('konumlandir');
        Route::get('kontrolmenu','TransferController@kontrolmenu')->name('kontrolmenu');
        Route::get('aktif_pasif','TransferController@aktif')->name('aktif');
        Route::get('users-pasif','TransferController@pasif')->name('users.passive');
        Route::get('aracciro','TransferController@araciro')->name('adminaraciro');
        Route::get('tazminat','TransferController@tazminat')->name('tazminat');
        Route::get('tazminat/{id}','TransferController@tazminatode')->name('tazminat-ode');
        Route::get('ilciro','TransferController@ilciro')->name('adminilciro');
        Route::get('turkeyciro','TransferController@turkeyciro')->name('adminturkeyciro');
        Route::post('/mailordersuccessful', 'MailOrderController@pay')->name('mailpay');
        Route::get('dunyaciro','TransferController@dunyaciro')->name('admindunyaciro');
        Route::get('bolgeciro','TransferController@bolgeciro')->name('adminbolgeciro');

        Route::resource('books', 'BookController');
        Route::get('books/{id}/a','BookController@destroy')->name('booksil');
        Route::get('books/{uuid}/download', 'BookController@download')->name('books.download');
        Route::resource('banka','BankaController');
        Route::resource('kargoyonetim','KargoYonetimController');
        Route::get('kargoyonetim/{id}','KargoYonetimController@destroy')->name('ksil');
        Route::resource('sss','SorularController');
        Route::get('sss/{id}','SorularController@destroy')->name('sssil');
        Route::resource('haberler','HaberlerController');
        Route::get('haberlers/{uuid}/download', 'HaberlerController@download')->name('haberlers.download');
        Route::get('haberler/{id}/sil', 'HaberlerController@destroy')->name('haberlersilme');
        Route::resource('etkinlikler','EtkinliklerController');
        Route::get('etkinliklers/{uuid}/download', 'EtkinliklerController@download')->name('etkinliklers.download');
        Route::resource('iletisim','IletisimController')->except(['show']);
        Route::resource('duyurular','DuyurularController');
        Route::resource('firsat','FirsatController');
        Route::resource('sponsorderinlik','SponsorDerinlikSeederController')->except('edit','update');
        Route::get('sponsorderinlik/{kariyer_id}/{derinlik}','SponsorDerinlikSeederController@edit')->name('sponsorderinlik.edit');
        Route::put('sponsorderinlik/{kariyer_id}/{derinlik}','SponsorDerinlikSeederController@update')->name('sponsorderinlik.update');
        Route::get('sponsorderinlik/{id}','SponsorDerinlikSeederController@destroy')->name('derinliksil');
        Route::resource('sosyalmedya','SosyalMedyaController');
        Route::get('sosyalmedya/{id}','SosyalMedyaController@destroy')->name('medyasil');
        Route::resource('adminodeme','AdminOdemeController');
        Route::get("odemeyapma","AdminOdemeController@index")->name('adminodemes');
        Route::resource('matching','MatchingController')->except('edit','update');
        Route::get('matching/{matching_id}/edit','MatchingController@edit')->name('matching.edit');
        Route::put('matcing/{matching_id}','MatchingController@update')->name('matching.update');
        Route::get('adminkazanc','TransferController@adminbinary')->name('adminbinary');
        Route::get('/PrimMenu/binary/datatables', 'TransferController@admin_datatable')->name('admin-datatable');
        Route::resource('bayiaktiflik','BayiAktiflikController');
        Route::get('odemetalebi','TransferController@odemetalebi')->name('odemetalebi');
        Route::resource('logo','LogoController');
        Route::get('Admin/logo/Silm/{id}','LogoController@destroy')->name('logosil');
        Route::get('logo/{uuid}/download', 'LogoController@download')->name('logo.download');
        Route::delete('odemetalebisil/{id}/ss','TransferController@odemetalebisil')->name('odemetalebisil');
        Route::resource('sponsorfark','SponsorFarkController');
        Route::resource('bayiindirimfark','BayiIndirimFarkController');
        Route::resource('efthavalebekleme','EftHavaleBeklemeController');
        Route::get('mesajlar','SosyalMedyaController@message')->name('camemessage');
        Route::delete('mesajlar/{id}/s','SosyalMedyaController@messagesil')->name('camemessagesil');
        Route::get('KullanıcıTipi/{id}','UsersController@update')->name('tip');
        Route::resource('lidercikarma','LiderCikarmaController');
        Route::resource('bayipromosyon','BayiPromosyonController');
        Route::resource('katalog','KatalogController');
        Route::get('Admin/Katalog/Silme/{id}','KatalogController@destroy')->name('katalogsil');
        Route::get('katalog/{uuid}/download', 'KatalogController@download')->name('katalog.download');
	    Route::resource('slider','SliderController');
        Route::get('slider/{uuid}/download', 'SliderController@download')->name('slider.download');
        Route::get('Admin/Slider/Silme/{id}','SliderController@destroy')->name('slidersil');
        Route::resource('alisverisderinlikdagilimi','AlisverisDerinlikDagilimiController');
        Route::get('yapilan-odemeler','TransferController@giderler')->name('admin.giderler');
        Route::get('admineslesme','TransferController@admineslesme')->name('admineslesme');
        Route::get('admingelenpv','TransferController@admingelenpv')->name('admingelenpv');
        Route::resource('anapara','AnaParaController');
        Route::resource('adminmail','AdminMailController');
        Route::get('siparisalınan','OrderController@siparisalindi')->name('siparisalindi');
        Route::get('kariyerraporu','AnaParaController@kariyerraporu')->name('kariyer-raporu');
        Route::get('/VeosNet/admingelenpv/datatable','TransferController@admingelenpv_datatable')->name('admingelenpv-datatable');
        Route::get('admineslesme_datatable','TransferController@admineslesme_datatable')->name('admineslesme_datatable');
        Route::get('excel-import/user','SliderController@excel_import')->name('user.excel.import');
        Route::post('excel-import/user/store','SliderController@excel_import_store')->name('user.excel.import.store');
        Route::resource('tazminatbelirle','TazminatBelirleController');
        Route::resource('alt-alan','AltAlanController');
    });
});

//Route::get('/guzzle1','GelirController@index')->name('guzzle');
//Route::post('guzzle1-post','GelirController@store')->name('guzzle.post');
Route::post('guzzle-sonuc/{order_no}','GelirController@sonuc')->name('guzzle.sonuc');

Route::group(['prefix' => 'basket'], function () {
    Route::get('/', 'BasketController@index')->name('basket')->middleware('auth');
    Route::post('/create', 'BasketController@create')->name('basket.create');
    Route::delete('/destroy', 'BasketController@destroy')->name('basket.destroy')->middleware('auth');
    Route::patch('/update/{rowid}', 'BasketController@update')->name('basket.update')->middleware('auth');
});

Route::get('/payment', 'PaymentController@index')->name('payment')->middleware('auth');
Route::post('/successful', 'PaymentController@pay')->name('pay');
//Route::get('/3Dguvenliodeme', 'PaymentController@paytrodemeekrani')->name('elekse.odeme.api')->middleware('auth');

Route::group(["middleware">'onyedi'],function (){

});


Route::get('/orders', 'OrderController@index')->name('orders');
Route::get('/orders/{id}', 'OrderController@detail')->name('order');
Route::resource('profile', 'UserDetailController')->middleware('auth');
Route::post('urun-arama','Admin\SliderController@product_search')->name('search.product');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
